package com.efounder.chat.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.model.AddFriendUser;
import com.efounder.utils.ResStringUtil;

import java.util.List;

public class AddFriendsUserListAdapter extends BaseAdapter{

	private List<AddFriendUser> users;

	public AddFriendsUserListAdapter(List<AddFriendUser> users) {
		super();
		this.users = users;
	}

	@Override
	public int getCount() {
		return users.size();
	}

	@Override
	public AddFriendUser getItem(int position) {
		return users.get(position);
	}

	@Override
	public long getItemId(int position) {
		return getItem(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		AddFriendUser user = getItem(position);
		if (convertView == null) {
			convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_addfriends_list_item,null);
			viewHolder = new ViewHolder();
			viewHolder.textViewName = (TextView) convertView.findViewById(R.id.textView_name);
			viewHolder.textViewAdName = (TextView) convertView.findViewById(R.id.textView_adname);
			viewHolder.textViewTel = (TextView) convertView.findViewById(R.id.textView_tel);
			viewHolder.userAvatarImageView = (ImageView) convertView.findViewById(R.id.imageView1);
			convertView.setTag(viewHolder);
		}else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		viewHolder.userAvatarImageView.setImageResource(R.drawable.default_useravatar);
		viewHolder.textViewName.setText(ResStringUtil.getString(R.string.wrchatview_username_mh) +user.getName());
		viewHolder.textViewAdName.setText(ResStringUtil.getString(R.string.wrchatview_yuuser_mh)+user.getAdName());
		viewHolder.textViewTel.setText(ResStringUtil.getString(R.string.wrchatview_phone_mh)+user.getTelphone());
		return convertView;
	}
	
	class ViewHolder{
		TextView textViewName;
		TextView textViewAdName;
		TextView textViewTel;
		ImageView userAvatarImageView;
	}
	
}
