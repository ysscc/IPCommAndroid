package com.efounder.chat.adapter;

import android.app.Activity;
import android.content.Context;

import androidx.fragment.app.Fragment;

import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.db.WeChatDBManager;
import com.efounder.chat.model.ChatListItem;
import com.efounder.chat.model.Group;
import com.efounder.chat.struct.MessageChildTypeConstant;
import com.efounder.chat.struct.StructFactory;
import com.efounder.chat.utils.ChatDateUtils;
import com.efounder.chat.utils.ChatTypeUtil;
import com.efounder.chat.utils.GroupAvatarUtil;
import com.efounder.chat.utils.GroupNameUtil;
import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.chat.utils.VoiceRecognitionSoundFileCacheUtil;
import com.efounder.chat.view.voicedictate.SoundFile;
import com.efounder.chat.view.voicedictate.WaveformView;
import com.efounder.chat.widget.BadgeView;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.message.manager.JFMessageManager;
import com.efounder.message.struct.IMStruct002;
import com.efounder.mobilecomps.contacts.User;
import com.efounder.util.EnvSupportManager;
import com.efounder.utils.JfResourceUtil;
import com.groupimageview.NineGridImageView;
import com.groupimageview.NineGridImageViewAdapter;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.efounder.frame.utils.Constants.CHAT_USER_ID;

/**
 * 聊天列表适配器
 *
 * @author yqs
 */
public class ChatListSwipeMenuAdapter extends BaseAdapter {
    private static final String TAG = "ChatListSwipeAdapter";
    private Context mContext;
    private List<ChatListItem> datas;

    private final static int ISREAD = -1;// 表示已读，角标不显示
    private final static int NOREAD = 0;// 表示公众号的未读
    private SoundFile mSoundFile;
    private final static int NOREADONE = 1;// 表示聊天的未读
    private LayoutInflater inflater;
    private OnRecognitonVoiceRecordStart onRecognitonVoiceRecordStart;
    //是否支持发送识别语音
    private boolean isSupportRecognizeVoiceSend;
    private int myUserId;
    private Fragment fragment;

    public ChatListSwipeMenuAdapter(Fragment fragment, List<ChatListItem> datas) {
        mContext = fragment.getActivity();
        this.fragment = fragment;
        isSupportRecognizeVoiceSend = EnvSupportManager.isSupportRecognizeVoiceSend();
        inflater = LayoutInflater.from(mContext);
        if (datas == null) {
            datas = new ArrayList<>();
        }
        this.datas = datas;

        try {
            myUserId = Integer.parseInt(EnvironmentVariable.getProperty(CHAT_USER_ID));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemViewType(int position) {
        ChatListItem item = datas.get(position);
        if ((item.getBadgernum() == -1)) {
            return 0;//0表示消息已读，侧滑时不显示标为已读的按钮
        } else {
            return 1;//1表示有未读消息，侧滑时显示标为已读 以及删除按钮
        }


    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getCount() {
        return datas.size();
    }

    @Override
    public Object getItem(int position) {
        return datas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        ViewHolder holder = null;
        final ChatListItem chatListItem = datas.get(position);
        int userType = -1;//普通好友或者公众号，group没有此属性
        User tempUser = null;
        IMStruct002 imStruct002 = chatListItem.getStruct002();
        if (imStruct002 == null) {
            imStruct002 = new IMStruct002();
            imStruct002.setBody(" ".getBytes());
            if (chatListItem.getUser() != null) {
                imStruct002.setToUserType(StructFactory.TO_USER_TYPE_PERSONAL);
            } else {
                imStruct002.setToUserType(StructFactory.TO_USER_TYPE_GROUP);
            }

            imStruct002.setState(IMStruct002.MESSAGE_STATE_SEND);
            imStruct002.setTime(0);
            imStruct002.setMessageChildType((byte) -1);
            chatListItem.setStruct002(imStruct002);
        }

        if (StructFactory.TO_USER_TYPE_GROUP != imStruct002.getToUserType()) {
            tempUser = chatListItem.getUser();
            if (tempUser != null) {
                userType = tempUser.getType();
            }
        }
        if (convertView == null) {
            if (isSupportRecognizeVoiceSend) {
                //支持光速短信的布局
                convertView = inflater.inflate(R.layout.chatlist_item, null, false);
            } else {
                //原来的布局
                convertView = inflater.inflate(R.layout.chatlist_item_without_recognition_voice, null, false);
            }
            holder = new ViewHolder();

            //消息item
            String TX_type = EnvironmentVariable.getProperty("TX_type", "Square");
            if ("Round".equals(TX_type)) {
                holder.icon = (ImageView) convertView.findViewById(R.id.icon1);
            } else {
                holder.icon = (ImageView) convertView.findViewById(R.id.icon2);
            }
//            holder.icon = (ImageView) convertView.findViewById(R.id.icon2);
            holder.icon.setVisibility(ImageView.VISIBLE);

//            holder.icon = (ImageView) convertView.findViewById(R.id.icon);
            holder.iconGroup = (NineGridImageView) convertView.findViewById(R.id.icon_group);
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.msg = (TextView) convertView.findViewById(R.id.msg);
            holder.time = (TextView) convertView.findViewById(R.id.time);
            holder.sendState = (ImageView) convertView.findViewById(R.id.sending);
            //显示[草稿]字样
            holder.editing = (TextView) convertView.findViewById(R.id.editing);
            //显示[有人@我]
            holder.mentionHint = convertView.findViewById(R.id.mentionHint);
            holder.chatListbagLayout = (RelativeLayout) convertView
                    .findViewById(R.id.chatlist_bag);
            holder.infoBother = (ImageView) convertView
                    .findViewById(R.id.infobother);
            holder.iconcontainerforbadage = (LinearLayout) convertView
                    .findViewById(R.id.iconcontainerforbadage);
            holder.badge = new BadgeView(mContext, holder.iconcontainerforbadage);
            //语音识别的控件
            holder.rlVoiceRecognition = convertView.findViewById(R.id.rl_voice_recognition);
            holder.rlVoiceWave = convertView.findViewById(R.id.rl_voice_wave);
            holder.ivVoicePlay = convertView.findViewById(R.id.iv_play);
            holder.voiceWaveFormView = convertView.findViewById(R.id.view_voice_line);
            holder.ivVoiceSending = (ImageView) convertView.findViewById(R.id.iv_sending);
            holder.tvVoiceMsg = (TextView) convertView.findViewById(R.id.tv_msg);
            holder.llMsg = convertView.findViewById(R.id.meslin);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        //FIXME 设置头像
        if (userType == 1 && chatListItem.getAvatar().equals("")) {//公众号
            holder.iconGroup.setVisibility(View.GONE);
            holder.icon.setVisibility(View.VISIBLE);
            if ("人力资源系统".equals(tempUser.getNickName())) {
                holder.icon.setImageResource(R.drawable.rlzy);
            } else if ("内控系统".equals(tempUser.getNickName())) {
                holder.icon.setImageResource(R.drawable.neikong);
            } else if ("固定资产系统".equals(tempUser.getNickName())) {
                holder.icon.setImageResource(R.drawable.gdzc);
            } else if ("财务核算系统".equals(tempUser.getNickName())) {
                holder.icon.setImageResource(R.drawable.czhs);
            } else if ("生产监控系统".equals(tempUser.getNickName())) {
                holder.icon.setImageResource(R.drawable.scjk2);
            } else if ("生产日报系统".equals(tempUser.getNickName())) {
                holder.icon.setImageResource(R.drawable.bj);
            } else if ("CRM系统".equals(tempUser.getNickName())) {
                holder.icon.setImageResource(R.drawable.crm);
            } else if ("普联餐厅".equals(tempUser.getNickName())) {
                holder.icon.setImageResource(R.drawable.ct);
            } else if ("普联健身".equals(tempUser.getNickName())) {
                holder.icon.setImageResource(R.drawable.jianshen);
            } else if ("奥体中心".equals(tempUser.getNickName())) {
                holder.icon.setImageResource(R.drawable.atzx);
            } else if ("乒乓球室".equals(tempUser.getNickName())) {
                holder.icon.setImageResource(R.drawable.ppq);
            } else if ("篮球场".equals(tempUser.getNickName())) {
                holder.icon.setImageResource(R.drawable.lanqiu);
            } else if ("足球场".equals(tempUser.getNickName())) {
                holder.icon.setImageResource(R.drawable.zuqiu);
            } else if ("新的朋友".equals(tempUser.getNickName())) {
                holder.icon.setImageResource(R.drawable.icon_official_account);
            } else {
                LXGlideImageLoader.getInstance().showRoundUserAvatar(fragment, holder.icon, R.drawable.default_useravatar,
                        LXGlideImageLoader.DEFATLT_AVATAR_RADIUS_12);
            }

        } else {
            if (chatListItem.getChatType() == StructFactory.TO_USER_TYPE_PERSONAL
                    || chatListItem.getChatType() == StructFactory.TO_USER_TYPE_OFFICIAL_ACCOUNT) {//单聊或者公众号
                holder.iconGroup.setVisibility(View.GONE);
                holder.icon.setVisibility(View.VISIBLE);
                if (chatListItem.getAvatar() != null && chatListItem.getAvatar().contains("http")) {
                    LXGlideImageLoader.getInstance().showRoundUserAvatar(fragment, holder.icon, chatListItem.getAvatar()
                            , LXGlideImageLoader.DEFATLT_AVATAR_RADIUS_12);
                } else {
                    LXGlideImageLoader.getInstance().showRoundUserAvatar(fragment, holder.icon, R.drawable.default_useravatar,
                            LXGlideImageLoader.DEFATLT_AVATAR_RADIUS_12);
                }
                holder.icon.setBackground(null);

            } else {//群聊
                // holder.icon.setVisibility(View.GONE);
                // holder.iconGroup.setVisibility(View.VISIBLE);

                holder.icon.setBackground(JfResourceUtil.getSkinDrawable(R.drawable.chat_item_round_shape));
                Group group = chatListItem.getGroup();
                if (group != null && group.getAvatar() != null && !"".equals(group.getAvatar())) {

                    String filePath = GroupAvatarUtil.getAvatar(group.getAvatar());
                    LXGlideImageLoader.getInstance().showGroupAvatar(fragment, holder.icon, filePath);
                    LXGlideImageLoader.getInstance().showRoundGrouAvatar(fragment, holder.icon, filePath
                            , LXGlideImageLoader.DEFATLT_AVATAR_RADIUS_12);
                } else {
                    LXGlideImageLoader.getInstance().showRoundGrouAvatar(fragment, holder.icon, R.drawable.default_groupavatar
                            , LXGlideImageLoader.DEFATLT_AVATAR_RADIUS_12);
                }
            }
        }


//        if (!TextFormatMessageUtil.hasUnreadMentionMessage(chatListItem.getUserId()+"",
//                chatListItem.getChatType()+"") &&
//                EnvironmentVariable.getProperty(chatListItem.getUserId() + "" + chatListItem.getChatType()) != null &&
//                !EnvironmentVariable.getProperty(chatListItem.getUserId() + "" + chatListItem.getChatType()).equals("")) {
        if (!JFMessageManager.getInstance().getHasAtMeMessage(chatListItem.getUserId(), (byte) chatListItem.getChatType()) &&
                EnvironmentVariable.getProperty(chatListItem.getUserId() + "" + chatListItem.getChatType()) != null &&
                !EnvironmentVariable.getProperty(chatListItem.getUserId() + "" + chatListItem.getChatType()).equals("")) {
            //FIXME 设置草稿消息内容
            holder.editing.setVisibility(View.VISIBLE);
            holder.mentionHint.setVisibility(View.GONE);
            //草稿消息屏蔽正在发送图标
            if (holder.ivVoiceSending != null) {
                holder.ivVoiceSending.setVisibility(View.GONE);
            }
            holder.sendState.setVisibility(View.GONE);
            //设置草稿消息
            if (holder.tvVoiceMsg != null) {
                holder.tvVoiceMsg.setText(EnvironmentVariable.getProperty(chatListItem.getUserId() + "" + chatListItem.getChatType()));
            }
            holder.msg.setText(EnvironmentVariable.getProperty(chatListItem.getUserId() + "" + chatListItem.getChatType()));
        } else {
            //if (TextFormatMessageUtil.hasUnreadMentionMessage(chatListItem.getUserId() + "", chatListItem.getChatType() + "")) {
            if (JFMessageManager.getInstance().getHasAtMeMessage(chatListItem.getUserId(), (byte) chatListItem.getChatType())) {
                holder.mentionHint.setVisibility(View.VISIBLE);
            } else {
                holder.mentionHint.setVisibility(View.GONE);
            }
            //FIXME 设置消息内容
            holder.editing.setVisibility(View.GONE);
            if (imStruct002 != null && imStruct002.getBody() != null) {
                //设置正常消息
                if (holder.tvVoiceMsg != null) {
                    setMessageContent(imStruct002, chatListItem, holder.tvVoiceMsg);
                }
                setMessageContent(imStruct002, chatListItem, holder.msg);
            } else {
                if (holder.tvVoiceMsg != null) {
                    holder.tvVoiceMsg.setText("");
                }
                holder.msg.setText("");
            }

//            /**如果存在草稿，显示[草稿]的TextView，发送状态图标隐藏，发送信息设为草稿信息 **/
//            if (EnvironmentVariable.getProperty(chatListItem.getUserId() + "" + chatListItem.getChatType()) != null &&
//                    !EnvironmentVariable.getProperty(chatListItem.getUserId() + "" + chatListItem.getChatType()).equals("")) {
//                holder.editing.setVisibility(View.VISIBLE);
//                holder.sendState.setVisibility(View.GONE);
//                holder.msg.setText(EnvironmentVariable.getProperty(chatListItem.getUserId() + "" + chatListItem.getChatType()));
//            } else {
//                holder.editing.setVisibility(View.GONE);
//
//                if (imStruct002 != null && imStruct002.getBody() != null) {
//                    setMessageContent(imStruct002, chatListItem, holder.msg);
//                } else {
//                    holder.msg.setText("");
//                }}


            //FIXME 设置消息状态
            if (imStruct002.getState() == IMStruct002.MESSAGE_STATE_SENDING
                    || imStruct002.getState() == IMStruct002.MESSAGE_STATE_PRESEND
                    || imStruct002.getState() == IMStruct002.MESSAGE_STATE_WAITSEND) {
                if (holder.ivVoiceSending != null) {
                    holder.ivVoiceSending.setVisibility(View.VISIBLE);
                }
                holder.sendState.setImageDrawable(mContext.getResources().getDrawable(R.drawable.chatlist_fragment_sending));
                holder.sendState.setVisibility(View.VISIBLE);
                if (imStruct002.getState() == IMStruct002.MESSAGE_STATE_PRESEND) {
                    //预发送状态，progress为空，或者为-1，则为发送失败
                    try {
                        if (imStruct002.getExtra("progress") == null || (int) imStruct002.getExtra("progress") == -1) {
                            if (holder.ivVoiceSending != null) {
                                holder.ivVoiceSending.setImageDrawable(mContext.getResources().getDrawable(R.drawable.msg_state_fail_resend));
                            }
                            holder.sendState.setImageDrawable(mContext.getResources().getDrawable(R.drawable.msg_state_fail_resend));
                        } else {
                            if (holder.ivVoiceSending != null) {
                                holder.ivVoiceSending.setImageDrawable(mContext.getResources().getDrawable(R.drawable.chatlist_fragment_sending));
                            }
                            holder.sendState.setImageDrawable(mContext.getResources().getDrawable(R.drawable.chatlist_fragment_sending));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if (imStruct002.getState() == IMStruct002.MESSAGE_STATE_FAILURE) {
                if (holder.ivVoiceSending != null) {
                    holder.ivVoiceSending.setImageDrawable(mContext.getResources().getDrawable(R.drawable.msg_state_fail_resend));
                }
                holder.sendState.setImageDrawable(mContext.getResources().getDrawable(R.drawable.msg_state_fail_resend));
            } else {
                if (holder.ivVoiceSending != null) {
                    holder.ivVoiceSending.setVisibility(View.GONE);
                }
                holder.sendState.setVisibility(View.GONE);
            }
        }
        //FIXME 设置时间
        if (imStruct002.getTime() == 0) {
            holder.time.setText("");
        } else {
            holder.time.setText(ChatDateUtils.getTimestampString(new Date(
                    imStruct002.getTime())));

        }
        //FIXME 设置置顶背景色
        if (chatListItem.getIsTop()) {
            holder.chatListbagLayout.setBackgroundColor(JfResourceUtil
                    .getSkinColor(R.color.chattop_bag));
            if (holder.rlVoiceRecognition != null) {
                holder.rlVoiceRecognition.setBackgroundColor(JfResourceUtil
                        .getSkinColor(R.color.chattop_bag));
            }
//            holder.chatListbagLayout.setBackgroundColor(ContextCompat.getColor(mContext,R.color.chattop_bag));
//            holder.chatListbagLayout.setBackgroundColor(Color.parseColor("#f2f3f7"));
        } else {
            holder.chatListbagLayout.setBackgroundColor(JfResourceUtil
                    .getSkinColor(R.color.transparent));
            if (holder.rlVoiceRecognition != null) {
                holder.rlVoiceRecognition.setBackgroundColor(JfResourceUtil
                        .getSkinColor(R.color.transparent));
            }
        }
        // FIXME 根据消息类型设置字体颜色和标题
        int type = imStruct002.getToUserType();
        switch (type) {
            case StructFactory.TO_USER_TYPE_OFFICIAL_ACCOUNT:
                holder.title.setTextColor(JfResourceUtil.getSkinColor(R.color.publicnumber_textColor));
                holder.title.setText(chatListItem.getName());
                break;

            case StructFactory.TO_USER_TYPE_GROUP:
                holder.title.setTextColor(JfResourceUtil.getSkinColor(R.color.black_text_color));
                holder.title.setText(chatListItem.getName());
                break;
            case StructFactory.TO_USER_TYPE_PERSONAL:
                if (userType == 1) {
                    holder.title.setTextColor(JfResourceUtil.getSkinColor(R.color.publicnumber_textColor));
                    holder.title.setText(chatListItem.getName());
                } else {
                    holder.title.setText(chatListItem.getName());
//                    holder.title.setTextColor(mContext.getResources().getColor(
//                            R.color.black_text_color));
                    holder.title.setTextColor(JfResourceUtil.getSkinColor(R.color.black_text_color));
                }
                break;
            default:
                break;
        }
        // FIXME 判断消息免打扰状态
        if (chatListItem.getIsBother()) {
            holder.infoBother.setVisibility(View.VISIBLE);
        } else {
            holder.infoBother.setVisibility(View.INVISIBLE);
        }


        int badgerNum = chatListItem.getBadgernum();

        switch (badgerNum) {
            // 设置公众号未读角标
            case NOREAD:
                holder.badge.setText(" ");
                holder.badge.setTag("badge");
                holder.badge.setBadgeMargin(3);
                holder.badge.setBadgeBackgroundColor(mContext.getResources().getColor(
                        R.color.chat_badge_color));
                holder.badge.setTextSize(9);
                holder.badge.show();
                break;
            // 已读，隐藏角标
            case ISREAD:
                holder.badge.setVisibility(View.GONE);
                break;
            // 设置其他消息未读角标
            default:
                holder.badge.setBadgeMargin(3);
                holder.badge.setTextSize(13);
                holder.badge.setTag("badge");
                if (chatListItem.getBadgernum() > 99) {
                    holder.badge.setText("99+");
                } else {
                    holder.badge.setText(chatListItem.getBadgernum() + "");
                }
                holder.badge.setBadgeBackgroundColor(mContext.getResources().getColor(
                        R.color.chat_badge_color));
                holder.badge.show();
                break;
        }

        if (isSupportRecognizeVoiceSend) {
            final WaveformView waveformView = holder.voiceWaveFormView;
            final ImageView imageViewPlay = holder.ivVoicePlay;
            if (chatListItem.getStruct002().getMessageChildType() == MessageChildTypeConstant.subtype_speechRecognize) {
                //存在草稿不显示语音布局
                if (EnvironmentVariable.getProperty(chatListItem.getUserId() + "" + chatListItem.getChatType()) != null &&
                        !EnvironmentVariable.getProperty(chatListItem.getUserId() + "" + chatListItem.getChatType()).equals("")) {
                    holder.llMsg.setVisibility(View.VISIBLE);
                    holder.rlVoiceWave.setVisibility(View.GONE);

                } else {
                    //不存在草稿显示语音布局
                    holder.llMsg.setVisibility(View.GONE);
                    holder.rlVoiceWave.setVisibility(View.VISIBLE);
                }
                holder.voiceWaveFormView.setShowCenterLine(false);
                //todo 下载音频文件
                ChatVoicePlayClickListener.downLoadVoiceFile(chatListItem.getStruct002(), new ChatVoicePlayClickListener.DownLoadVoiceFileCallBack() {
                    @Override
                    public void onSuccess(String filePath, String messageID) {
                        loadFromFile(chatListItem, waveformView, filePath, messageID);
                    }

                    @Override
                    public void onFail() {

                    }
                });
            } else {
                //不是语音消息不显示语音布局
                holder.llMsg.setVisibility(View.VISIBLE);
                holder.rlVoiceWave.setVisibility(View.GONE);
            }


            //发识别语音按钮
            holder.rlVoiceRecognition.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onRecognitonVoiceRecordStart != null) {
                        onRecognitonVoiceRecordStart.onRecordStart(position);
                    }
                }
            });
            holder.rlVoiceWave.setOnClickListener(new ChatVoicePlayClickListener(chatListItem.getStruct002(), mContext, new ChatVoicePlayClickListener.PlayVocieCallBack() {
                @Override
                public void startPlay(String msgID) {
                    if (!chatListItem.getStruct002().getMessageID().equals(msgID)) {
                        return;
                    }
                    imageViewPlay.setImageDrawable(mContext.getResources().getDrawable(R.drawable.qqstyle_chat_icon_receive_voice_stop));
                }

                @Override
                public void stopPlay(String msgID) {
                    if (!chatListItem.getStruct002().getMessageID().equals(msgID)) {
                        return;
                    }
                    imageViewPlay.setImageDrawable(mContext.getResources().getDrawable(R.drawable.qqstyle_chat_icon_receive_voice_start));
                }
            }));

            if (chatListItem.getStruct002().getMessageChildType() == MessageChildTypeConstant.subtype_speechRecognize) {

                //设置图标状态
                if (chatListItem.getStruct002().getExtra("isPlaying") == null) {
                    chatListItem.getStruct002().putExtra("isPlaying", false);
                    imageViewPlay.setImageDrawable(mContext.getResources().getDrawable(R.drawable.qqstyle_chat_icon_receive_voice_start));
                } else {
                    if ((boolean) chatListItem.getStruct002().getExtra("isPlaying") == true) {
                        imageViewPlay.setImageDrawable(mContext.getResources().getDrawable(R.drawable.qqstyle_chat_icon_receive_voice_stop));
                    } else {
                        imageViewPlay.setImageDrawable(mContext.getResources().getDrawable(R.drawable.qqstyle_chat_icon_receive_voice_start));
                    }
                }
            }
        }
        return convertView;
    }


    class ViewHolder {


        public ImageView icon;
        public NineGridImageView iconGroup;
        public TextView title;
        public TextView msg;
        public TextView time;
        public ImageView infoBother;// 消息免打扰图标
        LinearLayout llMsg;
        public ImageView sendState;//消息发送状态
        public TextView editing;
        public TextView mentionHint;
        public LinearLayout iconcontainerforbadage;
        public RelativeLayout chatListbagLayout;
        public BadgeView badge;
        //声音的波形控件
        public RelativeLayout rlVoiceRecognition;
        LinearLayout rlVoiceWave;
        ImageView ivVoicePlay;
        WaveformView voiceWaveFormView;
        ImageView ivVoiceSending;
        TextView tvVoiceMsg;

    }


    //Fixme 显示group的头像
    private void showGroupAvatar(Group group, NineGridImageView iconGroup) {
        int membersNum = 0;
        if (group.getUsers() != null) {
            membersNum = group.getUsers().size();
        }

        List<String> avatarList = new ArrayList<>();
        if (membersNum == 0) {
            for (int i = 0; i < 4; i++) {
                avatarList.add("");
            }
        }
        for (int i = 0; i < membersNum; i++) {
            if (i < 9) {
                String avatar = group.getUsers().get(i).getAvatar();
                if (avatar == null || !avatar.contains("http")) {
                    avatar = "";
                }
                avatarList.add(avatar);

            }
        }
        NineGridImageViewAdapter<String> mAdapter = new NineGridImageViewAdapter<String>() {
            @Override
            protected void onDisplayImage(Context context, ImageView imageView, String s) {
//                imageLoader.displayImage(s, imageView, options);
                LXGlideImageLoader.getInstance().showGroupAvatar(fragment, imageView, s);
            }

            @Override
            protected ImageView generateImageView(Context context) {
                return super.generateImageView(context);
            }
        };
        iconGroup.setAdapter(mAdapter);
        iconGroup.setImagesData(avatarList);
    }


    //todo 设置消息文字，群组消息前方加入发送人的姓名
    private void setMessageContent(IMStruct002 imStruct002, ChatListItem item, TextView msgView) {
        String frontText = "";
        if (item.getGroup() != null && imStruct002.getFromUserId() != myUserId) {
            User groupUser = WeChatDBManager.getInstance().getGroupUser(item.getGroup().getGroupId(), imStruct002.getFromUserId());
            if (groupUser != null) {
                frontText = GroupNameUtil.getGroupUserName(groupUser) + ":";
            }
        }
        msgView.setText(ChatTypeUtil.getMessageByType(mContext, imStruct002, frontText));
    }

    /**
     * 载入音频文件显示波形
     *
     * @param path
     */
    private void loadFromFile(final ChatListItem chatListItem, final WaveformView waveformView, String path, final String messageId) {

        final IMStruct002 imStruct002 = chatListItem.getStruct002();
        if (imStruct002 == null) {
            return;
        }
        if (!imStruct002.getMessageID().equals(messageId)) {
            return;
        }
        SoundFile soundFile1 = VoiceRecognitionSoundFileCacheUtil.getFromChatList(imStruct002.getMessageID());
        if (soundFile1 != null) {
            finishOpeningSoundFile(imStruct002, soundFile1, waveformView, messageId);
            return;
        }

        final File mFile = new File(path);

        Thread mLoadSoundFileThread = new Thread() {
            public void run() {
                try {
                    mSoundFile = SoundFile.create(mFile.getAbsolutePath(), null);
                    if (mSoundFile == null) {
                        return;
                    }
                    VoiceRecognitionSoundFileCacheUtil.addChatList(imStruct002.getMessageID(), mSoundFile);
//                    mPlayer = new SamplePlayer(mSoundFile);
                } catch (final Exception e) {
                    e.printStackTrace();
                    return;
                }
                Runnable runnable = new Runnable() {
                    public void run() {
                        finishOpeningSoundFile(imStruct002, mSoundFile, waveformView, messageId);

                    }
                };
                ((Activity) mContext).runOnUiThread(runnable);
            }
        };
        mLoadSoundFileThread.start();
    }

    /**
     * 音频文件载入波形完成
     *
     * @param messageId
     */

    private void finishOpeningSoundFile(IMStruct002 imStruct002, SoundFile mSoundFile, final WaveformView waveformView, String messageId) {
        if (imStruct002 == null) {
            return;

        }
        if (!imStruct002.getMessageID().equals(messageId)) {
            return;
        }
        try {
            waveformView.setSoundFile(mSoundFile);
            DisplayMetrics metrics = new DisplayMetrics();
            ((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(metrics);
            float mDensity = metrics.density;
            waveformView.recomputeHeights(mDensity);
            waveformView.setShowCenterLine(true);
            // TODO: 2018/9/27 声波图形绘制的图片已保存
//            Bitmap bitmap = ConvertUtils.view2Bitmap(waveformView);
//            ImageUtils.save(bitmap,new File(Environment.getExternalStorageDirectory()+"/test/"+System.currentTimeMillis()+"test.png"), Bitmap.CompressFormat.PNG,true);
            notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //  语音识别发送的点击回调
    public interface OnRecognitonVoiceRecordStart {
        void onRecordStart(int position);
    }

    public void setOnRecognitonVoiceRecordStart(OnRecognitonVoiceRecordStart onRecognitonVoiceRecordStart) {
        this.onRecognitonVoiceRecordStart = onRecognitonVoiceRecordStart;
    }

}
