package com.efounder.chat.utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Richard on 2018/5/5.
 * 获取文件编辑时间
 */

public class FileEditTimeUtil {

    public static String getFileEditTime(String path) {
        File f = new File(path);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(f.lastModified());
        String format = sdf.format(cal.getTime());
        return format;
    }
}
