package com.efounder.chat.zxing.qrcode;

import android.content.Context;

/**
 * 处理二维码识别结果
 * @author wang
 */
public interface IHandleQrContent {

    void handleQrContent(Context context, String resultString);

}
