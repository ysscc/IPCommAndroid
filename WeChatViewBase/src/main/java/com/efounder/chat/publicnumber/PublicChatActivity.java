package com.efounder.chat.publicnumber;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.efounder.chat.R;
import com.efounder.chat.activity.BaseActivity;

/**
 * 聊天Activity
 *
 * @author hudq
 */
@SuppressLint("ClickableViewAccessibility")
public class PublicChatActivity extends BaseActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.publicactivity_chat);
        if (savedInstanceState == null){
            replaceFragment();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        replaceFragment();
    }


    private void replaceFragment(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container,new PublicChatFragment());
        transaction.commit();
    }

}
