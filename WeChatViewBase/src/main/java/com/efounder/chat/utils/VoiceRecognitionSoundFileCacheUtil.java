package com.efounder.chat.utils;

import com.efounder.chat.view.voicedictate.SoundFile;

import java.util.HashMap;

/**
 * 语音识别缓存soundfile
 *
 * @author YQS
 */
public class VoiceRecognitionSoundFileCacheUtil {

    private static HashMap<String, Object> chatListCacheMap = new HashMap<>();
    private static HashMap<String, Object> chatMessageCacheMap = new HashMap<>();


    /**
     * 加入缓存
     *
     * @param messageId 消息id
     * @param soundFile
     */
    public static void addChatList(String messageId, SoundFile soundFile) {
        chatListCacheMap.put(messageId, soundFile);

    }

    public static void addMessageList(String messageId, SoundFile soundFile) {
        chatMessageCacheMap.put(messageId, soundFile);
    }

    public static SoundFile getFromChatList(String messageId) {
        if (chatListCacheMap.size() > 16) {
            chatListCacheMap.clear();
        }
        return (SoundFile) chatListCacheMap.get(messageId);
    }

    public static SoundFile getFromMessageList(String messageId) {
        return (SoundFile) chatMessageCacheMap.get(messageId);
    }

    //清除消息列表语音波形图缓存
    public static void clearChatList() {
        chatListCacheMap.clear();
    }

    //清除消息列表缓存（聊天）
    public static void clearMessageList() {
        chatMessageCacheMap.clear();
    }
}
