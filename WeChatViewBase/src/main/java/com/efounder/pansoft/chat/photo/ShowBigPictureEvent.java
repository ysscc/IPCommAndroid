package com.efounder.pansoft.chat.photo;

import java.util.ArrayList;

public class ShowBigPictureEvent {
    private ArrayList<SelectModel> mFiles;
    private int position;

    public ShowBigPictureEvent(ArrayList<SelectModel> mFiles, int position) {
        this.mFiles = mFiles;
        this.position = position;
    }

    public ArrayList<SelectModel> getmFiles() {
        return mFiles;
    }

    public int getPosition() {
        return position;
    }
}
