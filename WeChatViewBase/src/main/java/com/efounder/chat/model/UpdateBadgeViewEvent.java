package com.efounder.chat.model;

import com.efounder.message.struct.IMStruct002;

public class UpdateBadgeViewEvent {


    //-1:消息列表需要刷新 -2：通讯录需要刷新
    private String userID;

    //0:个人 ，1：群组 2：应用号 其他类型比如系统消息至0也可
    private byte chatType = -1;

    private boolean refreshAll = false;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public byte getChatType() {
        return chatType;
    }

    public void setChatType(byte chatType) {
        this.chatType = chatType;
    }

    public boolean isRefreshAll() {
        return refreshAll;
    }

    public void setRefreshAll(boolean refreshAll) {
        this.refreshAll = refreshAll;
    }

    public UpdateBadgeViewEvent(String _userID, byte _chatType) {
        this.chatType = _chatType;
        this.userID = _userID;
    }

}
