//package com.efounder.chat.http;
//
//import android.content.ComponentName;
//import android.content.Context;
//import android.content.Intent;
//import android.content.ServiceConnection;
//import android.os.IBinder;
//import android.util.Log;
//
//import com.efounder.chat.utils.ServiceUtils;
//import com.efounder.http.EFHttpRequest;
//import com.efounder.http.EFHttpRequest.HttpRequestListener;
//import com.efounder.message.manager.JFMessageManager;
//import com.efounder.message.socket.JFSocketHandler;
//import com.efounder.message.socket.JFSocketManagerService;
//import com.efounder.message.struct.IMStruct001;
//
//import org.json.JSONArray;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Random;
//
//public class WeChatHttpRequest implements HttpRequestListener,JFSocketHandler.OnChannelActiveListener {
//	private static final String TAG = "WeChatHttpRequest";
//	private static WeChatHttpRequest weChatHttpRequest;
//
//	private Context context;
//	private EFHttpRequest httpRequest;
//	private List<LoginListener> loginListenerList = new ArrayList<>();
//	private ServiceConnection serviceConnection;
//	private JFSocketManagerService socketManagerService;
//	private boolean serviceConnectionSuccess = false;
//
//	private WeChatHttpRequest(Context context) {
//		this.context = context;
//		this.httpRequest = new EFHttpRequest(context);
//	}
//
//	public static WeChatHttpRequest getInstance(Context context){
//		if (weChatHttpRequest == null){
//			weChatHttpRequest = new WeChatHttpRequest(context);
//		}
//		return weChatHttpRequest;
//	}
//
//	public void httpLogin(String userId,String password){
//		//TODO
//		if (JFSocketHandler.isChannelActive){//如果连接活着，直接return，不再登陆，否则会出现多终端登陆现象
//			for (LoginListener loginListener: loginListenerList) {
//				if (loginListener != null) {
//					loginListener.onLoginSuccess();
//				}
//			}
//			return;
//		}
//		//1.登录
//		String urlStr = "http://im.solarsource.cn/IMServer/user/login?userId="+userId+"&passWord=" + password;
////		String urlStr = "http://192.168.3.35:8080/IMServer/user/login?userId="+userId+"&passWord=" + password;
//		Log.i("-----", "-----登录--urlStr--" + urlStr);
//		httpRequest.setHttpRequestListener(this);
//		httpRequest.httpGet(urlStr);
//	}
//
//	@Override
//	public void onRequestSuccess(int requestCode,String response) {
//        Log.i(TAG, "onRequestSuccess-----------" + response);
//        try {
//			JSONObject jsonObject = new JSONObject(response);
//			String loginInfo = jsonObject.getString("login");
//			if ("success".equals(loginInfo)) {//返回成功
//				//{"token":"ae2b11bd-7205-47e0-86fc-89e93b7f3bf5","server":["TCP:192.168.1.115:8000"],"login":"success"}
//				//改为
//				//{"sessionTime":1470714865121,"token":"a8824966-1377-435b-a6a6-9559d3bedccb",
//				// "server":{"UDP":[],"TCP":["61.136.95.175:9690"]},"login":"success"}
//				JFMessageManager.sessionTime = jsonObject.getLong("sessionTime");
//				String token = jsonObject.getString("token");
//				JSONObject serverJsonObject = jsonObject.getJSONObject("server");
//				JSONArray jsonArrayTCP = serverJsonObject.getJSONArray("TCP");
//				//随机获取一个ip和端口
//				if (jsonArrayTCP.length() > 0){
//					String serverString = jsonArrayTCP.getString(new Random().nextInt(jsonArrayTCP.length()));
//					String[] ip_port = serverString.split(":");
//					String ip = ip_port[0];
//					String port = ip_port[1];
//					JFSocketManagerService.token = token;
//					JFSocketManagerService.URL = ip;
//					JFSocketManagerService.PORT = Integer.parseInt(port);
//					//2.登录成功，激活通道
//					serviceConnection = new ServiceConnection() {
//
//						@Override
//						public void onServiceDisconnected(ComponentName name) {
//							serviceConnectionSuccess = false;
//							Log.e("---","断线重连------登录成功，连接service 失败");
//						}
//
//						@Override
//						public void onServiceConnected(ComponentName name, IBinder service) {
//							serviceConnectionSuccess = true;
//							Log.i("---","断线重连------登录成功，连接service 成功");
//							JFSocketManagerService.JFSocketManagerServiceBinder serviceBinder = (JFSocketManagerService.JFSocketManagerServiceBinder) service;
//							socketManagerService = serviceBinder.getService();
//							socketManagerService.setOnChannelActiveListener(WeChatHttpRequest.this);
//							//TODO  连接service成功  获取service 设置监听 初始化通道 把service抽出去
//							socketManagerService.init();
//						}
//					};
//					context.startService(new Intent(context, JFSocketManagerService.class));
//					if (socketManagerService == null){
//						context.bindService(new Intent(context, JFSocketManagerService.class), serviceConnection, Context.BIND_AUTO_CREATE);
//					}else {
//						socketManagerService.init();
//					}
//
//				}
//
//			}else {
//				for (LoginListener loginListener: loginListenerList) {
//					if (loginListener != null) {
//						loginListener.onLoginFail("账号或密码错误...");
//					}
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			for (LoginListener loginListener: loginListenerList) {
//				if (loginListener != null) {
//					loginListener.onLoginFail("服务端返回数据异常，请联系管理员");
//				}
//			}
//		}
//    }
//
//	@Override
//	public void onRequestFail(int requestCode,String message) {
//        Log.e(TAG, "onRequestFail-----------" + message);
//		for (LoginListener loginListener: loginListenerList) {
//			if (loginListener != null) {
//				loginListener.onLoginFail("网络异常，请稍后再试");
//			}
//		}
//    }
//
//	public void addLoginListener(LoginListener loginListener) {
//		if (!loginListenerList.contains(loginListener)){
//			loginListenerList.add(loginListener);
//		}
//	}
//
//	public void removeLoginListener(LoginListener loginListener) {
//		if (loginListenerList.contains(loginListener)){
//			loginListenerList.remove(loginListener);
//		}
//	}
//
//
//	public interface LoginListener{
//
//		public void onLoginSuccess();
//		public void onLoginFail(String errorMsg);
//
//	}
//
//
//	@Override
//	public void onChannelActiveSuccess() {
////		//向服务器发送一条注册消息
////		IMStruct001 im = new IMStruct001();
////		im.setClientId(JFSocketManagerService.token);
////		JFMessageManager.getInstance().sendStruce(im);
//		//3.激活通道成功，表示真正登录成功，通知Activity跳转界面
//		for (LoginListener loginListener: loginListenerList) {
//			if (loginListener != null) {
//				loginListener.onLoginSuccess();
//			}
//		}
//	}
//
//	@Override
//	public void onChannelActiveFail() {
//		for (LoginListener loginListener: loginListenerList) {
//			if (loginListener != null) {
//				loginListener.onLoginFail("onChannelActiveFail：激活通道失败");
//			}
//		}
//	}
//
//	public void unBindJFSocketManagerService(){
//		if (serviceConnectionSuccess && serviceConnection != null && ServiceUtils.isServiceRunning(context,JFSocketManagerService.class.getName())) {
////			context.unbindService(serviceConnection);
//		}
//	}
//}
