package com.efounder.chat.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.activity.ChatGroupUserActivity;
import com.efounder.chat.activity.CreatChatRoomActivity;
import com.efounder.chat.model.Group;
import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.chat.widget.RoundImageView;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.mobilecomps.contacts.User;
import com.efounder.util.ToastUtil;

import java.io.Serializable;
import java.util.List;

import static com.efounder.frame.utils.Constants.CHAT_USER_ID;


public class GridUserAdapter extends BaseAdapter {

    private List<User> objects;
    Context context;
    private Group group;
    private int groupId;
    private int from;//标示来自加群的简介，还是来自群设置显示详情。根据情况显示群成员后的添加图片或者是更多图片。
    //0为来自加群，1为群设置

    public GridUserAdapter(Context context, List<User> objects, Group group, int from) {

        this.objects = objects;
        this.context = context;
        this.group = group;
        this.from = from;


    }

    public GridUserAdapter(Context context, List<User> objects, int groupId, int from) {

        this.objects = objects;
        this.context = context;
        this.groupId = groupId;
        this.from = from;
        //initImageLoader();

    }

    @Override
    public View getView(final int position, View convertView,
                        final ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.social_chatgroupfunctiongridview_item, null);
            holder.text = (TextView) convertView.findViewById(R.id.tv_username);
            holder.text.setVisibility(View.GONE);
            holder.circleAvatar = (RoundImageView) convertView.findViewById(R.id.icon1);
            holder.circleAvatar.setVisibility(View.VISIBLE);
            holder.avatar = (ImageView) convertView.findViewById(R.id.iv_avatar);
            holder.avatar.setVisibility(View.GONE);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        //// TODO: 17-1-10
        // 最后一个item，加人
        //如果from=0，来自加群请求的详情显示群成员，那么点击后仍是进入群成员界面。否则为加人
        //如果from=0,向ChatGroupUserActivity传递queryFrom=0;
        if (position == getCount() - 1) {
            convertView.setVisibility(View.VISIBLE);
            holder.circleAvatar.setImageDrawable(context.getResources().getDrawable(R.drawable.group_adduser));
            // imageLoader.displayImage("drawable://" + R.drawable.group_adduser, holder.circleAvatar, options);
            holder.circleAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (from == 1) {
                        User tempUser = null;
                        if (group.getUsers() != null) {
                            for (User user : group.getUsers()) {
                                if (user.getId() == Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID))) {
                                    tempUser = user;
                                }
                            }
                            if (tempUser == null) {
                                return;
                            }
                            if (group.getCreateId() != Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)) && tempUser.getGroupUserType() != User.GROUPCREATOR && tempUser.getGroupUserType() != User.GROUPMANAGER) {
                                ToastUtil.showToast(context, R.string.chat_only_owner_admin_this_operation);
                                return;
                            }
                            Intent intent = new Intent(context,
                                    CreatChatRoomActivity.class);
                            intent.putExtra("chatgroup", (Serializable) group);

                            context.startActivity(intent);


                        } else {
                            ToastUtil.showToast(context, R.string.chat_operation_unavailable);
                        }
                    } else {//进入群成员界面
                        Intent intent = new Intent(context, ChatGroupUserActivity.class);
                        intent.putExtra("id", groupId);
                        intent.putExtra("queryFrom", 0);
                        context.startActivity(intent);
                    }

                }
            });


        } else {
            User user = objects.get(position);
//            String usernick = "";
//            User friendUser = GroupNameUtil.getGroupUser(group.getGroupId(), user.getId(), WeChatDBManager.getInstance());
//            if (!friendUser.getNickName().equals(String.valueOf(user.getId()))) {
//                usernick = GroupNameUtil.getGroupUserName(friendUser);
//            } else {
//                usernick = user.getNickName();
//            }
//            holder.text.setText(usernick);

            holder.circleAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ChatGroupUserActivity.class);
                    if (from == 0) {
                        intent.putExtra("queryFrom", 0);
                        intent.putExtra("id", groupId);
                    } else {
                        intent.putExtra("id", group.getGroupId());
                    }
                    context.startActivity(intent);
                }
            });
            if (user.getAvatar().contains("http")) {
                LXGlideImageLoader.getInstance().showUserAvatar(context, holder.circleAvatar, user.getAvatar());

            } else {
                LXGlideImageLoader.getInstance().showUserAvatar(context, holder.circleAvatar, "");

            }
        }
        return convertView;
    }

    @Override
    public int getCount() {
        return objects.size() + 1;


    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * 异步加载头像
     */
//    public void initImageLoader() {
//        // 初始化异步加载图片的类
//        imageLoader = ImageLoader.getInstance();
//        // 设置异步加载图片的配置信息
//        options = ImageUtil.getImageLoaderOptions(R.drawable.default_useravatar);
//
//    }

    class ViewHolder {
        TextView text;
        ImageView avatar;
        RoundImageView circleAvatar;

    }
}