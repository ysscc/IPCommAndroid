package com.efounder.chat.struct;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.efounder.message.struct.IMStruct002;

public class MessageMap {
	private static Map<String, List<IMStruct002>> messageMap = new HashMap<String, List<IMStruct002>>();
	
	public static void putMessage(String fromUserId,IMStruct002 struct002){
		if (messageMap.containsKey(fromUserId)) {
			messageMap.get(fromUserId).add(struct002);
		}else {
			List<IMStruct002> struct002s = new ArrayList<IMStruct002>();
			struct002s.add(struct002);
			messageMap.put(fromUserId, struct002s);
		}
	}
	
	public static List<IMStruct002> getMessage(String fromUserId){
		List<IMStruct002> struct002s = messageMap.get(fromUserId);
		if (struct002s == null) {
			struct002s = new ArrayList<IMStruct002>();
		}
		return struct002s;
	}
	
	public static void removeMessage(){
		
	}
}
