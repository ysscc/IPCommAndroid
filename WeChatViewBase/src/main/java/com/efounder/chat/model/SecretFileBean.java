package com.efounder.chat.model;

/**
 * Created by Richard on 2018/5/5.
 */

public class SecretFileBean {
    //文件类型
    public String fileType;
    //文件路径
    public String filePath;
    //文件名字
    public String fileName;
    //文件大小
    public String fileSize;
}
