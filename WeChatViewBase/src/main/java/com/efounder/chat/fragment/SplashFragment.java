package com.efounder.chat.fragment;

import java.io.File;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.efounder.chat.R;
import com.efounder.frame.baseui.EFFragment;


/**
 * 开屏页
 *
 */
public class SplashFragment extends EFFragment {
	private static final String TAG = "SplashFragment";
	private View view;
	private SplashFragmentCallBack callBack;
//	private TextView versionText;
	
	private static final int sleepTime = 2000;

	@Override
    public void onAttach(Activity context) {
    	try {
			callBack = (SplashFragmentCallBack) context;
		} catch (Exception e) {
			Log.e(TAG, "使用SplashFragment，必须实现SplashFragmentCallBack接口");
			e.printStackTrace();
		}
    	super.onAttach(context);
    }
	@Override
	public View onCreateView(LayoutInflater inflater,
			 ViewGroup container,  Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.fragment_splash, container,false);
		initFile();
		return view;
	}

	
	@Override
	public void onStart() {
		super.onStart();

		new Thread(new Runnable() {
			public void run() {
				//判断是否已经登录
				if (false) {
//				if (DemoHXSDKHelper.getInstance().isLogined()) {
					// ** 免登陆情况 加载所有本地群和会话
					//不是必须的，不加sdk也会自动异步去加载(不会重复加载)；
					//加上的话保证进了主页面会话和群组都已经load完毕
					long start = System.currentTimeMillis();
//					EMGroupManager.getInstance().loadAllGroups();
//					EMChatManager.getInstance().loadAllConversations();
					long costTime = System.currentTimeMillis() - start;
					//等待sleeptime时长
					if (sleepTime - costTime > 0) {
						try {
							Thread.sleep(sleepTime - costTime);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					//
					//进入主页面
//					startActivity(new Intent(SplashFragment.this, MainActivity.class));
//					finish();
					callBack.hasLogin();
				}else {
					try {
						Thread.sleep(sleepTime);
					} catch (InterruptedException e) {
					}
//					startActivity(new Intent(SplashFragment.this, LoginActivity.class));
//					finish();

					callBack.hasNotLogin();
				}
			}
		}).start();

	}
	
	/**
	 * 获取当前应用程序的版本号
	 */
	private String getVersion() {
		PackageManager pm = getActivity().getPackageManager();
		try {
			PackageInfo packinfo = pm.getPackageInfo(getActivity().getPackageName(), 0);
			String version = packinfo.versionName;
			return version;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
			return "版本号错误";
		}
	}
	 @SuppressLint("SdCardPath")
     public void initFile() {

      File dir = new File("/sdcard/fanxin");

         if (!dir.exists()) {
             dir.mkdirs();
         }
     }
	 
	public interface SplashFragmentCallBack {
		public void hasLogin();
		public void hasNotLogin();
	}
}
