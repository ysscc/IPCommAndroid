package com.efounder.chat.model;

/**
 * 本地查询的bean
 *
 * @autor yqs
 * @date 2018/10/23 19:36
 **/
public class ChatLocalSerachBean {


    public static final int TYPE_FRIEND = 0;
    public static final int TYPE_GROUP = 1;
    public static final int TYPE_TITLE = 2;
    private Object bean;
    private int type;
    private int id;
    private String name;

    private ChatLocalSerachBean(Builder builder) {
        setBean(builder.bean);
        setType(builder.type);
        setId(builder.id);
        setName(builder.name);
    }

    public Object getBean() {
        return bean;
    }

    public ChatLocalSerachBean setBean(Object bean) {
        this.bean = bean;
        return this;
    }

    public int getType() {
        return type;
    }

    public ChatLocalSerachBean setType(int type) {
        this.type = type;
        return this;
    }

    public int getId() {
        return id;
    }

    public ChatLocalSerachBean setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public ChatLocalSerachBean setName(String name) {
        this.name = name;
        return this;
    }

    public static final class Builder {
        private Object bean;
        private int type;
        private int id;
        private String name;

        public Builder() {
        }

        public Builder bean(Object val) {
            bean = val;
            return this;
        }

        public Builder type(int val) {
            type = val;
            return this;
        }

        public Builder id(int val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public ChatLocalSerachBean build() {
            return new ChatLocalSerachBean(this);
        }
    }

}
