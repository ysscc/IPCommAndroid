package com.efounder.chat.view;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.efounder.chat.R;
import com.efounder.chat.interf.IChatTopFloatView;
import com.efounder.chat.model.ChatTopFloatModel;
import com.utilcode.util.SizeUtils;

/**
 * 聊天界面上方显示的悬浮窗
 *
 * @author wang
 */
public class ChatTopFloatView extends FrameLayout implements IChatTopFloatView {

    public ChatTopFloatView(@NonNull Context context) {
        this(context, null);
    }

    public ChatTopFloatView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }
    public ChatTopFloatView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setParentMargin(int left, int top, int right, int bottom) {
        setMargin(left, top, right, bottom);
    }

    /**
     * 设置布局的边距
     * margin dp单位
     */
    private void setMargin(int left, int top, int right, int bottom) {
        LayoutParams params = (LayoutParams) this.getLayoutParams();
        if (params == null) {
            params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        params.topMargin = (int) getResources().getDimension(R.dimen.height_top_bar) + SizeUtils.dp2px(top);
        params.leftMargin = SizeUtils.dp2px(left);
        params.rightMargin = SizeUtils.dp2px(right);
        params.bottomMargin = SizeUtils.dp2px(bottom);
        this.setLayoutParams(params);
    }


    @Override
    public View getCustomContentView() {
        return getChildAt(0);
    }

    @Override
    public void setParentView(View parentView) {

    }

    @Override
    public View getParentView() {
        return null;
    }

    @Override
    public void setData(ChatTopFloatModel data) {

    }

    @Override
    public void show() {
        setVisibility(VISIBLE);
    }

    @Override
    public void hide() {
        setVisibility(GONE);
    }
}
