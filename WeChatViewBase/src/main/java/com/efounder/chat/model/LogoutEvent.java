package com.efounder.chat.model;

/**
 * 下线的EventBus事件
 * Created by will on 17-7-4.
 */

public class LogoutEvent {

    //默认 停止服务 重启应用
    public static int TYPE_DEFAULT = 0;

    //应用下线消息
    public static int TYPE_OFFLINE = 1;
    //应用登陆过期
    public static int TYPE_LOGIN_OUT_OF_DATE = 2;
    int type;
    //应用下线的提示
    String msg;

    public LogoutEvent() {
    }

    public LogoutEvent(int type) {
        this.type = type;
    }

    public LogoutEvent(int type, String msg) {
        this.type = type;
        this.msg = msg;
    }

    public int getType() {
        return type;
    }

    public LogoutEvent setType(int type) {
        this.type = type;
        return this;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
