package com.efounder.pansoft.chat.messageview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

/**
 *	隐藏的messgeview item
 * @author yas
 *  增加系统消息类型
 */
public class JFMessageHideView extends JFMessageBaseView {

	public JFMessageHideView(Context context) {
		super(context);
		initView(context);
	}

	public JFMessageHideView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView(context);
	}

	private void initView(final Context context) {
		// 消息体区域
		this.removeAllViews();
		this.setVisibility(View.GONE);


	}
	

}
