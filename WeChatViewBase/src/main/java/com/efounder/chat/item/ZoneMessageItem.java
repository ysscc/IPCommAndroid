package com.efounder.chat.item;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.efounder.chat.R;
import com.efounder.chat.item.manager.IMessageItem;
import com.efounder.frame.ViewSize;
import com.efounder.message.struct.IMStruct002;
import com.efounder.ui.util.DisplayUtil;
import com.efounder.utils.ResStringUtil;
import com.efounder.view.RoundFrameLayout;
import com.utilcode.util.ReflectUtils;

import net.sf.json.JSONArray;

public class ZoneMessageItem extends RoundFrameLayout implements IMessageItem, View.OnClickListener {

    private static final String TAG = "ZoneMessageItem";


    private Context mContext;
    private LayoutInflater mInflater;
    private RoundFrameLayout contentRl;
    private ImageView mMapView;
    //详细地址
    private TextView addressTv;
    //地点名称
    private TextView tvNameView;
    private TextView tvTime;
    private IMStruct002 iMStruct002;


    private String picPath = "";
    private String contentText = "";
    private String postTime = "";
    private int postId;
    private String details;

    private MultiTransformation multi;

    public ZoneMessageItem(Context context) {
        super(context);
        this.mContext = context;
        mInflater = LayoutInflater.from(context);
        initView();
        initListener();
//         multi = new MultiTransformation(
//                new CenterCrop(),
//                 new RoundedCornersTransformation(11, 0,
//                         RoundedCornersTransformation.CornerType.TOP));

    }

    private void initListener() {
        this.setOnClickListener(this);
    }


    private void initView() {
        contentRl = (RoundFrameLayout) mInflater.inflate(R.layout.chat_item_zone_message, this);

        LinearLayout relativeLayout = contentRl.findViewById(R.id.rllayout);
//        GradientDrawable myGrad = (GradientDrawable) relativeLayout.getBackground();
//        myGrad.setColor(mContext.getResources().getColor(R.color.white));
        mMapView = (ImageView) contentRl.findViewById(R.id.iv_map);
        mMapView.setVisibility(GONE);
        addressTv = (TextView) contentRl.findViewById(R.id.tv_address);
        tvNameView = contentRl.findViewById(R.id.tv_name);
        tvTime = contentRl.findViewById(R.id.tv_time);

        FrameLayout.LayoutParams lp = new LayoutParams(
                DisplayUtil.getMobileWidth(mContext) * 2 / 3 + 50, ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setLayoutParams(lp);
    }

    private void showPic() {

        if (picPath.equals("")) {
            mMapView.setVisibility(GONE);
        } else {
            mMapView.setVisibility(VISIBLE);
        }
        Glide.with(mContext).load(picPath)
                .apply(new RequestOptions()
                                //.error(R.drawable.loading_image_background)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                        //  .bitmapTransform(multi)
                )
//                .listener(new RequestListener<Drawable>() {
//                    @Override
//                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//                        return false;
//                    }
//
//                    @Override
//                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
//                        return false;
//                    }
//                })
                .into(mMapView);

    }

//    public void setUpImageViewParams() {
//        int width = 500;
//        int height = 500 / 3 * 2;
//        int reSizeWidth = this.getWidth() - 20;
//        int reSizeHeight = (int) (height * ((float) reSizeWidth / (float) width));
//        // mMapView.getLayoutParams().width =reSizeWidth;
//        mMapView.getLayoutParams().height = reSizeHeight;
//    }

    @Override
    public View messageView() {

        return this;
    }

    @Override
    public ViewSize messageViewSize() {
        return null;
    }

    @Override
    public void onClick(View v) {
        ReflectUtils reflectUtils = ReflectUtils.reflect("com.efounder.zone.activity.MobileZoneDetailActivity");
        reflectUtils.method("start", mContext, postId);
    }

    @Override
    public void setIMStruct002(IMStruct002 message) {
        this.iMStruct002 = message;
        details = new String(iMStruct002.getBody());
        Log.i(TAG, "---details---" + details);


        try {
            net.sf.json.JSONObject jsonObject = net.sf.json.JSONObject.fromObject(details);
            String showMessage = jsonObject.optString("showMsg", "");
            if (jsonObject.containsKey("obj")) {
                net.sf.json.JSONObject obj = jsonObject.optJSONObject("obj");
                postId = jsonObject.optInt("objId");
                int transmitStatus = obj.optInt("transmitStatus");
//            objBean.setTransmitSourceUserid(obj.optInt("transmitSourceUserid"));
//            objBean.setTransmitSourceUsername(obj.optString("transmitSourceUsername"));
//            objBean.setTransmitContent(obj.optString("transmitContent"));
//                int postUserId = obj.optInt("postAuthor");
//                postUserName = obj.optString("postUserName");
//                if (postUserName.equals("")) {
//                    WeChatDBManager weChatDBManager = WeChatDBManager.getInstance();
//                    User user = weChatDBManager.getOneUserById(postUserId);
//                    postUserName = user.getNickName();
//                }
                String content = obj.optString("postContent");
                try {
                    net.sf.json.JSONObject contentObject = net.sf.json.JSONObject.fromObject(content);
                    int type = contentObject.optInt("type");
                    contentText = contentObject.optString("text");
                    if (type == 2) {
                        if (contentText.equals("")) {
                            tvNameView.setVisibility(GONE);
                        } else {
                            tvNameView.setVisibility(VISIBLE);
                        }
                        JSONArray imgArray = contentObject.optJSONArray("images");
                        if (imgArray.size() > 0) {

                            picPath = imgArray.getString(0);
                        } else {
                            picPath = "";
                        }

                    } else {
                        picPath = "";
                        if (type == 3) {
                            //视频
                            contentText = ResStringUtil.getString(R.string.wechatview_video_dynamics_kh) + contentText;
                        } else if (type == 4) {
                            //网页
                            if (!contentText.equals("")) {
                                contentText = ResStringUtil.getString(R.string.wechatview_web_dynamics_kh) + contentText;
                            }else {
                                contentText = ResStringUtil.getString(R.string.wechatview_web_dynamics_kh) + contentObject.optJSONObject("web").optString("url");
                            }
                        } else if (type == 5) {
                            //商品分享
                            //if (!contentText.equals("")) {
                                contentText = ResStringUtil.getString(R.string.wechatview_good_share_kh) + contentText;
                           // }
                            picPath = contentObject.optJSONObject("goods").getString("goodCover");
//                            else {
//                                contentText = "[分享]" + contentObject.optJSONObject("web").optString("url");
//                            }
                        }
                    }
                } catch (Exception e) {
                    //转发消息的postcontent是id
                    contentText = ResStringUtil.getString(R.string.wechatview_forword_dynamics_kh);
                    e.printStackTrace();
                }
                try {
                    String rawTime = obj.optString("postCreatetime");
                    if (rawTime.length() == 14) {
                        //20180703165147
                        postTime = rawTime.substring(0, 4) + "-" +
                                rawTime.substring(4, 6) + "-" + rawTime.substring(6, 8) + "- " +
                                rawTime.substring(8, 10) + ":" + rawTime.substring(10, 12) + ":" +
                                rawTime.substring(12, 14);
                    } else if (rawTime.length() == 13) {
                        //1530609332000,毫秒格式
                        postTime = com.utilcode.util.TimeUtils.millis2String(Long.valueOf(rawTime));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    postTime = "";
                }

            }

            showPic();

            tvNameView.setText(contentText);
            addressTv.setText(showMessage);
            tvTime.setText(postTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean getIsInUse() {
        return this.isShown();
    }

    @Override
    public void setIsInUse(boolean isInUse) {
    }

    @Override
    public void prepareForReuse() {
        mMapView.setVisibility(GONE);
        picPath = "";
        contentText = "";
        postTime = "";
        tvNameView.setVisibility(VISIBLE);
    }

}
