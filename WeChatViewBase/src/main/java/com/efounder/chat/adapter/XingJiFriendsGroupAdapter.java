package com.efounder.chat.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.chat.view.fixedheader.adapter.RecyclerExpandBaseAdapter;
import com.efounder.chat.view.fixedheader.entity.ExpandGroupItemEntity;
import com.efounder.mobilecomps.contacts.User;


/**
 * @author zzj
 * @desc 星际联系人分组悬浮列表的适配器
 */
public class XingJiFriendsGroupAdapter extends RecyclerExpandBaseAdapter<String, User, RecyclerView.ViewHolder> {

	private Context context;
	//列表组里成员点击事件
	private OnItemClickListener onItemClickListener;
	private OnItemLongClickListener onItemLongClickListener;
	private OnTitleItemLongClickListener onTitleItemLongClickListener;

	public XingJiFriendsGroupAdapter(Context context) {
		this.context = context;
	}

	/**
	 * 组列表的长按事件接口
	 */
	public interface OnTitleItemLongClickListener{
		void onTitleItemLongClickListener();
	}

	/**
	 * 列表中组里成员的长按事件
	 */
	public interface OnItemLongClickListener{
		void onItemLongClickListener(User subItem, int position);
	}

	public void setOnItemLongClickListener(OnItemLongClickListener onItemLongClickListener) {
		this.onItemLongClickListener = onItemLongClickListener;
	}

	public void setOnTitleItemLongClickListener(OnTitleItemLongClickListener onTitleItemLongClickListener) {
		this.onTitleItemLongClickListener = onTitleItemLongClickListener;
	}

	public interface OnItemClickListener{
		void onItemClickListener(User subItem, int position);
	}

	public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
		this.onItemClickListener = onItemClickListener;
	}

	/**
	 * 悬浮标题栏被点击的时候，展开收起切换功能
	 */
	public void switchExpand(int adapterPosition) {
		int groupIndex = mIndexMap.get(adapterPosition).getGroupIndex();
		ExpandGroupItemEntity entity = mDataList.get(groupIndex);
		entity.setExpand(!entity.isExpand());
		notifyDataSetChanged();
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		if (viewType == VIEW_TYPE_ITEM_TITLE) {
			TitleItemHolder holder = new TitleItemHolder(
				LayoutInflater.from(parent.getContext()).inflate(R.layout.espwechatview_recycler_item_xingji_friend_expand_title, parent, false));
			holder.itemView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					ExpandGroupItemEntity entity = (ExpandGroupItemEntity) v.getTag();
					entity.setExpand(!entity.isExpand());
					notifyDataSetChanged();
				}
			});

			return holder;
		} else {
			return new SubItemHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.espwechatview_recycler_item_xingji_friend_expand_sub, parent, false));
		}
	}

	@Override
	public RecyclerView.ViewHolder onCreatePinnedViewHolder(ViewGroup parent, int viewType) {
		TitleItemHolder holder = (TitleItemHolder) super.onCreatePinnedViewHolder(parent, viewType);
		return holder;
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
		if (getItemViewType(position) == VIEW_TYPE_ITEM_TITLE) {
			int groupIndex = mIndexMap.get(position).getGroupIndex();
			TitleItemHolder itemHolder = (TitleItemHolder) holder;
			itemHolder.itemView.setTag(mDataList.get(groupIndex));
			itemHolder.mTextTitle.setText(mDataList.get(groupIndex).getParent());
			ExpandGroupItemEntity expandGroupItemEntity = (ExpandGroupItemEntity)itemHolder.itemView.getTag();
			itemHolder.iv_recycler_item_friend_expand_title_arrow.setBackgroundResource(expandGroupItemEntity.isExpand()
					?R.drawable.espwechatview_ic_friends_arrow_down:R.drawable.espwechatview_ic_friends_arrow_right);
			itemHolder.tv_recycler_item_friend_expand_title_person.setText(mIndexMap.get(position).getChildCount()+"");
			//添加列表中的组的长按事件回调
			if(onTitleItemLongClickListener!=null){
				itemHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
					@Override
					public boolean onLongClick(View v) {
						onTitleItemLongClickListener.onTitleItemLongClickListener();
						return true;
					}
				});
			}
		} else {
			final SubItemHolder subHolder = (SubItemHolder) holder;
			int groupIndex = mIndexMap.get(position).getGroupIndex();
			int childIndex = mIndexMap.get(position).getChildIndex();
			final User subItem = mDataList.get(groupIndex).getChildList().get(childIndex);
			subHolder.itemView.setTag(subItem);
			subHolder.mTextUsers.setText(subItem.getReMark());
			subHolder.mTextState.setText(subItem.getSigNature());
			LXGlideImageLoader.getInstance().showRoundUserAvatar(context, subHolder.iv_linkman_friend_portrait, subItem.getAvatar()
					,LXGlideImageLoader.DEFATLT_AVATAR_RADIUS_10);
			//添加点击事件
			if( onItemClickListener!=null ){
				subHolder.itemView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						onItemClickListener.onItemClickListener(subItem,position);
					}
				});

			}
			if(onItemLongClickListener!=null){
				subHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
					@Override
					public boolean onLongClick(View v) {
						onItemLongClickListener.onItemLongClickListener(subItem,position);
						return true;
					}
				});
			}
		}
	}

	@Override
	public void onBindPinnedViewHolder(RecyclerView.ViewHolder holder, int position) {
		super.onBindPinnedViewHolder(holder, position);
		TitleItemHolder itemHolder = (TitleItemHolder) holder;


	}



	static class TitleItemHolder extends RecyclerView.ViewHolder {

		TextView  mTextTitle;//分组的名字
		TextView tv_recycler_item_friend_expand_title_person; //人数
		ImageView iv_recycler_item_friend_expand_title_arrow;//箭头


		TitleItemHolder(View itemView) {
			super(itemView);
			mTextTitle = itemView.findViewById(R.id.text_title);
			iv_recycler_item_friend_expand_title_arrow = itemView.findViewById(R.id.iv_recycler_item_friend_expand_title_arrow);
			tv_recycler_item_friend_expand_title_person = itemView.findViewById(R.id.tv_recycler_item_friend_expand_title_person);
		}
	}

	static class SubItemHolder extends RecyclerView.ViewHolder {

		TextView  mTextUsers;
		TextView  mTextState;
		ImageView iv_linkman_friend_portrait;
		SubItemHolder(View itemView) {
			super(itemView);
			mTextUsers = itemView.findViewById(R.id.tv_linkman_friend_name);
			mTextState = itemView.findViewById(R.id.tv_linkman_friend_status);
			iv_linkman_friend_portrait = itemView.findViewById(R.id.iv_linkman_friend_portrait);
		}
	}
}
