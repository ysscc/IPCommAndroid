package com.efounder.chat.utils;

import android.content.Context;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;

/**
 * Created by zhangshunyun on 2017/6/16.
 * <p>
 * 用于群打卡提示用户打开wifi gps等
 */

public class DialogUtil {
    public static void showDialog(Context context, String title, String message, String positiveBtnText, DialogInterface.OnClickListener positiveListener, String negativeBtnText,
                                  DialogInterface.OnClickListener negativeListener) {
        final AlertDialog.Builder normalDialog = new AlertDialog.Builder(context);
        normalDialog.setTitle(title);
        normalDialog.setPositiveButton(positiveBtnText, positiveListener);
        normalDialog.setNegativeButton(negativeBtnText, negativeListener);
        normalDialog.setMessage(message);
        normalDialog.show();
    }

}
