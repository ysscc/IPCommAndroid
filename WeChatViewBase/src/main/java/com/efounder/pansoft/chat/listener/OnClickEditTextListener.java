package com.efounder.pansoft.chat.listener;

/**
 * Created by caiyaoguan on 2017/6/8.
 */

public interface OnClickEditTextListener {
    void onTouchEditText();
}
