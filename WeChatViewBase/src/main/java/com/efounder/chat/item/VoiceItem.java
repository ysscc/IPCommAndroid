package com.efounder.chat.item;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.adapter.ChatVoicePlayClickListener;
import com.efounder.chat.item.manager.IMessageItem;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.frame.ViewSize;
import com.efounder.message.struct.IMStruct002;
import com.efounder.util.JSONUtil;

import static com.efounder.frame.utils.Constants.CHAT_USER_ID;

/**
 * @author yqs 语音消息item
 */
public class VoiceItem extends LinearLayout implements IMessageItem{

    private Context mContext;
    private IMStruct002 iMStruct002;
    private RelativeLayout recVoiceRelativeLayout;//接收到的消息布局
    private RelativeLayout sendVoiceRelativeLayout;//发送的消息布局
    private LayoutInflater inflate;
    private LinearLayout voiceLayout;//语音消息的线性布局
    private Activity activity;
    private ImageView recAnimImageView;// 播放语音会跳动的imageview
    private ImageView sendAnimImageView;// 播放语音会跳动的imageview
    private TextView recVoiceLengthView;// 语音时间
    private TextView sendVoiceLengthView;// 语音时间
    private ImageView unreadVoice;//收到消息是否播放//// TODO: 接收的语音消息是否被点开播放

    BaseAdapter baseAdapter;

    public VoiceItem(Context context, BaseAdapter adapter) {
        super(context);
        mContext = context;
        activity = (Activity) context;
        baseAdapter = adapter;

    }
    public VoiceItem(Context context) {
        super(context);
        mContext = context;
        activity = (Activity) context;
        initView(context);
    }

    private void initView(Context context) {

        inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        voiceLayout = (LinearLayout) inflate.inflate(R.layout.chat_item_voice,this);
        sendVoiceRelativeLayout = (RelativeLayout) voiceLayout.findViewById(R.id.chat_item_send_voice);
        recVoiceRelativeLayout = (RelativeLayout) voiceLayout.findViewById(R.id.chat_item_receive_voice);
        recAnimImageView = (ImageView) recVoiceRelativeLayout.findViewById(R.id.iv_receive_voice);
        sendAnimImageView = (ImageView) sendVoiceRelativeLayout.findViewById(R.id.iv_send_voice);
        recVoiceLengthView = (TextView) recVoiceRelativeLayout.findViewById(R.id.tv_receive_length);
        sendVoiceLengthView = (TextView) sendVoiceRelativeLayout.findViewById(R.id.tv_send_length);
        unreadVoice = (ImageView) recVoiceRelativeLayout.findViewById(R.id.iv_unread_voice);
    }


    @Override
    public View messageView() {
        return this;
    }

    @Override
    public ViewSize messageViewSize() {
        return null;
    }

    @Override
    public void setIMStruct002(IMStruct002 message) {
        this.iMStruct002 = message;
        int childType = (iMStruct002.getToUserType() & 0x000000ff);

        /**
         * initView()中同时加载了发送和接受的RelativeLayout,在这里通过对ImStruct002的判断,
         * 来控制发送的或接收的语音消息的显示隐藏
         */
        if (Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)) == message.getFromUserId()) {
            recVoiceRelativeLayout.setVisibility(View.GONE);
            sendVoiceRelativeLayout.setVisibility(View.VISIBLE);
            dataChanged(message, sendAnimImageView, null,sendVoiceLengthView);
        }else {
            recVoiceRelativeLayout.setVisibility(View.VISIBLE);
            sendVoiceRelativeLayout.setVisibility(View.GONE);
            dataChanged(message,recAnimImageView,unreadVoice,recVoiceLengthView);
        }
    }

    /**
     * IMStruct002改变时,操作View
     */
    private void dataChanged(IMStruct002 message,ImageView animImageView,ImageView unreadVoiceView,TextView voiceLengthView){
        String voiceTime = JSONUtil.parseJson(message.getMessage()).get("time").getAsString();
        voiceLengthView.setText(voiceTime+'"');
        animImageView.setVisibility(View.VISIBLE);
        voiceLengthView.setVisibility(View.VISIBLE);
        setOnClickListener(new ChatVoicePlayClickListener(message,animImageView,unreadVoiceView,mContext));
    }

    @Override
    public boolean getIsInUse() {
        return this.isShown();
    }

    @Override
    public void setIsInUse(boolean isInUse) {
    }

    @Override
    public void prepareForReuse() {
        sendAnimImageView.setImageDrawable(getResources().getDrawable(R.drawable.chatto_voice_playing));
        recAnimImageView.setImageDrawable(getResources().getDrawable(R.drawable.chatfrom_voice_playing));
    }
}
