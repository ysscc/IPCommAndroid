package com.efounder.chat.http;

/**
 * Created by will on 16-12-23.
 */

public interface ProgressResponseListener {
    void onResponseProgress(long bytesRead, long contentLength, boolean done);
}
