package com.efounder.chat.fragment;


import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.fragment.ChatSenderFragment.SendMessageCallback;
import com.efounder.chat.struct.StructFactory;
import com.efounder.chat.widget.TopPopMenu;
import com.efounder.message.struct.IMStruct002;


public class ChatOfficialAccountsSenderFragment extends Fragment implements OnClickListener{
	public static final String TAG = "OfficialAccountsSender";
	
	private OnOfficialAccountKyeboardClickListener OnOfficialAccountKyeboardClickListener;
	private SendMessageCallback sendMessageCallback;
	/** 公众号键盘 **/
	private ImageView iv_official_account_keyboard;
//	private String[] menuNames = {"费用报销","生产日报","我",};
	private String[] menuNames = {"需求计划","发货签认",};

//	private String [][] menuItemsArray = {
//			{"出差报销","招待费报销","通讯费报销"},
//			{"生产日报1","生产日报2","生产日报3","生产日报4"},
//			null
//		};
	private String [][] menuItemsArray = {null,
			null
		};

//	@SuppressWarnings("serial")
//	Map<String, String[]> datas = new HashMap<String, String[]>(){
//		{
//			put("我", new String[]{"免费通知提醒","一卡通余额查询","信用卡账单查询"});
//			put("发现", new String[]{"月开鑫","朝朝盈","经典游戏","本地特惠"});
//			put("无卡取款", null);
//		}
//	};
	
	TopPopMenu[] chatPopMenus;
	
	@Override
	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_chat_official_accounts, container, false);
		
		chatPopMenus = new TopPopMenu[menuNames.length];
		
		initView(view);
		setupView(view);
		
		return view;
	}
	
	private void initView(View view) {
		iv_official_account_keyboard = (ImageView) view.findViewById(R.id.iv_official_account_keyboard);
		iv_official_account_keyboard.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (OnOfficialAccountKyeboardClickListener != null) {
					OnOfficialAccountKyeboardClickListener.onOfficialAccountKyeboardClick(v, true);
				}
			}
		});
	}
	
	private void setupView(View view) {
		// 1.LinearLayout
		LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.item_container);
		// 2.TextView
		for (int i = 0; i < menuNames.length; i++) {
			String menuName = menuNames[i];
			LinearLayout.LayoutParams textViewParams = new LinearLayout.LayoutParams(
					0, LayoutParams.MATCH_PARENT, 1);
			if (i < menuNames.length - 1) {
				textViewParams.rightMargin = 1;
			}
			TextView textView = new TextView(getActivity());
			textView.setLayoutParams(textViewParams);
			textView.setGravity(Gravity.CENTER);
			textView.setText(menuName);
			textView.setTag(i);
			textView.setTextSize(17);
			textView.setOnClickListener(this);
			// 3.chatPopMenu
			String[] menuItems = menuItemsArray[i];
			if (menuItems != null && menuItems.length > 0) {
				textView.setBackgroundResource(R.drawable.official_account_tv_bg);
				chatPopMenus[i] = new TopPopMenu(getActivity());
				chatPopMenus[i].addItems(menuItems);
				final int textViewposition = i;
				chatPopMenus[i].setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(AdapterView<?> parent,
									View view, int position, long id) {
								TextView textView = (TextView) view;
//								Toast.makeText(getActivity(),
//										textView.getText(), Toast.LENGTH_SHORT)
//										.show();
								chatPopMenus[textViewposition].dismiss();
								if (sendMessageCallback != null) {
									String json = "{\"content\":[{\"title\":\""+textView.getText()+"\",\"image\":\"assets://00.png\",\"url\":\"http://www.sina.com.cn/\"},{\"title\":\"招待费\",\"image\":\"assets://01.png\",\"url\":\"http://www.163.com/\"},{\"title\":\"会议费\",\"image\":\"assets://02.png\",\"url\":\"http://www.sohu.com/\"}]}";
									IMStruct002 struct002 = StructFactory.getInstance().createOfficialAccountStruct(json);
									sendMessageCallback.sendMessage(struct002);
								}
							}
						});
			}else {
				textView.setBackgroundResource(R.color.white);
			}

			linearLayout.addView(textView);
		}
	}
	

	@Override
	public void onClick(View v) {
		int position = (Integer) v.getTag(); 
		TopPopMenu chatPopMenu = chatPopMenus[position];
		if (chatPopMenu != null) {
			chatPopMenu.show(v);
		}
	}
	

	public void setOnOfficialAccountKyeboardClickListener(
			OnOfficialAccountKyeboardClickListener onOfficialAccountKyeboardClickListener) {
		OnOfficialAccountKyeboardClickListener = onOfficialAccountKyeboardClickListener;
	}


	public void setSendMessageCallback(SendMessageCallback sendMessageCallback) {
		this.sendMessageCallback = sendMessageCallback;
	}


	/**
	 * 点击公众号键盘监听
	 * 
	 * @author hudq
	 * 
	 */
	public interface OnOfficialAccountKyeboardClickListener {
		/**
		 * 点击公众号键盘
		 * @param v
		 * @param isClickFromOfficialAccountView 点击事件是否来自公众号的View
		 */
		public void onOfficialAccountKyeboardClick(View v,boolean isClickFromOfficialAccountView);
	}
	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Log.i(TAG, TAG + "-----------requestCode:" + requestCode + ",resultCode" + resultCode);
	}
	
}
