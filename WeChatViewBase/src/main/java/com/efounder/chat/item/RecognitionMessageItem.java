package com.efounder.chat.item;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.efounder.chat.R;
import com.efounder.chat.item.manager.IMessageItem;
import com.efounder.frame.ViewSize;
import com.efounder.message.struct.IMStruct002;
import com.efounder.ui.util.DisplayUtil;

import org.json.JSONObject;

import java.text.SimpleDateFormat;

/**
 * 监控消息item
 * Created by slp on 2018/1/29.
 */

public class RecognitionMessageItem extends LinearLayout implements IMessageItem {

    /**
     * {
     "data":{

     },
     "content":"山大数字媒体中心2号机",
     "hint":"警告,未知人员闯入!",
     "image":"https://panserver.solarsource.cn/panserver/files/75fb9309-06f9-4736-b5cf-cd39f945f7f0/download",
     "smallimage":"https://panserver.solarsource.cn/panserver/files/3a9992d0-4ee9-47a2-965a-cd344c1691ca/download",
     "time":"2018-12-12 09:11:11"
     "faceImage"
     }
     */

    private Context mContext;
    private JSONObject mJSONObject;
    private IMStruct002 message;

    private TextView tvMonitorLocation;
    private TextView tvMonitorTime;
    private TextView tvWarning;
    private ImageView ivMonitorImg;
    private ImageView ivWarningImg;

    public RecognitionMessageItem(Context context) {
        super(context);
        this.mContext = context;
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.chat_item_recognition, this);//注意第二个参数

        tvMonitorLocation = (TextView) findViewById(R.id.tv_monitor_location);
        tvMonitorTime = (TextView) findViewById(R.id.tv_monitor_time);
        tvWarning = (TextView) findViewById(R.id.tv_warning);
        ivMonitorImg = (ImageView) findViewById(R.id.iv_monitor);
        ivWarningImg = (ImageView) findViewById(R.id.iv_warning);

        int width = DisplayUtil.getMobileWidth(context) * 2 / 3 - 20;
        LayoutParams lp = new LayoutParams(width, LayoutParams.MATCH_PARENT);
        this.setLayoutParams(lp);
    }

    @Override
    public void setIMStruct002(IMStruct002 message) {
        this.message = message;
        try {
            String json = new String(message.getBody(), "UTF-8");
            mJSONObject = new JSONObject(json);
            if (mJSONObject.has("content")) {
                tvMonitorLocation.setText(mJSONObject.getString("content"));
            }
            if (mJSONObject.has("time")) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
                long currentTime = Long.parseLong(mJSONObject.getString("time"));
                tvMonitorTime.setText(sdf.format(currentTime));
            }
            if (mJSONObject.has("faceImage")) {
                setNetPicture(mJSONObject.getString("faceImage"), ivMonitorImg);
            }
            if(mJSONObject.has("hint")){
                tvWarning.setText(mJSONObject.getString("hint"));
            }
            if(mJSONObject.has("smallimage")){
                setNetPicture(mJSONObject.getString("smallimage"), ivWarningImg);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public View messageView() {
        return this;
    }

    @Override
    public ViewSize messageViewSize() {
        return null;
    }

    @Override
    public boolean getIsInUse() {
        return this.isShown();
    }

    @Override
    public void setIsInUse(boolean isInUse) {

    }

    @Override
    public void prepareForReuse() {

    }

    /**
     * 加载网络图片
     *
     * @param url
     * @param img
     */
    private void setNetPicture(String url, ImageView img) {
        Glide
                .with(mContext)
                .load(url)
                .transition(new DrawableTransitionOptions().crossFade())
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(img);
    }
}
