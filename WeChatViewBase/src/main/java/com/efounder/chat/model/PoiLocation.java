package com.efounder.chat.model;

import java.util.Map;

/**
 * Created by zjk on 2017/6/5.
 * Desc:高德地图周边poi
 */

public class PoiLocation {
    String title;
    String address;
    double latitude;
    double longitude;
    Map<Integer, Boolean> stateMap;

    public Map<Integer, Boolean> getStateMap() {
        return stateMap;
    }

    public void setStateMap(Map<Integer, Boolean> stateMap) {
        this.stateMap = stateMap;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}