package com.efounder.chat.view;

import android.app.Activity;
import android.graphics.drawable.GradientDrawable;

import androidx.appcompat.app.AlertDialog;

import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.efounder.chat.R;

/**
 * 通知AlertDialog
 * Created by slp on 2018/4/26.
 */
public class NotificationAlertDialog {

    private Activity mContext;
    private int mWidth;
    private int mHeight;

    //设置scrollerview的宽高
    private static final float WIDTHFRACTION = 0.78f;
    private static final float HEIGHTFRACTION = 0.32f;
    private AlertDialog alertDialog;
    private View notificationView;

    private LinearLayout linearNotification;

    private TextView tvNotificationTitle;
    private TextView tvNotificationDetail;
    private ScrollView scNotificationDetail;

    //通知按钮
    private Button btnNotification;

    //关闭图片
    private ImageView ivCancel;

    private View space;

    //背景Drawable
    private GradientDrawable mBackGroundColor;

    private GradientDrawable btnBackgroundColor;

    private ClickListener mClickListener;

    private boolean canShowBtn = true;
    private boolean canShowCancel = true;

    public NotificationAlertDialog(Activity context) {
        this.mContext = context;
        getWindowWidthAndHeight();
        init();
    }

    private void init() {
        notificationView = LayoutInflater.from(mContext).inflate(R.layout.alertdialog_notification, null);
        tvNotificationTitle = (TextView) notificationView.findViewById(R.id.tv_notification_dialog_title);
        tvNotificationDetail = (TextView) notificationView.findViewById(R.id.tv_notification_dialog_detail);
        scNotificationDetail = (ScrollView) notificationView.findViewById(R.id.sc_notification_detail);
        btnNotification = (Button) notificationView.findViewById(R.id.btn_notification_alertDialog);
        ivCancel = (ImageView) notificationView.findViewById(R.id.iv_notification_dialog_cancel);
        linearNotification = (LinearLayout) notificationView.findViewById(R.id.linear_notification);

        mBackGroundColor = (GradientDrawable) linearNotification.getBackground();
        btnBackgroundColor = (GradientDrawable) btnNotification.getBackground();

        btnNotification.setVisibility(canShowBtn ? View.VISIBLE : View.GONE);
        ivCancel.setVisibility(canShowCancel ? View.VISIBLE : View.GONE);

        alertDialog = new AlertDialog.Builder(mContext, R.style.NoBackGroundDialog)
                .setCancelable(true).create();

        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setView(notificationView, 0, 0, 0, 0);

        ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }

    /**
     * 获取屏幕宽度和高度
     */
    private void getWindowWidthAndHeight() {
        WindowManager m = mContext.getWindowManager();
        Display d = m.getDefaultDisplay();
        mWidth = d.getWidth();
        mHeight = d.getHeight();
    }

    public void setListener(ClickListener ml) {
        this.mClickListener = ml;
        if (mClickListener == null) {
            return;
        }

        btnNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null) {
                    mClickListener.onBtnClick();
                }
            }
        });
    }

    /**
     * 设置内容
     *
     * @param msg
     */
    public void setDetailText(String msg) {
        tvNotificationDetail.setText(msg);
        tvNotificationDetail.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {

                tvNotificationDetail.getViewTreeObserver().removeGlobalOnLayoutListener(this);

                if (tvNotificationDetail.getLineCount() <= 8) {
                    scNotificationDetail.setLayoutParams(new LinearLayout.LayoutParams(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT));
                } else {
                    scNotificationDetail.setLayoutParams(new LinearLayout.LayoutParams(WindowManager.LayoutParams.MATCH_PARENT, (int) (mHeight * HEIGHTFRACTION)));
                }
            }
        });
    }

    /**
     * 设置内容字体颜色
     * @param color
     */
    public void setDetailTextColor(int color){
        tvNotificationDetail.setTextColor(color);
    }

    /**
     * 设置标题
     * @param msg
     */
    public void setTitleText(String msg) {
        tvNotificationTitle.setText(msg);
    }

    /**
     * 设置标题字体颜色
     * @param color
     */
    public void setTitleTextColor(int color){
        tvNotificationTitle.setTextColor(color);
    }

    /**
     * 设置按钮显示的文字
     *
     * @param msg
     */
    public void setBtnText(String msg) {
        btnNotification.setText(msg);
    }

    /**
     * 设置按钮显示的文字
     * @param color
     */
    public void setBtnTextColor(int color){
        btnNotification.setTextColor(color);
    }

    /**
     * 设置按钮背景颜色
     * @param color
     */
    public void setBtnBackgroundColor(int color){
        btnBackgroundColor.setColor(color);
    }


    /**
     * 设置界面背景颜色
     * @param color
     */
    public void setBackgroundColor(int color){
        mBackGroundColor.setColor(color);
    }

    /**
     * 是否显示按钮
     *
     * @param canShow
     */
    public void setCanShowBtn(boolean canShow) {
        this.canShowBtn = canShow;
        btnNotification.setVisibility(canShowBtn ? View.VISIBLE : View.GONE);

    }

    /**
     * 是否显示关闭键
     *
     * @param canShow
     */
    public void setCanShowCancel(boolean canShow) {
        this.canShowCancel = canShow;
        ivCancel.setVisibility(canShowCancel ? View.VISIBLE : View.GONE);

    }

    /**
     * 获得关闭键
     *
     * @return
     */
    public View getCancleIv() {
        return ivCancel;
    }

    /**
     * 获得按钮
     *
     * @return
     */
    public Button getButton() {
        return btnNotification;
    }


    /**
     * 显示提示框
     */
    public void show() {

        alertDialog.show();
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.new_version_scale_open);
        notificationView.startAnimation(animation);
        setDialogSize(alertDialog);

    }

    /**
     * 关闭提示框
     */
    public void dismiss() {
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.new_version_scale_close);
        notificationView.startAnimation(animation);
        alertDialog.dismiss();
    }

    /**
     * 根据屏幕的宽度来设置面板的宽度
     * 高度自适应
     *
     * @param alertDialog
     */
    private void setDialogSize(AlertDialog alertDialog) {
        //为获取屏幕宽、高
        WindowManager.LayoutParams p = alertDialog.getWindow().getAttributes();  //获取对话框当前的参数值
        p.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        p.height = (int) (mHeight * HEIGHTFRACTION);
        p.width = (int) (mWidth * WIDTHFRACTION);
        p.gravity = Gravity.CENTER;
        alertDialog.getWindow().setAttributes(p);

    }


    public interface ClickListener {

        /**
         * 点击通知界面的按钮
         */
        void onBtnClick();
    }
}
