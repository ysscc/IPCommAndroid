package com.efounder.chat.model;

/**
 * 群通知，新的好友的事件
 */
public class NoticeCountEvent {

    private int newFriendCount;
    private int GroupNoticeCount;

    public NoticeCountEvent(int newFriendCount, int groupNoticeCount) {
        this.newFriendCount = newFriendCount;
        GroupNoticeCount = groupNoticeCount;
    }
    public NoticeCountEvent() {

    }

    public int getNewFriendCount() {
        return newFriendCount;
    }

    public void setNewFriendCount(int newFriendCount) {
        this.newFriendCount = newFriendCount;
    }

    public int getGroupNoticeCount() {
        return GroupNoticeCount;
    }

    public void setGroupNoticeCount(int groupNoticeCount) {
        GroupNoticeCount = groupNoticeCount;
    }
}


