package com.efounder.chat.utils;

import com.efounder.chat.R;

import java.io.File;

/**
 * 获取群头像，本地，服务器，默认
 * yqs
 */

public class GroupAvatarUtil {
    private static final String TAG = "GroupAvatarUtil";


    /**
     * 获取头像
     *
     * @param avatar
     * @return
     */
    public static String getAvatar(String avatar) {
        String touxiang = "";
        if (avatar==null){
            return touxiang;
        }
        if ("".equals(avatar)) {
            avatar = "drawable://" + +R.drawable.default_groupavatar;
        } else if (avatar.contains("http")) {
            touxiang = avatar;
        } else {
            File file = new File(avatar);
            if (file.exists()) {
                touxiang = "file://" + avatar;
            }
        }
        return touxiang;
    }


    /**
     * 判断群头像是不是服务器的头像
     *
     * @param avatar
     * @return
     */
    public static boolean isServerAvatar(String avatar) {
        if (avatar==null){
            return  false;
        }
        if (avatar.contains("http")) {
            return true;
        } else {
            return false;
        }

    }


}
