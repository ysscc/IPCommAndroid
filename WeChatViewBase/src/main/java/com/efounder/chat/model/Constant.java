package com.efounder.chat.model;

import android.os.Environment;

import com.efounder.constant.EnvironmentVariable;

import java.io.File;

import static com.efounder.frame.utils.Constants.KEY_SETTING_APPID;



/**
 * Created by lch on 2016/8/25 0025.
 */

public class Constant {


    //tab_menu的根标签
    public static final String KEY_MENU_ROOT = "menuRoot";

//    private static StorageUtil storageUtil = new StorageUtil(AppContext.getInstance(), "storage");
    //edit for login
    /*********************/

//    public static String HttpType = "https";
//    public static String ADDRESS = "pgyd.zyof.com.cn";
//    public static String PORT = "443";
//    public static String PATH = "EnterpriseServer";


   // public static String HttpType =storageUtil.getBoolean("isSafe", false)==false?"http":"https";
    //public static String ADDRESS = storageUtil.getString("address", "192.168.0.136");

    /***** 中油铁工 ******/
    //以下常量全部用Environment变量代替
//    public static String HttpType =EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_HTTP_TYPE);
//    public static String ADDRESS = EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_ADDRESS);
//    public static String PORT = EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_PORT);
//    public static String PATH = EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_PATH);
    /********************/

     /*********************大庆********/
//    public static String ADDRESS = storageUtil.getString("address", "47.89.191.21");
//    public static String PORT = storageUtil.getString("port","8080");
//    public static String PATH = storageUtil.getString("path","ESTMobile");
//    public static String APPID = storageUtil.getString(KEY_SETTING_APPID,Env);
//    public static String HttpType =storageUtil.getBoolean("isSafe", false)==false?"https":"http";
//    public static String ADDRESS = storageUtil.getString("address", "pgyd.zyof.com.cn");
//    public static String PORT = storageUtil.getString("port","443");
//    public static String PATH = storageUtil.getString("path","EnterpriseServer");
//以下常量全部用Environment变量代替
//    public static String APPID = EnvironmentVariable.getProperty(KEY_SETTING_APPID);
//    public static String Service = EnvironmentVariable.getProperty("service");
//    public static String SIGN = EnvironmentVariable.getProperty("sign");
    public static String EAI_SERVICE = "Android";

    public static String Product = "PGYDYYWCM";

    public static String LoginCheckType = "ZYPTYTH";
    public static String Resource_Key = "mobile_resource";

    public static String AndroidVersion_Key = "androidVersion";

    /*****
     * 这个参数对于下载apk很关键,目前只要是微信相关的都是这个参数。
     ****/
    public static String AndroidApkPath = "androidForWeChatPath";

    //APP 升级时，通知栏的ID
    public static int UpdateAPP_NotifiCation_ID = 6665;

    //APP 升级时，通知栏的滚动text
    public static String UpdateAPP_NotifiCation_TEXT = "移动企业更新APK";

    /*****
     * 这个参数对于不同的项目需要重新配置
     **/
    public static String ApkFileName = EnvironmentVariable.getProperty(KEY_SETTING_APPID)+".apk";

    public static String ResFileName = "updateAndroidForWeChat.zip";

    //这个不同项目不一样
    public static String resKeyFromServer = "updateAndroidWC";
    //这个不同项目不一样
    public static String appSdcardBaseLocation = EnvironmentVariable.getProperty(KEY_SETTING_APPID);

    public static String appSdcardLocation = Environment.getExternalStorageDirectory()
            .getAbsolutePath()
            + File.separator + appSdcardBaseLocation
            + "/res/unzip_res/Package";

    public final static String RES_KEY_VERSION = "androidRESVersion";
    //这个不同项目不一样
    //以下resUrl常量用Environment变量代替
//    public static String resUrl = EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_HTTP_TYPE) + "://" + EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_ADDRESS) + ":"
//            + EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_PORT) + "/"
//            + EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_PATH) + "/MobileResource/" + EnvironmentVariable.getProperty(KEY_SETTING_APPID)
//            + "/updateAndroidForWeChat.zip";

    public static final String RES_DIR = AppConstant.APP_ROOT+"/res";
    //zip文件解压到的资源目录
    public static final String UNZIP_DIR = RES_DIR + "/" + "unzip_res";
    public static final String PACKAGE_DIR = UNZIP_DIR + "/" + "Package";
    public final static String Tab_Menu_Path = PACKAGE_DIR + "/" + "Tab_Menu.xml";

    //皮肤包文件夹
    public static final String SKIN_DIR = UNZIP_DIR + "/" + "skins";







}
