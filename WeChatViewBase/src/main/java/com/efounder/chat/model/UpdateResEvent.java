package com.efounder.chat.model;

/**
 * yqs
 */
//更新apk 资源文件的事件
public class UpdateResEvent {
    /**
     * 应用更新
     */
    public final  static String TYPE_APP_UPDATE="appupdate";
    /**
     * app资源文件更新
     */
    public final  static String TYPE_APP_RES_UPDATE="appresupdate";

    private String version;//版本号
    private String updateNote;//更新说明
    private int id;//消息发送者
    private String type;//类型 更新apk还是更新资源文件
    private String url;//下载链接


    public UpdateResEvent() {
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUpdateNote() {
        return updateNote;
    }

    public void setUpdateNote(String updateNote) {
        this.updateNote = updateNote;
    }
}
