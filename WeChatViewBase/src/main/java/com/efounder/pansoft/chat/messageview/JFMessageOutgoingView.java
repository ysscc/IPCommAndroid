package com.efounder.pansoft.chat.messageview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.efounder.chat.R;
import com.efounder.utils.CommonUtils;

/**
 * 仿 QQ样式 发送消息item
 * @author YQS  2018/04/17
 */
public class JFMessageOutgoingView extends JFMessageBaseView {

    public JFMessageOutgoingView(Context context) {
        super(context);
        initView(context);

    }

    public JFMessageOutgoingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);

    }


    private void initView(Context context) {

        // 头像区域
        RelativeLayout.LayoutParams chat_item_avatar_area_layoutparams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        chat_item_avatar_area_layoutparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);// 与父容器的右侧对齐
        chat_item_avatar_area_layoutparams.addRule(RelativeLayout.ALIGN_PARENT_TOP);// 与父容器的上侧对齐
        chat_item_avatar_area_layoutparams.topMargin=15;

        middleView.addView(itemAvatarLayout, chat_item_avatar_area_layoutparams);

        //消息体上方显示姓名 角色 认证的区域
        RelativeLayout.LayoutParams messageContentTopLayoutParams = new RelativeLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        messageContentTopLayoutParams.addRule(RelativeLayout.LEFT_OF, itemAvatarLayout.getId());
        messageContentTopLayoutParams.addRule(RelativeLayout.ALIGN_TOP, itemAvatarLayout.getId());
        messageContentTopLayoutParams.rightMargin = 20;
        middleView.addView(llMessageTopLayout, messageContentTopLayoutParams);

        // 消息体区域
        RelativeLayout.LayoutParams chat_item_layout_content_layoutparams = new RelativeLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        chat_item_layout_content_layoutparams.addRule(RelativeLayout.LEFT_OF, itemAvatarLayout.getId());
        chat_item_layout_content_layoutparams.addRule(RelativeLayout.BELOW, llMessageTopLayout.getId());
        chat_item_layout_content_layoutparams.rightMargin = 10;
        chat_item_layout_content_layoutparams.topMargin = 5;
        rlMessageContentLayout.setMinimumHeight(CommonUtils.dip2px(context, 42));
        rlMessageContentLayout.setBackgroundResource(R.drawable.qqstyle_message_out_bg);
        rlMessageContentLayout.setId(R.id.qqstyle_chat_item_content);
        middleView.addView(rlMessageContentLayout, chat_item_layout_content_layoutparams);

        // 消息体下方消息发送状态 的区域
        RelativeLayout.LayoutParams messageContentBottomParams = new RelativeLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        messageContentBottomParams.addRule(RelativeLayout.ALIGN_RIGHT, rlMessageContentLayout.getId());
        messageContentBottomParams.addRule(RelativeLayout.BELOW, rlMessageContentLayout.getId());
        messageContentBottomParams.topMargin = 1;
        // rlMessageContentBottomLayout.setBackgroundResource(R.drawable.qqstyle_message_out_bg);
        middleView.addView(rlMessageContentBottomLayout, messageContentBottomParams);


        // signview区域(发送失败，以及发送中的progressbar)
        RelativeLayout.LayoutParams signView_layoutparams = new RelativeLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        signView_layoutparams.addRule(RelativeLayout.LEFT_OF,
                rlMessageContentLayout.getId());
        signView_layoutparams.addRule(RelativeLayout.ALIGN_TOP, rlMessageContentLayout.getId());
        signView_layoutparams.rightMargin = 10;
        middleView.addView(signViewLayout, signView_layoutparams);
    }

}
