package com.efounder.chat.item.manager;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.core.xml.StubObject;
import com.efounder.chat.R;
import com.efounder.frame.ViewSize;
import com.efounder.frame.activity.EFTransformFragmentActivity;
import com.efounder.frame.utils.EFFrameUtils;
import com.efounder.frame.xmlparse.EFXmlConstants;
import com.efounder.message.struct.IMStruct002;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author lch
 *  费用报销item
 * 
 */
public class FeiyongBaoxiaoCardItem extends LinearLayout implements
		IMessageItem {

	private Context mContext;

	public FeiyongBaoxiaoCardItem(Context context) {
		super(context);
		mContext = context;
		this.setBackgroundResource(R.drawable.radius_bg);
		LayoutParams mLineaLayoutParams = new LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		this.setLayoutParams(mLineaLayoutParams);
//		this.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				Toast.makeText(mContext, "点击消费凭证", 500).show();
//			}
//		});
		initView();

	}

	// 设置数据 card 是一个包含头图片，头标题，还有子card的模型
	private void initView() {

		LayoutInflater inflater = LayoutInflater.from(mContext);
		ViewGroup chat_item_pay_frame = (ViewGroup) inflater.inflate(
				R.layout.chat_item_feiyongbaoxiao_item, this);

		TextView gate = (TextView) this.findViewById(R.id.gate);
		gate.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Bundle bundle = new Bundle();
				StubObject menuStub = new StubObject();
				menuStub.setString(EFXmlConstants.ATTR_FORM,"fybx.xml");

				bundle.putSerializable("stubObject", menuStub);
				bundle.putString(EFTransformFragmentActivity.EXTRA_TITLE_NAME,"费用报销");
				bundle.putInt(EFTransformFragmentActivity.EXTRA_TITLE_RIGHT_VISIBILITY,View.INVISIBLE);
				try {
					EFFrameUtils.pushFragment((Class<? extends Fragment>) Class.forName("com.efounder.chat.fragment.EFAppAccountAssetsFormFragment"),bundle);
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}


			}
		});
		
		LinearLayout chat_item_pay_detail  = (LinearLayout) this.findViewById(R.id.chat_item_pay_detail);
		
		Map<String,String> map = new HashMap<String, String>();
		map.put("申请人：", "杜志宏");
		map.put("费用部门：", "普光信息项目部");
		map.put("申请事由：", "达州出差");
		
        for(String key:map.keySet()){
		LinearLayout chat_item_callingcard_detailitem = (LinearLayout) inflater
				.inflate(R.layout.chat_item_pay_detailitem, null);
		
		TextView keyTextView = (TextView) chat_item_callingcard_detailitem.findViewById(R.id.chat_item_callingcard_detailitem_key);
		keyTextView.setText(key);
		TextView valueTextView = (TextView) chat_item_callingcard_detailitem.findViewById(R.id.chat_item_callingcard_detailitem_value);
		valueTextView.setText(map.get(key));
		chat_item_pay_detail.addView(chat_item_callingcard_detailitem);
		
        }

	}

	public interface OnPublicNumberItemClickListener {
		public void onClick(int position);

	}

	@Override
	public View messageView() {
		return this;
	}

	@Override
	public ViewSize messageViewSize() {
		return null;
	}

	@Override
	public void setIMStruct002(IMStruct002 message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean getIsInUse() {
		return this.isShown();
	}

	@Override
	public void setIsInUse(boolean isInUse) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void prepareForReuse() {
		
	}
}
