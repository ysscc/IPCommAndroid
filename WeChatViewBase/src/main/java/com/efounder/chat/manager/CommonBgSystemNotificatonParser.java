package com.efounder.chat.manager;

import com.efounder.chat.event.NotifyChatUIRefreshEvent;
import com.efounder.chat.model.UpdateBadgeViewEvent;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.interfaces.BadgeUtil;
import com.efounder.message.manager.JFMessageManager;
import com.efounder.message.struct.IMStruct002;
import com.utilcode.util.LogUtils;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * 处理消息类型是111的后台发送的系统消息
 *
 * @author YQS
 * @date 2018/10/31
 */
public class CommonBgSystemNotificatonParser {

    private static final String TAG = "CommonBgSystemNotificatonParser";
    /**
     * 新闻推送新的文章的CMD
     */
    public static final String NEW_NEWS_MESSAGE_CMD = "news";
    /**
     * 更改消息内容的CMD
     */
    public static final String CHANGE_MESSAGE_CONTENT = "changeMessage";

    /**
     * 收到后台发送的系统消息并处理
     *
     * @param imstruct002 消息体
     */
    public static void notifyCommonBgUpdata(IMStruct002 imstruct002) {

        String json = "";
        json = imstruct002.getMessage();
        LogUtils.i(TAG, "打印输出系统消息：" + json);
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(json);
            String CMD = jsonObject.optString("CMD");
            switch (CMD) {
                case NEW_NEWS_MESSAGE_CMD:
                    //新闻资讯的系统消息
                    EnvironmentVariable.setProperty("news_new_message_notify", "1");
                    EventBus.getDefault().post(new UpdateBadgeViewEvent(BadgeUtil.CHAT_ID_NEWS + "", (byte) 2));
                    break;
                case CHANGE_MESSAGE_CONTENT:
                    changeMessageContent(jsonObject);
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * 检查是否允许更改消息内容
     *
     * @param imstruct002
     * @return
     */
    private static boolean checkIsCanChange(IMStruct002 imstruct002) {
        short messagechildType = imstruct002.getMessageChildType();
        if (messagechildType >= 81 && messagechildType <= 89) {
            return false;
        }
        return false;
    }

    /**
     * 改变消息的内容
     *
     * @param jsonObject
     */
    private static void changeMessageContent(JSONObject jsonObject) throws JSONException {
        String messageID = jsonObject.getString("messageID");
        IMStruct002 imStruct002 = JFMessageManager.getInstance().queryMessage(messageID);
        if (imStruct002 == null) {
            return;
        }
        if (checkIsCanChange(imStruct002)) {
            return;
        }
        JSONObject messageJsonObject = new JSONObject(imStruct002.getBodyString());
        Iterator<String> iterator = jsonObject.keys();
        while (iterator.hasNext()) {
            String key = iterator.next();
            if (!"CMD".equals(key) && !"messageID".equals("key")) {
                Object value = jsonObject.get(key);
                messageJsonObject.put(key, value);
            }
        }
        imStruct002.setMessage(messageJsonObject.toString());
        JFMessageManager.dbManager.update(imStruct002);
        //通知界面刷新
        EventBus.getDefault().post(new NotifyChatUIRefreshEvent());
    }

}
