package com.efounder.chat.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.frame.language.MultiLanguageModel;

import java.util.List;

/**
 * @author : zzj
 * @e-mail : zhangzhijun@pansoft.com
 * @date : 2018/12/21 14:08
 * @desc : 多语言列表adapter
 * @version: 1.0
 */
public class MultiLanguageAdapter extends RecyclerView.Adapter<MultiLanguageAdapter.MultiLanguageHolder> {

    private List<MultiLanguageModel> multiLanguageModels;
    private Context mContext;
    public MultiLanguageAdapter(Context context, List<MultiLanguageModel> multiLanguageModels){
        mContext = context;
        this.multiLanguageModels = multiLanguageModels;

    }

    @Override
    public MultiLanguageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_multi_language,parent,false);
        return new MultiLanguageHolder(view);
    }

    @Override
    public void onBindViewHolder(MultiLanguageHolder holder, final int position) {
        MultiLanguageModel multiLanguageModel = multiLanguageModels.get(position);
        holder.tvLanguageName.setText(multiLanguageModel.getLanguageName());
        holder.rbLanguageSelect.setChecked(multiLanguageModel.isSelect());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int i = 0;i<multiLanguageModels.size();i++){
                   MultiLanguageModel multiLanguageModel1 =  multiLanguageModels.get(i);
                    if(position == i){
                        multiLanguageModel1.setSelect(true);
                    }else {
                        multiLanguageModel1.setSelect(false);
                    }
                }
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return multiLanguageModels.size();
    }

    class MultiLanguageHolder extends RecyclerView.ViewHolder{

        private TextView tvLanguageName;
        private RadioButton rbLanguageSelect;

        public MultiLanguageHolder(View itemView) {
            super(itemView);
            tvLanguageName = itemView.findViewById(R.id.tv_language_name);
            rbLanguageSelect = itemView.findViewById(R.id.rb_language_select);
        }

    }

}
