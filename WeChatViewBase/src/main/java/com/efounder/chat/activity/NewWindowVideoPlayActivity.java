//package com.efounder.chat.activity;
//
//import android.content.Intent;
//import android.content.pm.ActivityInfo;
//import android.content.res.Configuration;
//import android.os.Bundle;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.WindowManager;
//import android.widget.FrameLayout;
//
//import com.android.tedcoder.wkvideoplayer.util.DensityUtil;
//import com.android.tedcoder.wkvideoplayer.view.MediaController;
//import com.android.tedcoder.wkvideoplayer.view.SuperVideoPlayer;
//import com.efounder.chat.R;
//import com.utilcode.util.StatusBarUtil;
//
//import java.lang.reflect.Field;
//
///**
// * 这个用来播放本地视频
// * Created by will on 16-12-23.
// */
//@Deprecated
//public class NewWindowVideoPlayActivity extends BaseActivity {
//    private SuperVideoPlayer superVideoPlayer;
//
//    private SuperVideoPlayer.VideoPlayCallbackImpl videoPlayCallback = new SuperVideoPlayer.VideoPlayCallbackImpl() {
//        @Override
//        public void onCloseVideo() {
//            superVideoPlayer.close();
//            resetPageToPortrait();
//        }
//
//        @Override
//        public void onSwitchPageType() {
//            if (getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
//                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//                superVideoPlayer.setPageType(MediaController.PageType.SHRINK);
//            } else {
//                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//                superVideoPlayer.setPageType(MediaController.PageType.EXPAND);
//            }
//        }
//
//        @Override
//        public void onPlayFinish() {
//
//        }
//    };
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        StatusBarUtil.setColor(this, getResources().getColor(R.color.black));
//        setContentView(R.layout.new_window_video_play_layout);
//        superVideoPlayer = (SuperVideoPlayer) findViewById(R.id.video_player_1);
//        superVideoPlayer.setVideoPlayCallback(videoPlayCallback);
//        superVideoPlayer.setAutoHideController(false);
//
//        try {
//            hiddenControllerView();
//
//        } catch (NoSuchFieldException e) {
//            e.printStackTrace();
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        }
//
//        FrameLayout close = (FrameLayout) findViewById(R.id.video_close_view);
//        close.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
//
//        Intent intent = getIntent();
//        String videoPath = intent.getStringExtra("videoPath");
//
//        superVideoPlayer.loadLocalVideo(videoPath);
//    }
//
//    private void hiddenControllerView() throws NoSuchFieldException, IllegalAccessException {
//        Field mMediaController = superVideoPlayer.getClass().getDeclaredField("mMediaController");
//        mMediaController.setAccessible(true);
//        View o = (View) mMediaController.get(superVideoPlayer);
//        o.setVisibility(View.GONE);
//        o.findViewById(R.id.view_menu).setVisibility(View.GONE);
//        ViewGroup parent = (ViewGroup) o.findViewById(R.id.shrink).getParent();
//        parent.setVisibility(View.GONE);
////        parent.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//    }
//
//    @Override
//    public void onConfigurationChanged(Configuration newConfig) {
//        super.onConfigurationChanged(newConfig);
//        if (null == superVideoPlayer) return;
//        /***
//         * 根据屏幕方向重新设置播放器的大小
//         */
//        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
//            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
//            getWindow().getDecorView().invalidate();
//            float height = DensityUtil.getWidthInPx(this);
//            float width = DensityUtil.getHeightInPx(this);
//            superVideoPlayer.getLayoutParams().height = (int) width;
//            superVideoPlayer.getLayoutParams().width = (int) height;
//        } else if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
//            final WindowManager.LayoutParams attrs = getWindow().getAttributes();
//            attrs.flags &= (~WindowManager.LayoutParams.FLAG_FULLSCREEN);
//            getWindow().setAttributes(attrs);
//            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
//            float width = DensityUtil.getWidthInPx(this);
//            float height = DensityUtil.dip2px(this, 200.f);
//            superVideoPlayer.getLayoutParams().height = (int) height;
//            superVideoPlayer.getLayoutParams().width = (int) width;
//        }
//    }
//
//    /***
//     * 恢复屏幕至竖屏
//     */
//    private void resetPageToPortrait() {
//        if (getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//            superVideoPlayer.setPageType(MediaController.PageType.SHRINK);
//        }
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        if (superVideoPlayer != null) {
//            superVideoPlayer.close();
//        }
//    }
//}
