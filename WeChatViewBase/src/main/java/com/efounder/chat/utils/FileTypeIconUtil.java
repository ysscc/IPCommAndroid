package com.efounder.chat.utils;

import android.widget.ImageView;

import com.efounder.chat.R;

/**
 * Created by Richard on 2018/5/5.
 * 根据文件名获取文件类型并在imageview上显示文件图标
 * 或者根据文件类型显示文件图标
 */

public class FileTypeIconUtil {


    /**
     * 根据文件的本地路径设置文件的图标
     *
     * @param filePicture
     * @param fileName
     */
    public static void setFileIconByFilePath(ImageView filePicture, String fileName) {
        String fileType;
        if (null != fileName && fileName.contains(".")) {
            if (null != fileName && fileName.contains(".")) {
                fileType = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
            } else {
                fileType = "";
            }
            setFileIconByFileType(filePicture, fileType);
        } else {
            //未知文件
            filePicture.setImageResource(R.drawable.unknow);
        }
    }


    /**
     * 根据文件类型设置文件的图标
     *
     * @param filePicture
     * @param fileType
     */
    public static void setFileIconByFileType(ImageView filePicture, String fileType) {
        if (fileType == null) {
            return;
        }
        switch (fileType.toLowerCase()) {
            case "apk":
                filePicture.setImageResource(R.drawable.apk);
                break;
            case "pdf":
                filePicture.setImageResource(R.drawable.pdf);
                break;
            case "exe":
                filePicture.setImageResource(R.drawable.exe);
                break;
            case "txt":
                filePicture.setImageResource(R.drawable.txt);
                break;
            case "html":
                filePicture.setImageResource(R.drawable.html);
                break;
            case "xml":
                filePicture.setImageResource(R.drawable.xml);
                break;
            case "rar":
            case "zip":
            case "7-zip":
                filePicture.setImageResource(R.drawable.rar);
                break;
            case "doc":
            case "docx":
            case "dot":
            case "dotx":
            case "docm":
            case "dotm":
                filePicture.setImageResource(R.drawable.word);
                break;
            case "xls":
            case "xlsx":
            case "xlsb":
            case "xlsm":
            case "xlst":
                filePicture.setImageResource(R.drawable.excel);
                break;
            case "ppt":
            case "pot":
            case "pps":
            case "pptx":
            case "potm":
            case "potx":
            case "pptm":
            case "ppsx":
            case "ppsm":
                filePicture.setImageResource(R.drawable.ppt);
                break;
            case "m4a":
            case "cd":
            case "ogg":
            case "mp3":
            case "asf":
            case "wma":
            case "wav":
            case "mp3pro":
            case "rm":
            case "real":
            case "ape":
            case "module":
            case "midi":
            case "mid":
            case "vqf":
            case "xmf":
                filePicture.setImageResource(R.drawable.audio);
                break;
            case "avi":
            case "wmv":
            case "rmvb":
            case "flash":
            case "mp4":
            case "3gp":
            case "mpeg":
            case "mpg":
            case "dat":
                filePicture.setImageResource(R.drawable.video);
                break;
            case "png":
            case "bmp":
            case "gif":
            case "jpeg":
            case "jpg":
            case "swf":
            case "webp":
                filePicture.setImageResource(R.drawable.picture);
                break;
            default:
                filePicture.setImageResource(R.drawable.unknow);
        }
    }
}
