package com.efounder.chat.model;

public class RefreshChatItemEvent {
	private int messageWhat;

	public int getMessageWhat() {
		return messageWhat;
	}

	public void setMessageWhat(int messageWhat) {
		this.messageWhat = messageWhat;
	}

	public RefreshChatItemEvent(int messageWhat) {
		this.messageWhat = messageWhat;
	}
}
