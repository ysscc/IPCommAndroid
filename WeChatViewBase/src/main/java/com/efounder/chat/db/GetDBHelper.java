package com.efounder.chat.db;

import android.database.sqlite.SQLiteCantOpenDatabaseException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.efounder.chat.R;
import com.efounder.message.db.MessageDBHelper;
import com.efounder.message.db.MessageDBManager;
import com.efounder.util.AppContext;
import com.efounder.util.ToastUtil;
import com.efounder.utils.ResStringUtil;

/**
 * Created by cherise on 2016/8/18.
 */

public class GetDBHelper {

    private static GetDBHelper instance;

    private MessageDBHelper helper;

    private SQLiteDatabase db;

    private GetDBHelper(){
        super();
//        helper = new MessageDBHelper(AppContext.getInstance());
//        db = helper.getWritableDatabase();
        try {
            db = MessageDBManager.getInstance().getDb();
        } catch (SQLiteCantOpenDatabaseException e) {
            e.printStackTrace();
            ToastUtil.showToast(AppContext.getInstance(),ResStringUtil.getString(R.string.wrchatview_app_init_fail));
        }
        Log.i("db-----","GetDBHelper-----单例创建,数据库路径：" + db.getPath());
    }

    public static GetDBHelper getInstance(){
        synchronized (GetDBHelper.class){
            if(null==instance){
                instance = new GetDBHelper();
            }
        }
        return instance;
    }

    public SQLiteDatabase getDb() {
        return db;
    }

    public MessageDBHelper getDBHelper(){
        return helper;
    }

    public void release(){
//        if (helper != null){
//            helper.close();
//        }
        MessageDBManager.getInstance().release();
        instance = null;
        Log.i("db-----","GetDBHelper-----单例释放");
    }


}
