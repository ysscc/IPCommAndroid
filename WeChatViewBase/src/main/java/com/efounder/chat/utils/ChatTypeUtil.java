package com.efounder.chat.utils;

import android.content.Context;

import com.efounder.chat.R;
import com.efounder.chat.item.ChatTipsMessageItem;
import com.efounder.chat.struct.MessageChildTypeConstant;
import com.efounder.chat.struct.StructFactory;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.message.struct.IMStruct002;
import com.efounder.util.AppContext;
import com.efounder.util.JSONUtil;
import com.efounder.utils.ResStringUtil;
import com.utilcode.util.ReflectUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import static com.efounder.frame.utils.Constants.CHAT_USER_ID;

/**
 * 消息列表处理展示消息
 * Created by cherise on 2016/9/10.
 */

public class ChatTypeUtil {


    /**
     * 根据用户id，消息得到 聊天的人是单聊群聊还是公众号
     *
     * @param userId
     * @param imStruct002
     * @param context
     * @return
     */
    public static int getChatType(int userId, IMStruct002 imStruct002, Context context) {
        if (imStruct002.getToUserType() == StructFactory.TO_USER_TYPE_GROUP) {
            return StructFactory.TO_USER_TYPE_GROUP;
        } else {
            //WeChatDBManager weChatDBManager = WeChatDBManager.getInstance();
            //  User user = weChatDBManager.getOneUserById(userId);
            // if (user.getType() == User.PERSONALFRIEND){
            return StructFactory.TO_USER_TYPE_PERSONAL;
//            }else{
//                return  StructFactory.TO_USER_TYPE_OFFICIAL_ACCOUNT;
//            }
        }


    }

    public static String getMessageByType(Context mContext, IMStruct002 imStruct002, String frontText) {
        JSONObject messageObject = null;
        if (imStruct002.isRecall()) {
            //撤回消息
            return "[" + AppContext.getInstance().getResources().getString(R.string.wechatview_recallmessage) + "]";
        }
        try {
            if (imStruct002.getMessageChildType() != MessageChildTypeConstant.subtype_text) {
                messageObject = new JSONObject(imStruct002.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        switch (imStruct002.getMessageChildType()) {
            case MessageChildTypeConstant.subtype_image:
                return frontText + ResStringUtil.getString(R.string.wrchatview_image_kh);
            case MessageChildTypeConstant.subtype_voice:
                return frontText + ResStringUtil.getString(R.string.wrchatview_voice_kh);

            case MessageChildTypeConstant.subtype_smallVideo:
                return frontText + ResStringUtil.getString(R.string.wrchatview_video_kh);

            case MessageChildTypeConstant.subtype_feiyongbaoxiao:
                return frontText + ResStringUtil.getString(R.string.wrchatview_reimbursement_kh);

            case MessageChildTypeConstant.subtype_location:
                return frontText + ResStringUtil.getString(R.string.chat_layout_location);

            case MessageChildTypeConstant.subtype_red_package:
                return frontText + ResStringUtil.getString(R.string.wechatview_red_packet_kh);

            case MessageChildTypeConstant.subtype_oa:
                return frontText + ResStringUtil.getString(R.string.wrchatview_document_approval_kh);

            case MessageChildTypeConstant.subtype_task:
                return frontText + ResStringUtil.getString(R.string.wrchatview_task_kh);

            case MessageChildTypeConstant.subtype_form:
                return frontText + ResStringUtil.getString(R.string.wrchatview_form_kh);

            case MessageChildTypeConstant.subtype_form_native:
                return frontText + ResStringUtil.getString(R.string.wrchatview_form_kh);

            case MessageChildTypeConstant.subtype_file:
                return frontText + ResStringUtil.getString(R.string.wrchatview_file_kh);

            case MessageChildTypeConstant.subtype_meeting:
                return frontText + ResStringUtil.getString(R.string.wechatview_meeting_kh);

            case MessageChildTypeConstant.subtype_recognition:
                return frontText +  ResStringUtil.getString(R.string.wechatview_face_iden_kh);

            case MessageChildTypeConstant.subtype_anim:
                return frontText + ResStringUtil.getString(R.string.wechatview_advertising_info_kh);

            case MessageChildTypeConstant.subtype_secret_image:
                return frontText + "["+ResStringUtil.getString(R.string.wrchatview_secret_image)+"]";

            case MessageChildTypeConstant.subtype_secret_text:
                return frontText + "["+ResStringUtil.getString(R.string.wrchatview_secret_letter)+"]";

            case MessageChildTypeConstant.subtype_secret_file:
                return frontText + "["+ResStringUtil.getString(R.string.wrchatview_secret_file)+"]";

            case MessageChildTypeConstant.subtype_transfer:
                //发送方id等于我的id，是我发出的
                if (messageObject.optString("sendIMUserId", "").equals(EnvironmentVariable.getProperty(CHAT_USER_ID))) {
                    return frontText + "["+ResStringUtil.getString(R.string.wechatview_transaction_out)+"]";
                } else {
                    //发送方id不等于我的id，对方转入我的账户
                    if (!messageObject.optString("sendIMUserId", "").equals("")) {
                        return frontText + "["+ResStringUtil.getString(R.string.wechatview_transaction_transfer)+"]";
                    }
                }
                //若果接收方id等于我的，那么是我接收的
                if (messageObject.optString("receiveIMUserId", "").equals(EnvironmentVariable.getProperty(CHAT_USER_ID))) {
                    return frontText + "["+ResStringUtil.getString(R.string.wechatview_transaction_transfer)+"]";
                } else {
                    //接收方id不是我的，那么是我发出的
                    if (!messageObject.optString("receiveIMUserId", "").equals("")) {
                        return frontText +"["+ResStringUtil.getString(R.string.wechatview_transaction_out)+"]";
                    }
                }

                break;
           // case MessageChildTypeConstant.subtype_share_goods:
            case MessageChildTypeConstant.subtype_share_web: {
                String text = frontText;
                try {
                    text = text + "["+ResStringUtil.getString(R.string.chat_share)+"]" + messageObject.optString("subject");
                } catch (Exception e) {
                    e.printStackTrace();
                    text = text + "["+ResStringUtil.getString(R.string.chat_share)+"]";
                } finally {
                    return text;
                }


            }
            case MessageChildTypeConstant.subtype_gxtask: {
                String text = frontText;
                try {
                    String ty = messageObject.optString("systemName");
                    text = frontText + "[" + ty + "]";
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    return text;
                }
            }
            case MessageChildTypeConstant.subtype_common:
            case MessageChildTypeConstant.subtype_common_Image: {
                String ty = messageObject.optString("systemName", "");
                String text = frontText + "[" + ty + "]";
                return text;
            }
            case MessageChildTypeConstant.subtype_command:
                try {
                    JSONObject jsonObject = new JSONObject(imStruct002.getMessage());
                    String text = jsonObject.getString("text");
                    return frontText + text;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case MessageChildTypeConstant.subtype_mZoneNotification:
                try {
                    return mContext.getResources().getString(R.string.zone_message_hint) + JSONUtil.parseJson(new String(imStruct002.getBody(), "utf-8"))
                            .get("showMsg").getAsString();

                } catch (IllegalStateException e) {
                    try {
                        return mContext.getResources().getString(R.string.zone_message_hint) + new String(imStruct002.getBody(), "utf-8");
                    } catch (UnsupportedEncodingException ex) {
                        ex.printStackTrace();
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                break;
            case MessageChildTypeConstant.subtype_officalweb:
                try {
                    JSONObject jsonObject = new JSONObject(imStruct002.getMessage());
                    String text = jsonObject.getString("url");
                    return frontText + text;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case MessageChildTypeConstant.subtype_xj_item1:
            case MessageChildTypeConstant.subtype_TangZuItem: {
                String text = ResStringUtil.getString(R.string.wrchatview_new_message_kh);
                return text;
            }

            case MessageChildTypeConstant.subtype_notification:
                try {
                    JSONObject jsonObject = new JSONObject(imStruct002.getMessage());
                    return frontText + "["+ResStringUtil.getString(R.string.wrchatview_notice)+"]" + jsonObject.optString("webTitle", "");
                } catch (Exception e) {
                    e.printStackTrace();
                    return frontText + "["+ResStringUtil.getString(R.string.wrchatview_notice)+"]";
                }
            case MessageChildTypeConstant.subtype_zy_task: {
                return frontText + ResStringUtil.getString(R.string.wrchatview_new_message_kh);
            }
            case MessageChildTypeConstant.subtype_text:
                return frontText + SmileUtils.getSmiledText(mContext, imStruct002.getMessage());
            case MessageChildTypeConstant.subtype_speechRecognize:
                return frontText + messageObject.optString("text", "");
            case MessageChildTypeConstant.subtype_chat_tips:
//                return  messageObject.optString("content", "");
                return ChatTipsMessageItem.getNotificationText(imStruct002);
            case MessageChildTypeConstant.subtype_format_text:
                //json格式的文本消息
                return frontText + TextFormatMessageUtil.disposeJsonTextMessage(imStruct002);

            case MessageChildTypeConstant.subtype_subtype_general_common_message:
                //通用评论点赞的item  文本消息
//                return messageObject.optString("title", "");
                return messageObject.optString("content", "");
            case MessageChildTypeConstant.subtype_custom_with_avatar:
            case MessageChildTypeConstant.subtype_custom_without_avatar:
                //自定义消息返回消息文字
                return messageObject.optString("msgTypeName");

            default:
                return getOtherText(mContext, frontText, imStruct002);
        }
        return "";
    }

    //获取别的类型的消息的展示内容
    private static String getOtherText(Context mContext, String frontText, IMStruct002 imStruct002) {
        try {
            //反射配置的相应类
            String className = EnvironmentVariable.getProperty("chatMessageDescImplClass");
            IChatMessageText iChatMessageText = ReflectUtils.reflect(className).newInstance().get();
            String text = iChatMessageText.getDescTextFromMessage(mContext, frontText, imStruct002);
            return text;
        } catch (Exception e) {
            return frontText + SmileUtils.getSmiledText(mContext, imStruct002.getMessage());
        }
    }


    public interface IChatMessageText {
        /**
         * 获取消息描述
         *
         * @param mContext
         * @param frontText
         * @param imStruct002
         * @return
         */
        String getDescTextFromMessage(Context mContext, String frontText, IMStruct002 imStruct002);
    }
}
