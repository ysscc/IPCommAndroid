package com.efounder.chat.model;

public class GroupBanSpeakEvent {
    public static final int TYPE_GROUP = 0;
    public static final int TYPE_USER = 1;


    private int groupId;
    private int type;
    private int userId;
    //是否进制发言
    private boolean isBanSpeak;

    public GroupBanSpeakEvent(int groupId, boolean isBanSpeak) {
        this.groupId = groupId;
        this.type = TYPE_GROUP;
        this.isBanSpeak = isBanSpeak;
    }

    public GroupBanSpeakEvent(int groupId,  int userId, boolean isBanSpeak) {
        this.groupId = groupId;
        this.type = TYPE_USER;
        this.userId = userId;
        this.isBanSpeak = isBanSpeak;
    }

    public int getGroupId() {
        return groupId;
    }

    public GroupBanSpeakEvent setGroupId(int groupId) {
        this.groupId = groupId;
        return this;
    }

    public int getType() {
        return type;
    }

    public GroupBanSpeakEvent setType(int type) {
        this.type = type;
        return this;
    }

    public int getUserId() {
        return userId;
    }

    public GroupBanSpeakEvent setUserId(int userId) {
        this.userId = userId;
        return this;
    }

    public boolean isBanSpeak() {
        return isBanSpeak;
    }

    public GroupBanSpeakEvent setBanSpeak(boolean banSpeak) {
        isBanSpeak = banSpeak;
        return this;
    }
}
