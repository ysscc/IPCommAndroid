package com.efounder.chat.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.efounder.chat.R;
import com.efounder.chat.activity.TransformFragmentActivity.OnKeyBackListener;
/***
 * 公众号详情界面
 * @author lch
 *
 */
public class Public_Number_MessageContent_Fragment extends Fragment implements OnKeyBackListener{
	private View view;
	private WebView webview;
	private ProgressBar progressBar;

	public Public_Number_MessageContent_Fragment() {
		super();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.public_number_messgecontent,
				container, false);

		webview = (WebView) view.findViewById(R.id.public_number_messagecontent_webview);
		progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);

		// 覆盖WebView默认使用第三方或系统默认浏览器打开网页的行为，使网页用WebView打开
		webview.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				// TODO Auto-generated method stub
				// 返回值是true的时候控制去WebView打开，为false调用系统浏览器或第三方浏览器
				view.loadUrl(url);
				return true;
			}
		});
		
		webview.setWebChromeClient(new WebChromeClient(){
			
			@Override
			public void onProgressChanged(WebView view, int newProgress) {
				super.onProgressChanged(view, newProgress);
				if (newProgress == 100) {
					progressBar.setVisibility(View.INVISIBLE);
	              } else {
	                  if (!progressBar.isShown()) {
	                	  progressBar.setVisibility(View.VISIBLE);
	                  }
	                  progressBar.setProgress(newProgress);
	              }
			}
			
		});

		String url = getActivity().getIntent().getStringExtra("url");
		webview.loadUrl(url);

		WebSettings webSettings = webview.getSettings();
		webSettings.setJavaScriptEnabled(true);
		webSettings.setSupportZoom(false);

		return view;
	}

	@Override
	public boolean OnKeyBack(int keyCode, KeyEvent event) {
		if (webview.canGoBack()) {
			webview.goBack();
			return true;
		}
		return false;
	}
	
	
}
