//package com.efounder.chat.fragment;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.support.annotation.Nullable;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.ViewGroup;
//import android.widget.AdapterView;
//import android.widget.AdapterView.OnItemClickListener;
//import android.widget.ListAdapter;
//import android.widget.ListView;
//import android.widget.RelativeLayout;
//import android.widget.Toast;
//
//import com.efounder.chat.R;
//import com.efounder.chat.adapter.ChatListSlideAdapter;
//import com.efounder.chat.adapter.ChatListSlideAdapter.DeleteListItemListener;
//import com.efounder.chat.db.WeChatDBManager;
//import com.efounder.chat.http.GetHttpUtil;
//import com.efounder.chat.model.ChatListItem;
//import com.efounder.chat.model.Group;
//import com.efounder.chat.model.MessageEvent;
//import com.efounder.chat.model.UpdateBadgeViewEvent;
//import com.efounder.chat.service.MessageService;
//import com.efounder.chat.struct.StructFactory;
//import com.efounder.chat.utils.ChatActivitySkipUtil;
//import com.efounder.chat.utils.ChatTypeUtil;
//import com.efounder.constant.EnvironmentVariable;
//import com.efounder.frame.activity.EFAppAccountMainActivity;
//import com.efounder.frame.baseui.BaseFragment;
//import com.efounder.frame.utils.Constants;
//import com.efounder.message.manager.JFMessageListener;
//import com.efounder.message.manager.JFMessageManager;
//import com.efounder.message.struct.IMStruct002;
//import com.efounder.mobilecomps.contacts.User;
//import com.roamer.slidelistview.SlideListView;
//import com.roamer.slidelistview.SlideListView.SlideAction;
//import com.roamer.slidelistview.SlideListView.SlideMode;
//
//import org.greenrobot.eventbus.EventBus;
//import org.greenrobot.eventbus.Subscribe;
//import org.greenrobot.eventbus.ThreadMode;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.lang.ref.WeakReference;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.List;
//import java.util.Locale;
//import java.util.Map;
//
//import static com.efounder.chat.service.MsgReceiver.MSG_NEW_MSG_COMMING;
//import static com.efounder.chat.struct.MessageChildTypeConstant.subtype_App;
//import static com.efounder.chat.struct.MessageChildTypeConstant.subtype_AppRes;
//import static com.efounder.chat.struct.MessageChildTypeConstant.subtype_masterdata;
//import static com.efounder.chat.struct.MessageChildTypeConstant.subtype_officalAccountRes;
//import static com.efounder.frame.utils.Constants.CHAT_USER_ID;
//
///**
// * @author Administrator=lch 会话列表fragment
// */
//@Deprecated
//public class ChatListFragment extends BaseFragment implements
//        DeleteListItemListener, MessageService
//        .MessageServiceNetStateListener, MessageService.MessageServiceLoginListener
//        , ChatListSlideAdapter.CountCallBack {
//    private final static String TAG = "ChatListFragment";
//    public static final String UPDATEBADGNUM = "com.update.badgenum";
//
//    private ChatListMessageListener chatListMessageListener;
//    private WeChatDBManager weChatDBManager;
//
//    private ChatListSlideAdapter chatListAdapter;
//    // 临时写的实体类
//    private volatile List<ChatListItem> chatListItems;
//    private volatile Map<String, ChatListItem> chatListMap;
//    private View view;
//    private SlideListView listView;
//    private SimpleDateFormat dateFormat = new SimpleDateFormat(
//            "yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault());
//
//    private Handler messageHandler = new MessageHandler(this);
//
//    private int currentChatUserId = -1;
//    public static boolean ISDESTROY = true;
//
//    private Activity activity;
//    //网络错误提示布局
//    private RelativeLayout rl_error_item;
//
//    private volatile int topIndex = -1;//置顶计数
//
//    //更新角标接口
//    private UpdateBadageNumInterface updateBadageNumInterface;
//
//    //结束初始化网络事件
//    private InitprogressDismiss initprogressDismiss;
//    //是否初始化网络结束
//    private boolean isInitNetOver = false;
//
//
//    /**
//     * XXX 收到但未处理的消息
//     **/
//    private List<IMStruct002> unHandleReceiveMessage = new ArrayList<>();
//
//
//    private static class MessageHandler extends Handler {
//        private WeakReference<ChatListFragment> weakReference;
//
//        public MessageHandler(ChatListFragment fragment) {
//            weakReference = new WeakReference<ChatListFragment>(fragment);
//        }
//
//        @Override
//        public void handleMessage(Message msg) {
//            super.handleMessage(msg);
//            switch (msg.what) {
//                case 0:
//                    ChatListFragment fragment = weakReference.get();
//                    if (fragment != null) {
//                        fragment.chatListAdapter.notifyDataSetChanged();
//                    }
//                    break;
//
//                default:
//                    break;
//            }
//        }
//    }
//
//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//        this.activity = activity;
//    }
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        System.out.println("----onCreateView---");
//        chatListMessageListener = new ChatListMessageListener();
//        weChatDBManager = WeChatDBManager.getInstance();
//        chatListItems = Collections.synchronizedList(new ArrayList<ChatListItem>());
//        chatListMap = Collections.synchronizedMap(new HashMap<String, ChatListItem>());
//        topIndex = -1;
//
//        view = inflater.inflate(R.layout.chat_list, container, false);
//
//        listView = (SlideListView) view.findViewById(R.id.listView);
//        rl_error_item = (RelativeLayout) view.findViewById(R.id.rl_error_item);
//        rl_error_item.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (initprogressDismiss != null)
//                    initprogressDismiss.trigger(true);
//                Toast.makeText(getActivity(), R.string.wechatview_reconnect_server, Toast.LENGTH_LONG).show();
//                Intent intent = new Intent(getActivity(), MessageService.class);
//                intent.putExtra(MessageService.EXTRA_KEY_IS_LOGIN_IMMEDIATELY, true);
//                getActivity().startService(intent);
//
//            }
//        });
//        ISDESTROY = false;
//        //1.先初始化再加监听之前
//        initData();
//        //2.加监听
//        JFMessageManager.getInstance().addMessageListener(EnvironmentVariable.getProperty(CHAT_USER_ID), chatListMessageListener);
//        //3.注册完监听，再重新初始化一次，保证角标和最后一条消息时最新收到的（由于加了synchronized）,onReceive和initData会交替进行，谁先谁后不影响程序
//        initData();
//
//        View headerView = this.activity.getLayoutInflater()
//                .inflate(R.layout.chat_list_headview, container, false);
//        headerView.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View arg0) {
//                Toast.makeText(activity, R.string.wechatview_click_search, Toast.LENGTH_SHORT)
//                        .show();
//            }
//        });
//        // listView.addHeaderView(headerView);//暂时隐藏聊天列表上面的搜索框
//        listView.setSlideMode(SlideMode.RIGHT);
//        listView.setSlideLeftAction(SlideAction.SCROLL);
//        listView.setSelector(R.color.transparent);
//        chatListAdapter = new ChatListSlideAdapter(activity,
//                chatListItems, updateBadageNumInterface);
//        chatListAdapter.setDeleteListItemListener(this);
//        chatListAdapter.setCountCallBack(this);
//        listView.setAdapter(chatListAdapter);
//        chatListAdapter.notifyDataSetChanged();
//
//        listView.setOnItemClickListener(new OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view,
//                                    int position, long id) {
//
//                User user = chatListItems.get(position).getUser();
//                if (user != null && user.getType() == 1) {
//
//                    Intent intent = new Intent(activity, EFAppAccountMainActivity.class);
//                    intent.putExtra("id", user.getId());
//                    intent.putExtra("nickName", user.getNickName());
//                    intent.putExtra("type", "chat");
//
//                    startActivity(intent);
//
//                } else {
//
//                    Intent intent = new Intent();
//                    Log.i(TAG, "setOnItemClickListener-----getUserId()------"
//                            + chatListItems.get(position).getUserId());
//
//                    intent.putExtra("id", chatListItems.get(position)
//                            .getUserId());
//                    intent.putExtra("chattype", (byte) chatListItems.get(position)
//                            .getChatType());
//                    currentChatUserId = chatListItems.get(position).getUserId();
//                    EnvironmentVariable.setProperty("currentChatUserId", String.valueOf
//                            (currentChatUserId));
//
//                    ChatActivitySkipUtil.startChatActivity(activity, intent);
//                }
//                chatListAdapter.notifyDataSetChanged();
//
//            }
//        });
//        // setListViewHeightBasedOnChildren(listView);
//        // listView.setFocusable(false);
//        //添加网络监听事件
//        MessageService.addMessageServiceNetStateListener(this);
//        MessageService.addMessageServiceLoginListener(this);
//        //注册eventbus
//        EventBus.getDefault().register(this);
//        return view;
//    }
//
//    //TODO 处理订阅事件
//    @Subscribe(threadMode = ThreadMode.POSTING)
//    public synchronized void onSolveMessageEvent(MessageEvent messageEvent) {
//        ChatListItem chatListItem = messageEvent.getChatListItem();
//        String key = null;
//        if (chatListItem.getChatType() == StructFactory.TO_USER_TYPE_PERSONAL
//                || chatListItem.getChatType() == StructFactory
//                .TO_USER_TYPE_OFFICIAL_ACCOUNT) {
//            key = "0" + String.valueOf(chatListItem.getUserId());
//        } else {
//            key = "1" + String.valueOf(chatListItem.getUserId());
//        }
//        if (chatListMap.containsKey(key)) {
//            ChatListItem tempChatListItem = chatListMap.get(key);
//            if (messageEvent.getType() == MessageEvent.DELETE) {
//                chatListItems.remove(chatListMap.get(key));
//                chatListMap.remove(key);
//
//            } else if (messageEvent.getType() == MessageEvent.UPDATE) {
//                tempChatListItem.setAvatar(chatListItem.getAvatar());
//                tempChatListItem.setBadgernum(chatListItem.getBadgernum());
//                tempChatListItem.setName(chatListItem.getName());
//                tempChatListItem.setUser(chatListItem.getUser());
//                tempChatListItem.setGroup(chatListItem.getGroup());
//                if (tempChatListItem.getUser() != null) {
//                    tempChatListItem.setIsBother(chatListItem.getUser().getIsBother());
//                    if (tempChatListItem.getIsTop() != chatListItem.getUser().getIsTop()) {
//                        if (chatListItem.getUser().getIsTop()) {
//                            topIndex++;
//                            tempChatListItem.setIsTop(chatListItem.getUser().getIsTop());
//                            chatListItems.remove(tempChatListItem);
//                            chatListItems.add(0, tempChatListItem);
//                        } else {
//                            if (topIndex != -1) {
//                                topIndex--;
//                            }
//                            tempChatListItem.setIsTop(chatListItem.getUser().getIsTop());
//                        }
//                        sortConversationByLastChatTime(chatListItems);
//                        sortConversationByIsTop(chatListItems);
//                    }
//                }
//                if (tempChatListItem.getGroup() != null) {
//                    tempChatListItem.setIsBother(chatListItem.getGroup().getIsBother());
//                    if (tempChatListItem.getIsTop() != chatListItem.getGroup().getIsTop()) {
//                        if (chatListItem.getGroup().getIsTop()) {
//                            topIndex++;
//                            tempChatListItem.setIsTop(chatListItem.getGroup().getIsTop());
//                            chatListItems.remove(tempChatListItem);
//                            chatListItems.add(0, tempChatListItem);
//                        } else {
//                            if (topIndex != -1) {
//                                topIndex--;
//                            }
//                            tempChatListItem.setIsTop(chatListItem.getGroup().getIsTop());
//                        }
//                        sortConversationByLastChatTime(chatListItems);
//                        sortConversationByIsTop(chatListItems);
//                    }
//                }
//
//            }
//        }
//        setUnreadCount();
//        messageHandler.sendEmptyMessage(0);
//    }
//
//    @Override
//    public void resetCountNum() {
//        setUnreadCount();
//        chatListAdapter.notifyDataSetChanged();
//    }
//
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onSolveMessageEvent(UpdateBadgeViewEvent event) {
//        String eventUserID = event.getUserID();
//        int countNum = 0;
//        for (ChatListItem item : chatListItems) {
//
//            String chatID = item.getUserId() + "";
//            if (eventUserID.equals(chatID)) {
//                String isshow = EnvironmentVariable.getProperty("isshowAtChatListFragment" + chatID, "");
//                if (!isshow.equals("true")) {
//                    item.setBadgernum(0);
//                    chatListAdapter.notifyDataSetChanged();
//                    EnvironmentVariable.setProperty("ChatIdNotReachedByChatListItem", EnvironmentVariable.getProperty("ChatIdNotReachedByChatListItem", "") + "," + chatID);
//                    continue;
//                }
//
//            }
//
//            if (item.getBadgernum() != -1) {
//                countNum += item.getBadgernum();
//            }
////            UNREADCOUNT += JFMessageManager.getInstance().
////                    getUnReadCount(item.getUserId(), (byte) item.getChatType());
//        }
//        //EventBus.getDefault().post(new UnReadCountEvent(countNum, "message"));
//
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        System.out.println("----onResume---");
//        ISDESTROY = false;
//        currentChatUserId = -1;
//        EnvironmentVariable.setProperty("currentChatUserId", "-1");
//        // topIndex = -1;
//
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    Thread.sleep(4000);
//                    isInitNetOver = true;
//                    if (!JFMessageManager.isChannelActived()) {
//                        Message message = new Message();
//                        message.what = 0;
//                        netStateChangeHandler.sendMessage(message);
//                    }
//                    if (initprogressDismiss != null)
//                        initprogressDismiss.trigger(false);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }).start();
//
//    }
//
//    private synchronized void initData() {
//        List<ChatListItem> lists = weChatDBManager.getAllChatList();
//        chatListItems.clear();
//        chatListMap.clear();
//        Iterator<ChatListItem> iterator = lists.iterator();
//        while (iterator.hasNext()) {
//            ChatListItem chatListItem = iterator.next();
//            // 将查询出的 chatlist列表放入map
//            String key = null;
//            if (chatListItem.getChatType() == StructFactory.TO_USER_TYPE_PERSONAL
//                    || chatListItem.getChatType() == StructFactory
//                    .TO_USER_TYPE_OFFICIAL_ACCOUNT) {
//                key = "0" + String.valueOf(chatListItem.getUserId());
//            } else {
//                key = "1" + String.valueOf(chatListItem.getUserId());
//            }
//
//            chatListMap.put(key, chatListItem);
//            if (chatListItem.getIsTop() == true) {
//                topIndex++;
//            }
//        }
//        if (lists != null) {
//            chatListItems.addAll(lists);
//        }
//
//        if (isInitNetOver) {
//
//        }
//        // updateBadageNumInterface.update(WeChatDBManager.UNREADCOUNT);
//
//        sortConversationByLastChatTime(chatListItems);
//        sortConversationByIsTop(chatListItems);
//        setUnreadCount();
//        messageHandler.sendEmptyMessage(0);
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        System.out.println("----onPause---");
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//        System.out.println("----onStop---");
//    }
//
//
//    @SuppressWarnings("unused")
//    private void setListViewHeightBasedOnChildren(ListView listView) {
//        // 获取ListView对应的Adapter
//        ListAdapter listAdapter = listView.getAdapter();
//        if (listAdapter == null) {
//            return;
//        }
//
//        int totalHeight = 0;
//        for (int i = 0, len = listAdapter.getCount(); i < len; i++) {
//            // listAdapter.getCount()返回数据项的数目
//            View listItem = listAdapter.getView(i, null, listView);
//            // 计算子项View 的宽高
//            listItem.measure(0, 0);
//            // 统计所有子项的总高度
//            totalHeight += listItem.getMeasuredHeight();
//        }
//
//        ViewGroup.LayoutParams params = listView.getLayoutParams();
//        params.height = totalHeight
//                + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
//        // listView.getDividerHeight()获取子项间分隔符占用的高度
//        // params.height最后得到整个ListView完整显示需要的高度
//        listView.setLayoutParams(params);
//    }
//
//    /**
//     * 根据最后一条消息的时间排序
//     *
//     * @param
//     */
//    private void sortConversationByLastChatTime(
//            List<ChatListItem> messageItemsList) {
//
//        Collections.sort(messageItemsList, new Comparator<ChatListItem>() {
//            public int compare(final ChatListItem msg1, final ChatListItem msg2) {
//                if (msg1.getStruct002() == null) return -1;
//                if (msg2.getStruct002() == null) return 1;
//                String chatTime1 = dateFormat.format(msg1.getStruct002()
//                        .getLocalTime());
//                String chatTime2 = dateFormat.format(msg2.getStruct002()
//                        .getLocalTime());
//
//                if (chatTime1.compareTo(chatTime2) == 0) {
//                    return 0;
//                } else if (chatTime1.compareTo(chatTime2) > 0) {
//                    return -1;
//                } else {
//                    return 1;
//                }
//            }
//
//        });
//    }
//
//    /**
//     * 根据是否置顶排序
//     *
//     * @param
//     */
//    private void sortConversationByIsTop(
//            List<ChatListItem> messageItemsList) {
//
//        Collections.sort(messageItemsList, new Comparator<ChatListItem>() {
//            public int compare(final ChatListItem msg1, final ChatListItem msg2) {
//
//                if (msg1.getIsTop().compareTo(msg2.getIsTop()) == 0) {
//                    return 0;
//                } else if (msg1.getIsTop().compareTo(msg2.getIsTop()) > 0) {
//                    return -1;
//                } else {
//                    return 1;
//                }
//            }
//
//        });
//    }
//
//    @Override
//    public synchronized void deleteListItem(View v, int position) {
//        System.out.println(position);
//        // 注意：在ListView中，使用getChildAt(index)的取值，只能是当前可见区域（列表可滚动）的子项！
//        // 即取值范围在 >= ListView.getFirstVisiblePosition() && <=
//        // ListView.getLastVisiblePosition();
//        // 所以如果想获取前部的将会出现返回Null值空指针问题； ,解决方法position -
//        // listView.getFirstVisiblePosition()
//        ChatListItem willDeleteBean = (ChatListItem) chatListAdapter
//                .getItem(position);
//        chatListAdapter.deletePattern(
//                listView.getChildAt(position
//                        - listView.getFirstVisiblePosition()), position);
//
//        if (willDeleteBean.getBadgernum() != -1) {
//            //UNREADCOUNT = UNREADCOUNT - willDeleteBean.getBadgernum();
//            if (willDeleteBean.getUserId() != Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID))) {
//                if (willDeleteBean.getChatType() == StructFactory.TO_USER_TYPE_OFFICIAL_ACCOUNT) {
//                    JFMessageManager.getInstance().unreadZero(willDeleteBean.getUserId(), (byte) 0);
//                } else {
//                    JFMessageManager.getInstance().unreadZero(willDeleteBean.getUserId(), (byte)
//                            willDeleteBean.getChatType());
//                }
//
//            }
//            //  updateBadageNumInterface.update(UNREADCOUNT);
//
//        }
//
//        if (willDeleteBean.getIsTop() == true) {
//            topIndex--;
//        }
//        if (chatListItems.contains(willDeleteBean)) {
//            chatListItems.remove(willDeleteBean);
//        }
//
//        String deleteKey = null;
//        if (willDeleteBean.getChatType() == StructFactory.TO_USER_TYPE_PERSONAL
//                || willDeleteBean.getChatType() == StructFactory.TO_USER_TYPE_OFFICIAL_ACCOUNT) {
//            deleteKey = "0" + String.valueOf(willDeleteBean.getUserId());
//        } else {
//            deleteKey = "1" + String.valueOf(willDeleteBean.getUserId());
//        }
//        chatListMap.remove(deleteKey);
//
//        weChatDBManager.deleteChatListiItem(willDeleteBean);
//        setUnreadCount();
//        messageHandler.sendEmptyMessage(0);
//
//
//    }
//
//    /**
//     * 设置未读总数量
//     */
//    public synchronized void setUnreadCount() {
//        //UNREADCOUNT = 0;
//        int countNum = 0;
//        for (ChatListItem item : chatListItems) {
//            if (item.getBadgernum() != -1) {
//                countNum += item.getBadgernum();
//            }
////            UNREADCOUNT += JFMessageManager.getInstance().
////                    getUnReadCount(item.getUserId(), (byte) item.getChatType());
//        }
//        // EventBus.getDefault().post(new UnReadCountEvent(countNum, "message"));
//        // updateBadageNumInterface.update(countNum);
//    }
//
//    public class ChatListMessageListener implements JFMessageListener {
//
//        @Override
//        public void onFailure(int i, IMStruct002 imstruct002) {
//            Log.i(TAG, "onFailure------消息状态：" + imstruct002.getState());
//
//        }
//
//        @Override
//        public synchronized void onReceive(final IMStruct002 imstruct002) {
//            Log.i(TAG, "onReceive------消息状态：" + imstruct002.getState());
//            // 判断 如果不是系统消息（大于等于100代表系统消息）
//            if (imstruct002.getMessageChildType() < 100) {
//                createOrUpdateChatListItem(imstruct002);
//            }
//        }
//
//        @Override
//        public synchronized void onUpdate(int i, IMStruct002 imstruct002) {
//            Log.i(TAG, "onUpdate------消息状态：" + imstruct002.getState());
//            if (imstruct002.getState() != IMStruct002.MESSAGE_STATE_READ
//                    && imstruct002.getState() != IMStruct002.MESSAGE_STATE_RECEIVE
//                    && imstruct002.getState() != IMStruct002.MESSAGE_STATE_UNREAD
//                    && imstruct002.getState() != IMStruct002.MESSAGE_STATE_DELETE
//                    && imstruct002.getState() != IMStruct002.MESSAGE_STATE_DELIVER) {
//                createOrUpdateChatListItem(imstruct002);
////                ChatListItem chatListItem = null;
////                String key = null;
////                if (imstruct002.getToUserType() == StructFactory.TO_USER_TYPE_OFFICIAL_ACCOUNT ||
////                        imstruct002.getToUserType() == StructFactory.TO_USER_TYPE_PERSONAL) {
////                    key = "0" + String.valueOf(imstruct002.getToUserId());
////                } else {
////                    key = "1" + String.valueOf(imstruct002.getToUserId());
////                }
////                Log.i(TAG, "（自己发送给别人）onUpdate中拼接的key：" + key);
////
////                if (chatListItems.size() > 0 && (chatListMap.containsKey(key))) {
////                    chatListItem = chatListMap.get(key);
////                    chatListItem.setStruct002(imstruct002);
////                    int count = JFMessageManager.getInstance().
////                            getUnReadCount(imstruct002.getToUserId(), imstruct002.getToUserType());
////                    if (count == 0) {
////                        chatListItem.setBadgernum(-1);
////                    } else {
////                        chatListItem.setBadgernum(count);
////                    }
////
////                    if (chatListItems.contains(chatListItem)) {
////                        if (chatListItem.getIsTop() == true) {
////                            chatListItems.remove(chatListItem);
////                            chatListItems.add(0, chatListItem);
////                        } else {
////                            chatListItems.remove(chatListItem);
////                            chatListItems.add(topIndex + 1, chatListItem);
////                        }
////                        weChatDBManager.insertOrUpdateChatList(chatListItem);
////                        messageHandler.sendEmptyMessage(0);
////
////                    }
////
////                } else {
////                    if (imstruct002.getToUserId() != Integer.valueOf(EnvironmentVariable
////                            .getProperty(Constants.CHAT_USER_ID))) {
////                        ChatListItem isExistsChatListItem = weChatDBManager.getChatListItem(imstruct002.getToUserId(), imstruct002.getToUserType());
////                        if (isExistsChatListItem != null) {
////                            System.out.println("发送消息，本地有，map中没有");
////                            return;
////                        }
////
////                        try {
////                            addNewItem(imstruct002, 0);
////                        } catch (JSONException e) {
////                            e.printStackTrace();
////                        }
////                    }
////
////                }
//            }
//
//        }
//
//
//    }
//
//    private synchronized void createOrUpdateChatListItem(IMStruct002 imstruct002) {
//        ChatListItem chatListItem = null;
//        byte chatType = imstruct002.getToUserType();
//
//        //当前正跟你聊天的人的id 可能是个人id 也可能是 groupid
//        int chatUserId = chatType == StructFactory.TO_USER_TYPE_GROUP ? imstruct002
//                .getToUserId() : imstruct002.getFromUserId();
//        int id = chatUserId;//此id有用处
//        if (chatUserId == Integer.parseInt(EnvironmentVariable.getProperty(Constants.CHAT_USER_ID))) {
//            chatUserId = imstruct002.getToUserId();//如果fromid是自己（自己发给被人或者两个设备登陆一个账号，取touserID）
//        }
//        //用户类型，好友可能是公众号，是群聊的话忽略此类型
//        int userType = weChatDBManager.getOneUserById(chatUserId).getType();
//
//        String key = null;
//        if (chatType == StructFactory.TO_USER_TYPE_OFFICIAL_ACCOUNT || chatType == StructFactory.TO_USER_TYPE_PERSONAL) {
//            key = "0" + String.valueOf(chatUserId);
//        } else {
//            key = String.valueOf(chatType) + String.valueOf(chatUserId);
//        }
//        Log.i(TAG, "拼接的key：" + key);
//
//        chatListItem = chatListMap.get(key);
//        if (chatListItem != null) {
//            //  chatListItem.setStruct002(imstruct002);
//            int unReadCount = 0;
//            // 如果是单聊或者应用号消息
//            if (chatType == StructFactory.TO_USER_TYPE_PERSONAL || chatType == StructFactory
//                    .TO_USER_TYPE_OFFICIAL_ACCOUNT) {
//                IMStruct002 im0 = JFMessageManager.getInstance().getLastMessage(chatUserId, (byte) 0);
//                if (im0 == null) {
//                    Log.i(TAG, "---Lastmessage  is  null ----");
//                }
//                // if (im0 != null) {
//                chatListItem.setStruct002(im0);
//                // }
//                //调用方法获取未读消息条数
//                unReadCount = JFMessageManager.getInstance().getUnReadCount(chatUserId, (byte) 0);
//            } else if (chatType == StructFactory.TO_USER_TYPE_GROUP) {
//                IMStruct002 im1 = JFMessageManager.getInstance().getLastMessage(chatUserId, chatType);
//                if (im1 == null) {
//                    Log.i(TAG, "---Lastmessage  is  null ----");
//                }
//                // if (im1 != null) {
//                chatListItem.setStruct002(im1);
//                // }
//                unReadCount = JFMessageManager.getInstance().getUnReadCount(chatUserId, chatType);
//
//            }
//            if (unReadCount == 0) {
//                chatListItem.setBadgernum(-1);//-1表示不显示角标
//            } else {
//                chatListItem.setBadgernum(unReadCount);
//            }
//
//            Intent intent = new Intent();
//            intent.putExtra("badgernum", unReadCount);
//            intent.setAction(MSG_NEW_MSG_COMMING);
//            ChatListFragment.this.activity.sendBroadcast(
//                    intent);
//
//            if (chatListItems.contains(chatListItem)) {
//                if (chatListItem.getIsTop() == true) {
//                    chatListItems.remove(chatListItem);
//                    chatListItems.add(0, chatListItem);
//                } else {
//                    chatListItems.remove(chatListItem);
//                    chatListItems.add(topIndex + 1, chatListItem);
//                }
//
//            }
//            weChatDBManager.insertOrUpdateChatList(chatListItem);
//            setUnreadCount();
//            messageHandler.sendEmptyMessage(0);
//
//        } else {
//            //1对方发给我们的消息
//            int addType = 1;
//            if (id == Integer.parseInt(EnvironmentVariable.getProperty(Constants.CHAT_USER_ID))) {
//                // 如果消息来自自己
//                addType = 0;
//            }
//            try {
//                addNewItem(imstruct002, addType);
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//        }
//
//
//    }
//
//    /**
//     * 如果聊天列表中不存在,生成新的item
//     *
//     * @param
//     * @param imstruct002
//     * @param type        判断你发给对方还是对方发给你，1表示对方发给你，0表示你发给对方
//     */
//    private synchronized void addNewItem(final IMStruct002 imstruct002, int type) throws JSONException {
//        Log.i(TAG, "-----生成item----");
//        final ChatListItem chatListItem = new ChatListItem();
//        chatListItem.setLoginUserId(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)).intValue());
//        chatListItem.setStruct002(imstruct002);
//
//        if (imstruct002.getToUserType() == StructFactory.TO_USER_TYPE_PERSONAL
//                || imstruct002.getToUserType() == StructFactory.TO_USER_TYPE_OFFICIAL_ACCOUNT) {
//            int chatUserId = type == 0 ? imstruct002.getToUserId() : imstruct002.getFromUserId();
//
//            User user = weChatDBManager.getOneFriendById(chatUserId);
//            if (user.getNickName().equals(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)))) {
//                user = weChatDBManager.getOneUserById(chatUserId);
//            }
//            if (user != null) {
//                chatListItem.setUserId(chatUserId);
//                chatListItem.setUser(user);
//                chatListItem.setName(user.getReMark());
//                chatListItem.setAvatar(user.getAvatar());
//                chatListItem.setIsTop(user.getIsTop());
//                chatListItem.setIsBother(user.getIsBother());
//                chatListItem.setChatType(ChatTypeUtil.getChatType(user.getId(), imstruct002,
//                        activity));
//                completeChatListItem(chatListItem, imstruct002, type);
//            }
//
//
//        } else if (imstruct002.getToUserType() == StructFactory.TO_USER_TYPE_GROUP) {
//            int chatGroupId = imstruct002.getToUserId();
//            chatListItem.setUserId(chatGroupId);
//            Group group = weChatDBManager.getGroupWithUsers(chatGroupId);
//            //判断如果groupde名字是groupid，其实就是群组在本地不存在，从服务器取一下
//            if (group.getGroupName().equals(String.valueOf(imstruct002.getToUserId()))) {
//                GetHttpUtil.getGroupById(activity, imstruct002.getToUserId(), null, true);
//            }
//            if (group != null) {
//                chatListItem.setName(group.getGroupName());
//                chatListItem.setAvatar(group.getAvatar());
//                Log.i(TAG, "生成item，打印群组头像" + group.getAvatar());
//                chatListItem.setGroup(group);
//                chatListItem.setIsTop(group.getIsTop());
//                chatListItem.setIsBother(group.getIsBother());
//                chatListItem.setChatType(StructFactory.TO_USER_TYPE_GROUP);
//                completeChatListItem(chatListItem, imstruct002, type);
//            }
//        }
//    }
//
//    /**
//     * 完成新生成的chatlistitem
//     */
//    private synchronized void completeChatListItem(ChatListItem chatListItem, IMStruct002 imstruct002, int
//            type) {
//        int unReadCount = 0;
//        if (type == 0) {
//            // 我发送给对方
//            unReadCount = JFMessageManager.getInstance().
//                    getUnReadCount(imstruct002.getToUserId(), imstruct002.getToUserType());
//        } else {//对方发给我
//            if (imstruct002.getToUserType() == StructFactory.TO_USER_TYPE_GROUP) {
//                unReadCount = JFMessageManager.getInstance().
//                        getUnReadCount(imstruct002.getToUserId(), imstruct002.getToUserType());
//            } else {
//                unReadCount = JFMessageManager.getInstance().
//                        getUnReadCount(imstruct002.getFromUserId(), (byte) 0);
//            }
//        }
//        if (unReadCount == 0) {
//            chatListItem.setBadgernum(-1);
//        } else {
//            chatListItem.setBadgernum(unReadCount);
//        }
//        Log.i(TAG, "角标-----------" + chatListItem.getBadgernum());
//        if (chatListItem.getIsTop() == true) {
//            chatListItem.setIsTop(true);
//            chatListItems.add(0, chatListItem);
//            topIndex++;
//        } else {
//            chatListItem.setIsTop(false);
//            chatListItems.add(topIndex + 1, chatListItem);
//        }
//
//        String newKey = null;
//        if (imstruct002.getToUserType() == StructFactory.TO_USER_TYPE_OFFICIAL_ACCOUNT ||
//                imstruct002.getToUserType() == StructFactory.TO_USER_TYPE_PERSONAL) {
//            newKey = "0" + String.valueOf(chatListItem.getUserId());
//        } else {
//            newKey = String.valueOf(imstruct002.getToUserType()) + String.valueOf(chatListItem.getUserId());
//        }
//
//        chatListMap.put(newKey, chatListItem);
//        weChatDBManager.insertOrUpdateChatList(chatListItem);
//        //刷新界面
//        setUnreadCount();
//        messageHandler.sendEmptyMessage(0);
//    }
//
//
//    @Override
//    public void netStateChange(int net_state) {
//        Log.i(TAG, "netStateChange--监听网络状态--" + net_state);
//        if (net_state == 0) {
//            Message message = new Message();
//            message.what = 0;
//
//            if (isInitNetOver == true)
//                this.netStateChangeHandler.sendMessage(message);
//        } else {
//            Message message = new Message();
//            message.what = 1;
//
//            this.netStateChangeHandler.sendMessage(message);
//        }
//    }
//
//    //处理网络监听状态handle
//    Handler netStateChangeHandler = new Handler() {
//        public void handleMessage(Message msg) {
//            switch (msg.what) {
//                case 0:
//                    rl_error_item.setVisibility(View.VISIBLE);
//                    break;
//                case 1:
//                    rl_error_item.setVisibility(View.GONE);
//                    if (initprogressDismiss != null)
//                        initprogressDismiss.trigger(false);
//                    isInitNetOver = true;
//                    break;
//            }
//            super.handleMessage(msg);
//        }
//    };
//
//    //监听tcp是否可用事件
//    @Override
//    public void onLoginSuccess() {
//        Message message = new Message();
//        message.what = 1;
//
//        this.netStateChangeHandler.sendMessage(message);
//    }
//
//    @Override
//    public void onLoginFail(String errorMsg) {
//
//    }
//
//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        EventBus.getDefault().unregister(this);
//        System.out.println("----onDestroy---");
//        if (chatListMessageListener != null) {
//            JFMessageManager.getInstance().removeMessageListener(EnvironmentVariable.getProperty(CHAT_USER_ID),
//                    chatListMessageListener);
//        }
//
//        MessageService.removeMessageServiceNetStateListener(this);
//        MessageService.removeMessageServiceLoginListener(this);
//        ISDESTROY = true;
//
//    }
//
//    public interface UpdateBadageNumInterface {
//        public void update(int num);
//    }
//
//    public void setUpdateBadageNumInterface(UpdateBadageNumInterface _updateBadageNumInterface) {
//        updateBadageNumInterface = _updateBadageNumInterface;
//    }
//
//    public interface InitprogressDismiss {
//        public void trigger(boolean isShow);
//    }
//
//    public void setInitprogressDismiss(InitprogressDismiss _initprogressDismiss) {
//        initprogressDismiss = _initprogressDismiss;
//    }
//
//    //处理系统消息
//    private void solveSystemInfo(IMStruct002 imstruct002) {
//        String json = "";
//        String key = null;
//        try {
//            json = new String(imstruct002.getBody(), "UTF-8");
//            System.out.println("----solveSystemInfo：" + json + "----");
//            JSONObject jsonObject = new JSONObject(json);
//            if (jsonObject != null && jsonObject.has("CMD")) {
//                Log.i(TAG, jsonObject.getString("CMD"));
//            }
//
//
//            if (imstruct002.getMessageChildType() == subtype_masterdata) {
////                //主数据存储到本地文件
////                FileUtils.writeTextFile(EFAppAccountUtils
////                        .getAppAccountJsonPath() + "/" + "master_data.json", json);
//                return;
//            }
//            if (imstruct002.getMessageChildType() == subtype_officalAccountRes) {
//                Log.i(TAG, jsonObject.getString("version"));
//                //todo 应用号资源更新发送广播
//                Intent intent = new Intent();
//                intent.putExtra("num", "1");
//                intent.putExtra("id", imstruct002.getFromUserId());
//                intent.putExtra("type", "appaccountres");
//                intent.setAction("com.efounder.updateres");
//                String uniqueId = EnvironmentVariable.getProperty(CHAT_USER_ID) + String.valueOf(imstruct002
//                        .getFromUserId()) + "res";
//                EnvironmentVariable.setProperty(uniqueId, "true");
//                getActivity().sendBroadcast(intent);
//                return;
//            }
//            if (imstruct002.getMessageChildType() == subtype_App) {
//                //todo APP版本更新发送广播
//                Intent intent = new Intent();
//                intent.putExtra("version", jsonObject.getString("version"));//版本号
//                intent.putExtra("updateNote", jsonObject.getString("updateNote"));//更新说明
//                intent.putExtra("id", imstruct002.getFromUserId());
//                intent.putExtra("type", "appupdate");
//                intent.putExtra("url", jsonObject.getString("url"));
//                intent.setAction("com.efounder.updateres");
//                getActivity().sendBroadcast(intent);
//                return;
//            }
//            if (imstruct002.getMessageChildType() == subtype_AppRes) {
//                //todo APP资源文件更新发送广播 yqs
//                Intent intent = new Intent();
//                intent.putExtra("version", jsonObject.getString("version"));//版本号
//                intent.putExtra("updateNote", jsonObject.getString("updateNote"));//更新说明
//                intent.putExtra("id", imstruct002.getFromUserId());
//                intent.putExtra("type", "appresupdate");
//                intent.putExtra("url", "");
//                intent.setAction("com.efounder.updateres");
//                getActivity().sendBroadcast(intent);
//                return;
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//}
