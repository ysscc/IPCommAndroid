/**
 * Copyright (C) 2013-2014 EaseMob Technologies. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.efounder.chat.adapter;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.efounder.chat.R;
import com.efounder.chat.model.ChatMenuModel;
import com.efounder.chat.utils.ViewHolder;

import java.util.List;

/**
 * 聊天菜单adapter（GridView使用）
 * author yqs
 */
public class ChatMenuAdapter extends CommonAdapter {


    public ChatMenuAdapter(Context context, List datas, int layoutId) {
        super(context, datas, layoutId);
    }

    @Override
    public void convert(ViewHolder holder, Object o, int position) {

        ImageView btn = holder.getView(R.id.btn);
        TextView textView = holder.getView(R.id.text);

        String localIcon = ((ChatMenuModel) o).getLocalIcon();
        if (localIcon != null) {
//            ImageLoader.getInstance().displayImage("file://" + localIcon, btn,
//                    ImageUtil.getImageLoaderOptions(R.drawable.icon_chat_photo));
            Glide.with(context).load("file://" + localIcon)
                    .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(false))
                    // .placeholder(R.drawable.icon_chat_photo))
                    .into(btn);
            //LXGlideImageLoader.getInstance().displayImage(context,btn,"file://" + localIcon,R.drawable.icon_chat_photo);
        } else {
            btn.setImageResource(((ChatMenuModel) o).getIcon());

        }
        textView.setText(((ChatMenuModel) o).getName());
    }
}
