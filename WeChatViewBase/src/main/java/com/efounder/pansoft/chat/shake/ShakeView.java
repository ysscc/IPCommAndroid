package com.efounder.pansoft.chat.shake;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.adapter.ExpressionPagerAdapter;
import com.efounder.pansoft.chat.animation.ChatStyleFactory;
import com.efounder.utils.ResStringUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 抖动的金币动画
 */
public class ShakeView extends LinearLayout {

    private Context mContext;
    private ViewPager expressionViewpager;
    private List<View> dots;
    private RecyclerView recyclerView;
    private List<ShakeResource> shakeResourceList;
    private ShakeOnclickListener shakeOnclickListener;


    public ShakeView(Context context) {
        super(context, null);
        init(context, null);
    }

    public ShakeView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

//    @Override
//    public void onSoftKeyboardHeightChanged(int i) {
//    }

    public ShakeView(@NonNull Context context, @Nullable AttributeSet attrs,
                     @AttrRes int defStyleAttr) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        mContext = context;
        inflate(context, R.layout.layout_chatinput_shake, this);

        LinearLayout shakeLayout = findViewById(R.id.ll_shake_container);
        expressionViewpager = (ViewPager) findViewById(R.id.vPager);
//        recyclerView = findViewById(R.id.recycleview);
//        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));

        initShakeMenu();


    }


    private void initShakeMenu() {
        shakeResourceList = new ArrayList<>();
        // 初始化表情viewpager
        List<View> views = new ArrayList<View>();

        View gv1 = getGridView();
        // View gv2 = getGridView();
        views.add(gv1);
        // views.add(gv2);
        expressionViewpager.setAdapter(new ExpressionPagerAdapter(views));

        expressionViewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

//                recyclerView.smoothScrollToPosition(position);
//                emojiButtonAdater.setBgPosition(position);
//                emojiButtonAdater.notifyDataSetChanged();


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    private View getGridView() {
        View view = View.inflate(mContext, R.layout.qqstyle_layouy_shake_gridview,
                null);

        Drawable drawable = ChatStyleFactory.getAnimation("emoji", "1001");
        if (drawable != null) {
            ShakeResource shakeResource1 = new ShakeResource.Builder()
                    // .drawable(ContextCompat.getDrawable(mContext, R.drawable.jf_frame_shake_gold_animlist))
                    .drawable(drawable)
                    .name(ResStringUtil.getString(R.string.wrchatview_gold_coin)).type("gold").build();

            shakeResourceList.add(shakeResource1);
        }

        GridView gv = (GridView) view.findViewById(R.id.gridview);


        final ShakeGridViewAdapter shakeGridViewAdapter = new ShakeGridViewAdapter(
                mContext, 1, shakeResourceList);
        gv.setAdapter(shakeGridViewAdapter);
        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if (shakeOnclickListener != null) {
                    shakeOnclickListener.shakeItemOnClick(shakeResourceList.get(position).type);
                }

            }
        });
        return view;
    }

    public static class ShakeGridViewAdapter extends ArrayAdapter<ShakeResource> {

        private List<ShakeResource> list;

        public ShakeGridViewAdapter(Context context, int textViewResourceId, List<ShakeResource> objects) {
            super(context, textViewResourceId, objects);
            list = objects;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(getContext(), R.layout.jf_layout_shake_anim_grid_item, null);
            }

            ImageView imageView = (ImageView) convertView.findViewById(R.id.ivshakeview);
            TextView textView = (TextView) convertView.findViewById(R.id.textview);

            ShakeResource shakeResource = getItem(position);

            textView.setText(shakeResource.name);
            imageView.setImageDrawable(list.get(position).drawable);
            if (list.get(position).drawable instanceof AnimationDrawable) {
                ((AnimationDrawable) list.get(position).drawable).start();
            }


            return convertView;
        }

    }

    private static class ShakeResource {
        public String name;
        public Drawable drawable;
        public Object iconObjct;
        public String type;

        private ShakeResource(Builder builder) {
            name = builder.name;
            drawable = builder.drawable;
            iconObjct = builder.iconObjct;
            type = builder.type;
        }


        public static final class Builder {
            private String name;
            private Drawable drawable;
            private Object iconObjct;
            private String type;

            public Builder() {
            }

            public Builder name(String val) {
                name = val;
                return this;
            }

            public Builder drawable(Drawable val) {
                drawable = val;
                return this;
            }

            public Builder iconObjct(Object val) {
                iconObjct = val;
                return this;
            }

            public Builder type(String val) {
                type = val;
                return this;
            }

            public ShakeResource build() {
                return new ShakeResource(this);
            }
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (shakeResourceList != null) {
            for (int i = 0; i < shakeResourceList.size(); i++) {
                if (shakeResourceList.get(i).drawable instanceof AnimationDrawable
                        && ((AnimationDrawable) shakeResourceList.get(i).drawable).isRunning()) {
                    ((AnimationDrawable) shakeResourceList.get(i).drawable).stop();
                }
            }
        }
    }

    public interface ShakeOnclickListener {
        void shakeItemOnClick(String type);
    }

    public void setShakeOnclickListener(ShakeOnclickListener listener) {
        this.shakeOnclickListener = listener;
    }
}
