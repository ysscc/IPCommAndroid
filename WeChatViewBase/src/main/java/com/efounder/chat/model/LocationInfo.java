package com.efounder.chat.model;

import java.io.Serializable;

import com.google.gson.annotations.Expose;

public class LocationInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String curLocationAddressPic;
	
	private double latitude;
	
	private double longitude;
	
	private String address;
	
	private String name;

	public String getcurLocationAddressPic() {
		return curLocationAddressPic;
	}

	public void setCurLocationAddressPic(String curLocationAddress) {
		this.curLocationAddressPic = curLocationAddress;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "LocationInfo [curLocationAddressPic=" + curLocationAddressPic
				+ ", latitude=" + latitude + ", longitude=" + longitude
				+ ", address=" + address + ", name=" + name + "]";
	}
	
}
