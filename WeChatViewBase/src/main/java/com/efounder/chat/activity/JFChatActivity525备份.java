//package com.efounder.chat.activity;
//
//import android.app.NotificationManager;
//import android.content.Intent;
//import android.graphics.drawable.AnimationDrawable;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.support.annotation.NonNull;
//import android.text.TextUtils;
//import android.util.Log;
//import android.view.KeyEvent;
//import android.view.MotionEvent;
//import android.view.BaseView;
//import android.view.BaseView.OnClickListener;
//import android.widget.FrameLayout;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.ProgressBar;
//import android.widget.TextView;
//
//import com.core.xml.StubObject;
//import com.efounder.chat.R;
//import com.efounder.chat.adapter.ChatVoicePlayClickListener;
//import com.efounder.chat.db.WeChatDBManager;
//import com.efounder.chat.fragment.ChatSenderFragment;
//import com.efounder.chat.handler.ChatMessageListener;
//import com.efounder.chat.http.GetHttpUtil;
//import com.efounder.chat.manager.ChatInterfaceSkipManager;
//import com.efounder.chat.manager.ChatListManager;
//import com.efounder.chat.manager.ChatMessageSendManager;
//import com.efounder.chat.model.AnimationEvent;
//import com.efounder.chat.model.AppConstant;
//import com.efounder.chat.model.ChatMenuModel;
//import com.efounder.chat.model.Group;
//import com.efounder.chat.model.GroupRootBean;
//import com.efounder.chat.model.UpdateBadgeViewEvent;
//import com.efounder.chat.model.VoicePlayingEvent;
//import com.efounder.chat.service.MessageService;
//import com.efounder.chat.service.SystemInfoService;
//import com.efounder.chat.struct.StructFactory;
//import com.efounder.chat.utils.GroupAvatarHelper;
//import com.efounder.chat.utils.GroupAvatarUtil;
//import com.efounder.chat.utils.ImageUtil;
//import com.efounder.chat.utils.LXGlideImageLoader;
//import com.efounder.chat.widget.ChatListView;
//import com.efounder.constant.EnvironmentVariable;
//import com.efounder.frame.utils.Constants;
//import com.efounder.message.manager.JFMessageListener;
//import com.efounder.message.manager.JFMessageManager;
//import com.efounder.message.struct.IMStruct002;
//import com.efounder.mobilecomps.contacts.User;
//import com.efounder.pansoft.chat.animation.AnimationFactory;
//import com.efounder.pansoft.chat.animation.ChatStyleFactory;
//import com.efounder.pansoft.chat.input.ChatInputView;
//import com.efounder.pansoft.chat.input.JFChatAdapter;
//import com.efounder.pansoft.chat.input.SecretInputView;
//import com.efounder.pansoft.chat.listener.OnMenuClickListener;
//import com.efounder.pansoft.chat.record.voice.VoiceView;
//import com.efounder.service.Registry;
//import com.efounder.util.AbFragmentManager;
//import com.groupimageview.NineGridImageView;
//import com.pansoft.library.utils.LogUtils;
//import com.pansoft.library.utils.OkHttpUtils;
//import com.scwang.smartrefresh.layout.SmartRefreshLayout;
//import com.scwang.smartrefresh.layout.api.RefreshLayout;
//import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
//
//import org.greenrobot.eventbus.EventBus;
//import org.greenrobot.eventbus.Subscribe;
//import org.greenrobot.eventbus.ThreadMode;
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.File;
//import java.lang.ref.WeakReference;
//import java.util.ArrayList;
//import java.util.Hashtable;
//import java.util.List;
//
//import io.reactivex.Observable;
//import io.reactivex.ObservableEmitter;
//import io.reactivex.ObservableOnSubscribe;
//import io.reactivex.android.schedulers.AndroidSchedulers;
//import io.reactivex.disposables.Disposable;
//import io.reactivex.functions.Consumer;
//import io.reactivex.schedulers.Schedulers;
//
//import static com.efounder.frame.utils.NetStateBroadcastReceiver.isNetActive;
//
///**
// * 聊天对话Activity
// * 2018/04/12
// *
// * @author YQS
// */
//
//public class JFChatActivity525备份 extends BaseActivity implements MessageService.MessageServiceNetStateListener,
//        MessageService.MessageServiceLoginListener,
//        OnMenuClickListener, VoiceView.MyRecordVoiceListener {
//    public static final String TAG = "JFChatActivity";
//    //聊天文件路径
//    public static String chatFilePath = ImageUtil.chatpath;
//    //聊天输入框
//    private ChatInputView mChatInputView;
//    //密码输入view
//    private SecretInputView secretInputView;
//    // 聊天ListView
//    private ChatListView chatListView;
//    // 聊天ListView 的 adapter
//    private JFChatAdapter chatAdapter;
//    // 聊天类型：判断单聊，还是群聊，还是公众号
//    private byte chatType;
//    public JFChatActivity525备份 activityInstance = null;
//    //聊天对象id
//    private int chatUserId;
//    //聊天群组
//    private Group group;
//    //用户
//    private User user;
//    //下拉加载更多（转圈圈）
//    private ProgressBar loadmorePB;
//    //下拉刷新组件
//    private SmartRefreshLayout smartRefreshLayout;
//    String toUserNick = "";
//    // 设置按钮
//    private ImageView iv_setting;
//    private ImageView iv_setting_group;
//    //左上角显示的用户名称
//    private TextView toChatUserNickTextView;
//    //消息管理器
//    private JFMessageManager messageManager;
//    //消息监听
//    private JFMessageListener messageListener;
//    //数据库
//    private Handler messageHandler = new MessageHandler(this);
//    //消息列表
//    List<IMStruct002> messageList;
//
//    private ChatListManager chatListManager = new ChatListManager();
//    private ImageView iv_chat_add;
//    private LinearLayout llMenu;
//
//    //选择语音播放方式的layout
//    private LinearLayout llVoicePlayTypeLayout;
//    private ImageView ivVoicePlayView;
//    private TextView tvVoicePlayTypeView;
//    //动画布局
//    private FrameLayout animFrameLayout;
//    //群组人数
//    private TextView tvGroupUserCount;
//
//    private GroupRootBean bean;
//    private AbFragmentManager abFragmentManager;
//
//    //聊天权限String
//    private String current_Chat_permissionType = "";
//    private String current_Chat_permissionContent = "";
//
//    //消息发送管理
//    private ChatMessageSendManager chatMessageSendManager = new ChatMessageSendManager(this);
//    //更多菜单界面跳转管理
//    private ChatInterfaceSkipManager interfaceSkipManager;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.qqstyle_layout_activity_chat);
//        //注册eventbus
//        EventBus.getDefault().register(this);
//
//        initView();
//        initChatObjectInfo(getIntent());
//        initAttr();
//        setUpView();
//        getChatConfig();
//        //initUserChatPermission();
//
//        MessageService.addMessageServiceLoginListener(this);
//        MessageService.addMessageServiceNetStateListener(this);
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//
//        //！！！启动服务(再onResume中启动，防止这种情况：Activity处于前台，service先于Activity被回收掉)
//        startService(new Intent(this, MessageService.class));
//        //env存储正在聊天的人
//        EnvironmentVariable.setProperty("currentChatUserId", String.valueOf(chatUserId));
//        // 清除通知栏消息
//        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//        int requestCode = Integer.valueOf(chatUserId + "" + chatType);
//        notificationManager.cancel(requestCode);
//
//        updateChatUser();
//        initUserChatPermission();
//
//        //判断是否显示网络不可用
//        if (!isNetActive() || !JFMessageManager.isChannelActived()) {
//            if (chatType == StructFactory.TO_USER_TYPE_PERSONAL)
//                toChatUserNickTextView.setText(user.getReMark() + "(网络不可用)");
//            else {
//                tvGroupUserCount.setVisibility(BaseView.VISIBLE);
//                tvGroupUserCount.setText("(" + group.getUsers().size() + ")" + "(网络不可用)");
//                toChatUserNickTextView.setText(group.getGroupName());
//            }
//        }
//
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        chatListManager.clearunReadCount(weChatDBManager, chatUserId, chatType);
//        EventBus.getDefault().post(new UpdateBadgeViewEvent(chatUserId + "", chatType));
//        //停止语音播放
//        if (ChatVoicePlayClickListener.currentPlayListener != null && ChatVoicePlayClickListener.isPlaying) {
//            ChatVoicePlayClickListener.currentPlayListener.stopPlayVoice();
//            //隐藏播放方式的view
//            llVoicePlayTypeLayout.setVisibility(BaseView.GONE);
//        }
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        //使用id+type作为保存草稿的key
//        String unfinishedText = chatUserId + "" + chatType;
//        //将草稿保存到EV中
//        EnvironmentVariable.setProperty(unfinishedText, mChatInputView.getInputView().getText().toString());
//    }
//
//    @Override
//    protected void onDestroy() {
//        Log.i(TAG, "onDestroy");
//        super.onDestroy();
//        if (messageListener != null) {
//            messageManager.removeMessageListener(TAG, messageListener);
//        }
//        MessageService.removeMessageServiceLoginListener(this);
//        MessageService.removeMessageServiceNetStateListener(this);
//        //如果可以，请在chatListManager中释放资源
//        chatListManager.release(activityInstance);
//        chatMessageSendManager.release();
//        ChatStyleFactory.clear();
//        activityInstance = null;
//        chatAdapter.removeDownLoadListener();
//        EventBus.getDefault().unregister(this);
//        // mChatInputView =null;
//    }
//
//
//    //初始化聊天对象的信息
//    public void initChatObjectInfo(Intent intent) {
//        chatUserId = intent.getIntExtra("id", 1);
//        chatType = intent.getByteExtra("chattype", StructFactory.TO_USER_TYPE_GROUP);
//        Log.i(TAG, "--chatUserId:" + chatUserId + "");
//        Log.i(TAG, "--chattype:" + chatType + "");
//
//        weChatDBManager = WeChatDBManager.getInstance();
//        abFragmentManager = new AbFragmentManager(this);
//        chatMessageSendManager.setChatType(chatType);
//        chatMessageSendManager.setCurrentChatUserId(chatUserId);
//        chatMessageSendManager.setPreSendMessageCallback(preSendMessageCallback);
//
//    }
//
//    /*titleBar添加快捷按钮*/
//    private void getChatConfig() {
//        String userId = EnvironmentVariable.getProperty(Constants.CHAT_USER_ID);
//        String passWord = EnvironmentVariable.getProperty(Constants.CHAT_PASSWORD);
//        if (null != group) {
//            int groupId = group.getGroupId();
//            String url = "http://im.solarsource.cn:9692/IMServer/group/getConfig?userId=%s&passWord=%s&groupId=%s&configKey=groupChatIcon";
//            String urlPath = String.format(url, userId, passWord, groupId);
//            OkHttpUtils.netRequest("get", urlPath, null, null, new OkHttpUtils.DownloadListener() {
//                @Override
//                public void onSuccess(byte[] bytes) {
//                    String json = new String(bytes);
//                    try {
//                        JSONObject object = new JSONObject(json);
//                        if (object == null) {
//                            return;
//                        }
//                        if (!object.has("result")) {
//                            return;
//                        }
//                        String result = object.getString("result");
//                        if ("success".equals(result)) {
//                            if (!object.has("configValue")) {
//                                return;
//                            }
//                            String value = object.getString("configValue");
//                            if (null != value) {
////                            "configValue":"punchcard|common"
//                                LogUtils.e("configValue=" + value);
//                                List<StubObject> officeRoot = Registry.getRegEntryList("GroupRoot");
////                            final ArrayList<String> typeList = new ArrayList<>();
////                            final ArrayList<String> activityList = new ArrayList<>();
////                            final ArrayList<String> iconList = new ArrayList<>();
//                                /*展示在titleBar上的快捷按钮容器*/
//                                final List<GroupRootBean> groupMenuList = new ArrayList<>();
//                                for (int i = 0; i < officeRoot.size(); i++) {
//                                    StubObject stubObject = officeRoot.get(i);
//                                    Hashtable stubTable = stubObject.getStubTable();
//
//                                    String id = (String) stubTable.get("chatUserId");
//                                    if (value.contains(id)) {
//                                        GroupRootBean bean = new GroupRootBean();
//                                        bean.setIcon((String) stubTable.get("icon"));
//                                        bean.setViewType((String) stubTable.get("viewType"));
//                                        bean.setShow((String) stubTable.get("AndroidShow"));
//                                        groupMenuList.add(bean);
//                                    }
//                                }
//                                if (groupMenuList != null && groupMenuList.size() > 0) {
//                                    for (int i = 0; i < groupMenuList.size(); i++) {
//                                        String filepath = AppConstant.APP_ROOT + "/res" + "/" + "unzip_res" + "/" + "menuImage/" + groupMenuList.get(i).getIcon();
//                                        ImageView iv = new ImageView(JFChatActivity525备份.this);
//                                        if (!TextUtils.isEmpty(filepath))
////                                            imageLoader.displayImage("file://" + filepath, iv, options);
//                                            LXGlideImageLoader.getInstance().displayImage(JFChatActivity525备份.this, iv, "file://" + filepath);
//                                        final int finalI = i;
//                                        iv.setOnClickListener(new OnClickListener() {
//                                            @Override
//                                            public void onClick(BaseView view) {
//                                                if ("group_function_2_".equals(groupMenuList.get(finalI).getIcon())) {
//                                                    String viewType = groupMenuList.get(finalI).getViewType();
//                                                    if (null != viewType)
//                                                        if ("display".equals(viewType)) {
////                                                    跳转页面
//                                                            String aName = groupMenuList.get(finalI).getShow();
//                                                            AbFragmentManager abFragmentManager = new AbFragmentManager(JFChatActivity525备份.this);
//                                                            StubObject stubObject = new StubObject();
//                                                            Hashtable<String, String> hashtable = new Hashtable<String, String>();
//                                                            hashtable.put("groupId", group.getGroupId() + "");
//                                                            hashtable.put("groupName", group.getGroupName());
//                                                            hashtable.put("AndroidShow", aName);
//                                                            stubObject.setStubTable(hashtable);
//                                                            try {
//                                                                abFragmentManager.startActivity(stubObject, 0, 0);
//                                                            } catch (Exception e) {
//                                                                e.printStackTrace();
//                                                            }
//                                                           /* //显示意图
////                                                    Intent intent = new Intent(ChatActivity.this, WorkCardActivity.class);
//                                                            Intent intent = new Intent();
////                                                    intent.setClass(ChatActivity.this, WorkCardActivity.class);
////                                                    隐式意图
//                                                            String packageName;
//                                                            packageName = "com.efounder.chat";
//                                                            if (!TextUtils.isEmpty(aName)) {
//                                                                ComponentName componentName = new ComponentName(ChatActivity.this, packageName + ".activity." + aName);
//                                                                intent.setComponent(componentName);
//                                                                intent.putExtra("groupName", group.getGroupName());
//                                                                intent.putExtra("groupId", group.getGroupId());
//                                                                startActivity(intent);
//                                                            }*/
//                                                        } else if ("webView".equals(viewType)) {
////                                                    webview 展示
//
//                                                        }
//                                                }
//                                            }
//                                        });
//                                        llMenu.addView(iv);
//                                    }
//                                }
//                            }
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//
//                @Override
//                public void onFailure(String exception) {
//                    LogUtils.e(TAG + ":getChatConfig.exception=" + exception);
//                }
//            });
//        }
//    }
//
//    /**
//     * 初始化一些属性
//     */
//    private void initAttr() {
//        messageManager = JFMessageManager.getInstance();
//        if (messageListener != null) {
//            messageManager.removeMessageListener(TAG, messageListener);
//        }
//        messageListener = new ChatMessageListener(this, chatUserId, weChatDBManager, messageHandler);
//        messageManager.addMessageListener(TAG, messageListener);
//        interfaceSkipManager = new ChatInterfaceSkipManager(this);
//    }
//
//    /**
//     * initView
//     */
//    protected void initView() {
//        mChatInputView = (ChatInputView) findViewById(R.id.chat_input);
//        secretInputView = (SecretInputView) findViewById(R.id.secret_inut);
//        //必须设置
//        mChatInputView.setMyRecordVoiceListener(this);
//        //设置按钮回调
//        mChatInputView.setMenuClickListener(this);
//
//        chatListView = (ChatListView) findViewById(R.id.list);
//        chatListView.setOnTouchListener(new ChatListViewOnTouchListener());
//        loadmorePB = (ProgressBar) findViewById(R.id.pb_load_more);
//        toChatUserNickTextView = ((TextView) findViewById(R.id.name));
//        animFrameLayout = (FrameLayout) findViewById(R.id.framelayout_anim);
//        llVoicePlayTypeLayout = (LinearLayout) findViewById(R.id.ll_voice_play_style);
//        ivVoicePlayView = (ImageView) findViewById(R.id.iv_voice_playtype);
//        tvVoicePlayTypeView = (TextView) findViewById(R.id.tv_voice_type);
//        smartRefreshLayout = (SmartRefreshLayout) findViewById(R.id.refreshLayout);
//        iv_setting = (ImageView) this.findViewById(R.id.iv_setting);
//        iv_setting_group = (ImageView) this.findViewById(R.id.iv_setting_group);
//        iv_chat_add = (ImageView) this.findViewById(R.id.iv_chat_add);
//        llMenu = (LinearLayout) findViewById(R.id.ll_chat_menu);
//        tvGroupUserCount = (TextView) findViewById(R.id.group_count);
//    }
//
//    private void setUpView() {
//        activityInstance = this;
//        //设置草稿文字
//        mChatInputView.getInputView().setText(EnvironmentVariable.getProperty(chatUserId + "" + chatType));
//        //设置语音播放方式的view
//        if (voicePlayType.equals("0")) {
//            tvVoicePlayTypeView.setText("听筒");
//            ivVoicePlayView.setImageDrawable(getResources().getDrawable(R.drawable.qqstyle_voice_play_type_tingtong));
//        } else {
//            tvVoicePlayTypeView.setText("免提");
//            ivVoicePlayView.setImageDrawable(getResources().getDrawable(R.drawable.qqstyle_voice_play_type_mainti));
//        }
//        updateChatUser();
//        iv_setting.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(BaseView v) {
//                Intent intent = new Intent(JFChatActivity525备份.this,
//                        ChatSingleSettingActivity.class);
//                intent.putExtra("id", chatUserId);
//                intent.putExtra("chattype", chatType);
//                startActivity(intent);
//            }
//
//        });
//
//        // iv_chat_add.setOnClickListener(this); Toast.makeText(this, "群聊快捷按钮", Toast.LENGTH_LONG).show();
//        iv_setting_group.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(BaseView v) {
//                Intent intent = new Intent(JFChatActivity525备份.this,
//                        ChatGroupSettingActivity.class);
//                intent.putExtra("id", chatUserId);
//                intent.putExtra("chattype", chatType);
//                startActivity(intent);
//            }
//
//        });
//        smartRefreshLayout.setEnableAutoLoadMore(false);//开启自动加载功能（非必须）
//        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
//            @Override
//            public void onRefresh(@NonNull final RefreshLayout refreshLayout) {
//                tryLoadHistoryMessage();
//            }
//        });
//        setupChatListView();
//    }
//
//    private void setupChatListView() {
//        messageList = JFMessageManager.getInstance().getMessageList(chatUserId, chatType);
//        // TODO 获取历史消息
//        if (messageList.size() == 0) {
//            List<IMStruct002> historyImStruct002s = chatListManager.getHistoryMessage(chatType, user,
//                    messageManager, group, null, 20, null);
//        }
//
//        for (int i = messageList.size() - 1; i >= 0; i--) {
//            if (messageList.get(i).getFromUserId() != Integer.valueOf(EnvironmentVariable
//                    .getProperty(Constants.CHAT_USER_ID))
//                    && messageList.get(i).getState() != IMStruct002.MESSAGE_STATE_READ
//                    && messageList.get(i).getToUserType() != StructFactory.TO_USER_TYPE_GROUP
//                    && messageList.get(i).getReadState() != IMStruct002.MESSAGE_STATE_READ
//                    ) {
//                JFMessageManager.getInstance().sendReadMessage(messageList.get(i));
//            }
////            else {
////                if (messageList.get(i).getToUserType() == StructFactory.TO_USER_TYPE_GROUP) {
////
////                }
////            }
//        }
//        chatAdapter = new JFChatAdapter(this, chatListView, messageList);
//        chatListView.setAdapter(chatAdapter);
//        chatListView.setSelection(chatListView.getCount() - 1);
//    }
//
//    //初始化用户权限
//    private void initUserChatPermission() {
//        if (chatType == StructFactory.TO_USER_TYPE_PERSONAL) {// 单聊
//            /*****************增加聊天权限控制************************/
//            String chat_permission = EnvironmentVariable.getProperty("chatpermission", "");
//            if (!chat_permission.equals("")) {
//                JSONObject chat_permissionJson = null;
//                try {
//                    chat_permissionJson = new JSONObject(chat_permission);
//                    JSONArray chat_permissionJsonArray = chat_permissionJson.getJSONArray("chatPermission");
//                    //JsonObject chat_permissionJson = new JsonParser().parse(chat_permission).getAsJsonObject();
//                    //JsonArray chat_permissionJsonArray = chat_permissionJson.getAsJsonArray("chatPermission");
//                    for (int i = 0; i < chat_permissionJsonArray.length(); i++) {
//                        JSONObject jsonObject = (JSONObject) chat_permissionJsonArray.get(i);
//                        String userID = jsonObject.getString("userID");
//                        if (userID.equals(chatUserId + "")) {
//                            current_Chat_permissionType = jsonObject.getString("type");
//                            current_Chat_permissionContent = jsonObject.getString("content");
//                        }
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                if (current_Chat_permissionType.equals("0")) {
//                    mChatInputView.setVisibility(BaseView.GONE);
//                }
//                mChatInputView.setChatUserInfo(chatUserId, chatType, current_Chat_permissionContent);
//            } else {
//                mChatInputView.setChatUserInfo(chatUserId, chatType, current_Chat_permissionContent);
//            }
//            /*****************增加聊天权限控制************************/
//
//        } else {
//            mChatInputView.setChatUserInfo(chatUserId, chatType, current_Chat_permissionContent);
//        }
//    }
//
//    //刷新用户信息
//    private void updateChatUser() {
//        if (chatType == StructFactory.TO_USER_TYPE_PERSONAL) {
//            user = weChatDBManager.getOneUserById(chatUserId);
//            toUserNick = user.getReMark();
//            toChatUserNickTextView.setText(toUserNick);
//            tvGroupUserCount.setVisibility(BaseView.GONE);
//            iv_setting.setVisibility(BaseView.VISIBLE);
//            iv_chat_add.setVisibility(BaseView.GONE);
//            iv_setting_group.setVisibility(BaseView.GONE);
//
//        } else if (chatType == StructFactory.TO_USER_TYPE_OFFICIAL_ACCOUNT) {
//            user = new User();
//            toChatUserNickTextView.setText(getIntent().getStringExtra("nickName"));
//            tvGroupUserCount.setVisibility(BaseView.GONE);
//
//        } else if (chatType == StructFactory.TO_USER_TYPE_GROUP) {
//            group = weChatDBManager.getGroupWithUsers(chatUserId);
//            tvGroupUserCount.setVisibility(BaseView.VISIBLE);
//            tvGroupUserCount.setText("(" + group.getUsers().size() + ")");
//            toChatUserNickTextView.setText(group.getGroupName());
//            iv_setting_group.setVisibility(BaseView.VISIBLE);
//            iv_setting.setVisibility(BaseView.GONE);
//            if (group != null) {
//                // 从服务器获取群组联系人
//                if (!isNetActive()) {
//                    return;
//                }
//                //如果map中没有此id，说明本地登陆没有请求过群组成员被数据
//                if (SystemInfoService.CHATMAP.containsKey(chatUserId)) {
//                    return;
//                }
//                try {
//                    GetHttpUtil.getGroupUsers(JFChatActivity525备份.this, group.getGroupId(),
//                            new GetHttpUtil.ReqCallBack<List<User>>() {
//                                @Override
//                                public void onReqSuccess(List<User> result) {
//                                    onGetGroupUserSuccess(result, group.getGroupId());
//                                }
//
//                                @Override
//                                public void onReqFailed(String errorMsg) {
//
//                                }
//                            });
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }
//
//    /**
//     * 返回
//     *
//     * @param view
//     */
//    public void back(BaseView view) {
//        onBackPressed();
//    }
//
//
//    public void sendMessage(IMStruct002 struct002) {
//        try {
//            if (struct002 != null) {
//                struct002.setToUserId(chatUserId);
//                struct002.setFromUserId(Integer.parseInt(EnvironmentVariable.getProperty(Constants.CHAT_USER_ID)));
//                struct002.setToUserType(chatType);
//                boolean b = messageManager.sendMessage(struct002);
//                chatAdapter.notifyDataSetChanged();
//                chatListView.setSelection(chatListView.getCount() - 1);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//
//    @Override
//    protected void onNewIntent(Intent intent) {
//        super.onNewIntent(intent);
//        initChatObjectInfo(intent);
//        initAttr();
//        setUpView();
//        getChatConfig();
//        //initUserChatPermission();
//    }
//
//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            if (mChatInputView.getMenuState() == BaseView.VISIBLE) {
//                mChatInputView.onBackPressed();
//                return true;
//            }
//        }
//        return super.onKeyDown(keyCode, event);
//    }
//
//    //处理语音播放显示选择播放方式的弹框
//    private String voicePlayType = EnvironmentVariable.getProperty("voicePlayType", "0");
//
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onVoicePlaying(VoicePlayingEvent event) {
//        //语音正在播放时的事件
//        if (event.isPlaying()) {
//            llVoicePlayTypeLayout.setVisibility(BaseView.VISIBLE);
//            llVoicePlayTypeLayout.setOnClickListener(new OnClickListener() {
//                @Override
//                public void onClick(BaseView v) {
//                    if ("0".equals(voicePlayType)) {
//                        voicePlayType = "1";
//                        tvVoicePlayTypeView.setText("免提");
//                        ivVoicePlayView.setImageDrawable(getResources().getDrawable(R.drawable.qqstyle_voice_play_type_mainti));
//                    } else {
//                        voicePlayType = "0";
//                        tvVoicePlayTypeView.setText("听筒");
//                        ivVoicePlayView.setImageDrawable(getResources().getDrawable(R.drawable.qqstyle_voice_play_type_tingtong));
//
//                    }
//                    EnvironmentVariable.setProperty("voicePlayType", voicePlayType);
//                    ChatVoicePlayClickListener.setPlayType(Integer.valueOf(voicePlayType));
//                }
//            });
//        } else {
//            llVoicePlayTypeLayout.setVisibility(BaseView.GONE);
//        }
//    }
//
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    //播放帧动画的时间
//    public void startAnim(AnimationEvent event) {
//        final AnimationDrawable drawable = event.getAnimationDrawable();
//        if (drawable == null || animFrameLayout.getVisibility() == BaseView.VISIBLE) {
//            //如果animFrameLayout 显示，说明正在播放动画，不操作
//            return;
//        }
//        animFrameLayout.setVisibility(BaseView.VISIBLE);
//        //animFrameLayout.setBackground(drawable);
//        mChatInputView.hideKeyBoardAndMenuContainer();
//        final ImageView imageView = animFrameLayout.findViewById(R.id.ivAnimView);
//        imageView.setBackground(drawable);
//        drawable.start();
//        imageView.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if (drawable.isRunning()) {
//                    drawable.stop();
//                }
//                animFrameLayout.setVisibility(BaseView.GONE);
//                imageView.setBackground(null);
//            }
//        }, event.getDurationTime());
//
//
//    }
//
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    //点击加密消息事件
//    public void secretMessageOnclick(final IMStruct002 imStruct002) {
//            showSecretInputView(imStruct002);
//    }
//
//    private void showSecretInputView(IMStruct002 imStruct002) {
//        mChatInputView.hideKeyBoardAndMenuContainer();
//        //点击密图密信 item
//        secretInputView.setImstruct(imStruct002);
//        secretInputView.setSecrectInputViewListener(new SecretInputView.SecrectInputViewListener() {
//            @Override
//            public void secretInputViewClose() {
//                mChatInputView.setVisibility(BaseView.VISIBLE);
//            }
//        });
//    }
//
//    public void onGetGroupUserSuccess(final List<User> groupUsers, int groupId) {
//        Log.i(TAG, "--请求群组联系人成功--");
//        SystemInfoService.CHATMAP.put(chatUserId, true);
//        Disposable disposable = Observable.create(new ObservableOnSubscribe<Group>() {
//
//            @Override
//            public void subscribe(ObservableEmitter<Group> emitter) throws Exception {
//                Log.e(TAG, "Observable thread is : " + Thread.currentThread().getName());
//                group = weChatDBManager.getGroupWithUsers(chatUserId);
//                emitter.onNext(group);
//                emitter.onComplete();
//            }
//        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Group>() {
//            @Override
//            public void accept(final Group group) throws Exception {
//                Log.e(TAG, "After observeOn(io)，Current thread is " + Thread.currentThread().getName());
//
//                if (group.getUsers().size() < groupUsers.size()) {
//                    group.setUsers(groupUsers);
//                }
//                tvGroupUserCount.setVisibility(BaseView.VISIBLE);
//                tvGroupUserCount.setText("(" + group.getUsers().size() + ")");
//                toChatUserNickTextView.setText(group.getGroupName());
//                final String avatar = group.getAvatar();
//                //如果群头像是网络图片就不自动生成了
//                if (GroupAvatarUtil.isServerAvatar(avatar)) {
//                    return;
//                }
//                File file = new File(group.getAvatar());
//
//                if (avatar.equals("") || !file.exists()) {
//                    List<String> avatars = new ArrayList<>();
//                    for (User user : groupUsers) {
//                        if (user.getId() == group.getCreateId()) {
//                            avatars.add(0, user.getAvatar());
//                        } else {
//                            avatars.add(user.getAvatar());
//                        }
//                    }
//                    final GroupAvatarHelper helper = new GroupAvatarHelper(JFChatActivity525备份.this);
//                    helper.getAvatar(group, avatars, new GroupAvatarHelper.LoadImageListener() {
//                        @Override
//                        public void loadImageComplete(NineGridImageView view) {
//                            if (view == null) {
//                                return;
//                            }
//                            String path = helper.getAvatarPath(view);
//                            group.setAvatar(path);
//                            weChatDBManager.insertOrUpdateGroup(group);
//                        }
//                    });
//                }
//            }
//        });
//
//
//    }
//
//    @Override
//    public void netStateChange(int net_state) {
//        Log.i(TAG, "netStateChange--监听网络状态--" + net_state);
//        if (net_state == 0) {
//            this.netStateChangeHandler.sendEmptyMessage(0);
//        } else {
//            this.netStateChangeHandler.sendEmptyMessage(1);
//
//        }
//    }
//
//    //处理网络监听状态handle
//    Handler netStateChangeHandler = new Handler() {
//        public void handleMessage(Message msg) {
//            switch (msg.what) {
//                case 0:
//                    if (chatType == StructFactory.TO_USER_TYPE_PERSONAL)
//                        toChatUserNickTextView.setText(user.getReMark() + "(网络不可用)");
//                    else {
//                        tvGroupUserCount.setVisibility(BaseView.VISIBLE);
//                        tvGroupUserCount.setText("(" + group.getUsers().size() + ")" + "(网络不可用)");
//                        toChatUserNickTextView.setText(group.getGroupName());
//                    }
//                    break;
//                case 1:
//                    if (chatType == StructFactory.TO_USER_TYPE_PERSONAL)
//                        toChatUserNickTextView.setText(user.getReMark());
//                    else {
//                        tvGroupUserCount.setVisibility(BaseView.VISIBLE);
//                        tvGroupUserCount.setText("(" + group.getUsers().size() + ")");
//                        toChatUserNickTextView.setText(group.getGroupName());
//                    }
//
//                    break;
//                default:
//                    break;
//            }
//            super.handleMessage(msg);
//        }
//    };
//
//    //监听tcp是否可用事件
//    @Override
//    public void onLoginSuccess() {
//        this.netStateChangeHandler.sendEmptyMessage(1);
//    }
//
//    @Override
//    public void onLoginFail(String errorMsg) {
//
//    }
//
//    private class ChatListViewOnTouchListener implements BaseView.OnTouchListener {
//
//        private long chatListViewTouchMoveTime;
//        private int chatListViewDownY;
//        private int chatListViewDownYSlop;
//
//        public ChatListViewOnTouchListener() {
//            chatListViewDownYSlop = (int) (5 * getResources().getDisplayMetrics().density);
//        }
//
//        @Override
//        public boolean onTouch(BaseView v, MotionEvent event) {
//            switch (event.getAction()) {
//                case MotionEvent.ACTION_DOWN:
//
//                    chatListViewDownY = (int) event.getY();
//                    mChatInputView.hideKeyBoardAndMenuContainer();
//                    break;
////                case MotionEvent.ACTION_UP:
////                    v.performClick();
////                    break;
////                case MotionEvent.ACTION_MOVE:
////                    //触发下拉事件，每0.5秒执行一次
////                    if (event.getY() - chatListViewDownY > chatListViewDownYSlop && System.currentTimeMillis() - chatListViewTouchMoveTime > 500) {
////                        Log.e("==", "=====每0.5秒执行一次：" + event.getAction());
////                        tryLoadHistoryMessage();
////                        chatListViewTouchMoveTime = System.currentTimeMillis();
////                    }
////                    break;
//
//                default:
//                    break;
//            }
//            return false;
//        }
//    }
//
//    private void tryLoadHistoryMessage() {
//        BaseView topView = chatListView.getChildAt(0);
//        if (chatListView.getFirstVisiblePosition() == 0 && topView != null && topView.getTop() == 0
//                && loadmorePB.getVisibility() != BaseView.VISIBLE) {
//            Log.e("==", "加载更多--onScroll==firstVisibleItem：" + topView.getTop());
//            //1.先显示ProgressBar
//            //  loadmorePB.setVisibility(BaseView.VISIBLE);
//            Disposable disposable = Observable.create(new ObservableOnSubscribe<List<IMStruct002>>() {
//                @Override
//                public void subscribe(ObservableEmitter<List<IMStruct002>> emitter) throws Exception {
//                    List<IMStruct002> historyImStruct002s = chatListManager.getHistoryMessage(chatType, user,
//                            messageManager, group, messageList.get(0).getMessageID(), 20, null);
//                    chatListManager.solveUnRead(messageList, historyImStruct002s);
//                    emitter.onNext(historyImStruct002s);
//                    emitter.onComplete();
//                }
//            }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(new Consumer<List<IMStruct002>>() {
//                        @Override
//                        public void accept(List<IMStruct002> imStruct002s) throws Exception {
//                            chatAdapter.notifyDataSetChanged();
//                            int historyMessageCount = imStruct002s.size();
//                            chatListView.setSelection(historyMessageCount - 1);
//                            //3.隐藏显示ProgressBar
//                            loadmorePB.setVisibility(BaseView.GONE);
//                            smartRefreshLayout.finishRefresh();
//                        }
//                    });
//
//
//        } else {
//            loadmorePB.setVisibility(BaseView.GONE);
//            smartRefreshLayout.finishRefresh();
//        }
//    }
//
//    //todo 各种回调 ，点击按钮 录音，拍照
//
//    //预发送回调
//    ChatSenderFragment.PreSendMessageCallback preSendMessageCallback = new ChatSenderFragment.PreSendMessageCallback() {
//        @Override
//        public void preSendMessage(IMStruct002 struct002) {
//            if (struct002 != null) {
//                struct002.setToUserId(chatUserId);
//                struct002.setFromUserId(Integer.parseInt(EnvironmentVariable.getProperty(Constants.CHAT_USER_ID)));
//                struct002.setToUserType(chatType);
//                struct002.putExtra("progress", 0);
//                messageManager.preSendMessage(struct002);
//                chatAdapter.notifyDataSetChanged();
//                chatListView.setSelection(chatListView.getCount() - 1);
//            }
//        }
//
//        @Override
//        public void updateProgress(IMStruct002 struct002, double percent) {
//            chatAdapter.notifyDataSetChanged();
//        }
//
//        @Override
//        public void sendPreMessage(final IMStruct002 struct002) {
//            if (struct002 != null) {
//                messageManager.sendPreMessage(struct002);
//                Log.i(TAG, "发送 ------ sendPreMessage:" + struct002.toString());
//            }
//        }
//    };
//
//
//    @Override
//    public void recordFinish(int time, File file) {
//        //录音完成
//        chatMessageSendManager.sendVoiceMesage(time, file);
//
//    }
//
//    @Override
//    public boolean onSendTextMessage(CharSequence input) {
//        if (input.length() == 0) {
//            return false;
//        }
//        IMStruct002 struct002 = StructFactory.getInstance().createTextStruct(input.toString(), chatType);
//        sendMessage(struct002);
//        return true;
//    }
//
//    @Override
//    public void onSendFiles(List<String> mSelectedPics, boolean isRawPic) {
//        //发送按钮发送图片
//        chatMessageSendManager.sendPicture(mSelectedPics, isRawPic);
//    }
//
//    @Override
//    public boolean switchToMicrophoneMode() {
//        return true;
//    }
//
//    @Override
//    public boolean switchToGalleryMode() {
//        return true;
//    }
//
//    @Override
//    public boolean switchToCameraMode() {
//        return true;
//    }
//
//    @Override
//    public boolean switchToEmojiMode() {
//        return true;
//    }
//
//    @Override
//    public void onSendShakeAnim(String name) {
//        //抖动动画
//        IMStruct002 imStruct002 = StructFactory.getInstance().createShakeImstruct002(name);
//        if (imStruct002 != null) {
//            //播放动画
//            AnimationFactory.startAnimFromIMStruct(JFChatActivity525备份.this, imStruct002);
//            sendMessage(imStruct002);
//        }
//    }
//
//    @Override
//    public void clickMoreMenuItem(ChatMenuModel model) {
//        //更多中的菜单被点击
//        interfaceSkipManager.setChatType(chatType);
//        interfaceSkipManager.setCurrentChatUserId(chatUserId);
//        interfaceSkipManager.onChatMenuClick(model);
//    }
//
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == RESULT_OK && requestCode == ChatInputView.REQUEST_CODE_CAMERA) {
//            // 相机拍摄的照片
//            File cameraFile = mChatInputView.getTakePictureFile();
//            chatMessageSendManager.sendPicture(cameraFile);
//        } else {
//            chatMessageSendManager.onActivityResult(requestCode, resultCode, data);
//        }
//    }
//
//    private static class MessageHandler extends Handler {
//        private WeakReference<JFChatActivity525备份> weakReference;
//
//        public MessageHandler(JFChatActivity525备份 activity) {
//            weakReference = new WeakReference<JFChatActivity525备份>(activity);
//        }
//
//        @Override
//        public synchronized void handleMessage(Message msg) {
//            super.handleMessage(msg);
//            if (weakReference == null) {
//                return;
//            }
//            JFChatActivity525备份 activity = weakReference.get();
//            switch (msg.what) {
//                case 1:
//                    if (activity != null && activity.chatAdapter != null) {
//                        // activity.chatListView.setVisibility(BaseView.GONE);
//                        activity.chatListView.requestLayout();
//                        activity.chatAdapter.notifyDataSetChanged();
//                        //activity.chatListView.setVisibility(BaseView.VISIBLE);
//                        if (activity.chatListView.getLastVisiblePosition() == activity.chatListView.getCount() - 2) {
//                            activity.chatListView.setSelection(activity.chatListView.getCount() - 1);
//                        } else {
//                            Log.i(TAG, "不滚动到最后一条-------");
//                        }
//                    }
//                    break;
//                case 2:
//                    activity.chatAdapter.notifyDataSetChanged();
//                    activity.chatListView.setSelection(activity.chatListView.getCount() - 1);
//                    break;
//                default:
//                    break;
//            }
//        }
//    }
//}
