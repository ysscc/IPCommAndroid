package com.efounder.chat.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.model.Group;
import com.efounder.chat.utils.GroupAvatarUtil;
import com.efounder.chat.utils.LXGlideImageLoader;
import com.groupimageview.NineGridImageView;

import java.util.ArrayList;
import java.util.List;

@SuppressLint({"SdCardPath", "InflateParams"})
public class ChatRoomAdapter extends BaseAdapter {

    Context context;
    List<Group> grouplist;
    private LayoutInflater inflater;
//    private ImageLoader imageLoader;
//    private DisplayImageOptions options;
//    private DisplayImageOptions options1;


    public ChatRoomAdapter(Context context, List<Group> grouplist) {
        //initImageLoader();
        this.context = context;
        this.grouplist = grouplist;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return grouplist.size();
    }

    @Override
    public Group getItem(int position) {
        return grouplist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        final Group group = grouplist.get(position);
        String groupName = group.getGroupName();
        final int groupId = group.getGroupId();
        int membersNum = group.getUsers().size();
        List<String> avatarList = new ArrayList<>();
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = creatConvertView(membersNum, group.getAvatar());
            holder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);
            holder.tv_name.setText(groupName);
            holder.iv_avatar1 = (ImageView) convertView
                    .findViewById(R.id.iv_avatar1);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();

        }

        holder.tv_name.setText(groupName);
        //2017.3.7 yqs屏蔽
  /*      if (!group.getAvatar().equals("")) {
            String avatar = "file://" + group.getAvatar();
            File file = new File(group.getAvatar());
            if (!file.exists()) {
                Log.i("chatroomadapter", "头像找不到，加载默认头像");
                avatar = "";
            }
            imageLoader.displayImage(avatar, holder.iv_avatar1, options1);
        } else {
            imageLoader.displayImage("", holder.iv_avatar1, options1);
        }*/
        String avatar = GroupAvatarUtil.getAvatar(group.getAvatar());
        try {
//            imageLoader.displayImage(URLModifyDynamic.getInstance().replace(avatar), holder.iv_avatar1, options1);
            LXGlideImageLoader.getInstance().showRoundGrouAvatar(context,holder.iv_avatar1,avatar,LXGlideImageLoader.DEFATLT_AVATAR_RADIUS_10);
        }catch (Exception e) {
            e.printStackTrace();
            LXGlideImageLoader.getInstance().showRoundGrouAvatar(context,holder.iv_avatar1,R.drawable.default_useravatar,LXGlideImageLoader.DEFATLT_AVATAR_RADIUS_10);
        }

        final String groupName_tem = groupName;

        // 为了item变色在此处写监听
        RelativeLayout re_item = (RelativeLayout) convertView
                .findViewById(R.id.re_item);

        return convertView;
    }

    private static class ViewHolder {

        TextView tv_name;
        NineGridImageView iv_avatar9;
        ImageView iv_avatar1;

    }

    private void showUserAvatar(ImageView iamgeView, String avatar) {

//        imageLoader.displayImage(avatar, iamgeView, options);

    }

    private View creatConvertView(int size, String avatar) {
        View convertView;

        //if (!avatar.equals("") || size==0){
        convertView = inflater.inflate(R.layout.item_chatroom_1, null,
                false);
//        }else{
//        convertView = inflater.inflate(R.layout.item_chatroom_9, null,
//                false);
//        }
        return convertView;
    }

    /**
     * 异步加载头像
     */
//    public void initImageLoader() {
//        // 初始化异步加载图片的类
//        imageLoader = ImageLoader.getInstance();
//        // 设置异步加载图片的配置信息
//        options = ImageUtil.getImageLoaderOptions(R.drawable.default_useravatar);
//        options1 = ImageUtil.getImageLoaderOptions(R.drawable.default_groupavatar);
//
//    }

    public static Bitmap convertViewToBitmap(View view, int bitmapWidth, int bitmapHeight) {
        Bitmap bitmap = Bitmap.createBitmap(bitmapWidth, bitmapHeight, Bitmap.Config.ARGB_8888);
        view.draw(new Canvas(bitmap));

        return bitmap;
    }

}
