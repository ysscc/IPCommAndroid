package com.efounder.chat.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import com.efounder.chat.R;
import com.efounder.utils.CommonUtils;

/**
 * 
 * @author lch 5-25 发送消息体
 */
public class MessageOutgoingView extends MessageBaseView {

	public MessageOutgoingView(Context context) {
		super(context);
		initView(context);

	}

	public MessageOutgoingView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView(context);

	}

	@SuppressLint("ResourceAsColor")
	private void initView(Context context) {

		// 头像区域
		RelativeLayout.LayoutParams chat_item_avatar_area_layoutparams = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		chat_item_avatar_area_layoutparams
				.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);// 与父容器的左侧对齐
		chat_item_avatar_area_layoutparams
				.addRule(RelativeLayout.ALIGN_PARENT_TOP);// 与父容器的上侧对齐
		// chat_item_avatar_area.setLayoutParams(chat_item_avatar_area_layoutparams);
		chat_item_avatar_area.setId(R.id.chat_item_avatar_area);
		middleView.addView(chat_item_avatar_area,
				chat_item_avatar_area_layoutparams);

		// 消息体区域
		RelativeLayout.LayoutParams chat_item_layout_content_layoutparams = new RelativeLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
		chat_item_layout_content_layoutparams.addRule(RelativeLayout.LEFT_OF,
				chat_item_avatar_area.getId());
		chat_item_layout_content_layoutparams
				.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		chat_item_layout_content_layoutparams.rightMargin = 10;
		chat_item_layout_content_layoutparams.topMargin = 2;
		chat_item_layout_content.setMinimumHeight(CommonUtils.dip2px(context,
				40));
		middleView.addView(chat_item_layout_content,
				chat_item_layout_content_layoutparams);
		chat_item_layout_content
				//.setBackgroundResource(R.drawable.chat_to_bg_normal);
				.setBackgroundResource(R.drawable.chatting_setmode_chattobg);

//		chat_item_layout_content.setPadding(10, 10, 18, 10);
		//chat_item_layout_content.setPadding(2, 1, 16, 1);
		/*
		 * ImageView content = new ImageView(context); LinearLayout.LayoutParams
		 * lp = new LayoutParams(200,11400);
		 * content.setBackgroundColor(R.color.black_deep);
		 */
		// chat_item_layout_content.setBackgroundColor(R.color.black_deep);
		chat_item_layout_content.setId(R.id.chat_item_layout_content);
		// chat_item_layout_content.addView(content,lp);
		
		
		//消息体下方的补充区域
		RelativeLayout.LayoutParams chat_item_layout_contentbottom_supplement_layoutparams = new RelativeLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
		chat_item_layout_contentbottom_supplement_layoutparams.addRule(RelativeLayout.BELOW,chat_item_layout_content.getId());
		chat_item_layout_contentbottom_supplement_layoutparams.addRule(RelativeLayout.ALIGN_RIGHT,chat_item_layout_content.getId());
//		chat_item_layout_contentbottom_supplement_layoutparams.rightMargin = 10;
		chat_item_layout_contentbottom_supplement_layoutparams.topMargin = 2;
		//chat_item_layout_contentbottom_supplement.setBackgroundColor(R.color.black);
		chat_item_layout_contentbottom_supplement.setVisibility(View.GONE);
		middleView.addView(chat_item_layout_contentbottom_supplement,
				chat_item_layout_contentbottom_supplement_layoutparams);
		//chat_item_layout_contentbottom_supplement.setVisibility(View.GONE);

		// signview区域
		RelativeLayout.LayoutParams signView_layoutparams = new RelativeLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		signView_layoutparams.addRule(RelativeLayout.LEFT_OF,
				chat_item_layout_content.getId());
		signView_layoutparams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		signView_layoutparams.rightMargin = 10;
		// mprogressbar.setVisibility(View.VISIBLE);

		middleView.addView(signView, signView_layoutparams);

		// 消息体下部补充区域
		/*RelativeLayout.LayoutParams bottom_layoutparams = new RelativeLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		bottom_layoutparams.addRule(RelativeLayout.ALIGN_LEFT,
				chat_item_layout_content.getId());
		bottom_layoutparams.addRule(RelativeLayout.BELOW,
				chat_item_layout_content.getId());
		
		middleView.addView(signView, bottom_layoutparams);*/

		
		
//		middleView.setPadding(CommonUtils.dip2px(context, 42), 0, 0, 0);
	}

	public void setImagePadding() {
//		chat_item_layout_content.setPadding(3, 3, 16, 3);
	}
}
