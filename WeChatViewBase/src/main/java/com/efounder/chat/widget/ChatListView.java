package com.efounder.chat.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * 聊天ListView
 * @author hudq
 *
 */
public class ChatListView extends ListView{

	public ChatListView(Context context) {
		super(context);
	}


	public ChatListView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
	}

	public ChatListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}
	
	

}
