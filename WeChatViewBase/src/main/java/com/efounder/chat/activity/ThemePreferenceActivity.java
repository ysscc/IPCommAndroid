package com.efounder.chat.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.frame.bean.MyThemeBean;
import com.efounder.frame.manager.ThemeManager;
import com.efounder.recycleviewhelper.CommonAdapter;
import com.efounder.recycleviewhelper.base.ViewHolder;
import com.efounder.util.ToastUtil;
import com.utilcode.util.ReflectUtils;

import java.util.List;

import skin.support.SkinCompatManager;

/**
 * @author : yqs
 * @date : 2019/01/29 13:55
 * @desc : 主题切换Activity
 * @version: 1.0
 */
public class ThemePreferenceActivity extends BaseActivity implements View.OnClickListener {

    private RecyclerView recycler;
    private ThemeListAdapter themeListAdapter;
    private List<MyThemeBean> myThemeBeans;

    public static void start(Context context) {
        Intent starter = new Intent(context, ThemePreferenceActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theme_toggle);
        iniData();
        initView();


    }

    private void iniData() {
        myThemeBeans = ThemeManager.getInstance(this.getApplication()).getThemeList();
    }

    private void initView() {
        TextView tvTitle = (TextView) findViewById(R.id.tv_title);
        tvTitle.setText(R.string.wechatview_theme);
        TextView tvSave = (TextView) findViewById(R.id.tv_save);
        tvSave.setVisibility(View.VISIBLE);
        tvSave.setOnClickListener(this);
        recycler = (RecyclerView) findViewById(R.id.recycler);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        themeListAdapter = new ThemeListAdapter(this, R.layout.item_multi_language, myThemeBeans);
        recycler.setAdapter(themeListAdapter);
    }

    @Override
    public void onClick(View v) {
        //保存
        if (v.getId() == R.id.tv_save) {
            for (int i = 0; i < myThemeBeans.size(); i++) {
                if (myThemeBeans.get(i).isSelected()) {
                    final int finalI = i;
                    ThemeManager.getInstance(ThemePreferenceActivity.this.getApplication()).changeSkin(myThemeBeans.get(i), new SkinCompatManager.SkinLoaderListener() {
                        @Override
                        public void onStart() {
                            showLoading(R.string.common_text_please_wait);
                        }

                        @Override
                        public void onSuccess() {
                            dismissLoading();
                            //切换成功后记得保存一下
                            ThemeManager.getInstance(ThemePreferenceActivity.this.getApplication()).setCurrentSkin(myThemeBeans.get(finalI));
                            //跳转首页
                            try {
                                String className = getResources().getString(R.string.from_group_backto_first);
                                ReflectUtils reflectUtils = ReflectUtils.reflect(className);
                                reflectUtils.method("start", ThemePreferenceActivity.this);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailed(String errMsg) {
                            dismissLoading();
                            ToastUtil.showToast(getApplicationContext(), R.string.common_text_failed_please_retry);
                        }
                    });
                    return;
                }
            }
        }
    }


    private class ThemeListAdapter extends CommonAdapter<MyThemeBean> {
        public ThemeListAdapter(Context context, int layoutId, List<MyThemeBean> datas) {
            super(context, layoutId, datas);
        }

        @Override
        protected void convert(ViewHolder holder, MyThemeBean bean, final int position) {
            TextView tvTitle;
            RadioButton rbLanguageSelect;

            tvTitle = holder.getView(R.id.tv_language_name);
            rbLanguageSelect = holder.getView(R.id.rb_language_select);

            tvTitle.setText(bean.getThemeTitle());
            rbLanguageSelect.setChecked(bean.isSelected());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (int i = 0; i < myThemeBeans.size(); i++) {
                        MyThemeBean myThemeBean = myThemeBeans.get(i);
                        if (position == i) {
                            myThemeBean.setSelected(true);
                        } else {
                            myThemeBean.setSelected(false);
                        }
                    }
                    notifyDataSetChanged();
                }
            });
        }
    }
}
