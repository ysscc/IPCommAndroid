package com.efounder.chat.item;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.core.xml.StubObject;
import com.efounder.builder.base.data.EFRowSet;
import com.efounder.builder.base.json.JSONUtil;
import com.efounder.chat.R;
import com.efounder.chat.item.manager.IMessageItem;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.frame.ViewSize;
import com.efounder.frame.activity.EFTransformFragmentActivity;
import com.efounder.frame.baseui.BaseFragment;
import com.efounder.frame.utils.EFAppAccountUtils;
import com.efounder.frame.utils.EFFrameUtils;
import com.efounder.frame.xmlparse.EFXmlConstants;
import com.efounder.imageloader.GlideImageLoader;
import com.efounder.message.struct.IMStruct002;
import com.efounder.ui.util.DisplayUtil;

import org.json.JSONObject;

import static com.efounder.frame.utils.Constants.CHAT_USER_ID;
import static com.efounder.ui.util.DisplayUtil.getMobileWidth;

/**
 * 应用跳转
 * @author hudq
 *	json模板示例
 * {
	"contentViewID":"form1",
	"formName":"需求计划单",
	"formID":"xqjh_show",
	"localFormID":"xqjh_show",
	"formInfo":"单号：XQJH468170109094531130",
	"FormModel":{}
	}
 *
 */
public class FormMessageItem extends LinearLayout  implements IMessageItem {
	private Context mContext;
	private TextView formNameTV;
	private TextView formInfoTV;
	private TextView formNameSubTV;
	private TextView  formStateView;
	private ImageView formImageView;
//	private ImageLoader imgLoader;

	private JSONObject jsonObject;

	private IMStruct002 message;

	public FormMessageItem(Context context) {
		super(context);
		mContext = context;
//		imgLoader = ImageLoader.getInstance();
		LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflate.inflate(R.layout.chat_item_form, this);//注意第二个参数
		formNameTV = (TextView) findViewById(R.id.form_name);
		formInfoTV = (TextView) findViewById(R.id.form_info);
		formInfoTV.setMaxWidth(getMobileWidth(context)*2/3);

		formNameSubTV = (TextView) findViewById(R.id.form_name_sub);
		formImageView = (ImageView) findViewById(R.id.iv_formImage);
		formStateView = (TextView) findViewById(R.id.tv_enter);
		//param
		int width = DisplayUtil.getMobileWidth(context)*2/3-40;
		//LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.MATCH_PARENT);
		LayoutParams lp = new LayoutParams(width,LayoutParams.MATCH_PARENT);
		this.setLayoutParams(lp);
		this.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
                //Intent intent = new Intent((Activity) mContext,EFTransformFragmentActivity.class);
				String json = null;
				try {
					json = new String(message.getBody(),"UTF-8");
					String formModelJson = new JSONObject(json).getString("FormModel");
					EFRowSet rowSet = JSONUtil.JSON2RowSet(formModelJson);
					Log.i("","---");
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (jsonObject == null) return;
				try {
					StubObject stubObject = new StubObject();
					String formName="";
					if( EnvironmentVariable.getProperty(CHAT_USER_ID).equals(message.getToUserId()+"")) {
						formName = jsonObject.getString("formID") + ".xml";
					}else {
						formName = jsonObject.getString("localFormID") + ".xml";
					}
					stubObject.setString(EFXmlConstants.ATTR_FORM,formName);
					stubObject.setString("contentViewID",jsonObject.getString("contentViewID"));
					//消息id
					stubObject.setString("messageID",message.getMessageID());

					Bundle bundle = new Bundle();
					bundle.putSerializable("stubObject", stubObject);
					bundle.putString(EFTransformFragmentActivity.EXTRA_TITLE_NAME,jsonObject.getString("formName"));
					bundle.putInt(EFTransformFragmentActivity.EXTRA_TITLE_RIGHT_VISIBILITY,View.INVISIBLE);
					bundle.putInt("id", EFAppAccountUtils.getAppAccountID());
					EFFrameUtils.pushFragment((Class<? extends BaseFragment>) Class.forName("com.efounder.chat.fragment.EFAppAccountFormFragment"),bundle);
				} catch (Exception e) {
					e.printStackTrace();
				}


				//mContext.startActivity(intent);
			}
		});
	}

	@Override
	public View messageView() {
		return this;
	}

	@Override
	public ViewSize messageViewSize() {
		return null;
	}

	@Override
	public void setIMStruct002(IMStruct002 message) {
		this.message = message;
		try {
			String json = new String(message.getBody(),"UTF-8");
//			{
//				"contentViewID" : "form1",
//					"FormModel" : {
//
//			},
//				"formName" : "发货确认单",
//					"formInfo" : "已发货物：72#汽油1吨。。。。",
//					"formID" : "fhqr"
//			}

			jsonObject = new JSONObject(json);
			formNameTV.setText(jsonObject.getString("formName"));
			if(jsonObject.has("formInfo")){
				formInfoTV.setText(jsonObject.getString("formInfo"));
			}else{
				formInfoTV.setText("点击查看"+jsonObject.getString("formName"));
			}
			if(jsonObject.has("formNameSub")){
				formNameSubTV.setText(jsonObject.getString("formNameSub"));
			}else{
				formNameSubTV.setText(jsonObject.getString("formName"));
			}
			if(jsonObject.has("formImage")){
				String imageString = jsonObject.getString("formImage");
				if (imageString.equals("")){
				formImageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.message_item_form));
				}else{
					GlideImageLoader.getInstance().displayImage(mContext,formImageView,imageString);
//					imgLoader.displayImage(imageString,formImageView);
				}

			}else{
				formImageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.message_item_form));
			}
			if(jsonObject.has("formState")){
				String formState = jsonObject.getString("formState");
				if (!formState.equals("")){
					formStateView.setVisibility(VISIBLE);
					formStateView.setText(formState);
				}else {
					formStateView.setVisibility(GONE);
				}
			}else {
				formStateView.setVisibility(GONE);
			}



		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean getIsInUse() {
		return this.isShown();
	}

	@Override
	public void setIsInUse(boolean isInUse) {

		
	}

	@Override
	public void prepareForReuse() {
		
	}
}
