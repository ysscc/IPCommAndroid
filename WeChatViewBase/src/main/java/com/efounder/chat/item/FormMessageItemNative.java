package com.efounder.chat.item;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.item.manager.IMessageItem;
import com.efounder.frame.ViewSize;
import com.efounder.imageloader.GlideImageLoader;
import com.efounder.message.struct.IMStruct002;
import com.efounder.ui.util.DisplayUtil;

import org.json.JSONException;
import org.json.JSONObject;

import static com.efounder.ui.util.DisplayUtil.getMobileWidth;

/**
 * 应用跳转
 * @author hudq
 *	json模板示例
 * {
	"contentViewID":"form1",
	"formName":"需求计划单",
	"formID":"xqjh_show",
	"localFormID":"xqjh_show",
	"formInfo":"单号：XQJH468170109094531130",
	"FormModel":{}
	}
 *
 */
public class FormMessageItemNative extends LinearLayout  implements IMessageItem {
	private Context mContext;
	private TextView formNameTV;
	private TextView formInfoTV;
	private TextView formNameSubTV;
	private TextView  formStateView;
	private ImageView formImageView;
//	private ImageLoader imgLoader;

	private JSONObject jsonObject;

	private IMStruct002 message;

	public FormMessageItemNative(Context context) {
		super(context);
		mContext = context;
//		imgLoader = ImageLoader.getInstance();
		LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflate.inflate(R.layout.chat_item_form_native, this);//注意第二个参数
		formNameTV = (TextView) findViewById(R.id.form_name);
		formInfoTV = (TextView) findViewById(R.id.form_info);
		formInfoTV.setMaxWidth(getMobileWidth(context)*2/3);

		formNameSubTV = (TextView) findViewById(R.id.form_name_sub);
		formImageView = (ImageView) findViewById(R.id.iv_formImage);
		formStateView = (TextView) findViewById(R.id.tv_enter);
		//param
		int width = DisplayUtil.getMobileWidth(context)*2/3-40;
		//LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.MATCH_PARENT);
		LayoutParams lp = new LayoutParams(width,LayoutParams.MATCH_PARENT);
		this.setLayoutParams(lp);
		this.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String json = message.getMessage();
				Log.i("lcusky","---lcusky:"+json);
				try {
					JSONObject jsonObject = new JSONObject(json);
					String className = jsonObject.getString("localFormID");
					Intent intent = new Intent(getContext(), Class.forName(className));//EFAppAccountDJZReimburseActivity  EFAppAccountDJZAffixAct
					intent.putExtra("message",message);
					intent.putExtra("position",(int)getTag());
					getContext().startActivity(intent);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	public View messageView() {
		return this;
	}

	@Override
	public ViewSize messageViewSize() {
		return null;
	}

	@Override
	public void setIMStruct002(IMStruct002 message) {
		this.message = message;
		try {
			String json = new String(message.getBody(),"UTF-8");
//			{
//				"contentViewID" : "form1",
//					"FormModel" : {
//
//			},
//				"formName" : "发货确认单",
//					"formInfo" : "已发货物：72#汽油1吨。。。。",
//					"formID" : "fhqr"
//			}

			jsonObject = new JSONObject(json);
			formNameTV.setText(jsonObject.getString("formName"));
			if(jsonObject.has("formInfo")){
				formInfoTV.setText(jsonObject.getString("formInfo"));
			}else{
				formInfoTV.setText("点击查看"+jsonObject.getString("formName"));
			}
			if(jsonObject.has("formNameSub")){
				formNameSubTV.setText(jsonObject.getString("formNameSub"));
			}else{
				formNameSubTV.setText(jsonObject.getString("formName"));
			}
			if(jsonObject.has("formImage")){
				String imageString = jsonObject.getString("formImage");
				if (imageString.equals("")){
				formImageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.message_item_form));
				}else{
					GlideImageLoader.getInstance().displayImage(mContext,formImageView,imageString);
//					imgLoader.displayImage(imageString,formImageView);
				}

			}else{
				formImageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.message_item_form));
			}

			setupFormStateView(jsonObject);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setupFormStateView(JSONObject body) {
		try {
			//1.设置状态文字，是否可见
			if(body.has("formState")){
                String formState = body.getString("formState");
                if (!formState.equals("")){
                    formStateView.setVisibility(VISIBLE);
                    formStateView.setText(formState);
                }else {
                    formStateView.setVisibility(GONE);
                }
            }else {
                formStateView.setVisibility(GONE);
            }
			//1.根据 stateLevel 设置显示颜色：1,3,5 绿黄红
			if (body.has("stateLevel")){
				String stateLevel = body.getString("stateLevel");
				if ("1".equals(stateLevel)){//绿色
					formStateView.setBackgroundResource(R.drawable.form_textviewboard_green);
					formStateView.setTextColor(getResources().getColor(R.color.ef_green_dark));
				}else if ("2".equals(stateLevel)) {//蓝色
					formStateView.setBackgroundResource(R.drawable.form_textviewboard_blue);
					formStateView.setTextColor(getResources().getColor(R.color.ef_blue));
				}else if ("3".equals(stateLevel)) {//黄色
					formStateView.setBackgroundResource(R.drawable.form_textviewboard_yellow);
					formStateView.setTextColor(getResources().getColor(R.color.ef_yellow));
				}else if ("4".equals(stateLevel)) {//紫色
					formStateView.setBackgroundResource(R.drawable.form_textviewboard_purple);
					formStateView.setTextColor(getResources().getColor(R.color.ef_purple));
				}else if ("5".equals(stateLevel)) {//红色
					formStateView.setBackgroundResource(R.drawable.form_textviewboard_red);
					formStateView.setTextColor(getResources().getColor(R.color.chat_red));
				}else {//默认
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean getIsInUse() {
		return this.isShown();
	}

	@Override
	public void setIsInUse(boolean isInUse) {
	}

	@Override
	public void prepareForReuse() {
	}



}
