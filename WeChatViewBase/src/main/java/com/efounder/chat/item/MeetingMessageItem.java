package com.efounder.chat.item;

import android.content.Context;
import android.content.Intent;
import android.util.MalformedJsonException;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.efounder.chat.R;
import com.efounder.chat.item.manager.IMessageItem;
import com.efounder.frame.ViewSize;
import com.efounder.message.struct.IMStruct002;
import com.efounder.ui.util.DisplayUtil;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 会议消息item
 * Created by slp on 2018/1/17.
 * json
 * {
 * "data": {
 * "meetingId": "12345678",
 * "creator": "154368",
 * "starttime": "2017/09/24 09:00:00",
 * "meetingTitle": "会议",
 * "role": "1",
 * "groupId": "236774",
 * "price": "",
 * "password": "",
 * "memberUserId":"14332;45332;12990;12112"
 * },
 * "content": "查看会议详情",
 * "title": "会议详情",
 * "caption": "会议详情",
 * "time": "1508840463361",
 * "currentID": "14880",
 * "currentNickName": "sdoc",
 * "toUserID": "9367",
 * "messageType": "67",
 * "viewType": "display",
 * "show": "SDItemDetailViewController",
 * "AndroidShow": "com.efounder.SDoctorsActivity",
 * "image": "https://panserver.solarsource.cn/panserver/files/4f0bb3a2-2c9b-4e8b-9003-c226147c9494/download",
 * "smallIcon": "https://panserver.solarsource.cn/panserver/files/4f0bb3a2-2c9b-4e8b-9003-c226147c9494/download",
 * "systemName": "会议信息"
 * }
 */

public class MeetingMessageItem extends LinearLayout implements IMessageItem {

    private TextView tvMeetingName;
    private TextView tvMeetingTime;
    private TextView tvMeetingId;
    private TextView tvMeetingRole;
    private TextView tvMeetingOwner;
    private ImageView ivBigPic;
    private ImageView ivsSmallPic;

    private JSONObject mJSONObject;
    private Context mContext;
    private IMStruct002 message;

    public MeetingMessageItem(Context context) {
        super(context);
        this.mContext = context;
        final LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflate.inflate(R.layout.chat_item_meeting, this);//注意第二个参数

        tvMeetingName = (TextView) findViewById(R.id.tv_meeting_name);
        tvMeetingTime = (TextView) findViewById(R.id.tv_meeting_time);
        tvMeetingId = (TextView) findViewById(R.id.tv_meeting_id);
        tvMeetingRole = (TextView) findViewById(R.id.tv_meeting_role);
        tvMeetingOwner = (TextView) findViewById(R.id.tv_meeting_owner);
        ivBigPic = (ImageView) findViewById(R.id.iv_meeting_big_pic);
        ivsSmallPic = (ImageView) findViewById(R.id.iv_meeting_small_pic);

        int width = DisplayUtil.getMobileWidth(context) * 2 / 3 - 20;
        LayoutParams lp = new LayoutParams(width, LayoutParams.MATCH_PARENT);
        this.setLayoutParams(lp);
        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String json = null;
                try {
                    json = new String(message.getBody(), "UTF-8");
                    JSONObject jsonObject = new JSONObject(json);
                    if (jsonObject.has("AndroidShow")) {
                        String showClass = jsonObject.getString("AndroidShow");

                        if (jsonObject.has("data")) {
                            JSONObject dataJson = mJSONObject.getJSONObject("data");
                            if (dataJson.has("meetingId")) {
                                String meetingId = dataJson.getString("meetingId");
                                if (dataJson.has("groupId")) {
                                    int groupId = Integer.parseInt(dataJson.getString("groupId"));
                                    if(dataJson.has("host")) {
                                        String host = dataJson.getString("host");
                                        if (showClass.equals("com.efounder.conference.activity.ConferenceActivity")) {
                                            Intent intent = new Intent(mContext, Class.forName(showClass));
                                            intent.putExtra("meetingId", meetingId);
                                            intent.putExtra("host",host);
                                            intent.putExtra("groupId", groupId);
                                            mContext.startActivity(intent);
                                        }
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void setIMStruct002(IMStruct002 message) {
        this.message = message;
        try {
            String json = new String(message.getBody(), "UTF-8");
            mJSONObject = new JSONObject(json);
            if (mJSONObject.has("image")) {
                setNetPicture(mJSONObject.getString("image"), ivBigPic);
            }
            if (mJSONObject.has("smallIcon")) {
                setNetPicture(mJSONObject.getString("smallIcon"), ivsSmallPic);
            }
            if (mJSONObject.has("title")) {
                tvMeetingName.setText(mJSONObject.getString("title"));
            }
            if (mJSONObject.has("time")) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
                long meetingTime = Long.parseLong(mJSONObject.getString("time"));
//                    Date meetingDate = new Date(meetingTime);
//                    String meetingTimeStr = sdf.format(meetingDate);
                tvMeetingTime.setText(sdf.format(meetingTime));
            }
            if (mJSONObject.has("systemName")) {
                tvMeetingOwner.setText(mJSONObject.getString("systemName"));
            }
            if (mJSONObject.has("content")) {
                String content = mJSONObject.getString("content");
                if(content.equals("1")){
                    tvMeetingRole.setText("主持人");
                }else if(content.equals("2")){
                    tvMeetingRole.setText("主讲人");
                }else if(content.equals("3")){
                    tvMeetingRole.setText("参会人");
                }else {
                    tvMeetingRole.setText(content);
                }
            }
            if (mJSONObject.has("data")) {
                JSONObject dataJson = mJSONObject.getJSONObject("data");

                if (dataJson.has("meetingId")) {
                    tvMeetingId.setText(dataJson.getString("meetingId"));
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public View messageView() {
        return this;
    }

    @Override
    public ViewSize messageViewSize() {
        return null;
    }

    @Override
    public boolean getIsInUse() {
        return this.isShown();
    }

    @Override
    public void setIsInUse(boolean isInUse) {

    }

    @Override
    public void prepareForReuse() {

    }

    /**
     * 加载网络图片
     *
     * @param url
     * @param img
     */
    private void setNetPicture(String url, ImageView img) {
        Glide
                .with(mContext)
                .load(url)
                .transition(new DrawableTransitionOptions().crossFade())
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(img);
    }
}
