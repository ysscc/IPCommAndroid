package com.efounder.chat.model;

import java.io.Serializable;
import java.util.List;

/**
 * 红包领取记录model
 * @author YQS
 */
public class RedPackageObtainModel implements Serializable {

    /**
     * result : success
     * redPacket : {"endTime":1534485761000,"packetId":"d9c4e0f7-f907-4268-96a5-078117aa5e60","remainMoney":8.7E-4,"remainSize":1,"remarks":"恭喜发财，大吉大利","startTime":1534399361000,"status":1,"totalMoney":0.001,"totalSize":2,"trusteeshipCoinId":1,"type":0,"userId":"18363825829"}
     * data : 1.3E-4
     * list : [{"best":1,"coin":1.3E-4,"from":"18363825829","id":260,"imUserId":"192205","packetId":"d9c4e0f7-f907-4268-96a5-078117aa5e60","time":1534401443000,"to":"18363825829","trusteeshipCoidId":1,"type":1}]
     */

    private String result;
    private RedPacketBean redPacket;
    private double data;

    public double getCoin() {
        return coin;
    }

    public RedPackageObtainModel setCoin(double coin) {
        this.coin = coin;
        return this;
    }

    private double coin;
    private List<ListBean> list;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public RedPacketBean getRedPacket() {
        return redPacket;
    }

    public void setRedPacket(RedPacketBean redPacket) {
        this.redPacket = redPacket;
    }

    public double getData() {
        return data;
    }

    public void setData(double data) {
        this.data = data;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class RedPacketBean {
        /**
         * endTime : 1534485761000
         * packetId : d9c4e0f7-f907-4268-96a5-078117aa5e60
         * remainMoney : 8.7E-4
         * remainSize : 1
         * remarks : 恭喜发财，大吉大利
         * startTime : 1534399361000
         * status : 1
         * totalMoney : 0.001
         * totalSize : 2
         * trusteeshipCoinId : 1
         * type : 0
         * userId : 18363825829
         */

        private long endTime;
        private String packetId;
        private double remainMoney;
        private int remainSize;
        private String remarks;
        private long startTime;
        private int status;
        private double totalMoney;
        private int totalSize;
        private int trusteeshipCoinId;
        private int type;
        private String userId;

        public long getEndTime() {
            return endTime;
        }

        public void setEndTime(long endTime) {
            this.endTime = endTime;
        }

        public String getPacketId() {
            return packetId;
        }

        public void setPacketId(String packetId) {
            this.packetId = packetId;
        }

        public double getRemainMoney() {
            return remainMoney;
        }

        public void setRemainMoney(double remainMoney) {
            this.remainMoney = remainMoney;
        }

        public int getRemainSize() {
            return remainSize;
        }

        public void setRemainSize(int remainSize) {
            this.remainSize = remainSize;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

        public long getStartTime() {
            return startTime;
        }

        public void setStartTime(long startTime) {
            this.startTime = startTime;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public double getTotalMoney() {
            return totalMoney;
        }

        public void setTotalMoney(double totalMoney) {
            this.totalMoney = totalMoney;
        }

        public int getTotalSize() {
            return totalSize;
        }

        public void setTotalSize(int totalSize) {
            this.totalSize = totalSize;
        }

        public int getTrusteeshipCoinId() {
            return trusteeshipCoinId;
        }

        public void setTrusteeshipCoinId(int trusteeshipCoinId) {
            this.trusteeshipCoinId = trusteeshipCoinId;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }
    }

    public static class ListBean {
        /**
         * best : 1
         * coin : 1.3E-4
         * from : 18363825829
         * id : 260
         * imUserId : 192205
         * packetId : d9c4e0f7-f907-4268-96a5-078117aa5e60
         * time : 1534401443000
         * to : 18363825829
         * trusteeshipCoidId : 1
         * type : 1
         */

        private int best;
        private double coin;
        private String from;
        private int id;
        private String imUserId;
        private String packetId;
        private long time;
        private String to;
        private int trusteeshipCoidId;
        private int type;

        public int getBest() {
            return best;
        }

        public void setBest(int best) {
            this.best = best;
        }

        public double getCoin() {
            return coin;
        }

        public void setCoin(double coin) {
            this.coin = coin;
        }

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getImUserId() {
            return imUserId;
        }

        public void setImUserId(String imUserId) {
            this.imUserId = imUserId;
        }

        public String getPacketId() {
            return packetId;
        }

        public void setPacketId(String packetId) {
            this.packetId = packetId;
        }

        public long getTime() {
            return time;
        }

        public void setTime(long time) {
            this.time = time;
        }

        public String getTo() {
            return to;
        }

        public void setTo(String to) {
            this.to = to;
        }

        public int getTrusteeshipCoidId() {
            return trusteeshipCoidId;
        }

        public void setTrusteeshipCoidId(int trusteeshipCoidId) {
            this.trusteeshipCoidId = trusteeshipCoidId;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }
    }
}
