package com.efounder.chat.item.manager;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

import com.efounder.chat.R;
import com.efounder.frame.ViewSize;
import com.efounder.frame.activity.EFTransformFragmentActivity;
import com.efounder.frame.baseui.BaseFragment;
import com.efounder.frame.utils.EFFrameUtils;
import com.efounder.message.struct.IMStruct002;
import com.tamic.jswebview.view.ProgressBarWebView;

import org.json.JSONObject;

import java.util.Arrays;

/**
 *
 * @author lch 网页
 *
 */
public class WebMessageItem extends LinearLayout implements IMessageItem,View.OnClickListener {
	private Context mContext;

	private LinearLayout messageItemLayout;
	private ProgressBarWebView webView;
	//数据M
	private IMStruct002 iMStruct002;
	private JSONObject jsonObject;
	private String url;
	public WebMessageItem(Context context) {
		super(context);
		mContext = context;
		initView(context);
	}

	private void initView(Context context){
		LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		messageItemLayout = (LinearLayout) inflate.inflate(R.layout.chat_item_message_web, this);
		LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, 500);
		this.setLayoutParams(lp);
		webView = (ProgressBarWebView) messageItemLayout
				.findViewById(R.id.message_item_webView);
		this.requestFocus();
//		webView.getWebView().setClickable(false);
//		webView.getWebView().setFocusable(false);
//		webView.getWebView().setFocusableInTouchMode(false);
//		webView.setClickable(false);
//		webView.setFocusable(false);
//		webView.setFocusableInTouchMode(false);

		//设置监听
		setOnClickListener(this);
	}

	@Override
	public void setIMStruct002(IMStruct002 message) {
		if(this.iMStruct002!=null&&(Arrays.equals(message.getBody(),this.iMStruct002.getBody())))
			return;
		this.iMStruct002 = message;

		try {
			String json = new String(message.getBody(), "UTF-8");
			jsonObject = new JSONObject(json);
			url = jsonObject.getString("url");
			webView.loadUrl(url);

		}catch (Exception e){

		}
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		return true;
	}

	@Override
	public void onClick(View v) {
		//Toast.makeText(mContext, "点击图片", Toast.LENGTH_LONG).show();
//		Intent intent = new Intent(mContext, ViewPagerActivity.class);
//		intent.putExtra("urls",urls);
//		int position = imageDatas.indexOf(iMStruct002);
//		intent.putExtra("position",position);
////		path = path.replace("file://", "")
////				.replace("content://", "").replace("assets://", "");
////		File file = new File(path);
////		if (file.exists()) {
////			Uri uri = Uri.fromFile(file);
////			intent.putExtra("uri", uri);
////			mContext.startActivity(intent);
////		} else {
////			intent.putExtra("path", path);
//		mContext.startActivity(intent);
////			//Toast.makeText(mContext, "打开失败！", Toast.LENGTH_SHORT)
////			//.show();
////		}

		try {

			Bundle bundle = new Bundle();

			bundle.putString("URL",url);
			bundle.putString(EFTransformFragmentActivity.EXTRA_TITLE_NAME,"");

			EFFrameUtils.pushFragment((Class<? extends BaseFragment>) Class.forName("com.efounder.chat.fragment.WebViewFragment"),bundle);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	@Override
	public View messageView() {
		return this;
	}

	@Override
	public ViewSize messageViewSize() {
		return null;
	}

	@Override
	public boolean getIsInUse() {
//		return isInUse;
		return this.isShown();
	}

	@Override
	public void setIsInUse(boolean isInUse) {
	}

	@Override
	public void prepareForReuse() {


	}

}