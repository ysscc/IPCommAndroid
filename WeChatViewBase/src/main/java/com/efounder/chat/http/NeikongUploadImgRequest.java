package com.efounder.chat.http;

import com.efounder.chat.R;
import com.efounder.chat.model.LogoutEvent;
import com.efounder.common.BaseRequestManager;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.util.AppContext;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import static com.efounder.frame.utils.Constants.KEY_SETTING_APPID;

/**
 * 内控影像上传
 * Created by kongmeng on 2018/9/4.
 */

public class NeikongUploadImgRequest {
    public static final String BASE_URL = EnvironmentVariable.getProperty("TalkChainUrl") + "/tcserver";
    private static final String TAG = "NeikongUploadImgRequest";

    /**
     * NeikongUploadImg的get请求方法：请求失败，判断需要获取access_token时调用getTokenRequest方法，成功后再次调用原请求
     *
     * @param tag      tag
     * @param method   url: 传入"path/method"
     * @param params   参数
     * @param callback callback
     */
    public static void neikongCommonRequest(final String tag, final String method, final HashMap<String, String> params, final NeikongUploadImgRequestCallback callback) {
        getCommonRequest(tag, method, params, new NeikongUploadImgRequestCallback() {
            @Override
            public void onSuccess(String response) {
                //成功的回调
                if (callback != null) {
                    callback.onSuccess(response);
                }
            }

            @Override
            public void onFail(String error) {
                if (callback == null) {
                    return;
                }
                // java.net.UnknownHostException: Unable to resolve host "tc.solarsource.cn": No address associated with hostname
                if (error != null && error.contains("UnknownHostException")) {
                    callback.onFail("UnknownHostException");
                } else {
                    callback.onFail("fail");
                }
            }
        });

    }

    /**
     * 通用get请求
     *
     * @param tag       tag
     * @param method    url: 传入"path/method"
     * @param paramsMap 参数
     * @param callback  callback
     */
    public static void getCommonRequest(final String tag, final String method, final HashMap<String, String> paramsMap, final NeikongUploadImgRequestCallback callback) {

        String requestUrl = String.format("%s/%s", BASE_URL, method);
        JFCommonRequestManager.getInstance().requestGetByAsyn(tag, requestUrl, paramsMap,
                new BaseRequestManager.ReqCodeCallBack<String>() {
                    @Override
                    public void onReqFailed(int errorCode, String errorMsg) {
                        if (callback != null) {
                            callback.onFail(errorMsg);
                        }
                        if (errorCode == 400 && EnvironmentVariable.getProperty(KEY_SETTING_APPID, "").equals(AppContext.getInstance()
                                .getString(R.string.special_appid))) {
                            //发送弹框提示用户
                            EventBus.getDefault().post(new LogoutEvent(LogoutEvent.TYPE_LOGIN_OUT_OF_DATE));
                        }
                    }

                    @Override
                    public void onReqSuccess(String result) {
                        callback.onSuccess(result);
                    }

                    @Override
                    public void onReqFailed(String errorMsg) {

                    }
                });
    }

    /**
     * 通用post方法请求
     *
     * @param tag       class name
     * @param method    url: 传入"path/method"
     * @param paramsMap 参数
     * @param callback  callback
     */
    public static void postCommonRequest(final String tag, String method, HashMap<String, String> paramsMap, final NeikongUploadImgRequestCallback callback) {
        JFCommonRequestManager.getInstance().requestPostByAsyn(tag, String.format("%s/%s", BASE_URL, method),
                paramsMap, new BaseRequestManager.ReqCodeCallBack<String>() {
                    @Override
                    public void onReqFailed(int errorCode, String errorMsg) {
                        if (callback != null) {
                            callback.onFail(errorMsg);
                        }
                        //todo 针对区块链应用，如果errorcode =400 表示用户登录失效，需要重新登录 yqs
                        if (errorCode == 400 && EnvironmentVariable.getProperty(KEY_SETTING_APPID, "").equals(AppContext.getInstance()
                                .getString(R.string.special_appid))) {
                            //发送弹框提示用户
                            EventBus.getDefault().post(new LogoutEvent(LogoutEvent.TYPE_LOGIN_OUT_OF_DATE));
                        }
                    }

                    @Override
                    public void onReqSuccess(String result) {
                        if (callback != null) {
                            callback.onSuccess(result);
                        }
                    }

                    @Override
                    public void onReqFailed(String errorMsg) {

                    }
                });
    }

    public interface NeikongUploadImgRequestCallback {

        void onSuccess(String response);

        void onFail(String error);
    }
}
