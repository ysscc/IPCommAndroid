package com.efounder.chat.utils;

import android.app.ActivityManager;
import android.content.Context;

import java.util.List;

/**
 * Created by hudq on 2016/8/3.
 */

public class ServiceUtils {
    /**
     * 用来判断服务是否运行.
     * @param mContext
     * @param className 判断的服务名字
     * @return true 在运行 false 不在运行
     */
    public static boolean isServiceRunning(Context mContext,String className) {
        boolean isRunning = false;
        ActivityManager activityManager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> serviceList
                = activityManager.getRunningServices(300);
        if (!(serviceList.size()>0)) {
            return false;
        }
        for (int i=0; i<serviceList.size(); i++) {
            ActivityManager.RunningServiceInfo serviceInfo = serviceList.get(i);
           // int processID = serviceInfo.pid;
//            if (processID == android.os.Process.myPid() && serviceInfo.service.getClassName().equals(className)) {
            if (serviceInfo.service.getClassName().equals(className)) {
                isRunning = true;
                break;
            }
        }
        return isRunning;
    }
}
