package com.efounder.chat.publicnumber;

import android.app.AlertDialog;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.text.Spannable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.activity.PublicNumerInfoActivity;
import com.efounder.chat.db.WeChatDBManager;
import com.efounder.chat.fragment.ChatSenderFragment;
import com.efounder.chat.http.GetHttpUtil;
import com.efounder.chat.http.OKHttpUtils;
import com.efounder.chat.interf.DismissCallbacks;
import com.efounder.chat.item.ImageMessageItem;
import com.efounder.chat.item.TextMessageItemForDes;
import com.efounder.chat.item.manager.IMessageItem;
import com.efounder.chat.item.manager.JFMessageItemManager;
import com.efounder.chat.manager.ChatMessageViewTypeDelegate;
import com.efounder.chat.struct.MessageChildTypeConstant;
import com.efounder.chat.utils.ChatActivitySkipUtil;
import com.efounder.chat.utils.ChatDateUtils;
import com.efounder.chat.utils.ChatMessageMenuDialogUtil;
import com.efounder.chat.utils.IMStruct002Util;
import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.chat.utils.MessagePanSoftUtils;
import com.efounder.chat.utils.SmileUtils;
import com.efounder.chat.view.MessageBaseView;
import com.efounder.chat.view.MessageHideView;
import com.efounder.chat.view.MessageIncomingView;
import com.efounder.chat.view.MessageOutgoingView;
import com.efounder.chat.view.MessagePublicNumberView;
import com.efounder.chat.view.MessageRecallView;
import com.efounder.chat.widget.ChatListView;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.imageloader.GlideImageLoader;
import com.efounder.message.manager.JFMessageManager;
import com.efounder.message.struct.IMStruct002;
import com.efounder.mobilecomps.contacts.User;
import com.efounder.util.JSONUtil;
import com.utilcode.util.ToastUtils;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.content.Context.CLIPBOARD_SERVICE;
import static com.efounder.chat.manager.ChatMessageViewTypeDelegate.VIEW_TYPE_INCOMMING;
import static com.efounder.chat.manager.ChatMessageViewTypeDelegate.VIEW_TYPE_OFFICIAL_ACCTOUNT;
import static com.efounder.chat.manager.ChatMessageViewTypeDelegate.VIEW_TYPE_OUTGOING;
import static com.efounder.chat.manager.ChatMessageViewTypeDelegate.VIEW_TYPE_SYSTEM;
import static com.efounder.chat.manager.ChatMessageViewTypeDelegate.VIEW_TYPE_TIPS;
import static com.efounder.chat.struct.MessageChildTypeConstant.subtype_officalAccount;
import static com.efounder.frame.utils.Constants.CHAT_USER_ID;
import static com.efounder.message.struct.IMStruct002.MESSAGE_STATE_DELIVER;

//import com.efounder.chat.item.LocationMapItem;

/**
 * 公众号会话adapter
 */
public class PublicChatAdapter extends BaseAdapter implements OKHttpUtils.DownLoadCallBack {


    private final Context mContext;
    private List<IMStruct002> datas;
    private Myhandler myhandler;
    private JFMessageManager messageManager;

    private ChatSenderFragment.PreSendMessageCallback preSendMessageCallback = new ChatSenderFragment.PreSendMessageCallback() {
        @Override
        public void preSendMessage(IMStruct002 struct002) {
        }

        @Override
        public void updateProgress(IMStruct002 struct002, double percent) {
            if (percent == -1.0d) {
                JFMessageManager.getInstance().updateMessage(struct002);
            }
            notifyDataSetChanged();
        }

        @Override
        public void sendPreMessage(final IMStruct002 struct002) {
            if (struct002 != null) {
                messageManager = JFMessageManager.getInstance();
                messageManager.sendPreMessage(struct002);
            }
        }
    };


    ChatListView chatListView;
    private int myUserId;
    private ChatMessageViewTypeDelegate viewTypeDelegate;

    public PublicChatAdapter(Context context, ChatListView chatListView,
                             List<IMStruct002> datas) {
        this.mContext = context;
        if (datas == null) {
            datas = new ArrayList<IMStruct002>(0);
        }
        this.datas = datas;
        this.chatListView = chatListView;
        myhandler = new Myhandler();
        try {
            myUserId = Integer.parseInt(EnvironmentVariable.getProperty(CHAT_USER_ID));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        viewTypeDelegate = new ChatMessageViewTypeDelegate(myUserId);
    }

    /*
     * adpter 刷新方法
     */
    public void refresh(List<IMStruct002> datas) {
        if (datas == null) {
            datas = new ArrayList<IMStruct002>(0);
        }
        this.datas = datas;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return datas.size();
    }

    @Override
    public IMStruct002 getItem(int position) {
        return datas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        IMStruct002 iMStruct002 = getItem(position);
        return viewTypeDelegate.getItemViewType(iMStruct002);
    }

    @Override
    public int getViewTypeCount() {
        return viewTypeDelegate.getViewTypeCount();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final IMStruct002 iMStruct002 = datas.get(position);
        int type = getItemViewType(position);
        if (convertView == null || ((MessageBaseView) convertView).getItemType() != type) {
            switch (type) {
                case VIEW_TYPE_OUTGOING:// 发送container
                    convertView = new MessageOutgoingView(mContext);
                    ((MessageBaseView) convertView).setItemType(VIEW_TYPE_OUTGOING);
                    break;
                case VIEW_TYPE_INCOMMING: // 接受container
                    convertView = new MessageIncomingView(mContext);
                    ((MessageBaseView) convertView).setItemType(VIEW_TYPE_INCOMMING);
                    break;
                case VIEW_TYPE_OFFICIAL_ACCTOUNT:// 公众号container
                    convertView = new MessagePublicNumberView(mContext);
                    ((MessageBaseView) convertView).setItemType(VIEW_TYPE_OFFICIAL_ACCTOUNT);
                    break;
                case VIEW_TYPE_TIPS://撤回消息container
                    convertView = new MessageRecallView(mContext);
                    ((MessageBaseView) convertView).setItemType(VIEW_TYPE_TIPS);
                    break;
                case VIEW_TYPE_SYSTEM:
                    convertView = new MessageHideView(mContext);
                    ((MessageBaseView) convertView).setItemType(VIEW_TYPE_SYSTEM);
                    break;
            }
        }

        MessageBaseView messageBaseView = (MessageBaseView) convertView;
        IMessageItem imessageItem = null;
        final int messageChildType = iMStruct002.getMessageChildType();
        messageBaseView.setSupplementVisible(false);
        switch (messageChildType) {
            case MessageChildTypeConstant.subtype_text:
            case subtype_officalAccount:
            case MessageChildTypeConstant.subtype_callingCard:
            case MessageChildTypeConstant.subtype_payCard:
            case MessageChildTypeConstant.subtype_form:// 表单
            case MessageChildTypeConstant.subtype_form_native:// 表单-原生
            case MessageChildTypeConstant.subtype_task:// 任务
//            case MessageChildTypeConstant.subtype_image:// 图片
            case MessageChildTypeConstant.subtype_voice:// 语音
            case MessageChildTypeConstant.subtype_smallVideo:// 视频
            case MessageChildTypeConstant.subtype_location:
            case MessageChildTypeConstant.subtype_command:
            case MessageChildTypeConstant.subtype_recallMessage://撤回消息
            case MessageChildTypeConstant.subtype_mZoneNotification://空间通知
            case MessageChildTypeConstant.subtype_xj_item1://新疆item1
            case MessageChildTypeConstant.subtype_zy_task://中油铁工item
            case MessageChildTypeConstant.subtype_meeting://会议消息item
            case MessageChildTypeConstant.subtype_recognition://面部识别消息item
            case MessageChildTypeConstant.subtype_gxtask://共享任务消息item
                imessageItem = JFMessageItemManager.getMessageItem(mContext,
                        iMStruct002);
                break;
            case MessageChildTypeConstant.subtype_officalweb:// 网址
            case MessageChildTypeConstant.subtype_common:// 通用页面
                imessageItem = JFMessageItemManager.getMessageItem(mContext,
                        iMStruct002);
//                ((MessageBaseView) convertView).getItemAvatarLayout().setVisibility(View.GONE);
//                ((MessageBaseView) convertView).getSignViewLayout().setVisibility(View.GONE);
//                ((MessageBaseView) convertView).getMiddleView().setPadding(0,0,0,0);
                break;
            case MessageChildTypeConstant.subtype_bornText:
                DismissCallbacks mCallbacks = new DismissCallbacks() {
                    @Override
                    public void onDismiss(View view, Object token) {
                        datas.remove(position);
                        refresh(datas);
                    }

                    @Override
                    public boolean canDismiss(Object token) {
                        return true;
                    }
                };
                // messageBaseView.getRlMessageContentLayout().setBackgroundResource(0);
                byte[] body1 = iMStruct002.getBody();
                Spannable text = SmileUtils.getSmiledText(mContext, new String(
                        body1));
                imessageItem = new TextMessageItemForDes(mContext, chatListView,
                        convertView, text, mCallbacks);
                break;
            case MessageChildTypeConstant.subtype_image:// 图片
                imessageItem = new ImageMessageItem(mContext, datas);
                break;
            default:
                imessageItem = JFMessageItemManager.getMessageItem(mContext,
                        iMStruct002);
                break;
        }
        // TODO 设置iMStruct002
        if (!iMStruct002.isRecall()) {
            if (messageChildType == MessageChildTypeConstant.subtype_file) {
                //文件
                IMStruct002 imStruct002 = IMStruct002Util.getImStruct002(iMStruct002, position);
                imessageItem.setIMStruct002(imStruct002);
            } else if (messageChildType == MessageChildTypeConstant.subtype_image) {
                //图片
                ((ImageMessageItem) imessageItem).setMessageList(datas);
                imessageItem.setIMStruct002(iMStruct002);
            } else {
                imessageItem.setIMStruct002(iMStruct002);
            }
        } else {
            imessageItem.setIMStruct002(iMStruct002);
        }
        //给messageItem传递position
        if (imessageItem instanceof View) {
            ((View) imessageItem).setTag(position);
        }
        //TODO 设置MessageItem
        messageBaseView.setMessageItem(imessageItem);
        //TODO 设置文字消息双击事件
        if (iMStruct002.getMessageChildType() == MessageChildTypeConstant.subtype_text && !iMStruct002.isRecall()) {
            messageBaseView.getMessageItem().messageView().setOnClickListener(new View
                    .OnClickListener() {
                int count = 0;
                long firstClickTime = 0;
                long secondClickTime = 0;

                @Override
                public void onClick(View view) {
                    // 如果第二次点击 距离第一次点击时间过长 那么将第二次点击看为第一次点击
                    if (firstClickTime != 0 && System.currentTimeMillis() - firstClickTime >
                            500) {
                        count = 0;
                    }
                    count++;
                    if (count == 1) {
                        firstClickTime = System.currentTimeMillis();
                    } else if (count == 2) {
                        secondClickTime = System.currentTimeMillis();
                        // 两次点击小于500ms 也就是连续点击
                        if (secondClickTime - firstClickTime < 500) {
                            //ToastUtil.showToast(mContext, "双击");
                            ChatMessageMenuDialogUtil.showBigTextView(mContext, iMStruct002);
                            count = 0;
                            firstClickTime = 0;
                            secondClickTime = 0;
                        }
                    }
                }
            });
        }
        //TODO:消息长按事件
        messageBaseView.getMessageItem().messageView().setOnLongClickListener(new View
                .OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                ChatMessageMenuDialogUtil.showDialog(mContext, iMStruct002, PublicChatAdapter.this);
                return false;
            }
        });

        //TODO topView 设置时间
        if (position == 0) {
            messageBaseView.getTopTextView().setText(ChatDateUtils.getTimestampString(
                    new Date(iMStruct002.getLocalTime())));
            messageBaseView.getTopTextView().setVisibility(View.VISIBLE);
        } else {
            // 两条消息时间离得如果稍长，显示时间
            if (position - 1 > 0) {

                if (ChatDateUtils.isCloseEnough(iMStruct002.getTime(), datas
                        .get(position - 1).getLocalTime())) {
                    messageBaseView.getTopTextView().setVisibility(View.GONE);
                } else {
                    messageBaseView.getTopTextView().setText(ChatDateUtils.getTimestampString(new Date(
                            iMStruct002.getLocalTime())));
                    messageBaseView.getTopTextView().setVisibility(View.VISIBLE);
                }
            } else {
                messageBaseView.getTopTextView().setVisibility(View.GONE);
            }
        }
        //SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm",
        //Locale.getDefault());// 设置日期格式
        //String dateTime = dateFormat.format(iMStruct002.getTime());
        //messageBaseView.getTopTimeTextView().setText(dateTime);
//        // TODO 显示对方的用户名
        if (myUserId != iMStruct002.getFromUserId()) {
            messageBaseView.getUserNameTextView().setVisibility(View.VISIBLE);
            String name = "";
            User currentUser = null;
            if (iMStruct002.getpUserId() == 0) {
                currentUser = WeChatDBManager.getInstance().getOneUserById(iMStruct002.getFromUserId());
            } else {
                currentUser = WeChatDBManager.getInstance().getOneUserById(iMStruct002.getpUserId());
            }
            if (currentUser != null) {
                name = currentUser.getNickName();
                // FIXME: 17-8-13 应用号如果没有昵称，但是有username，会一直请求服务器
                if (!currentUser.isExist()) {
                    GetHttpUtil.getUserInfo(currentUser.getId(), mContext, null);
                }
            } else {
                name = "";
            }

            messageBaseView.getUserNameTextView().setText(name);
        } else {
            messageBaseView.getUserNameTextView().setVisibility(View.INVISIBLE);
        }


        // TODO 设置用户头像
        User user = null;
        if (iMStruct002.getpUserId() == 0) {
            user = WeChatDBManager.getInstance().getOneUserById(iMStruct002.getFromUserId());
        } else {
            if (iMStruct002.getFromUserId() == myUserId) {
                user = WeChatDBManager.getInstance().getOneUserById(iMStruct002.getFromUserId());
            } else {
                user = WeChatDBManager.getInstance().getOneUserById(iMStruct002.getpUserId());
            }

        }

        if (user != null && user.getAvatar().contains("http")) {
            LXGlideImageLoader.getInstance().showUserAvatar(mContext, messageBaseView.getAvatarImageView(), user.getAvatar());
        } else {
            LXGlideImageLoader.getInstance().showUserAvatar(mContext, messageBaseView.getAvatarImageView(), "");
        }

        if (user.getId() == 365) {
            Object avatar = R.drawable.daqingyoutian;
            GlideImageLoader.getInstance().displayImage(mContext, messageBaseView.getAvatarImageView(), avatar);
        }
        // TODO 设置头像点击事件
        messageBaseView.getAvatarImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Intent intent = null;
                if (iMStruct002.getFromUserId() == myUserId) {
//                    intent = new Intent(mContext, UserInfoActivity.class);
//                    intent.putExtra("id", iMStruct002.getFromUserId());
//                    mContext.startActivity(intent);
                    Intent intent = new Intent();
                    intent.putExtra("id", iMStruct002.getFromUserId());

                    ChatActivitySkipUtil.startUserInfoActivity(mContext, intent);

                } else if (iMStruct002.getpUserId() == 0) {//不是自己的头像并且puserid等于0
                    User user1 = WeChatDBManager.getInstance().getOneUserById(iMStruct002.getFromUserId());
                    Intent intent1 = new Intent(mContext, PublicNumerInfoActivity.class);
                    intent1.putExtra("id", user1.getId());
                    intent1.putExtra("finish", "true");
                    mContext.startActivity(intent1);
                }
            }
        });


        //		//TODO 设置发送中状态 已发送 失败状态
        int state = iMStruct002.getState();
        //  System.out.println("--state---" + state);
        if (convertView instanceof MessageOutgoingView)

        {
            if (state == IMStruct002.MESSAGE_STATE_SENDING) {
                ((MessageBaseView) convertView).getMprogressbar().setVisibility(View.VISIBLE);
                ((MessageBaseView) convertView).getLoadingErrorImage().setVisibility(View.GONE);
                ((MessageBaseView) convertView).setSupplementVisible(true);
                ((MessageBaseView) convertView).getChatStateView().setText(R.string.wechat_chat_state_sending);
            } else if (state == IMStruct002.MESSAGE_STATE_WAITSEND) {
                ((MessageBaseView) convertView).getMprogressbar().setVisibility(View.VISIBLE);
                ((MessageBaseView) convertView).getLoadingErrorImage().setVisibility(View.GONE);
                ((MessageBaseView) convertView).setSupplementVisible(true);
                ((MessageBaseView) convertView).getChatStateView().setText(R.string.wechat_chat_state_pending);
            } else if (state == IMStruct002.MESSAGE_STATE_PRESEND) {
                //预发送,进度为0，待发送
                //进度为其他不为-1数值，正在发送
                Long startUpLoadTime = (Long) iMStruct002.getExtra("startUploadTime");
                if (startUpLoadTime == null) {
                    startUpLoadTime = System.currentTimeMillis();
                }
                //一分钟发送出不出去就是超时了
                boolean isTimeOut = System.currentTimeMillis() - startUpLoadTime > 1000 * 60;
                if (isTimeOut && iMStruct002.getExtra("progress") != null && (int) iMStruct002.getExtra("progress") != -1) {
                    iMStruct002.putExtra("progress", -1);
                    JFMessageManager.getInstance().updateMessage(iMStruct002);
                    notifyDataSetChanged();
                }
                if (iMStruct002.getExtra("progress") != null && (int) iMStruct002.getExtra("progress") != -1) {
                    ((MessageBaseView) convertView).getMprogressbar().setVisibility(View.VISIBLE);
                    ((MessageBaseView) convertView).getLoadingErrorImage().setVisibility(View.GONE);
                    ((MessageBaseView) convertView).setSupplementVisible(true);
                    ((MessageBaseView) convertView).getChatStateView().setText(R.string.wechat_chat_state_pending);
                } else {
                    //进度为空，但是停留在预发送状态，发送失败，重新发送（以前的版本）
                    //发送失败，进度为-1，重新发送
                    ((MessageBaseView) convertView).getMprogressbar().setVisibility(View.GONE);
                    ((MessageBaseView) convertView).getLoadingErrorImage().setVisibility(View
                            .VISIBLE);
                    ((MessageBaseView) convertView).setSupplementVisible(false);
                    ((MessageBaseView) convertView).getLoadingErrorImage().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(mContext);
                            builder.setItems(R.array.chat_message,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog,
                                                            int which) {
                                            if (which == 0) {
                                                String msgPath;
                                                if (messageChildType == MessageChildTypeConstant.subtype_file
                                                        || messageChildType == MessageChildTypeConstant.subtype_secret_file) {
                                                    msgPath = JSONUtil.parseJson(iMStruct002.getMessage()).get("FileLocalPath").getAsString();
                                                } else if (messageChildType == MessageChildTypeConstant.subtype_location) {
                                                    msgPath = JSONUtil.parseJson(iMStruct002.getMessage()).get("localPath").getAsString();
                                                } else {
                                                    msgPath = JSONUtil.parseJson(iMStruct002.getMessage()).get("path").getAsString();
                                                }
                                                //重新发送进度设为0 刷新界面
                                                iMStruct002.putExtra("progress", 0);
                                                iMStruct002.putExtra("startUploadTime", System.currentTimeMillis());
                                                notifyDataSetChanged();
                                                MessagePanSoftUtils.upload(msgPath, iMStruct002, preSendMessageCallback, messageChildType);
                                            } else if (which == 1) {
                                                dialog.dismiss();
                                            }
                                        }
                                    });
                            builder.show();
                        }
                    });
                }
            } else if (state == MESSAGE_STATE_DELIVER) {
                ((MessageBaseView) convertView).getMprogressbar().setVisibility(View.GONE);
                ((MessageBaseView) convertView).getLoadingErrorImage().setVisibility(View.GONE);
                ((MessageBaseView) convertView).setSupplementVisible(true);
                ((MessageBaseView) convertView).getChatStateView().setText(R.string.wechat_chat_state_delivered);
            } else if (state == IMStruct002.MESSAGE_STATE_SEND) {
                ((MessageBaseView) convertView).getMprogressbar().setVisibility(View.GONE);
                ((MessageBaseView) convertView).getLoadingErrorImage().setVisibility(View.GONE);
                ((MessageBaseView) convertView).setSupplementVisible(true);
                ((MessageBaseView) convertView).getChatStateView().setText(R.string.wechat_chat_state_sended);
            } else if (state == IMStruct002.MESSAGE_STATE_RECEIVE) {
                ((MessageBaseView) convertView).getMprogressbar().setVisibility(View.GONE);
                ((MessageBaseView) convertView).getLoadingErrorImage().setVisibility(View.GONE);
                ((MessageBaseView) convertView).setSupplementVisible(true);
                ((MessageBaseView) convertView).getChatStateView().setText(R.string.wechat_chat_state_received);
            } else if (state == IMStruct002.MESSAGE_STATE_READ) {
                ((MessageBaseView) convertView).getMprogressbar().setVisibility(View.GONE);
                ((MessageBaseView) convertView).getLoadingErrorImage().setVisibility(View.GONE);
                ((MessageBaseView) convertView).setSupplementVisible(true);
                ((MessageBaseView) convertView).getChatStateView().setText(R.string.wechat_chat_state_read);
            } else if (state == IMStruct002.MESSAGE_STATE_FAILURE) {
                ((MessageBaseView) convertView).getMprogressbar().setVisibility(View.GONE);
                ((MessageBaseView) convertView).getLoadingErrorImage().setVisibility(View.VISIBLE);
                ((MessageBaseView) convertView).setSupplementVisible(false);
                ((MessageBaseView) convertView).getLoadingErrorImage().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                        builder.setItems(R.array.chat_message,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        if (which == 0) {
                                            datas.remove(iMStruct002);
                                            iMStruct002.setState(-1);
                                            JFMessageManager.getInstance().reSendMessage(iMStruct002);
                                        } else if (which == 1) {
                                            dialog.dismiss();
                                        }


                                    }
                                });
                        builder.show();
                        // JFMessageManager.getInstance().sendMessage(iMStruct002);
                    }
                });

            } else {
                ((MessageBaseView) convertView).setSupplementVisible(false);
            }
        } else

        {
            ((MessageBaseView) convertView).setSupplementVisible(false);
        }
        return convertView;
    }

    private void showDialog(final IMStruct002 imstruct002) {
        final AlertDialog dlg = new AlertDialog.Builder(mContext).create();
        dlg.show();
        Window window = dlg.getWindow();
        // *** 主要就是在这里实现这种效果的.
        // 设置窗口的内容页面,shrew_exit_dialog.xml文件中定义view内容
        window.setContentView(R.layout.alertdialog);
        // 为确认按钮添加事件,执行退出应用操作
        TextView copyText = (TextView) window.findViewById(R.id.tv_content1);
        copyText.setText(R.string.chat_copy_message);
        copyText.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                try {
                    String message = new String(imstruct002.getBody(), "utf-8");
                    ClipboardManager cmb = (ClipboardManager) mContext.getSystemService
                            (CLIPBOARD_SERVICE);
                    cmb.setText(message);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                ToastUtils.showShort(R.string.chat_copy_to_clipboard);

                dlg.cancel();
            }
        });
        TextView recallText = (TextView) window.findViewById(R.id.tv_content2);
        recallText.setText(R.string.wechatview_recallmessage);
        recallText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ToastUtils.showShort(R.string.wechatview_recallmessage);
                dlg.cancel();
            }
        });

    }

    @Override
    public void downLoadCallBack(String result) {
        myhandler.sendEmptyMessage(1);

    }

    public static class ViewHolder {
        public MessageBaseView messageBaseView;
    }

    public class Myhandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 1) {
                notifyDataSetChanged();
            }
        }
    }
}
