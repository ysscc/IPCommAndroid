package com.efounder.chat.utils;

import com.efounder.constant.EnvironmentVariable;
import com.efounder.mobilecomps.contacts.User;

import org.json.JSONObject;

import static com.efounder.frame.utils.Constants.CHAT_USER_ID;

/**
 * IM json数据解析 user group等
 */
public class ImJsonObjectParseUtil {


    /**
     * 从json中更新用户数据
     *
     * @param user 本地User
     * @return User
     */

    public static User updateUserFromJson(User user, String jsonString) {
        try {
            return updateUserFromJson(user, new JSONObject(jsonString));
        } catch (Exception e) {
            e.printStackTrace();
            return user;
        }
    }

    public static User updateUserFromJson(User user, JSONObject jsonObject) {
        if (user == null) {
            return null;
        }
        if (jsonObject == null) {
            return user;
        }
        user.setId(jsonObject.optInt("userId"));
        user.setLoginUserId(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)));
        user.setName(jsonObject.optString("userName", ""));
        user.setNickName(jsonObject.optString("nickName", user.getName()));
        user.setSex(jsonObject.optString("sex", "M"));
        user.setPhone(jsonObject.optString("phone", ""));
        user.setMobilePhone(jsonObject.optString("mobilePhone", ""));
        user.setEmail(jsonObject.optString("email", ""));

        //财务共享中显示，组织机构名称、部门名称
        user.setCompany(jsonObject.optString("zzjgmc", ""));
        user.setBmmc(jsonObject.optString("bmmc", ""));

        user.setIsTop("1".equals(jsonObject.optString("isTop", "0")));
        user.setIsBother("1".equals(jsonObject.optString("isBother", "0")));

        //这里注意，json 中0 表示允许聊天
        user.setAllowStrangerChat(jsonObject.optInt("disableStrangers", 0) == 0 ? true : false);
        if (!jsonObject.optString("avatar", "").equals("")
                && jsonObject.optString("avatar", "").contains("http")) {
            user.setAvatar(jsonObject.optString("avatar"));
        } else {
            user.setAvatar("");
        }
        //备注名
        user.setReMark(jsonObject.optString("note", user.getNickName()));
        user.setOtherGroupRemark(jsonObject.optString("note", ""));
        user.setOwnGroupRemark(jsonObject.optString("note", ""));

        //个性签名
        user.setSigNature(jsonObject.optString("sign", ""));
        if (jsonObject.has("userType")) {
            user.setType(Integer.valueOf(jsonObject.optString("userType", "0")));
        } else {
            user.setType(User.PERSONALFRIEND);//好友类型
        }
        //如果用户是应用号 会有此属性
        user.setPublicType(jsonObject.optInt("publicType", 0));
        //todo 星际通讯专用字段
        user.setAllowStrangerChat(jsonObject.optInt("disableStrangers", 0) == 0 ? true : false);
        //加密公钥
        user.setRSAPublicKey(jsonObject.optString("comPublicKey"));
        //钱包地址
        String walletAddress = jsonObject.optString("ethAddress", "");
        if (!walletAddress.toLowerCase().startsWith("0x")) {
            walletAddress = "0x" + walletAddress;
        }
        user.setWalletAddress(walletAddress);
        //钱包公钥
        user.setPublicKey(jsonObject.optString("ethPublicKey"));
        //微信收款二维码
        user.setWeixinQrUrl(jsonObject.optString("weChatQRCode", user.getWeixinQrUrl()));
        //支付宝
        user.setZhifubaoQrUrl(jsonObject.optString("aliPayQRCode", user.getZhifubaoQrUrl()));
        //星球
        user.setPlanet(jsonObject.optString("planet", user.getPlanet()));
        return user;
    }
}
