package com.efounder.chat.http.download;

/**
 * Created by zhangshunyun on 2017/7/27.
 */

public interface FileDownloadListener {
    void update(String messageID);
}
