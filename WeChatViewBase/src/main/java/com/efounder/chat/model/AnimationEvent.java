package com.efounder.chat.model;

import android.graphics.drawable.AnimationDrawable;

import net.sf.json.JSONObject;

/**
 * 动画的事件
 */
public class AnimationEvent {
    private String mesageBody;
    private String type;
    private String duration;
    private String repeat;
    private String no;
    private String name;
    private long durationTime;

    private AnimationDrawable animationDrawable;
    //body={"type":"animation","name":"gold","no":"00001","duration":"2.000000","repeat":"1"}


    public AnimationEvent(String mesageBody) {
        this.mesageBody = mesageBody;
        if (mesageBody == null || "".equals(mesageBody)) {
            return;
        }
        JSONObject jsonObject = null;
        try {
            jsonObject = JSONObject.fromObject(mesageBody);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        name = jsonObject.optString("name", null);
        type = jsonObject.optString("type", null);
        repeat = jsonObject.optString("repeat", null);
        duration = jsonObject.optString("duration", null);
        no = jsonObject.optString("no", null);

    }

    public String getMesageBody() {
        return mesageBody;
    }

    public String getType() {
        return type;
    }

    public String getDuration() {
        return duration;
    }

    public String getRepeat() {
        return repeat;
    }

    public String getNo() {
        return no;
    }

    public String getName() {
        return name;
    }

    public AnimationDrawable getAnimationDrawable() {
        return animationDrawable;
    }

    public AnimationEvent setAnimationDrawable(AnimationDrawable animationDrawable) {
        this.animationDrawable = animationDrawable;
        return this;
    }

    public long getDurationTime() {
        return durationTime;
    }

    public AnimationEvent setDurationTime(long durationTime) {
        this.durationTime = durationTime;
        return this;
    }

    public AnimationEvent() {
    }
}
