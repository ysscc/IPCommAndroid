package com.efounder.chat.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.efounder.chat.R;
import com.efounder.chat.db.WeChatDBManager;
import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.mobilecomps.contacts.User;
import com.efounder.util.ToastUtil;

import static com.efounder.frame.utils.Constants.CHAT_USER_ID;


/**
 * 扫码登录baseactivity
 *
 * @author yqs
 */
public abstract class ScanLoginAbstractActivity extends BaseActivity implements View.OnClickListener {


    protected ImageView iv_avatar;
    protected TextView tv_nickname;
    protected TextView tv_text;
    protected String jsonString = "";
    protected ImageView ivBack;
    protected TextView tvTitle;
    protected User user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loginweb);
        initview();
        initData();
    }

    protected void initview() {
        iv_avatar = (ImageView) findViewById(R.id.iv_avatar);
        tv_nickname = (TextView) findViewById(R.id.tv_nickname);
        tv_text = (TextView) findViewById(R.id.tv_text);
        ivBack = (ImageView) findViewById(R.id.iv_back);
        tvTitle = (TextView) findViewById(R.id.tv_title);

        Button btn_accept = (Button) findViewById(R.id.btn_accept);
        Button btn_cancle = (Button) findViewById(R.id.btn_cancle);

        btn_accept.setOnClickListener(this);
        btn_cancle.setOnClickListener(this);

        ImageView iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    protected void initData() {
        jsonString = getIntent().getStringExtra("result");
        if (TextUtils.isEmpty(jsonString)) {
            Toast.makeText(ScanLoginAbstractActivity.this, R.string.wrchatview_qr_code_fail, Toast.LENGTH_SHORT).show();
            ScanLoginAbstractActivity.this.finish();
        }

        user = WeChatDBManager.getInstance().getOneUserById(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)));
        String avatar = user.getAvatar();
        //设置头像
        showUserAvatar(avatar);
        tv_nickname.setText(user.getNickName().toString());
        tv_text.setText(R.string.wrchatview_request_login);
        tvTitle.setText(R.string.wrchatview_scan_code_login);
    }

    /**
     * 显示用户头像
     */
    protected void showUserAvatar(String url) {
        LXGlideImageLoader.getInstance().showRoundUserAvatar(ScanLoginAbstractActivity.this, iv_avatar,
                url, LXGlideImageLoader.DEFATLT_AVATAR_RADIUS_12);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_accept) {
            confirmButtonClick();
        } else if (id == R.id.btn_cancle) {
            cancelButtonClick();
        }
    }

    /**
     * 确定按钮点击事件
     */
    public abstract void confirmButtonClick();

    public void cancelButtonClick() {
        finish();
    }

    /**
     * 登录成功提示
     */
    protected void loginSuccess() {
        if (isFinishing() || isDestroyed()) {
            return;
        }
        dismissLoading();
        Toast.makeText(ScanLoginAbstractActivity.this, R.string.wrchatview_login_success, Toast.LENGTH_SHORT).show();
        Class<?> clazz = null;
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        try {
            clazz = loader.loadClass(getResources().getString(R.string.from_group_backto_first));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (clazz != null) {
            Intent intent = new Intent(ScanLoginAbstractActivity.this, clazz);
            startActivity(intent);
        } else {
            finish();
        }
    }

    /**
     * 登录失败提示
     */
    protected void loginFailed(String text) {
        if (isFinishing() || isDestroyed()) {
            return;
        }
        dismissLoading();
        String tip = TextUtils.isEmpty(text) ? getResources().getString(R.string.wrchatview_login_fail) : text;
        ToastUtil.showToast(ScanLoginAbstractActivity.this, tip);
    }


}
