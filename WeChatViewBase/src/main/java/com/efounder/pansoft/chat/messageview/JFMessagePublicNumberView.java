package com.efounder.pansoft.chat.messageview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * 
 * @author lch 5-26
 *  公众号
 */
public class JFMessagePublicNumberView extends JFMessageBaseView {

	public JFMessagePublicNumberView(Context context) {
		super(context);
		initView(context);
	}

	public JFMessagePublicNumberView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView(context);
	}

	@SuppressLint("ResourceAsColor")
	private void initView(final Context context) {
		// 消息体区域
		RelativeLayout.LayoutParams chat_item_layout_content_layoutparams = new RelativeLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
//		chat_item_layout_content_layoutparams
//				.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		chat_item_layout_content_layoutparams
				.addRule(RelativeLayout.CENTER_HORIZONTAL);
		chat_item_layout_content_layoutparams.leftMargin = 10;
		chat_item_layout_content_layoutparams.rightMargin = 10;
		middleView.addView(rlMessageContentLayout,
				chat_item_layout_content_layoutparams);
	}
	

}
