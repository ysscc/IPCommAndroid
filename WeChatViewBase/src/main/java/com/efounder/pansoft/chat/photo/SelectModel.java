package com.efounder.pansoft.chat.photo;

import java.io.Serializable;
import java.util.Objects;

/**
 * 图片、视频的路径，类型
 */
public class SelectModel implements Serializable{

    private String path;//路径
    private int type;//类型 MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO
                    //MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE
    private String size;//视频大小


    public SelectModel(String path, int type) {
        this.path = path;
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public int getType() {
        return type;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SelectModel that = (SelectModel) o;
        return type == that.type &&
                Objects.equals(path, that.path) &&
                Objects.equals(size, that.size);
    }

    @Override
    public int hashCode() {

        return Objects.hash(path, type, size);
    }
}
