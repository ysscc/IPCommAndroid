package com.efounder.chat.event;

/**
 * Created by Marcello on 2018/12/11.
 */
public class FragmentChange {
    private int position;

    public FragmentChange(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
