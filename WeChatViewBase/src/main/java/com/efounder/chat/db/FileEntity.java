package com.efounder.chat.db;

import android.content.Context;
import android.util.Log;


import com.efounder.chat.http.download.DownloadInfo;
import com.efounder.chat.utils.SPUtils;


/**
 * Created by zhangshunyun on 2017/7/13.
 */

public class FileEntity {


    public static void saveEntityByAppId(DownloadInfo downloadInfo, String state, Context context) {
        String messageID = downloadInfo.messageID;

        String fileEntity = (String) SPUtils.get(context, messageID + "", "");
//        if (fileEntity == "") {
        fileEntity = downloadInfo.messageID + "," + downloadInfo.fileName + "," + downloadInfo.size + "," +
                downloadInfo.downloadSize + "," + downloadInfo.state + "," + downloadInfo.downloadUrl;
        SPUtils.put(context, messageID + "", fileEntity);
//        } else {
////            downloadSPUtils.remove(context,fileID+"");
//            fileEntity = downloadInfo.messageID + "," + downloadInfo.fileName + "," + downloadInfo.size + "," +
//                    downloadInfo.downloadSize + "," + downloadInfo.state + "," + downloadInfo.downloadUrl;
//            SPUtils.put(context, messageID + "", fileEntity);
//        }
//        Log.i("","");
        SPUtils.put(context, messageID + "State", state);
    }

    public static String getDownloadState(String messageID, Context context) {
        return (String) SPUtils.get(context, messageID + "State","");
    }

    public static void deleteEntity(DownloadInfo downloadInfo, Context context) {
        String messageID = downloadInfo.messageID;
        SPUtils.remove(context, messageID);
    }

    public static void saveItemPosition(String messageID, int position, Context context) {
        SPUtils.put(context, messageID, position);
    }

    public static String getEntity(DownloadInfo downloadInfo, Context context) {
        return (String) SPUtils.get(context, downloadInfo.messageID + "", "");
    }
}

