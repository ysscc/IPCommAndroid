package com.efounder.chat.model;

/**
 * 语音是否正在播放
 */
public class VoicePlayingEvent {
	private boolean isPlaying;



	public VoicePlayingEvent(boolean isPlaying) {
		this.isPlaying = isPlaying;
	}

	public boolean isPlaying() {
		return isPlaying;
	}

	public VoicePlayingEvent setPlaying(boolean playing) {
		isPlaying = playing;
		return this;
	}

}
