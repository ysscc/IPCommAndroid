package com.efounder.chat.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.efounder.chat.R;
import com.efounder.frame.baseui.EFFragment;

/**
 * Created by Will on 2016/11/18.
 */

public class ShakeSettingsFragment extends EFFragment implements View.OnClickListener{



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shake_settings,container,false);

        return view;
    }

    @Override
    public void onClick(View v) {
        // TODO: 2016/11/18  各设置项
    }
}
