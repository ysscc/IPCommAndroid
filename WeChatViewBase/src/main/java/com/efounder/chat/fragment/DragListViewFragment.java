package com.efounder.chat.fragment;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.adapter.CommonDragAdapter;
import com.efounder.frame.baseui.BaseFragment;
import com.efounder.imageselector.bean.Image;

import java.util.ArrayList;
import java.util.List;

/**
 * 可与哦拽list
 * Created by kongmeng on 2018/8/31.
 */

public class DragListViewFragment extends BaseFragment {

    private ImageView iv_add;
    private ImageView iv_del;
    private Button bn_del;
    private TextView title;

}
