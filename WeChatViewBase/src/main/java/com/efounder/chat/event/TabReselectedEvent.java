package com.efounder.chat.event;

import android.view.View;

import com.core.xml.StubObject;

/**
 * pageslidetabqqfragment OnTabSelectedListener
 * tablayout 被再次选中时的事件
 *
 * @author YQS
 */
public class TabReselectedEvent {
    private View view;
    private StubObject stubObject;

    public TabReselectedEvent(View view, StubObject stubObject) {
        this.view = view;
        this.stubObject = stubObject;
    }

    public View getView() {
        return view;
    }

    public TabReselectedEvent setView(View view) {
        this.view = view;
        return this;
    }

    public StubObject getStubObject() {
        return stubObject;
    }

    public TabReselectedEvent setStubObject(StubObject stubObject) {
        this.stubObject = stubObject;
        return this;
    }
}
