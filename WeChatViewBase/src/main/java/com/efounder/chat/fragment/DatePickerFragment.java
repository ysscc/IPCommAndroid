package com.efounder.chat.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import android.widget.DatePicker;
import java.util.Calendar;

/**
 * 打卡日期选择器
 * @author 张顺运
 */
public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    private String date;
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(),this,year,month,day){
            @Override
            protected void onStop() {
            }
        };
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        if(getActivity() instanceof DataCallBack){
            //将activity强转为DataCallBack
            DataCallBack dataCallBack = (DataCallBack) getActivity();
            String dayTmp = day<10? "0"+day:day+"";
            date = (month+1)<10?year +"-0"+ (month+1) + "-" + dayTmp:year +"-"+ (month+1) + "-" + dayTmp;
//            date = year +"年"+ (month+1) + "月" + day + "日";
            //调用activity的getData方法将数据传回activity显示
            dataCallBack.getData(date);
        }
    }

    public interface DataCallBack {
        void getData(String data);
    }
}
