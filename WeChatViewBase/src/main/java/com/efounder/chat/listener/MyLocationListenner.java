//package com.efounder.chat.listener;
//
//import android.util.Log;
//
//import com.baidu.location.BDLocation;
//import com.baidu.location.BDLocationListener;
//import com.baidu.mapapi.map.BaiduMap;
//import com.baidu.mapapi.map.BitmapDescriptorFactory;
//import com.baidu.mapapi.map.MapStatusUpdate;
//import com.baidu.mapapi.map.MapStatusUpdateFactory;
//import com.baidu.mapapi.map.MarkerOptions;
//import com.baidu.mapapi.map.OverlayOptions;
//import com.baidu.mapapi.model.LatLng;
//import com.baidu.mapapi.utils.CoordinateConverter;
//import com.efounder.chat.R;
//
//import org.greenrobot.eventbus.EventBus;
//
///**
//     * 监听函数，有新位置的时候，格式化成字符串，输出到屏幕中
//     */
//public class MyLocationListenner implements BDLocationListener {
//    private BaiduMap mBaiduMap;
//    private MapStatusUpdate myselfU;
//
//    public MyLocationListenner(BaiduMap map){
//        this.mBaiduMap = map;
//    }
//    @Override
//    public void onReceiveLocation(BDLocation location) {
//        if (location == null) {
//            return;
//        }
//        mBaiduMap.clear();
//        LatLng llA = new LatLng(location.getLatitude(), location.getLongitude());
//
//        LatLng ll = new LatLng(location.getLatitude() - 0.0002, location.getLongitude());
//        CoordinateConverter converter = new CoordinateConverter();//坐标转换工具类
//        converter.coord(ll);//设置源坐标数据
//        converter.from(CoordinateConverter.CoordType.COMMON);//设置源坐标类型
//        LatLng convertLatLng = converter.convert();
//        OverlayOptions myselfOOA = new MarkerOptions().position(convertLatLng).icon(BitmapDescriptorFactory
//                .fromResource(R.mipmap.icon_yourself_lication))
//                .zIndex(0).draggable(true);
//        mBaiduMap.addOverlay(myselfOOA);
////            BitmapDescriptor bitmap = BitmapDescriptorFactory
////    			    .fromResource(R.drawable.icon_yourself_lication);
////    			//构建MarkerOption，用于在地图上添加Marker
////    			OverlayOptions option = new MarkerOptions()
////    			    .position(convertLatLng)
////    			    .icon(bitmap).zIndex(0);
////    			//在地图上添加Marker，并显示
////    			mBaiduMap.addOverlay(option);
//        myselfU = MapStatusUpdateFactory.newLatLngZoom(convertLatLng, 17.0f);
//        mBaiduMap.animateMapStatus(myselfU);
//
//        //Receive Location
//        String curTime,curAddress,latitude,longtitude;
//        curAddress = location.getAddrStr();
//        StringBuffer sb = new StringBuffer(256);
//        sb.append("time : ");
//        curTime = location.getTime();
//        sb.append(curTime);
//        sb.append("\nerror code : ");
//        sb.append(location.getLocType());
//        sb.append("\nlatitude : ");
//        sb.append(location.getLatitude());
//        sb.append("\nlontitude : ");
//        sb.append(location.getLongitude());
//        sb.append("\nradius : ");
//        sb.append(location.getRadius());
//        if (location.getLocType() == BDLocation.TypeGpsLocation){// GPS定位结果
//            sb.append("\nspeed : ");
//            sb.append(location.getSpeed());// 单位：公里每小时
//            sb.append("\nsatellite : ");
//            sb.append(location.getSatelliteNumber());
//            sb.append("\nheight : ");
//            sb.append(location.getAltitude());// 单位：米
//            sb.append("\ndirection : ");
//            sb.append(location.getDirection());// 单位度
//            sb.append("\naddr : ");
//            sb.append(location.getAddrStr());
//            sb.append("\ndescribe : ");
//            sb.append("gps定位成功");
//
//        } else if (location.getLocType() == BDLocation.TypeNetWorkLocation){// 网络定位结果
//            sb.append("\naddr : ");
//            sb.append(location.getAddrStr());
//            //运营商信息
//            sb.append("\noperationers : ");
//            sb.append(location.getOperators());
//            sb.append("\ndescribe : ");
//            sb.append("网络定位成功");
//        } else if (location.getLocType() == BDLocation.TypeOffLineLocation) {// 离线定位结果
//            sb.append("\ndescribe : ");
//            sb.append("离线定位成功，离线定位结果也是有效的");
//        } else if (location.getLocType() == BDLocation.TypeServerError) {
//            sb.append("\ndescribe : ");
//            sb.append("服务端网络定位失败，可以反馈IMEI号和大体定位时间到loc-bugs@baidu.com，会有人追查原因");
//        } else if (location.getLocType() == BDLocation.TypeNetWorkException) {
//            sb.append("\ndescribe : ");
//            sb.append("网络不同导致定位失败，请检查网络是否通畅");
//        } else if (location.getLocType() == BDLocation.TypeCriteriaException) {
//            sb.append("\ndescribe : ");
//            sb.append("无法获取有效定位依据导致定位失败，一般是由于手机的原因，处于飞行模式下一般会造成这种结果，可以试着重启手机");
//        }
////        sb.append("\nlocationdescribe : ");
////        sb.append(location.getLocationDescribe());// 位置语义化信息
////        List<Poi> list = location.getPoiList();// POI数据
////        if (list != null) {
////            sb.append("\npoilist size = : ");
////            sb.append(list.size());
////            for (Poi p : list) {
////                sb.append("\npoi= : ");
////                sb.append(p.getId() + " " + p.getName() + " " + p.getRank());
////            }
////        }
//        Log.i("lcusky", sb.toString());
//        EventBus.getDefault().post(curTime +","+curAddress+","+location.getLatitude()+","+location.getLongitude());
//    }
//
//}