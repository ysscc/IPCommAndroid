//package com.efounder.chat.view;
//
//import android.content.Context;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.widget.ImageView;
//
//import com.efounder.chat.R;
//import com.efounder.chat.utils.ImageUtil;
//import com.nostra13.universalimageloader.core.DisplayImageOptions;
//import com.nostra13.universalimageloader.core.ImageLoader;
//
//import java.util.List;
//
///**
// * Created by cherise on 2016/10/10.
// */
//
//public class AvatarView {
//
//    private List<String> list;
//    private LayoutInflater inflater;
//    private View view;
//    private ImageView[] imageviews;
//    private ImageView imageView1, imageView2, imageView3, imageView4,
//            imageView5, imageView6;
//    private ImageLoader imageLoader;
//    private DisplayImageOptions options;
//    private Context mContext;
//    private Bitmap bitmap;
//
//
//    public AvatarView(Context context, List<String> avatarList) {
//        this.list = avatarList;
//        this.mContext = context;
//        imageviews = new ImageView[6];
//        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        // 初始化异步加载图片的类
////        imageLoader = ImageLoader.getInstance();
//        // 设置异步加载图片的配置信息
////        options = ImageUtil.getImageLoaderOptions(R.drawable.default_useravatar);
//        view = inflater.inflate(R.layout.avatar_groupimageviews, null);
//        if (avatarList.size() == 1) {
//            initView1();
//        } else {
//            initView2();
//        }
//
//    }
//
//    private void initView1() {
//        ImageView imageView = (ImageView) view.findViewById(R.id.oneImage);
//        String s = list.get(0);
//        if ("".equals(s)) {
//            s = "drawable://" + R.drawable.default_useravatar;
//        }
//
//        try {
//            Bitmap bitmap = imageLoader.loadImageSync(s, options);
//            Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.default_useravatar);
//            imageView.setImageBitmap(bitmap);
//            imageView.setVisibility(View.VISIBLE);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    private void initView2() {
//
//        imageView1 = (ImageView) view.findViewById(R.id.imageView1);
//        imageView2 = (ImageView) view.findViewById(R.id.imageView2);
//        imageView3 = (ImageView) view.findViewById(R.id.imageView3);
//        imageView4 = (ImageView) view.findViewById(R.id.imageView4);
//        imageView5 = (ImageView) view.findViewById(R.id.imageView5);
//        imageView6 = (ImageView) view.findViewById(R.id.imageView6);
//        imageviews[0] = imageView1;
//        imageviews[1] = imageView2;
//        imageviews[2] = imageView3;
//        imageviews[3] = imageView4;
//        imageviews[4] = imageView5;
//        imageviews[5] = imageView6;
//
//        if (list.size() == 2) {
//            //如果头像个数是2，单独处理
//            for (int i = 0; i < list.size(); i++) {
//                String s = list.get(i);
//                if ("".equals(s)) {
//                    s = "drawable://" + R.drawable.default_useravatar;
//                }
//
//                try {
//                    Bitmap x = imageLoader.loadImageSync(s, options);
//                    if (i == 1) {
//                        i = 5;
//                    }
//                    imageviews[i].setImageBitmap(bitmap);
//                    imageviews[i].setVisibility(View.VISIBLE);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
//        } else {
//            for (int i = 0; i < list.size(); i++) {
//
//                if (i < 6) {
//                    String s = list.get(i);
//                    if ("".equals(s)) {
//                        s = "drawable://" + R.drawable.default_useravatar;
//                    }
//
//                    try {
//                        Bitmap bitmap = imageLoader.loadImageSync(s, options);
//                        imageviews[i].setImageBitmap(bitmap);
//                        imageviews[i].setVisibility(View.VISIBLE);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        }
//
//
//    }
//
//    public View getGroupAvatarView() {
//        return view;
//    }
//}
