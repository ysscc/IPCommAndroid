package com.efounder.chat.publicnumber.fragment;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.efounder.chat.R;
import com.efounder.chat.publicnumber.adapter.AppAccountFormStateSearchAdapter;
import com.efounder.form.comp.shoppingcar.DividerItemDecoration;
import com.efounder.message.struct.IMStruct002;
import com.efounder.utils.ResStringUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by XinQing on 2016/12/26.
 * 表单状态查询Fragment（类似淘宝物流状态）
 */

public class AppAccountFormStateSearchFragment extends Fragment implements View.OnClickListener{

    private List<AppAccountFormStateSearchAdapter.DataItem> dataItems;
    private AppAccountFormStateSearchAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_app_account_form_state_search,container,false);
        IMStruct002 imStruct002 = (IMStruct002) getArguments().getSerializable("imStruct002");
        //订单编号
        EditText formNoEditText = (EditText) view.findViewById(R.id.form_no);
        formNoEditText.setText(getFormNo(imStruct002));
        //查询按钮
        Button searchButton = (Button) view.findViewById(R.id.button_search);
        searchButton.setOnClickListener(this);
        //RecycleView
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        initRecyclerView(recyclerView);

        return view;
    }

    /**
     * 初始化RecyclerView
     */
    private void initRecyclerView(RecyclerView recyclerView){

        //设置布局管理器
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        //设置adapter
        dataItems = new ArrayList<>();
        adapter = new AppAccountFormStateSearchAdapter(getContext(),dataItems);
        recyclerView.setAdapter(adapter);
        //设置Item增加、移除动画
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        //添加分割线
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
    }

    private String getFormNo(IMStruct002 imStruct002){
        String formNo = null;
        try {
            JSONObject jsonObject = new JSONObject(imStruct002.getMessage());
            String formNoRaw = jsonObject.getString("formInfo");
            formNo = formNoRaw.substring(3,formNoRaw.length());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return formNo;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_search){
            new AsyncTask<String,Integer,String>(){
                ProgressDialog progressDialog = new ProgressDialog(getContext());
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    progressDialog.setMessage(ResStringUtil.getString(R.string.wrchatview_loading));
                    progressDialog.show();
                }

                @Override
                protected String doInBackground(String... params) {
                    try {
                        Thread.sleep(1 * 1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    dataItems.clear();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    int i = 1;
                    //1
                    AppAccountFormStateSearchAdapter.DataItem dataItem = new AppAccountFormStateSearchAdapter.DataItem();
                    dataItem.setSelectionState(1);
                    dataItem.setTime(dateFormat.format(new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 5 * i))));
                    dataItem.setDescription("项目(中铁二局成兰指挥部)待收货");
                    dataItems.add(dataItem);
                    i ++;
                    //2
                    dataItem = new AppAccountFormStateSearchAdapter.DataItem();
                    dataItem.setSelectionState(0);
                    dataItem.setTime(dateFormat.format(new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 5 * i))));
                    dataItem.setDescription("正在配送途中（联系电话：18615207881）");
                    i ++;
                    dataItems.add(dataItem);
                    //3
                    dataItem = new AppAccountFormStateSearchAdapter.DataItem();
                    dataItem.setSelectionState(0);
                    dataItem.setTime(dateFormat.format(new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 5 * i))));
                    dataItem.setDescription("付油通知单已出单");
                    i ++;
                    dataItems.add(dataItem);
                    //4
                    dataItem = new AppAccountFormStateSearchAdapter.DataItem();
                    dataItem.setSelectionState(0);
                    dataItem.setTime(dateFormat.format(new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 5 * i))));
                    dataItem.setDescription("需求计划已接收，已安排配送（联系电话：18615367621）");
                    i ++;
                    dataItems.add(dataItem);
                    //5
                    dataItem = new AppAccountFormStateSearchAdapter.DataItem();
                    dataItem.setSelectionState(0);
                    dataItem.setTime(dateFormat.format(new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 5 * i))));
                    dataItem.setDescription("需求计划已提报");
                    i ++;
                    dataItems.add(dataItem);

                    return null;
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    if (progressDialog !=null && progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }
                    if (adapter != null){
                        adapter.notifyDataSetChanged();
                    }
                }
            }.execute();
        }
    }
}
