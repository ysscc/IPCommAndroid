package com.efounder.chat.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.efounder.chat.R;
import com.efounder.chat.fragment.LuckMoneyHistoryFragment;
import com.efounder.utils.ResStringUtil;
import com.marlonmafra.android.widget.SegmentedTab;

import java.util.ArrayList;
import java.util.List;

/**
 * 红包记录历史界面
 *
 * @author yqs 2018/08/07
 */

public class LuckMoneyHistoryActivity extends BaseActivity {

    private RelativeLayout tabsLl;
    private ImageView ivBack;
    private SegmentedTab segmentControl;
    private ViewPager viewPager;

    private String[] titles1 = new String[]{ResStringUtil.getString(R.string.wechatview_i_receive), ResStringUtil.getString(R.string.wechatview_i_send_out)};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wechatview_activity_luck_money_history);
        initView();
        initListener();
        initData();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public static void start(Context context) {
        Intent starter = new Intent(context, LuckMoneyHistoryActivity.class);
        context.startActivity(starter);
    }

    private void initView() {
        tabsLl = (RelativeLayout) findViewById(R.id.tabs_ll);
        ivBack = (ImageView) findViewById(R.id.iv_back);
        segmentControl = (SegmentedTab) findViewById(R.id.segment_control);
        viewPager = (ViewPager) findViewById(R.id.viewPager);

    }

    private void initListener() {

    }

    private void initData() {
        viewPager.setAdapter(new MyAdapter(getSupportFragmentManager()));


        List<String> titles = new ArrayList<>();
        for (int i = 0; i < titles1.length; i++) {
            titles.add(titles1[i]);
        }
        this.segmentControl.setupWithViewPager(this.viewPager);
        this.segmentControl.setup(titles);

    }

    public class MyAdapter extends FragmentPagerAdapter {


        public MyAdapter(FragmentManager fm) {
            super(fm);

        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return LuckMoneyHistoryFragment.newInstance("receive");
            } else if (position == 1) {
                return LuckMoneyHistoryFragment.newInstance("send");
            } else {
                return null;
            }

        }

        @Override
        public int getCount() {
            return titles1.length;
        }


    }

}
