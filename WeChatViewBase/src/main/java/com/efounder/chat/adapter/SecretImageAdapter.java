package com.efounder.chat.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.efounder.chat.R;
import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.imageloader.GlideImageLoader;

import java.util.List;

public class SecretImageAdapter extends BaseAdapter {

    private Context context;
    private List<String> urls;
    private OnImgRemovedListener onImgRemovedListener;

    public SecretImageAdapter(Context context, List<String> urls) {
        this.context = context;
        this.urls = urls;
    }

    @Override
    public int getCount() {
        return urls.size();
    }

    @Override
    public Object getItem(int position) {
        return urls.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.layout_wechat_item_secret_image, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.ivDelete = convertView.findViewById(R.id.iv_delete);
            viewHolder.ivImage = convertView.findViewById(R.id.iv_img);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (position == urls.size()-1){
            GlideImageLoader.getInstance().displayImage(context, viewHolder.ivImage, R.drawable.btn_add_img);
            viewHolder.ivDelete.setVisibility(View.INVISIBLE);
        }else {
            LXGlideImageLoader.getInstance().displayImage(context, viewHolder.ivImage, urls.get(position));
            viewHolder.ivDelete.setVisibility(View.VISIBLE);
        }
        viewHolder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position!=urls.size() && onImgRemovedListener != null) {
                    onImgRemovedListener.onImgRemoved(position);
                }
            }
        });
        return convertView;
    }

    public class ViewHolder{
        ImageView ivImage;
        ImageView ivDelete;
    }

    public interface OnImgRemovedListener{
        void onImgRemoved(int position);
    }

    public void setOnImgRemovedListener(OnImgRemovedListener onImgRemovedListener) {
        this.onImgRemovedListener = onImgRemovedListener;
    }
}
