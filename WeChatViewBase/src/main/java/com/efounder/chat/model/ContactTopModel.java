package com.efounder.chat.model;

import com.core.xml.StubObject;

import java.util.Hashtable;

/**
 * 通讯录菜单配置
 * Created by yqs on 2017/6/13.
 */

public class ContactTopModel {
private String cName;
    private String image;
    private StubObject stubObject;
    private String gnbh;//功能编号
    private Hashtable hashtable;
    private int badgeNum;//角标
    public ContactTopModel() {
    }

    public ContactTopModel(String cName, String image, StubObject stubObject, String gnbh, int count) {
        this.cName = cName;
        this.image = image;
        this.stubObject = stubObject;
        this.gnbh = gnbh;
        this.badgeNum = count;
    }

    public int getBadgeNum() {
        return badgeNum;
    }

    public void setBadgeNum(int badgeNum) {
        this.badgeNum = badgeNum;
    }

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public StubObject getStubObject() {
        return stubObject;
    }

    public void setStubObject(StubObject stubObject) {
        this.stubObject = stubObject;
    }

    public String getGnbh() {
        return gnbh;
    }

    public void setGnbh(String gnbh) {
        this.gnbh = gnbh;
    }

    public Hashtable getHashtable() {
        return hashtable;
    }

    public void setHashtable(Hashtable hashtable) {
        this.hashtable = hashtable;
    }
}
