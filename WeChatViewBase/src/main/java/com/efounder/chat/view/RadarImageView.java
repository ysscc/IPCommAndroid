package com.efounder.chat.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.efounder.chat.R;

/**
 * Created by Tian Yu on 2016/9/9.
 */

public class RadarImageView extends ImageView {
    private int w, h;// 获取控件宽高
    private Matrix matrix;
    private int degrees;

    private IScanningListener iScanningListener;//扫描时监听回调接口
    public void setScanningListener(IScanningListener iScanningListener) {
        this.iScanningListener = iScanningListener;
    }
    public interface IScanningListener {
        //正在扫描（此时还没有扫描完毕）时回调
        void onScanning(int position, float scanAngle);
        //扫描成功时回调
        void onScanSuccess();
    }

    private Handler mHandler = new Handler();
    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            degrees+=5;
            matrix.postRotate(degrees, w / 2, h / 2);
            RadarImageView.this.invalidate();// 重绘
            mHandler.postDelayed(mRunnable,50);
        }
    };

    public RadarImageView(Context context) {
        this(context, null);
    }

    public RadarImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RadarImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        w = getMeasuredWidth();//获取view的宽度
        h = getMeasuredHeight();//获取view的高度
    }

    /**
     * 初始化
     */
    private void init() {
        setBackgroundResource(R.drawable.radar_addfriend_bg);
        matrix = new Matrix();
        mHandler.postDelayed(mRunnable,500);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.setMatrix(matrix);
        super.onDraw(canvas);
        matrix.reset();
    }
}
