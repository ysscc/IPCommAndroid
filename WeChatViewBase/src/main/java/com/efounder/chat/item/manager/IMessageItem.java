package com.efounder.chat.item.manager;

import android.view.View;

import com.efounder.frame.ViewSize;
import com.efounder.message.struct.IMStruct002;

/**
 * 
 * @author fengshuquan
 *
 */
public interface IMessageItem {
	public void setIMStruct002(IMStruct002 message);
	/** 显示内容View **/
	public View messageView();
	/** 显示内容View的Size **/
	public ViewSize messageViewSize();
	/**是否占用**/
	public boolean getIsInUse();
	public void setIsInUse(boolean isInUse);
	/**复用之前调用该方法**/
	public void prepareForReuse();
}
