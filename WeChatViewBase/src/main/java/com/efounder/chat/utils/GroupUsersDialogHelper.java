package com.efounder.chat.utils;

//import android.app.AlertDialog;

import android.content.Context;
import androidx.appcompat.app.AlertDialog;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.db.WeChatDBManager;
import com.efounder.chat.fragment.GroupUserFragment;
import com.efounder.chat.http.GetHttpUtil;
import com.efounder.mobilecomps.contacts.User;
import com.efounder.util.ToastUtil;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * 消息长按弹窗
 * Created by cherise on 2016/10/15.
 */

public class GroupUsersDialogHelper {

    private final static String TAG = "GroupUsersDialogHelper";
    private int groupId;
    private Context context;
    private GroupUserFragment.UpdateDataCallBack updateCallBack;


    public GroupUsersDialogHelper(Context context, int groupId, GroupUserFragment.UpdateDataCallBack callBack) {
        this.groupId = groupId;
        this.context = context;
        this.updateCallBack = callBack;
    }

    public void showDialog(final User user, final boolean isCreator, final List<User> sourceDataList) {

        final AlertDialog dlg = new AlertDialog.Builder(context).create();
        dlg.show();
        Window window = dlg.getWindow();
        window.setContentView(R.layout.alertdialog);
        // 为确认按钮添加事件,执行退出应用操作

        //
        if (isCreator) {

            TextView content1 = (TextView) window.findViewById(R.id.tv_content1);
            View lineView = window.findViewById(R.id.line1);
            lineView.setVisibility(View.VISIBLE);
            content1.setVisibility(View.VISIBLE);
            if (user.getGroupUserType() == User.GROUPMEMBER) {
                content1.setText(R.string.chat_set_admin);
            } else {
                content1.setText(R.string.chat_cancel_set_admin);
            }
            content1.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    if (user.getGroupUserType() == User.GROUPMEMBER) {
                        setGroupAdmin(user, dlg);//设为群管理员
                    } else {
                        cancelGroupAdmin(user, dlg);//取消设置为管理员
                    }

                }


            });
            LinearLayout linearLayout2 = (LinearLayout) window.findViewById(R.id.ll_content2);
            linearLayout2.setVisibility(View.VISIBLE);
            TextView content2 = (TextView) window.findViewById(R.id.tv_content2);
            setRemoveLayout(content2, user, sourceDataList, dlg);
        } else {
            TextView content1 = (TextView) window.findViewById(R.id.tv_content1);
            content1.setVisibility(View.VISIBLE);
            setRemoveLayout(content1, user, sourceDataList, dlg);
            LinearLayout linearLayout2 = (LinearLayout) window.findViewById(R.id.ll_content2);
            linearLayout2.setVisibility(View.GONE);

        }


    }

    /**
     * 设置移除群成员的 view
     *
     * @param
     */
    private void setRemoveLayout(TextView content3, final User user, final List<User> sourceDataList, final AlertDialog dlg) {

        content3.setText(R.string.chat_remove_group_people);
        content3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeGroupUser(user, sourceDataList, dlg);

            }
        });
    }

    private void cancelGroupAdmin(final User user, final AlertDialog dlg) {
        try {
//            GetHttpUtil.cancelGroupAdmin(context, groupId, user.getId(), new GetHttpUtil.GroupAdminCallBack() {
//                @Override
//                public void setGroupAdminResult(boolean isSuccess) {
//                    dlg.dismiss();
//                    if (!isSuccess) {
//
//                        ToastUtil.showToast(context, "取消设为群管理员失败");
//                        return;
//                    }
//                    List<User> list = new ArrayList<User>();
//                    user.setGroupUserType(User.GROUPMEMBER);
//                    list.add(user);
//                    weChatDBManager.insertorUpdateMyGroupUser(groupId, list);
//                    if (updateCallBack != null) {
//                        updateCallBack.updateDataNotication();
//                    }
//                    // adapter.notifyDataSetChanged();
//                    ToastUtil.showToast(context, "取消设为群管理员成功");
//                }
//            });

            GetHttpUtil.cancelGroupAdmin(context, groupId, user.getId(), new GetHttpUtil.ReqCallBack() {
                @Override
                public void onReqSuccess(Object result) {
                    dlg.dismiss();

                    List<User> list = new ArrayList<User>();
                    user.setGroupUserType(User.GROUPMEMBER);
                    list.add(user);
                    WeChatDBManager.getInstance().insertorUpdateMyGroupUser(groupId, list);
                    if (updateCallBack != null) {
                        updateCallBack.updateDataNotication();
                    }
                    // adapter.notifyDataSetChanged();
                    ToastUtil.showToast(context.getApplicationContext(), R.string.chat_cancel_set_admin_success);
                }

                @Override
                public void onReqFailed(String errorMsg) {
                    dlg.dismiss();

                    ToastUtil.showToast(context, R.string.chat_cancel_set_admin_fail);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setGroupAdmin(final User user, final AlertDialog dlg) {
        try {
//            GetHttpUtil.setGroupAdmin(context, groupId, user.getId(), new GetHttpUtil.GroupAdminCallBack() {
//                @Override
//                public void setGroupAdminResult(boolean isSuccess) {
//                    dlg.dismiss();
//                    if (!isSuccess) {
//                        ToastUtil.showToast(context, "设为群管理员失败");
//                        return;
//                    }
//                    List<User> list = new ArrayList<User>();
//                    user.setGroupUserType(User.GROUPMANAGER);
//                    user.setSortLetters("☆");
//                    list.add(user);
//                    weChatDBManager.insertorUpdateMyGroupUser(groupId, list);
//                    if (updateCallBack != null) {
//                        updateCallBack.updateDataNotication();
//                    }
//                    // adapter.notifyDataSetChanged();
//                    ToastUtil.showToast(context, "设为群管理员成功");
//                }
//            });
            GetHttpUtil.setGroupAdmin(context, groupId, user.getId(), new GetHttpUtil.ReqCallBack<Boolean>() {
                @Override
                public void onReqSuccess(Boolean result) {
                    dlg.dismiss();

                    List<User> list = new ArrayList<User>();
                    user.setGroupUserType(User.GROUPMANAGER);
                    user.setSortLetters("☆");
                    list.add(user);
                    WeChatDBManager.getInstance().insertorUpdateMyGroupUser(groupId, list);
                    if (updateCallBack != null) {
                        updateCallBack.updateDataNotication();
                    }
                    ToastUtil.showToast(context, R.string.chat_set_admin_success);

                }

                @Override
                public void onReqFailed(String errorMsg) {
                    dlg.dismiss();
                    ToastUtil.showToast(context, R.string.chat_set_admin_fail);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void removeGroupUser(final User user, final List<User> list, final AlertDialog dlg) {
        try {
            GetHttpUtil.AdminDeleteGroupUser(context, groupId
                    , user.getId(), new GetHttpUtil.ReqCallBack<Boolean>() {
                        @Override
                        public void onReqSuccess(Boolean result) {
                            dlg.dismiss();
                            WeChatDBManager.getInstance().quitGroup(user.getId(), groupId);
                            list.remove(user);
                            if (updateCallBack != null) {
                                updateCallBack.updateDataNotication();
                            }
                            ToastUtil.showToast(context, R.string.chat_remove_success);
                        }

                        @Override
                        public void onReqFailed(String errorMsg) {
                            dlg.dismiss();
                            ToastUtil.showToast(context, R.string.chat_remove_fail);
                        }
                    });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String getNowTime() {
        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMddHHmmssSS");
        return dateFormat.format(date);
    }


}
