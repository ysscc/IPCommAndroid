//package com.efounder.chat.manager;
//
//import android.content.Intent;
//import android.net.Uri;
//
//import com.utilcode.util.LogUtils;
//
///**
// * url schemeManager 接续scheme数据
// */
//public class UrlSchemeManager {
//
//    public  static boolean hasUrlScheme(Intent intent) {
//
//
//        String scheme = intent.getScheme();
//        String dataString = intent.getDataString();
//        Uri uri = intent.getData();
//        LogUtils.i("", scheme, dataString);
//        if (uri != null) {
//            /**
//             * caishilive://caishi:8080/loadtooldetail?tool_id=100
//             * caishilive：即Scheme 该Scheme协议名称
//             * caishi：即Host,代表Scheme作用于哪个地址域
//             * 8080：即port，代表端口号
//             * loadtooldetail：即path，代表打开的页面
//             * tool_id：即query，代表传递的参数
//
//             */
//
//            //完整的url信息
//            String url = uri.toString();
//            //scheme部分
//            String schemes = uri.getScheme();
//            //host部分
//            String host = uri.getHost();
//            //port部分
//            int port = uri.getPort();
//            //访问路径
//            String path = uri.getPath();
//            //编码路径
//            String path1 = uri.getEncodedPath();
//            //query部分
//            String queryString = uri.getQuery();
//            //获取参数值
//            String systemInfo = uri.getQueryParameter("system");
//            String id = uri.getQueryParameter("id");
//            return  true;
//
//        }
//        return false;
//    }
//
//
//}
