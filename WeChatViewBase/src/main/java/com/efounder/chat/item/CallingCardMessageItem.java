package com.efounder.chat.item;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.efounder.chat.R;
import com.efounder.chat.item.manager.IMessageItem;
import com.efounder.frame.ViewSize;
import com.efounder.message.struct.IMStruct002;

/**
 * 
 * @author lch
 *  名片
 *
 */
public class CallingCardMessageItem extends LinearLayout  implements IMessageItem {
	private Context mContext;
	@SuppressLint("ResourceAsColor") public CallingCardMessageItem(Context context) {
		super(context);
		mContext = context;
		LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
		LinearLayout callCardLinearLayout = (LinearLayout) inflate.inflate(R.layout.chat_item_callingcard, this);
		LinearLayout.LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.MATCH_PARENT);
		this.setLayoutParams(lp);
		//this.setBackgroundColor(R.color.black);
		//LinearLayout.LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.MATCH_PARENT);
		//this.addView(callCardLinearLayout,layoutParams);
		this.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Toast.makeText(mContext, R.string.wechatview_click_card, 500).show();
			}
		});
	}

	@Override
	public View messageView() {
		return this;
	}

	@Override
	public ViewSize messageViewSize() {
		return null;
	}

	@Override
	public void setIMStruct002(IMStruct002 message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean getIsInUse() {
		return this.isShown();
	}

	@Override
	public void setIsInUse(boolean isInUse) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void prepareForReuse() {
		
	}
}
