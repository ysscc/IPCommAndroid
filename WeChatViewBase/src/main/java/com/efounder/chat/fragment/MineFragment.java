//package com.efounder.chat.fragment;
//
//import android.annotation.SuppressLint;
//import android.content.Intent;
//import android.os.Bundle;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.efounder.baseui.BaseFragment;
//import com.efounder.chat.R;
//import com.efounder.chat.activity.MyUserInfoActivity;
//import com.efounder.chat.db.WeChatDBManager;
//import com.efounder.chat.utils.ImageUtil;
//import com.efounder.constant.EnvironmentVariable;
//import com.efounder.mobilecomps.contacts.User;
//import com.nostra13.universalimageloader.core.DisplayImageOptions;
//import com.nostra13.universalimageloader.core.ImageLoader;
//
//@SuppressLint("SdCardPath")
//public class MineFragment extends BaseFragment {
//
//
//    private String avatar = "";
//    private ImageView iv_avatar;
//    private TextView tv_name;
//    private TextView tv_fxid;
//    private String fxid;
//    private String nick;
//    private User user;
//    private View rootView;
//    private ImageLoader imageLoader;
//    private DisplayImageOptions options;
//
//    @Override
//    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        rootView = inflater.inflate(R.layout.fragment_profile, container, false);
//        initImageLoader();
//        user = WeChatDBManager.getInstance().
//                getOneUserById(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)));
//
//        RelativeLayout re_myinfo = (RelativeLayout) rootView.findViewById(
//                R.id.re_myinfo);
//        re_myinfo.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getActivity(), MyUserInfoActivity.class);
//                intent.putExtra("id", user.getId());
//                startActivity(intent);
//            }
//
//        });
//        RelativeLayout re_setting = (RelativeLayout) rootView.findViewById(
//                R.id.re_setting);
//        re_setting.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                //  startActivity(new Intent(getActivity(), SettingActivity.class));
//            }
//
//        });
//
//
//        fxid = user.getName();//帐号
//
//        avatar = user.getAvatar();
//        iv_avatar = (ImageView) re_myinfo.findViewById(R.id.iv_avatar);
//        tv_name = (TextView) re_myinfo.findViewById(R.id.tv_name);
//        tv_fxid = (TextView) re_myinfo.findViewById(R.id.tv_fxid);
//        tv_name.setText(user.getNickName());
////        if (fxid != null && !fxid.equals("")) {
////            tv_fxid.setText("微信号:" + fxid);
////
////        } else {
////            tv_fxid.setText("微信号：未设置");
////        }
////        if (user != null) {
////            tv_name.setText(user.getNickName());
////        }
//
//        return rootView;
//    }
//
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        user = WeChatDBManager.getInstance().
//                getOneUserById(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)));
//        avatar = user.getAvatar();
//        tv_name.setText(user.getNickName());
//        if (fxid != null && !fxid.equals("")) {
//            tv_fxid.setText("帐号:" + fxid);
//
//        } else {
//            tv_fxid.setText("帐号：未设置");
//        }
//        if (user != null) {
//            tv_name.setText(user.getNickName());
//        }
//        if (avatar!= null && avatar.contains("http")){
//            imageLoader.displayImage(avatar, iv_avatar, options);
//        }else{
//            imageLoader.displayImage("", iv_avatar, options);
//        }
//
//
//    }
//
//    /**
//     * 异步加载头像
//     */
//    public void initImageLoader() {
//        // 初始化异步加载图片的类
//        imageLoader = ImageLoader.getInstance();
//        // 设置异步加载图片的配置信息
//        options = ImageUtil.getImageLoaderOptions(R.drawable.default_user_avatar);
//
//    }
//}
