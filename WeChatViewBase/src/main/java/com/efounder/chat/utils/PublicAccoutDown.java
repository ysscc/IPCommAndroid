//package com.efounder.chat.utils;
//
//import android.content.Context;
//import android.content.Intent;
//import android.os.Handler;
//import android.os.Message;
//import android.widget.Toast;
//
//import com.efounder.utils.ZipUtil;
//import com.zhuiji7.filedownloader.download.DownLoadListener;
//import com.zhuiji7.filedownloader.download.DownLoadManager;
//import com.zhuiji7.filedownloader.download.DownLoadService;
//import com.zhuiji7.filedownloader.download.TaskInfo;
//import com.zhuiji7.filedownloader.download.dbcontrol.FileHelper;
//import com.zhuiji7.filedownloader.download.dbcontrol.bean.SQLDownLoadInfo;
//
//import java.io.File;
//import java.util.Timer;
//import java.util.TimerTask;
//
//
//
//
//
///**
// * Created by lch on 2016/8/25 0025.
// */
//
//public class PublicAccoutDown {
//
//
//    private String publicAccoutDownUrl;
//    private Context mContext;
//    private String TAG = "PublicAccoutDown";
//    private PublicAccoutDownInterface publicAccoutDownInterface;
//    /*使用DownLoadManager时只能通过DownLoadService.getDownLoadManager()的方式来获取下载管理器，不能通过new DownLoadManager()的方式创建下载管理器*/
//    private DownLoadManager manager;
//
//    private static float lastScale,nowScale;
//    private static boolean isPaused = false;
//    private static String fileName;
//    TaskInfo info = new TaskInfo();
//
//
//    public PublicAccoutDown(Context context) {
//        mContext = context;
//
//    }
//
//    public void start(String _publicAccoutDownUrl, PublicAccoutDownInterface _publicAccoutDownInterface) {
//
//
//        this.publicAccoutDownUrl = _publicAccoutDownUrl;
//        this.publicAccoutDownInterface = _publicAccoutDownInterface;
//
//        fileName = publicAccoutDownUrl.substring(publicAccoutDownUrl.lastIndexOf("/") + 1);
//
//        mContext.startService(new Intent(mContext, DownLoadService.class));
//        //需要延时下，等待启动DownloadService
//        downHandler.sendEmptyMessageDelayed(1, 50);
//    }
//    private  Handler downHandler = new Handler() {
//        @Override
//        public void handleMessage(Message msg) {
//            super.handleMessage(msg);
//		            /*获取下载管理器*/
//            manager = DownLoadService.getDownLoadManager();
//		            /*设置用户ID，客户端切换用户时可以显示相应用户的下载任务*/
//
//            manager.changeUser("PublicAccount");
//
//            /*断点续传需要服务器的支持，设置该项时要先确保服务器支持断点续传功能*/
//            manager.setSupportBreakpoint(true);
//
//
//
//            /*服务器一般会有个区分不同文件的唯一ID，用以处理文件重名的情况*/
//            info.setFileName(fileName);
//            info.setTaskID(fileName);
//
//
//            //info.setOnDownloading(true);
//             /*将任务添加到下载队列，下载器会自动开始下载*/
//            int i = manager.addTask(fileName, publicAccoutDownUrl, fileName);
//
//            final Timer timerFresh = new Timer();
//            timerFresh.schedule(new TimerTask() {
//                public void run() {
//                    if(lastScale == nowScale&&!isPaused&&nowScale!=0){
//
//                        downErrorHandler.sendEmptyMessage(0);
//                       // Toast.makeText(mContext, "网络不稳定", 200).show();
//                    }
//                    nowScale = lastScale;
//                    if((int) (lastScale*100)==100)
//                        timerFresh.cancel();
//                }
//            }, 1000,10000);
//
//            manager.setSingleTaskListener(fileName, new DownLoadListener() {
//                @Override
//                public void onStart(SQLDownLoadInfo sqlDownLoadInfo) {
//                   // checkAppVersionUtilInterface.startDown(0);
//                }
//
//                @Override
//                public void onProgress(SQLDownLoadInfo sqlDownLoadInfo, boolean isSupportBreakpoint) {
//                    info.setDownFileSize(sqlDownLoadInfo.getDownloadSize());
//                    info.setFileSize(sqlDownLoadInfo.getFileSize());
//
//                    float lastScale = (float) sqlDownLoadInfo.getDownloadSize() / sqlDownLoadInfo.getFileSize();
//
//
//                    publicAccoutDownInterface.updateProgress(lastScale);
//                }
//
//                @Override
//                public void onStop(SQLDownLoadInfo sqlDownLoadInfo, boolean isSupportBreakpoint) {
//                    publicAccoutDownInterface.stop();
//
//                }
//
//                @Override
//                public void onError(SQLDownLoadInfo sqlDownLoadInfo) {
//                    publicAccoutDownInterface.error();
//                    manager.deleteTask(fileName);
//                }
//
//                @Override
//                public void onSuccess(SQLDownLoadInfo sqlDownLoadInfo) {
//                    mContext.stopService(new Intent(mContext, DownLoadService.class));
//                    final String TEMP_FILEPATH = FileHelper.getFileDefaultPath();
//                    String filePath = TEMP_FILEPATH + "/(" + FileHelper.filterIDChars(sqlDownLoadInfo.getFileName()) + ")" + sqlDownLoadInfo.getFileName();
//                    publicAccoutDownInterface.downloadOver(filePath);
//                }
//            });
//            //如果已经下载完毕
//            if (i == -1) {
//
//                final String TEMP_FILEPATH = FileHelper.getFileDefaultPath();
//                String filePath = TEMP_FILEPATH + "/(" + FileHelper.filterIDChars(fileName) + ")" + fileName;
//                publicAccoutDownInterface.downloadOver(filePath);
//            } else {
//
//                info.setOnDownloading(true);
//                manager.startTask(fileName);
//
//            }
//            publicAccoutDownInterface.startDown(((float) info.getDownFileSize() / info.getFileSize()) * 100);
//            }
//
//    };
//    public void stopTaskID(){
//        info.setOnDownloading(false);
//        manager.stopTask(fileName);
//        isPaused = true;
//    }
//    public void startTaskID(){
//        info.setOnDownloading(true);
//        manager.startTask(fileName);
//        isPaused = false;
//    }
//
//    /**
//     * 解压文件
//     */
//    public void unzipAndEncryptFile(final String filePath,final String  taskfilePath){
//
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//
//                FileHelper.copyFile(filePath, taskfilePath);
//
//                //删除原有的解压缩后的文件
//                //deleteUnzipRes();
//                ZipUtil zUtil = new ZipUtil();
//                boolean issuccessZip ;
//
//
//
//                //将更新的zip解压到临时目录
//                String taskbasefolderUrl = taskfilePath.substring(0,taskfilePath.lastIndexOf("/")+1);
//                File taskbasefolder =  new File(taskbasefolderUrl) ;
//                if(!taskbasefolder.exists()) {
//                    try {
//                        taskbasefolder.mkdir();
//                    }catch (Exception e){
//                        e.printStackTrace();
//                    }
//
//                }
//                issuccessZip = zUtil.unZip(taskfilePath, taskbasefolderUrl);
//
//
//                if(issuccessZip){
//                    //资源文件下载并处理完毕 ,添加标志文件
//                    File file = new File(taskfilePath+"/"+"0.0.1");
//                    if(!file.exists()) {
//                        try {
//                            file.createNewFile();
//                        }catch (Exception e){
//
//                        }
//
//                    }
//                    publicAccoutDownInterface.unZIPOVER();
//                }
//
//            }
//        }).start();
//
//    }
//    private  Handler downErrorHandler = new Handler(){
//        @Override
//        public void handleMessage(Message msg) {
//            super.handleMessage(msg);
//            Toast.makeText(mContext, "网络不稳定。", 200).show();
//
//        }
//    };
//}
//
//
//
