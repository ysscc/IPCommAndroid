package com.efounder.chat.activity;

import android.os.Bundle;
import androidx.annotation.Nullable;

import com.core.xml.StubObject;
import com.efounder.chat.R;
import com.efounder.chat.db.WeChatDBManager;
import com.efounder.chat.model.Group;
import com.efounder.chat.model.ShareBean;
import com.efounder.chat.struct.StructFactory;
import com.efounder.chat.widget.ShareNameCardDialog;
import com.efounder.message.manager.JFMessageManager;
import com.efounder.message.struct.IMStruct002;
import com.efounder.mobilecomps.contacts.User;
import com.efounder.util.AppContext;
import com.efounder.util.ToastUtil;
import com.efounder.utils.ResStringUtil;

/**
 * 分享名片（好友名片，群名片）
 *
 * @author yqs
 * 2018/10/15
 */

public class ShareNameCardActivity extends SelectBaseActivity {

    private int toUserId;
    private byte chatType;

    //是否是从JFuserinfoactivity好友详情界面跳过来
    private boolean isFromUserInfo;
    private int shareUserId;
    /**
     * 判断是从群名片跳转分享还是从聊天界面跳转分享
     */
    private String shareAddress;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initIntentData();
        setBaseTitle(ResStringUtil.getString(R.string.wechatview_share_name_card_title));
    }

    private void initIntentData() {
        //hashtable.put("toUserId", String.valueOf(currentChatUserId));
        //hashtable.put("chatType", String.valueOf(chatType));
        if (getIntent().hasExtra("id")) {
            isFromUserInfo = true;
            shareUserId = getIntent().getIntExtra("id", 0);
            //判断是从群名片跳转分享还是从聊天界面跳转分享
            shareAddress = getIntent().getStringExtra("shareAddress");
        } else {
            //聊天界面菜单跳过来
            StubObject mStubObject = (StubObject) getIntent().getSerializableExtra("stubObject");
            toUserId = Integer.valueOf((String) mStubObject.getString("toUserId", "0"));
            chatType = Byte.valueOf((String) mStubObject.getString("chatType", "0"));

        }
    }

    @Override
    public void completeSelected(ShareBean shareBean) {

        if (isFromUserInfo) {
            //群分享设置分享联系人
            if (shareAddress != null && shareAddress.equals("group_setting")) {
                IMStruct002 imStruct002 = createShareGroupMessage(shareUserId);
                if (imStruct002 != null) {
                    imStruct002.setToUserId(shareBean.getChatId());
                    imStruct002.setToUserType((byte) shareBean.getChatType());
                    showGroupTipDialog(imStruct002, shareBean);
                } else {
                    ToastUtil.showToast(AppContext.getInstance(), R.string.common_text_app_is_wrong);
                }
            } else {
                User user = WeChatDBManager.getInstance().getOneUserById(shareUserId);
                IMStruct002 imStruct002 = StructFactory.getInstance()
                        .createShareNameCardStruct(user.getNickName(), user.getAvatar(), user.getId(), true);
                //消息发送给选择的这个人
                imStruct002.setToUserId(shareBean.getChatId());
                //聊天类型设置为你要分享给的那个人
                imStruct002.setToUserType((byte) shareBean.getChatType());
                showShareOneUserTipDialog(imStruct002, shareBean);
            }

        } else {
            IMStruct002 imStruct002 = createShareMessage(shareBean);
            if (imStruct002 != null) {
                imStruct002.setToUserId(toUserId);
                imStruct002.setToUserType(chatType);
                showTipDialog(imStruct002, shareBean);
            } else {
                ToastUtil.showToast(AppContext.getInstance(), R.string.common_text_app_is_wrong);
            }

        }

    }

    /**
     * 分享一个人
     *
     * @param imStruct002
     * @param shareBean
     */
    private void showShareOneUserTipDialog(final IMStruct002 imStruct002, ShareBean shareBean) {
        String titleUserName = null;
        String cardUserName = null;
        String avatarString = null;
        String info = null;

        //显示在里面的数据（被分享人） 1分享给2  1是被分享人
        User user = WeChatDBManager.getInstance().getOneFriendById(shareUserId);
        avatarString = user.getAvatar();
        cardUserName = user.getNickName();
        info = ResStringUtil.getString(R.string.im_chat_number) + ":" + user.getId();

        //这是获取dialog 的标题（要分享给的人 2）
        if (shareBean.getChatType() == StructFactory.TO_USER_TYPE_PERSONAL) {
            User user1 = WeChatDBManager.getInstance().getOneFriendById(imStruct002.getToUserId());
            titleUserName = user1.getReMark();
        } else {
            Group group = WeChatDBManager.getInstance().getGroup(imStruct002.getToUserId(), false);
            titleUserName = group.getGroupName();
        }
        ShareNameCardDialog shareNameCardDialog = new ShareNameCardDialog(this, new ShareNameCardDialog.OnEnterClick() {
            @Override
            public void buttonClick(boolean isSend) {
                if (isSend) {
                    JFMessageManager.getInstance().sendMessage(imStruct002);
                    ToastUtil.showToast(AppContext.getInstance(), R.string.chat_share_success);
                    finish();
                }
            }
        });
        shareNameCardDialog.setTitle(ResStringUtil.getString(R.string.chat_send_to) + titleUserName);
        shareNameCardDialog.showAvatar(avatarString);
        shareNameCardDialog.setUserName(cardUserName);
        shareNameCardDialog.setInfo(info);
        shareNameCardDialog.show();
    }

    @Override
    public boolean showPublicNumberUser() {
        return false;
    }


    /**
     * 显示提示dialog
     *
     * @param imStruct002
     */
    private void showTipDialog(final IMStruct002 imStruct002, ShareBean shareBean) {
        String titleUserName = null;
        String cardUserName = null;
        String avatarString = null;
        String info = null;
        //显示在里面的数据
        if (shareBean.getChatType() == StructFactory.TO_USER_TYPE_PERSONAL) {
            User user = WeChatDBManager.getInstance().getOneFriendById(shareBean.getChatId());
            avatarString = user.getAvatar();
            cardUserName = user.getNickName();
            info = ResStringUtil.getString(R.string.im_chat_number) + ":" + user.getId();
        } else {
            Group group = WeChatDBManager.getInstance().getGroup(shareBean.getChatId(), false);
            avatarString = group.getAvatar();
            cardUserName = group.getGroupName();

            User createUser = WeChatDBManager.getInstance().getOneUserById(group.getCreateId());
            info = ResStringUtil.getString(R.string.wechatview_create_user_name) + ":" + createUser.getNickName();
        }
        //这是获取dialog 的标题
        if (chatType == StructFactory.TO_USER_TYPE_PERSONAL) {
            User user = WeChatDBManager.getInstance().getOneFriendById(toUserId);
            titleUserName = user.getReMark();

        } else {
            Group group = WeChatDBManager.getInstance().getGroup(toUserId, false);
            titleUserName = group.getGroupName();
        }
        ShareNameCardDialog shareNameCardDialog = new ShareNameCardDialog(this, new ShareNameCardDialog.OnEnterClick() {
            @Override
            public void buttonClick(boolean isSend) {
                if (isSend) {
                    JFMessageManager.getInstance().sendMessage(imStruct002);
                    finish();
                }
            }
        });
        shareNameCardDialog.setTitle(ResStringUtil.getString(R.string.chat_send_to) + titleUserName);
        shareNameCardDialog.showAvatar(avatarString);
        shareNameCardDialog.setUserName(cardUserName);
        shareNameCardDialog.setInfo(info);
        shareNameCardDialog.show();
    }

    /**
     * 显示提示dialog
     * 从群设置进来分享的名片提示
     *
     * @param imStruct002
     */
    private void showGroupTipDialog(final IMStruct002 imStruct002, ShareBean shareBean) {
        String titleUserName = null;
        String cardUserName = null;
        String avatarString = null;
        String info = null;
        //显示在里面的数据

        Group group = WeChatDBManager.getInstance().getGroup(shareUserId, false);
        avatarString = group.getAvatar();
        cardUserName = group.getGroupName();
        User createUser = WeChatDBManager.getInstance().getOneUserById(group.getCreateId());
        info = ResStringUtil.getString(R.string.wechatview_create_user_name) + ":" + createUser.getNickName();
        //这是获取dialog 的标题
        if (shareBean.getChatType() == StructFactory.TO_USER_TYPE_PERSONAL) {
            //User user = WeChatDBManager.getInstance().getOneFriendById(shareBean.getChatId());
            //titleUserName = user.getNickName();
            titleUserName = shareBean.getName();
        } else {
            Group group1 = WeChatDBManager.getInstance().getGroup(shareBean.getChatId(), false);
            titleUserName = group1.getGroupName();
        }
        ShareNameCardDialog shareNameCardDialog = new ShareNameCardDialog(this, new ShareNameCardDialog.OnEnterClick() {
            @Override
            public void buttonClick(boolean isSend) {
                if (isSend) {
                    JFMessageManager.getInstance().sendMessage(imStruct002);
                    finish();
                }
            }
        });
        shareNameCardDialog.setTitle(ResStringUtil.getString(R.string.chat_send_to) + titleUserName);
        shareNameCardDialog.showAvatar(avatarString);
        shareNameCardDialog.setUserName(cardUserName);
        shareNameCardDialog.setInfo(info);
        shareNameCardDialog.show();
    }

    /**
     * 创建分享的名片消息
     *
     * @param shareBean
     * @return
     */
    private IMStruct002 createShareMessage(ShareBean shareBean) {
        if (shareBean.getChatType() == StructFactory.TO_USER_TYPE_PERSONAL) {
            User user = WeChatDBManager.getInstance().getOneUserById(shareBean.getChatId());
            return StructFactory.getInstance().createShareNameCardStruct(user.getNickName(), user.getAvatar(), user.getId(), true);
        } else if (shareBean.getChatType() == StructFactory.TO_USER_TYPE_GROUP) {
            Group group = WeChatDBManager.getInstance().getGroup(shareBean.getChatId(), false);
            return StructFactory.getInstance().createShareNameCardStruct(group.getGroupName(), group.getAvatar(), group.getGroupId(), false);
        }
        return null;
    }

    /**
     * 创建分享群的名片消息
     *
     * @param groupId id
     * @return
     */
    private IMStruct002 createShareGroupMessage(int groupId) {

        Group group = WeChatDBManager.getInstance().getGroup(groupId, false);
        return StructFactory.getInstance().createShareNameCardStruct(group.getGroupName(), group.getAvatar(), group.getGroupId(), false);


    }
}
