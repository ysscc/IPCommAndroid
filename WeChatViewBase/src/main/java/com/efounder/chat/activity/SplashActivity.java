//package com.efounder.chat.activity;
//
//import android.content.Intent;
//import android.os.Bundle;
//
//import com.efounder.chat.R;
//import com.efounder.chat.fragment.SplashFragment.SplashFragmentCallBack;
//
//
///**
// * 开屏页
// *
// */
//public class SplashActivity extends BaseActivity implements SplashFragmentCallBack{
//
//	@Override
//	protected void onCreate(Bundle arg0) {
//		super.onCreate(arg0);
//		setContentView(R.layout.activity_splash);
//	}
//
//	@Override
//	public void hasLogin() {
//		startActivity(new Intent(this, MainActivity.class));
//		finish();
//	}
//
//	@Override
//	public void hasNotLogin() {
//		startActivity(new Intent(this, LoginActivity.class));
//		finish();
//	}
//}
