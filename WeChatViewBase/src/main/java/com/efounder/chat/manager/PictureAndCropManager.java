package com.efounder.chat.manager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;

import com.efounder.chat.R;
import com.efounder.chat.utils.ImageUtil;
import com.efounder.util.AppContext;
import com.efounder.util.ToastUtil;
import com.utilcode.util.UriUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 拍照，裁剪图片
 * Created by yqs .
 */

public class PictureAndCropManager {

    public static final int PHOTO_REQUEST_TAKEPHOTO = 1;// 拍照
    public static final int PHOTO_REQUEST_GALLERY = 2;// 从相册中选择
    public static final int PHOTO_REQUEST_CUT = 3;// 结果
    private Context context;
    private String picturePath;


    public PictureAndCropManager(Context context) {
        this.context = context;
    }

    //TODO: 2017/3/7  拍照
    public void takePicture() {
        try {
            String imageName = getNowTime() + ".png";
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            // 指定调用相机拍照后照片的储存路径
//            intent.putExtra(MediaStore.EXTRA_OUTPUT,
//                    Uri.fromFile(new File(ImageUtil.chatpath, imageName)));
            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    UriUtils.getUriForFile(new File(ImageUtil.chatpath, imageName)));
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            picturePath = ImageUtil.chatpath + imageName;
            ((Activity) context).startActivityForResult(intent, PHOTO_REQUEST_TAKEPHOTO);
        } catch (Exception e) {
            e.printStackTrace();
            ToastUtil.showToast(context.getApplicationContext(), R.string.chat_photo_open_fail);
        }
    }

    //todo 从相册选择
    public void selectPhoto() {
        try {
            String imageName = getNowTime() + ".png";
            picturePath = ImageUtil.chatpath + imageName;
            Intent intent = new Intent(Intent.ACTION_PICK, null);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            intent.setDataAndType(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
            ((Activity) context).startActivityForResult(intent, PHOTO_REQUEST_GALLERY);
        } catch (Exception e) {
            e.printStackTrace();
            ToastUtil.showToast(AppContext.getCurrentActivity(),  R.string.chat_photo_open_fail);
        }
    }

    private String getNowTime() {
        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMddHHmmssSS");
        return dateFormat.format(date);
    }

    /**
     * 获取图片路径
     *
     * @return
     */
    public String getPicturePath() {
        return picturePath;
    }

    /**
     * 剪裁图片
     *
     * @param uri1
     * @param size
     */
    public void startPhotoZoom(Uri uri1, int size) {
        try {

            Intent intent = new Intent("com.android.camera.action.CROP");
            intent.setDataAndType(uri1, "image/*");
            // crop为true是设置在开启的intent中设置显示的view可以剪裁
            intent.putExtra("crop", "true");

            // aspectX aspectY 是宽高的比例
//            intent.putExtra("aspectX", 1);
//            intent.putExtra("aspectY", 1);
            if (Build.MANUFACTURER.equals("HUAWEI")) {
                intent.putExtra("aspectX", 9998);
                intent.putExtra("aspectY", 9999);
            } else {
                intent.putExtra("aspectX", 1);
                intent.putExtra("aspectY", 1);
            }
            // outputX,outputY 是剪裁图片的宽高
            intent.putExtra("outputX", size);
            intent.putExtra("outputY", size);
            intent.putExtra("return-data", false);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(picturePath)));
            intent.putExtra("outputFormat", Bitmap.CompressFormat.PNG.toString());
            intent.putExtra("noFaceDetection", true); // no face detection
            ((Activity) context).startActivityForResult(intent, PHOTO_REQUEST_CUT);
        } catch (Exception e) {
            e.printStackTrace();
            ToastUtil.showToast(AppContext.getCurrentActivity(),  R.string.chat_photo_open_fail);
        }
    }

    public interface CropCallBack {


    }


}
