package com.efounder.chat.item;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.efounder.chat.R;
import com.efounder.chat.item.manager.IMessageItem;
import com.efounder.frame.ViewSize;
import com.efounder.message.struct.IMStruct002;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author lch
 *  消费凭证item
 * 
 */
public class PayCardItem extends LinearLayout implements
		IMessageItem {

	private Context mContext;

	public PayCardItem(Context context) {
		super(context);
		mContext = context;
		this.setBackgroundResource(R.drawable.radius_bg);
		LinearLayout.LayoutParams mLineaLayoutParams = new LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		this.setLayoutParams(mLineaLayoutParams);
		this.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Toast.makeText(mContext, "点击消费凭证", 500).show();
			}
		});
		initView();

	}

	// 设置数据 card 是一个包含头图片，头标题，还有子card的模型
	private void initView() {

		LayoutInflater inflater = LayoutInflater.from(mContext);
		ViewGroup chat_item_pay_frame = (ViewGroup) inflater.inflate(
				R.layout.chat_item_pay_frame, this);
		
		LinearLayout chat_item_pay_detail  = (LinearLayout) this.findViewById(R.id.chat_item_pay_detail);
		
		Map<String,String> map = new HashMap<String, String>();
		map.put("支付方式：", "零钱");
		map.put("商品详情：", "thinkPad T440");
		map.put("交易单号：", "1508934235");
		
        for(String key:map.keySet()){
		LinearLayout chat_item_callingcard_detailitem = (LinearLayout) inflater
				.inflate(R.layout.chat_item_pay_detailitem, null);
		
		TextView keyTextView = (TextView) chat_item_callingcard_detailitem.findViewById(R.id.chat_item_callingcard_detailitem_key);
		keyTextView.setText(key);
		TextView valueTextView = (TextView) chat_item_callingcard_detailitem.findViewById(R.id.chat_item_callingcard_detailitem_value);
		valueTextView.setText(map.get(key));
		chat_item_pay_detail.addView(chat_item_callingcard_detailitem);
		
        }

	}

	public interface OnPublicNumberItemClickListener {
		public void onClick(int position);

	}

	@Override
	public View messageView() {
		return this;
	}

	@Override
	public ViewSize messageViewSize() {
		return null;
	}

	@Override
	public void setIMStruct002(IMStruct002 message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean getIsInUse() {
		return this.isShown();
	}

	@Override
	public void setIsInUse(boolean isInUse) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void prepareForReuse() {
		
	}
}
