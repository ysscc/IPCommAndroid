package com.efounder.pansoft.chat.photo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.utils.LXGlideImageLoader;
import com.utilcode.util.ImageUtils;
import com.utilcode.util.SizeUtils;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by will on 18-4-9.
 */

public class JFPicturePickViewAdapter extends RecyclerView.Adapter<JFPicturePickViewAdapter.PicViewHolder> {

    private Context context;
    private ArrayList<SelectModel> mFiles;
    private LinkedList<String> selectedFiles = new LinkedList<>();
    OnPicSelectedChangedListener onPicSelectedChangedListener;
    private int imageHeight = 800;

   // public static List<Object> eventAdapters= new ArrayList<>();

    public JFPicturePickViewAdapter(Context context, ArrayList<SelectModel> mFiles) {
        this.context = context;
        this.mFiles = mFiles;
       // eventAdapters.
        //EventBus.getDefault().register(this);
    }

    @Override
    public PicViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.qq_style_item_message_picture_pick, parent, false);
        return new PicViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PicViewHolder holder, int position) {

        holder.imageView.getLayoutParams().height = imageHeight;
        holder.imageView.getLayoutParams().width = Math.round(ImageUtils.getPicViewWidth(mFiles.get(position).getPath(), imageHeight));
        LXGlideImageLoader.getInstance().displayImage(context, holder.imageView, mFiles.get(position).getPath());
        int index = selectedFiles.indexOf(mFiles.get(position).getPath());
        if (index != -1) {
            holder.tvNumber.setText(index + 1 + "");
            holder.tvNumber.setBackground(context.getResources().getDrawable(R.drawable.qq_style_circle_number_bg));
            holder.surfaceView.setVisibility(View.VISIBLE);
        } else {
            holder.tvNumber.setText("");
            holder.tvNumber.setBackground(context.getResources().getDrawable(R.drawable.jf_circle_no_number_bg));
            holder.surfaceView.setVisibility(View.GONE);
        }
        holder.tvNumberTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int index = selectedFiles.indexOf(mFiles.get(holder.getAdapterPosition()).getPath());
                if (index == -1) {//不存在则加入数组里
                    holder.surfaceView.setVisibility(View.VISIBLE);
                    selectedFiles.add(mFiles.get(holder.getAdapterPosition()).getPath());
                    holder.tvNumber.setText(selectedFiles.size() + "");
                } else {
                    selectedFiles.remove(index);
                    holder.tvNumber.setText("");
                    holder.surfaceView.setVisibility(View.GONE);
                }
                onPicSelectedChangedListener.onPicSelectedChanged(selectedFiles);
                notifyDataSetChanged();
            }
        });

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, JFBigPicturePickActivity.class);
                intent.putExtra("mPictureList", selectedFiles);
                intent.putExtra("curIndex", holder.getAdapterPosition());
                ((Activity) context).startActivityForResult(intent, JFMessagePicturePickView.REQUEST_PIC_SELECTE_CODE);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mFiles.size();
    }

    class PicViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        View surfaceView;
        TextView tvNumber;
        TextView tvNumberTop;

        public PicViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.iv_preview);
            surfaceView = itemView.findViewById(R.id.view_trans);
            tvNumber = itemView.findViewById(R.id.tv_number);
            tvNumberTop = itemView.findViewById(R.id.tv_number_top);
        }
    }

    public interface OnPicSelectedChangedListener {
        void onPicSelectedChanged(LinkedList<String> selectedList);

        void onPicSelectedChanged(LinkedList<String> selectedList, boolean isRawPic);
    }

    public void setOnPicSelectedChangedListener(OnPicSelectedChangedListener onPicSelectedChangedListener) {
        this.onPicSelectedChangedListener = onPicSelectedChangedListener;
    }

    public void setImageHeight(int height) {

        imageHeight = height - SizeUtils.dp2px(46);

    }

}
