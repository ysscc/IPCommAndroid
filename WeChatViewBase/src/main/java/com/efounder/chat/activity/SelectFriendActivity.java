package com.efounder.chat.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.fragment.SelectGroupContactsFragment;
import com.efounder.mobilecomps.contacts.User;

import java.util.ArrayList;

/**
 * 通用选择用户界面选择好友的 activity
 *
 * @author yqs
 */

public class SelectFriendActivity extends BaseActivity implements SelectGroupContactsFragment.SelectedCallBack {

    private static final String TAG = "SelectFriendActivity";

    public static final int SELECT_USER_REQUEST_CODE = 127;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wechatview_activity_select_friend);
        initView();
    }

    /**
     * 选择好友
     *
     * @param context
     * @param isSingleMode     是否是单选
     * @param selectedUser     已经选中的人员
     * @param isCancelSelected 是否可以取消选中 传过来的已选中的人
     * @param
     */
    public static void startSelectUser(Context context, boolean isSingleMode, ArrayList<User> selectedUser, boolean isCancelSelected, int requestCode) {
        Intent starter = new Intent(context, SelectFriendActivity.class);
        starter.putExtra("isSingleMode", isSingleMode);
        if (selectedUser != null) {
            starter.putExtra("selectuser", selectedUser);
        }
        starter.putExtra("isCancleSelected", isCancelSelected);
        ((Activity) context).startActivityForResult(starter, requestCode);
    }

    private void initView() {
        TextView tvTitle = (TextView) findViewById(R.id.tv_title);
        tvTitle.setText(R.string.chat_choose_friend);
        ImageView backView = (ImageView) findViewById(R.id.iv_back);
        backView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        SelectGroupContactsFragment selectGroupContactsFragment = (SelectGroupContactsFragment)
                getSupportFragmentManager()
                        .findFragmentById(R.id.fragment_contact);
        selectGroupContactsFragment.setSelectedCallBack(this);
    }


    @Override
    public void getSelectUsers(ArrayList<User> selectList) {
        if (selectList != null && selectList.size() > 0) {
            User user = selectList.get(0);
            Intent intent = new Intent();
            intent.putExtra("userId", user.getId());
            intent.putExtra("nickName", user.getNickName());

            setResult(RESULT_OK, intent);
            finish();
        }
    }
}
