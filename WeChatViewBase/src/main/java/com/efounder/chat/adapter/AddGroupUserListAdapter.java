package com.efounder.chat.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.model.Group;
import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.utils.ResStringUtil;

import java.util.List;

/**
 * Created by will on 17-1-10.
 */

public class AddGroupUserListAdapter extends BaseAdapter {
    private List<Group> groups;
//    private ImageLoader imageLoader;
//    private DisplayImageOptions options;

    public AddGroupUserListAdapter(List<Group> groups) {
        super();
        this.groups = groups;
    }

    @Override
    public int getCount() {
        return groups.size();
    }

    @Override
    public Group getItem(int position) {
        return groups.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getGroupId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        Group group = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_addfriends_list_item,null);
            viewHolder = new ViewHolder();
            viewHolder.textViewName = (TextView) convertView.findViewById(R.id.textView_name);
            viewHolder.textViewAdName = (TextView) convertView.findViewById(R.id.textView_adname);
            viewHolder.textViewTel = (TextView) convertView.findViewById(R.id.textView_tel);
            viewHolder.userAvatarImageView = (ImageView) convertView.findViewById(R.id.imageView1);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

//        imageLoader = ImageLoader.getInstance();
//        options = ImageUtil.getImageLoaderOptions(R.drawable.default_groupavatar);
//        imageLoader.displayImage(group.getAvatar(),viewHolder.userAvatarImageView,options);
        LXGlideImageLoader.getInstance().showRoundGrouAvatar(viewHolder.userAvatarImageView.getContext(),viewHolder.userAvatarImageView,group.getAvatar(),LXGlideImageLoader.DEFATLT_AVATAR_RADIUS_10);

        viewHolder.textViewName.setText(ResStringUtil.getString(R.string.wrchatview_group_name_mh)+group.getGroupName());
        viewHolder.textViewAdName.setText(ResStringUtil.getString(R.string.wechatview_create_user_name)+":"+group.getCreateId());
        //服务器下来的是String类型的201707070707的时间
        //所以需要转换两次
        String time = String.valueOf(group.getCreateTime());
        time = time.substring(0,4)+ResStringUtil.getString(R.string.wrchatview_year)+time.substring(4,6)+ResStringUtil.getString(R.string.wrchatview_month)+time.substring(6,8)+ResStringUtil.getString(R.string.wrchatview_day);
        viewHolder.textViewTel.setText(ResStringUtil.getString(R.string.chatgroupsettingactivity_string4)+":"+time);
        return convertView;
    }

    class ViewHolder{
        TextView textViewName;
        TextView textViewAdName;
        TextView textViewTel;
        ImageView userAvatarImageView;
    }
}
