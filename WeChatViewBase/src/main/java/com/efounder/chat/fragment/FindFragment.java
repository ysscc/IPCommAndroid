package com.efounder.chat.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.efounder.chat.R;
import com.efounder.chat.zxing.qrcode.MipcaActivityCapture;
import com.efounder.frame.activity.EFTransformFragmentActivity;
import com.efounder.frame.baseui.BaseFragment;
import com.efounder.frame.utils.EFAppAccountUtils;
import com.efounder.frame.utils.EFFrameUtils;

/**
 * 发现页面
 * @author YQS
 */
public class FindFragment  extends BaseFragment implements View.OnClickListener {

	private Context context;

	public FindFragment(){

	}
//	public FindFragment(StubObject stubObject) {
//		Hashtable<String, String> hashtable = new Hashtable<>();
//		Bundle bundle = new Bundle();
//		bundle.putSerializable("stubObject", stubObject);
//		setArguments(bundle);
//
//	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_find, container, false);
	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		context=getActivity();
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		RelativeLayout re_erweima=(RelativeLayout) view.findViewById(R.id.re_erweima);
		re_erweima.setOnClickListener(this);
		RelativeLayout re_yaoyiyao = (RelativeLayout) view.findViewById(R.id.re_yaoyiyao);
		re_yaoyiyao.setOnClickListener(this);
	}


    @Override
    public void onClick(View v) {
        int id=v.getId();
        if (id == R.id.re_erweima) {
			Intent intent = new Intent();
			intent.setClass(getActivity(),MipcaActivityCapture.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
        }else if (id == R.id.re_yaoyiyao){
			try {
				Bundle bundle = new Bundle();
				bundle.putInt("id", EFAppAccountUtils.getAppAccountID());
				bundle.putBoolean(EFTransformFragmentActivity.EXTRA_HIDE_TITLE_BAR,true);
//				bundle.putInt(EFTransformFragmentActivity.EXTRA_TITLE_LEFT_VISIBILITY,View.VISIBLE);
//				bundle.putInt(EFTransformFragmentActivity.EXTRA_TITLE_RIGHT_VISIBILITY,View.VISIBLE);
//				bundle.putString(EFTransformFragmentActivity.EXTRA_TITLE_NAME,"摇一摇");
				EFFrameUtils.pushFragment((Class<? extends BaseFragment>) Class.forName("com.efounder.chat.fragment.ShakeToFindFragment"),bundle);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
    }
}
