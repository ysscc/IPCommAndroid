package com.efounder.chat.presenter.publicnumberlist;

import android.content.Context;
import android.text.TextUtils;

import com.efounder.chat.R;
import com.efounder.frame.baseui.BasePresenterImpl;
import com.efounder.mobilecomps.contacts.HanyuParser;
import com.efounder.mobilecomps.contacts.PinyinComparator;
import com.efounder.mobilecomps.contacts.User;
import com.efounder.util.StringUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * author : zzj
 * e-mail : zhangzhijun@pansoft.com
 * date   : 2018/8/28 14:18
 * desc   : 公众号 presenter  用来处理 网络请求 或 数据加载 转化
 * version: 1.0
 */
public class PublicNumberListPresenter extends BasePresenterImpl<PublicNumberListContract.View>
        implements PublicNumberListContract.Presenter {

    public PublicNumberListPresenter(PublicNumberListContract.View view) {
        super(view);
    }

    @Override
    public List<User> filledData(List<User> users) {
        List<User> usersList = new ArrayList<User>();
        // String[] idData = getResources().getStringArray(R.array.id);
        for (User user : users) {
            // 汉字转换成拼音
            String userName = user.getNickName();
            String pinyin = new HanyuParser().getStringPinYin(userName);
            String sortString;
            if (!StringUtil.isEmpty(pinyin)) {
                sortString = pinyin.substring(0, 1).toUpperCase(
                        Locale.getDefault());
            } else {
                sortString = "";
            }

            if (userName.equals("群聊") || userName.equals("新的朋友")
                    || userName.equals("组织机构")) {
                user.setSortLetters("↑");
            } else if (userName.equals("")) {
                user.setSortLetters("☆");
            } else if (sortString.matches("[A-Z]")) {// 正则表达式，判断首字母是否是英文字母
                user.setSortLetters(sortString.toUpperCase(Locale.getDefault()));
            } else {
                user.setSortLetters("#");
            }
            usersList.add(user);
        }

        return usersList;
    }

    @Override
    public List<User> filledData(Context context, String[] data) {
        List<User> mSortList = new ArrayList<User>();
        String[] idData = context.getResources().getStringArray(R.array.publicid);
        for (int i = 0; i < data.length; i++) {
            User user = new User();
            user.setNickName(data[i]);
            user.setId(Integer.valueOf(idData[i]));
            user.setType(-1);
            // 汉字转换成拼音
            String pinyin = new HanyuParser().getStringPinYin(data[i]);
            String sortString = pinyin.substring(0, 1).toUpperCase(
                    Locale.getDefault());

            if (data[i].equals("群聊") || data[i].equals("公众号")
                    || data[i].equals("组织机构")) {
                user.setSortLetters("↑");
            } else if (data[i].equals("")) {
                user.setSortLetters("☆");
            } else if (sortString.matches("[A-Z]")) {// 正则表达式，判断首字母是否是英文字母
                user.setSortLetters(sortString.toUpperCase(Locale
                        .getDefault()));
            } else {
                user.setSortLetters("#");
            }
            mSortList.add(user);
        }
        return mSortList;
    }

    @Override
    public void filterData(String filterStr, List<User> sourceDataList,PinyinComparator pinyinComparator) {
        List<User> filterDateList = new ArrayList<User>();
        if (TextUtils.isEmpty(filterStr)) {
            filterDateList = sourceDataList;
        } else {
            filterDateList.clear();
            for (User user : sourceDataList) {
                String name = user.getNickName();
                // if (name.indexOf(filterStr.toString()) != -1 ||
                // characterParser.getSelling(name).startsWith(filterStr.toString()))
                // {
                // if (name.contains(filterStr.toString()) ||
                // characterParser.getSelling(name).contains(filterStr.toString()))
                // {
                // filterDateList.add(user);
                // }
                if (containString(name, filterStr)) {
                    filterDateList.add(user);
                }
            }
        }
        // 根据a-z进行排序
        Collections.sort(filterDateList, pinyinComparator);
        //将排序好的集合传给 fragment

        if(isViewAttached()){
            mView.filterDataSuccess(filterDateList);
        }
    }

    private boolean containString(String name, String filterStr) {
        String namePinyin = new HanyuParser().getStringPinYin(name);
        for (int i = 0; i < filterStr.length(); i++) {
            String singleStr = filterStr.substring(i, i + 1);
            // 汉字
            if (name.contains(singleStr)) {
                if (i == filterStr.length() - 1) {
                    return true;
                }
                continue;
            }
            // 英文
            if (namePinyin.contains(singleStr)) {
                int currentIndex = namePinyin.indexOf(singleStr);
                namePinyin = namePinyin.substring(currentIndex + 1,
                        namePinyin.length());
            } else {// 不包含
                break;
            }
            if (i == filterStr.length() - 1) {
                return true;
            }
        }
        return false;
    }
}
