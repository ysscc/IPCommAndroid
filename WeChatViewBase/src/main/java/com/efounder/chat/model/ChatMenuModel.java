package com.efounder.chat.model;

import com.core.xml.StubObject;

/**
 * 聊天下方 图片 视频 拍照的 model
 * Created by yqs on 2017/6/13.
 */

public class ChatMenuModel {
    public static final int PICTURE = 0;//图片
    public static final int TAKEPIC = 1;//拍照
    public static final int VEDIO = 2;//小视频
    public static final int TASK = 3;//任务
    public static final int LOCATION = 4;//位置
    public static final int FILE = 5;//文件
    public static final int MINGPIAN = 6;//名片
    public static final int VOUCHERS = 7;//付款凭证
    public static final int ALLPICATION_TASK = 8;//应用任务
    public static final int WEBPAGE = 9;//网址页面
    public static final int COMMON=10;//通用页面
    public static final int NATIVEPAGE=11;//原生页面（反射方式加载）


    private int type;
    private String name;
    private int icon;
    private String localIcon;
    private StubObject stubObject;

    public ChatMenuModel(int type, String name, int icon) {
        this.type = type;
        this.name = name;
        this.icon = icon;
    }

    public ChatMenuModel() {

    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public StubObject getStubObject() {
        return stubObject;
    }

    public void setStubObject(StubObject stubObject) {
        this.stubObject = stubObject;
    }

    public String getLocalIcon() {
        return localIcon;
    }

    public void setLocalIcon(String localIcon) {
        this.localIcon = localIcon;
    }
}
