package com.efounder.chat.widget;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.efounder.chat.R;


public class TopPopMenu {
	private ArrayList<String> itemList;
	private Context context;
	private PopupWindow popupWindow;
	private ListView listView;
	private PopAdapter adapter;
	/** menu的根layout */
	private LinearLayout rootMenuLL;
	/** 菜单小项背景 */
	private int MenuItemBackgroundResid = -1;
	/** 字体颜色 */
	private ColorStateList textColorStateList;
	private int textSize = -1;

	@SuppressLint("InflateParams")
	public TopPopMenu(Context context) {
		this.context = context;

		itemList = new ArrayList<String>(5);

		View view = LayoutInflater.from(context).inflate(R.layout.chat_top_popmenu, null);

		initView(view);
	}

	private void initView(View view) {
		// 设置 listview
		listView = (ListView) view.findViewById(R.id.listView);
		adapter = new PopAdapter();
		listView.setAdapter(adapter);
		popupWindow = new PopupWindow(view, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);
		// 这个是为了点击“返回Back”也能使其消失，并且并不会影响你的背景（很神奇的）
		popupWindow.setBackgroundDrawable(new ColorDrawable());
//		popupWindow.setAnimationStyle(R.style.mypopwindow_anim_style);
		rootMenuLL = (LinearLayout) view.findViewById(R.id.popup_view_cont);
	}

	/**
	 * 设置菜单项点击监听器
	 * 
	 * @param listener
	 */
	public void setOnItemClickListener(OnItemClickListener listener) {
		listView.setOnItemClickListener(listener);
	}

	/**
	 * 批量添加菜单项
	 * 
	 * @param items
	 */
	public void addItems(String[] items) {
		for (String s : items)
			itemList.add(s);
	}

	/**
	 * 单个添加菜单项
	 * 
	 * @param item
	 */
	public void addItem(String item) {
		itemList.add(item);
	}

	/**
	 * 下拉式 弹出 pop菜单
	 * 
	 * @param anchor
	 */
	public void show(View anchor) {
		// XXX
		popupWindow.getContentView().measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		int popWidth = popupWindow.getContentView().getMeasuredWidth();
		int x = (anchor.getWidth() - popWidth) / 2;
		Log.i("", "popWidth---------------" + popWidth);
		int popHeight = getListViewHeight(listView);//popupWindow.getContentView().getMeasuredHeight();////popupWindow.getContentView().getMeasuredHeight() * itemList.size();
		int y = anchor.getHeight() + popHeight;
		Log.i("", "popHeight---------------" + popupWindow.getContentView().getMeasuredHeight() + ",listView高度：" + getListViewHeight(listView));
		
		LinearLayout.LayoutParams listviewParams = new LinearLayout.LayoutParams(popWidth, popHeight);
		listView.setLayoutParams(listviewParams);
		popupWindow.setHeight(popHeight);
		popupWindow.setWidth(popWidth);
		popupWindow.showAsDropDown(anchor, x, -y);
		// 使其聚集
		popupWindow.setFocusable(true);
		// 设置允许在外点击消失
		popupWindow.setOutsideTouchable(true);
		// 刷新状态
		popupWindow.update();
	}

	/**
	 * 隐藏菜单
	 */
	public void dismiss() {
		popupWindow.dismiss();
	}

	// 适配器
	private final class PopAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return itemList.size();
		}

		@Override
		public Object getItem(int position) {
			return itemList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@SuppressLint("InflateParams")
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				convertView = LayoutInflater.from(context).inflate(R.layout.top_pomenu_item, parent,false);
				holder = new ViewHolder();

				convertView.setTag(holder);

				holder.groupItem = (TextView) convertView
						.findViewById(R.id.textView);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			holder.groupItem.setText(itemList.get(position));
			if (MenuItemBackgroundResid != -1)
				holder.groupItem.setBackgroundResource(MenuItemBackgroundResid);
			if (textColorStateList != null)
				holder.groupItem.setTextColor(textColorStateList);
			if (textSize != -1) {
				holder.groupItem.setTextSize(textSize);
			}
			return convertView;
		}

		private final class ViewHolder {
			TextView groupItem;
		}
	}

	/**
	 * 设置PopMenu背景色
	 * 
	 * @param resid
	 */
	public void setBackgroundResource(int resid) {
		rootMenuLL.setBackgroundResource(resid);
	}

	/**
	 * 设置PopMenu背景色
	 * 
	 * @param color
	 */
	public void setBackgroundColor(int color) {
		rootMenuLL.setBackgroundColor(color);
	}

	/**
	 * 设置菜单小项背景
	 * 
	 * @param resid
	 *            最好传入一个selector
	 */
	public void setMenuItemBackground(int resid) {
		this.MenuItemBackgroundResid = resid;
		// adapter.notifyDataSetChanged();
	}

	/**
	 * 设置字体颜色
	 * 
	 * @param resid
	 */
	public void setTextColorResid(int resid) {
		this.textColorStateList = context.getResources().getColorStateList(resid);
	}

	/**
	 * 设置字体大小
	 * 
	 * @param textSize
	 */
	public void setTextSize(int textSize) {
		this.textSize = textSize;
	}

	/**
	 * 计算ListView高度
	 * @param listView
	 * @return
	 */
	private int getListViewHeight(ListView listView) {
		int totalHeight = 0;
		ListAdapter adapter = listView.getAdapter();
		for (int i = 0; i < adapter.getCount(); i++) { // listAdapter.getCount()返回数据项的数目
			View listItem = adapter.getView(i, null, listView);
			listItem.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED); // 计算子项View 的宽高
			totalHeight += listItem.getMeasuredHeight(); // 统计所有子项的总高度
		}

		int height = totalHeight + (listView.getDividerHeight() * (listView.getCount() - 1));
		return height;
	}  
	
}
