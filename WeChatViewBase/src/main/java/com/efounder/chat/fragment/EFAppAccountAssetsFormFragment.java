package com.efounder.chat.fragment;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.core.xml.StubObject;
import com.efounder.form.application.ApplicationContainer;
import com.efounder.form.application.FormApplication;
import com.efounder.form.application.FormViewContainer;
import com.efounder.form.application.util.Form;
import com.efounder.form.application.util.FormAppUtil;
import com.efounder.frame.ViewSize;
import com.efounder.frame.baseui.EFFragment;
import com.efounder.frame.xmlparse.EFXmlConstants;
import com.efounder.mobilemanager.R;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by hudq on 2016/8/29. xx
 */

public class EFAppAccountAssetsFormFragment extends EFFragment{
    private int position;
    private StubObject stubObject;

    public static EFAppAccountAssetsFormFragment newInstance(int position, StubObject stubObject){
        EFAppAccountAssetsFormFragment appAccountFragment = new EFAppAccountAssetsFormFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("position",position);
        bundle.putSerializable("stubObject", stubObject);
        appAccountFragment.setArguments(bundle);
        return appAccountFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt("position");
        stubObject = (StubObject) getArguments().getSerializable("stubObject");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ef_fragment_app_account_form,container,false);
        TextView textView = (TextView) view.findViewById(R.id.textView_test);
        textView.setText(position+"--" + stubObject.getCaption() +"--"+ stubObject.getString(EFXmlConstants.ATTR_FORM,""));

        final LinearLayout ll = (LinearLayout) view.findViewById(R.id.rootLayout);

        ll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @SuppressLint("NewApi")
            public void onGlobalLayout() {

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    ll.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    ll.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }

                addForm(ll);
            }

        });


        return view;
    }


    private void addForm(LinearLayout ll ) {
        String xmlName = stubObject.getString(EFXmlConstants.ATTR_FORM,"");
        try {
            InputStream inputStream =  getActivity().getAssets().open(xmlName);
            ViewSize viewSize = new ViewSize(ll.getMeasuredWidth(),ll.getMeasuredHeight());
            Form form = new Form();
            form.setFormName(xmlName.substring(0,xmlName.indexOf(".xml")));
            form.setInputStream(inputStream);
            form.setViewSize(viewSize);

            ApplicationContainer applicationContainer = FormAppUtil.generateForm(form);
            FormApplication formApplication = applicationContainer.getFormApplication(xmlName.substring(0,xmlName.indexOf(".xml")));
            FormViewContainer formViewContainer = (FormViewContainer) formApplication.getFormContainer("form1");
            if (formViewContainer != null){
                ll.removeAllViews();
                ll.addView(formViewContainer);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
