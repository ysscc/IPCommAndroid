package com.efounder.chat.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.zxing.qrcode.MipcaActivityCapture;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.frame.utils.Constants;

/**
 * 
 * @author yqs
 *
 */
public class AddFriendsOneActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addfriends);
        TextView tvTitle = (TextView) findViewById(R.id.tv_title);
        tvTitle.setText(R.string.chat_addfriend);
        TextView tv_search=(TextView) this.findViewById(R.id.tv_search);
        RelativeLayout re_saoyisao= (RelativeLayout) this.findViewById(R.id.re_saoyisao);
        RelativeLayout re_radaraddfriend= (RelativeLayout) this.findViewById(R.id.re_radaraddfriend);
        RelativeLayout re_yingyonghao = (RelativeLayout) this.findViewById(R.id.yingyonghao);
        RelativeLayout re_searchGroup = (RelativeLayout) this.findViewById(R.id.re_search_group);

        tv_search.setOnClickListener(new OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddFriendsOneActivity.this, AddAllTypesFriends.class);
                //共享，查找朋友 kongmeng
                if (EnvironmentVariable.getProperty(Constants.KEY_SETTING_APPID).equals("ZSYGXOSPMobileYS") || EnvironmentVariable.getProperty(Constants.KEY_SETTING_APPID).equals("ZSYGXOSPMobile")) {
                    intent = new Intent(AddFriendsOneActivity.this, AddAllTypeFriendsGX.class);
                }
                if (EnvironmentVariable.getProperty("useNewAddPage","").equals("1")){
                    //普光专用
                     intent = new Intent(AddFriendsOneActivity.this, AddAllTypesFriendsPG.class);
                }
                intent.putExtra("type",0);
                startActivity(intent);
            }

        });

        re_saoyisao.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(AddFriendsOneActivity.this,MipcaActivityCapture.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        re_radaraddfriend.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AddFriendsOneActivity.this,RadarAddFriendActivity.class));
            }
        });


        re_yingyonghao.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AddFriendsOneActivity.this, AddAllTypesFriends.class);
                intent.putExtra("type",2);
                startActivity(intent);
            }
        });

        re_searchGroup.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddFriendsOneActivity.this, AddAllTypesFriends.class);
                intent.putExtra("type",1);
                startActivity(intent);
            }
        });

    }



    @Override
    public void back(View view){
        finish();
    }
}
