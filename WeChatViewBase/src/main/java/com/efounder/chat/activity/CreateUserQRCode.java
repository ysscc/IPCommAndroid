package com.efounder.chat.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.db.WeChatDBManager;
import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.chat.zxing.qrcode.QRCode;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.mobilecomps.contacts.User;
import com.efounder.utils.ResStringUtil;
import com.google.gson.JsonObject;

import static com.efounder.frame.utils.Constants.CHAT_USER_ID;

/**
 * 二维码名片界面
 * Created by Tian Yu on 2016/9/7.
 */

public class CreateUserQRCode extends BaseActivity {
    // private DisplayImageOptions options;
    private ImageView iv_avatar;
    //private ImageLoader imageLoader;
    private int userType = 0;//好友还是公众号

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_erweima);

        //initImageLoader();

        iv_avatar = (ImageView) findViewById(R.id.iv_avatar);
        TextView tv_name = (TextView) findViewById(R.id.tv_name);
        ImageView iv_sex = (ImageView) findViewById(R.id.iv_sex);
        ImageView iv_erweima = (ImageView) findViewById(R.id.iv_erweima);
        TextView tv_fxid = (TextView) findViewById(R.id.tv_fxid);
        ImageView iv_back = (ImageView) findViewById(R.id.iv_back);
        TextView tvTitle = (TextView) findViewById(R.id.tv_title);
        tvTitle.setText(R.string.chat_max_card);
        TextView tv_tip = (TextView) findViewById(R.id.tv_tip);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateUserQRCode.this.finish();
            }
        });


        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();


        String type = bundle.getString("type");
        String user_id = bundle.getString("id");
        String avatar = bundle.getString("avatar");

        if (TextUtils.isEmpty(type) || TextUtils.isEmpty(user_id)) {
            //这里从配置文件进入后会拿不到上面三个参数
            User user = WeChatDBManager.getInstance().getOneUserById(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)));
            if (user.isExist()) {
                bundle.putString("type", "addFriend");
                bundle.putString("id", EnvironmentVariable.getProperty(CHAT_USER_ID));
                bundle.putString("avatar", user.getAvatar());
                bundle.putString("nickname", user.getNickName());
                bundle.putString("name", user.getName());
                bundle.putString("sex", user.getSex());
            }

            //再取一次.我为什么要先保存到bundle中然后再取? 因为下面还有很多代码从bundle里面取值,下面的代码不是我写的,为了程序稳固,我直接将需要的数据存如bundle,那么下面的代码就不用动了
            type = bundle.getString("type");
            user_id = bundle.getString("id");
            avatar = bundle.getString("avatar");
        }


        if (!"addGroup".equals(type)) {
            User user =WeChatDBManager.getInstance().getOneUserById(Integer.valueOf(user_id));
            avatar = user.getAvatar();
            userType = user.getType();
            if (userType == 1) {
                tv_tip.setText(R.string.chat_sweep_and_follow);
            }
        } else {
            tv_tip.setText(ResStringUtil.getString(R.string.chat_sweep_and_add) + getResources().getString(R.string.chat_room_name));
        }

        showUserAvatar(avatar);


        //设置昵称
        tv_name.setText(bundle.getString("nickname"));

        if ("addFriend".equals(type) || "addPublicNum".equals(type)) {
            //设置账号
            tv_fxid.setText(getResources().getString(R.string.im_chat_number) + "：" + bundle.getString("id"));
            //设置性别
            String sex = bundle.getString("sex");
            if ("".equals(sex)) {
                iv_sex.setVisibility(View.INVISIBLE);
            }
            if ("M".equals(sex)) {
                iv_sex.setImageResource(R.drawable.ic_sex_male);
            }
            if ("F".equals(sex)) {
                iv_sex.setImageResource(R.drawable.ic_sex_female);
            } else {
                iv_sex.setVisibility(View.INVISIBLE);
            }
            //设置头像
            showUserAvatar(avatar);
        } else {
            tv_fxid.setVisibility(TextView.GONE);
            iv_sex.setVisibility(ImageView.GONE);
        }

        JsonObject js = new JsonObject();
        js.addProperty("type", type);
        js.addProperty("userID", user_id);
        String url_erweima = js.toString();
        Bitmap bm = QRCode.setQRcode(url_erweima, 500, 500);
        iv_erweima.setImageBitmap(bm);

    }

    /**
     * 显示用户头像
     */
    private void showUserAvatar(String url) {
//        if (url.contains("http") || url.contains("file://")) {
//            imageLoader.displayImage(URLModifyDynamic.getInstance().replace(url), iv_avatar, options);
//        } else {
//            imageLoader.displayImage("", iv_avatar, options);
//        }
        LXGlideImageLoader.getInstance().showRoundUserAvatar(this, iv_avatar, url
                , LXGlideImageLoader.DEFATLT_AVATAR_RADIUS_10);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    /**
     * 异步加载头像
     */
//    public void initImageLoader() {
//        // 初始化异步加载图片的类
//        imageLoader = ImageLoader.getInstance();
//        // 设置异步加载图片的配置信息
//        options = ImageUtil.getImageLoaderOptions(R.drawable.default_useravatar);
//
//    }
}
