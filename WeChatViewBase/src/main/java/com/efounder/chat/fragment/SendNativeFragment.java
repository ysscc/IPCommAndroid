package com.efounder.chat.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.core.xml.StubObject;
import com.efounder.chat.R;
import com.efounder.chat.struct.MessageChildTypeConstant;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.frame.baseui.BaseFragment;
import com.efounder.message.manager.JFMessageManager;
import com.efounder.message.struct.IMStruct002;
import com.efounder.util.ToastUtil;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import static com.efounder.frame.utils.Constants.CHAT_USER_ID;

@Deprecated
public class SendNativeFragment extends BaseFragment implements View.OnClickListener {

    private Context context;


    private Button button;
    private StubObject stubObject;
    private String userId;
    private String chatType;


    public SendNativeFragment() {

    }
    public SendNativeFragment(StubObject stubObject) {
        Hashtable<String, String> hashtable = stubObject.getStubTable();
        Bundle bundle = new Bundle();
        bundle.putSerializable("stubObject",stubObject);

        setArguments(bundle);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sendnative, container, false);


        button = (Button) rootView.findViewById(R.id.button);
        button.setOnClickListener(this);
        stubObject = (StubObject) getArguments().getSerializable("stubObject");
        userId = (String) stubObject.getStubTable().get("toUserId");
        chatType = (String) stubObject.getStubTable().get("chatType");
        return rootView;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context = getActivity();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.button) {

            if (stubObject == null) {
                ToastUtil.showToast(getActivity(), R.string.wrchatview_send_fail);
                return;
            }

            Map<String, String> map = new HashMap<>();
            Hashtable<String, String> hashtable = stubObject.getStubTable();

            map.put("title", "点击跳转原生页面");
            // map.put("subTitle", "这是副标题");
            map.put("image", "http://panserver.solarsource.cn:9692/panserver/files/3df20c10-4f0a-4ae6-ac8d-7b5299c92f1d/download");
            map.put("smallIcon",
                    "http://panserver.solarsource.cn:9692/panserver/files/3df20c10-4f0a-4ae6-ac8d-7b5299c92f1d/download");
            map.put("time", "2017.06.23");
            map.put("content", "这是内容详情，这是内容详情，这是内容详情，这是内容详情，这是内容详情，这是内容详情...");
            map.put("systemName", "原生页面");
            map.put("viewType", "display");
            map.put("AndroidShow", "com.efounder.chat.fragment.FindFragment");
            map.put("show", "ShowNativeViewController");
            if (hashtable.containsKey("state")) {
                map.put("urlState", hashtable.get("state"));
            }
            if (hashtable.containsKey("tcms")) {
                map.put("tcms", hashtable.get("tcms"));
            }
            if (hashtable.containsKey("show")) {
                map.put("show", hashtable.get("show"));
            }
            if (hashtable.containsKey("width")) {
                map.put("width", hashtable.get("width"));
            }
            if (hashtable.containsKey("height")) {
                map.put("height", hashtable.get("width"));
            }
            if (hashtable.containsKey("tcms")) {
                map.put("tcms", hashtable.get("tcms"));
            }
            JSONObject jsonObject = new JSONObject(map);
            String json = jsonObject.toString();


            IMStruct002 imStruct002 = new IMStruct002();
            try {
                imStruct002.setBody(json.getBytes("UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            long time = System.currentTimeMillis();
            imStruct002.setTime(time);
            imStruct002.setToUserType(Integer.valueOf(chatType).byteValue());
            imStruct002.setToUserId(Integer.valueOf(userId));
            imStruct002.setFromUserId(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)));
            imStruct002.setMessageChildType(MessageChildTypeConstant.subtype_common);
            JFMessageManager.getInstance().sendMessage(imStruct002);

            getActivity().finish();
        }

    }
}
