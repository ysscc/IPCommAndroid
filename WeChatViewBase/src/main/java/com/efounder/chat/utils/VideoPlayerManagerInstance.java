//package com.efounder.chat.utils;
//
//import com.volokh.danylo.video_player_manager.manager.PlayerItemChangeListener;
//import com.volokh.danylo.video_player_manager.manager.SingleVideoPlayerManager;
//import com.volokh.danylo.video_player_manager.manager.VideoPlayerManager;
//import com.volokh.danylo.video_player_manager.meta.MetaData;
//
////懒汉式单例类.在第一次调用的时候实例化自己
//public class VideoPlayerManagerInstance {
//	private VideoPlayerManagerInstance() {
//	}
//
//	// private static VideoPlayerManagerInstance single = null;
//	private static VideoPlayerManager<MetaData> mVideoPlayerManager = null;
//
//	// 静态工厂方法
//	public static VideoPlayerManager<MetaData> getInstance() {
//		if (mVideoPlayerManager == null) {
//			mVideoPlayerManager = new SingleVideoPlayerManager(
//					new PlayerItemChangeListener() {
//						@Override
//						public void onPlayerItemChanged(MetaData metaData) {
//
//						}
//					});
//		}
//		return mVideoPlayerManager;
//	}
//}