package com.efounder.pansoft.chat.listener;

/**
 * author : zzj
 * e-mail : zhangzhijun@pansoft.com
 * date   : 2018/9/10 9:17
 * desc   : 语音听写发送接口
 * version: 1.0
 */
public interface VoiceDictateSendListener {

    /**
     *  语音听写发送回调接口
     * @param content 发送的文本
     * @param voicePath 语音的路径
     */
    void onViceDictateSendListener(String content,String voicePath);
}
