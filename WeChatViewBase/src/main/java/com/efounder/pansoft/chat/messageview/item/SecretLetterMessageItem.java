package com.efounder.pansoft.chat.messageview.item;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;


import com.efounder.chat.R;
import com.efounder.chat.item.manager.IMessageItem;

import com.efounder.frame.ViewSize;
import com.efounder.message.struct.IMStruct002;
import com.efounder.ui.util.DisplayUtil;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

/**
 * @author slp
 * 密信消息item
 */
public class SecretLetterMessageItem extends LinearLayout implements IMessageItem {

    private JSONObject mJSONObject;
    private Context mContext;
    private IMStruct002 mIMStruct002;

    public SecretLetterMessageItem(Context context) {
        super(context);

        this.mContext = context;
        final LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflate.inflate(R.layout.chat_item_message_secret_letter, this);//注意第二个参数

        int width = DisplayUtil.getMobileWidth(context) * 2 / 4 - 20;
        LayoutParams lp = new LayoutParams(width, LayoutParams.MATCH_PARENT);
        this.setLayoutParams(lp);
    }

    @Override
    public void setIMStruct002(IMStruct002 message) {
        this.mIMStruct002 = message;
        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(mIMStruct002);
            }
        });
    }

    @Override
    public View messageView() {
        return this;
    }

    @Override
    public ViewSize messageViewSize() {
        return null;
    }

    @Override
    public boolean getIsInUse() {
        return this.isShown();
    }

    @Override
    public void setIsInUse(boolean isInUse) {

    }

    @Override
    public void prepareForReuse() {

    }

}
