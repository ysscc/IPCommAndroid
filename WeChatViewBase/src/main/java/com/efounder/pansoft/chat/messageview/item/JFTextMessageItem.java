package com.efounder.pansoft.chat.messageview.item;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.item.manager.IMessageItem;
import com.efounder.chat.struct.MessageChildTypeConstant;
import com.efounder.chat.utils.SmileUtils;
import com.efounder.chat.widget.ChatTextLayout;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.frame.ViewSize;
import com.efounder.message.struct.IMStruct002;
import com.efounder.ui.util.DisplayUtil;
import com.efounder.util.JSONUtil;
import com.efounder.utils.JfResourceUtil;

import static com.efounder.frame.utils.Constants.CHAT_USER_ID;

/**
 * @author lch
 * 支持文本、超链接、打电话
 */
public class JFTextMessageItem extends ChatTextLayout implements IMessageItem {

    private Context mContext;
    private int loginUserId;//登录的用户id

    public JFTextMessageItem(Context context) {
        super(context);
        //this.setTextSize(15);
        mContext = context;
        this.getTextView().setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16); //15DP
        this.getTextView().setMaxWidth(DisplayUtil.getMobileWidth(context) * 2 / 3 + getoneTextWidth());

        this.getTranslateTextView().setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16); //15DP
        this.getTranslateTextView().setMaxWidth(DisplayUtil.getMobileWidth(context) * 2 / 3 + getoneTextWidth());


        this.getTextView().setTextColor(JfResourceUtil.getSkinColor(R.color.black_deep_chat));
        //设置linkTextColor
        this.getTextView().setLinkTextColor(JfResourceUtil.getSkinColor(R.color.chat_link_text_color));
        this.getTranslateTextView().setLinkTextColor(JfResourceUtil.getSkinColor(R.color.chat_link_text_color));

        this.setPadding(15, 11, 15, 11);
        //设置一行最多15个字
        // this.setMaxEms(14);
        String imUserId = EnvironmentVariable.getProperty(CHAT_USER_ID, "0");
        if (imUserId.equals("")) {
            imUserId = "0";
        }
        loginUserId = Integer.valueOf(imUserId);
    }

    public int getoneTextWidth() {
        // 1 获取一个字的宽度
        TextView plainTextView = new TextView(mContext);
        plainTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
        Paint paint = plainTextView.getPaint();
        Rect rect = new Rect();
        // 返回包围整个字符串的最小的一个Rect区域
        paint.getTextBounds("序", 0, 1, rect);
        // oneTextWidth = rect.width();
        return rect.width();
    }
//todo 将点击事件设置给textview  解决点击事件冲突的问题
    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        this.getTextView().setOnClickListener(l);
    }

    @Override
    public void setOnLongClickListener(@Nullable OnLongClickListener l) {
        this.getTextView().setOnLongClickListener(l);
    }

    @Override
    public View messageView() {
        return this;
    }

    @Override
    public ViewSize messageViewSize() {
        return null;
    }

    @Override
    public void setIMStruct002(IMStruct002 message) {
        String messageText = message.getMessage();
        try {
            if (message.getMessageChildType() == MessageChildTypeConstant.subtype_mZoneNotification) {
                try {
                    String text = JSONUtil.parseJson(messageText).get("showMsg").getAsString();
                    this.getTextView().setText(SmileUtils.getSmiledText(this.getContext(), text));
                } catch (Exception e) {
                    this.getTextView().setText(SmileUtils.getSmiledText(this.getContext(), messageText));
                }
                hideTranslateView();
            } else {
                this.getTextView().setText(SmileUtils.getSmiledText(this.getContext(), messageText));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (message.getFromUserId() == loginUserId) {
            //发送消息文本颜色
            this.getTextView().setTextColor(JfResourceUtil.getSkinColor(R.color.wechat_send_message_text_color));
            this.getTranslateTextView().setTextColor(JfResourceUtil.getSkinColor(R.color.wechat_send_message_text_color));

        } else {
            //接受消息文本颜色
            this.getTextView().setTextColor(JfResourceUtil.getSkinColor(R.color.black_deep_chat));
            this.getTranslateTextView().setTextColor(JfResourceUtil.getSkinColor(R.color.black_deep_chat));

        }

        //已经翻译过，显示翻译内容
        if (!TextUtils.isEmpty((CharSequence) message.getExtra("translate"))) {
            showTranslateView();
            this.getTranslateTextView().setText((CharSequence) message.getExtra("translate"));
        } else {
            hideTranslateView();
        }
    }

    @Override
    public boolean getIsInUse() {
        return this.isShown();
    }

    @Override
    public void setIsInUse(boolean isInUse) {


    }

    @Override
    public void prepareForReuse() {

    }
}
