package com.efounder.chat.http.download;

/**
 * Created by zhangshunyun.
 */
public interface DownloadTaskObserve {
    void update(String id);

    String getAppId();
}
