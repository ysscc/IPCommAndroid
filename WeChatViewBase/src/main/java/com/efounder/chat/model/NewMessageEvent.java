package com.efounder.chat.model;

import com.efounder.message.struct.IMStruct002;

public class NewMessageEvent {

    public IMStruct002 getImStruct002() {
        return imStruct002;
    }

    public void setImStruct002(IMStruct002 imStruct002) {
        this.imStruct002 = imStruct002;
    }

    private IMStruct002 imStruct002;

//    public int getUnReadNum() {
//        return unReadNum;
//    }
//
//    public void setUnReadNum(int unReadNum) {
//        this.unReadNum = unReadNum;
//    }
//
//    //这个字段本来是没有的，但是原来的计算未读数量的代码有这个字段，所以保险起见，使用了这个字段
//    private int unReadNum;


    public NewMessageEvent(IMStruct002 _imStruct002) {
        this.imStruct002 = _imStruct002;
        //this.unReadNum = _unReadNum;

    }

}
