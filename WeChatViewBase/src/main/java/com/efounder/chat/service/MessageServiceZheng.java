package com.efounder.chat.service;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;

public class MessageServiceZheng extends Service {

	public static final String TAG  = "MessageService";
	
	MsgReceiver mMsgReceiver ;
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		mMsgReceiver = new MsgReceiver();
		IntentFilter curIntentFilter = new IntentFilter();
		curIntentFilter.addAction(MsgReceiver.MSG_NEW_MSG_COMMING);
		registerReceiver(mMsgReceiver, curIntentFilter);
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(mMsgReceiver);
	}
}
