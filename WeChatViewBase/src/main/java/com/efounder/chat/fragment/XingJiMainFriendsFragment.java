package com.efounder.chat.fragment;

import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.View;
import android.widget.Toast;


import com.efounder.chat.R;
import com.efounder.chat.adapter.XingJiFriendsGroupAdapter;
import com.efounder.chat.db.WeChatDBManager;
import com.efounder.chat.presenter.publicnumberlist.PublicNumberListContract;
import com.efounder.chat.presenter.publicnumberlist.PublicNumberListPresenter;
import com.efounder.chat.view.fixedheader.PinnedHeaderItemDecoration;
import com.efounder.chat.view.fixedheader.PinnedHeaderRecyclerView;
import com.efounder.chat.view.fixedheader.entity.ExpandGroupItemEntity;
import com.efounder.frame.baseui.BasePresenterFragment;
import com.efounder.mobilecomps.contacts.PinyinComparator;
import com.efounder.mobilecomps.contacts.User;
import com.efounder.utils.ResStringUtil;
import com.utilcode.util.ToastUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * author : zzj
 * e-mail : zhangzhijun@pansoft.com
 * date   : 2018/8/2916:41
 * desc   :
 * version: 1.0
 */
public class XingJiMainFriendsFragment extends BasePresenterFragment<PublicNumberListContract.Presenter>
        implements PublicNumberListContract.View{

    private PinnedHeaderRecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private XingJiFriendsGroupAdapter mAdapter;

    private boolean fadeHeader = true;

    private int newFriendCount = 0;
    private int groupNotiveCount = 0;

    /**
     * 根据拼音来排列ListView里面的数据类
     */
    private PinyinComparator pinyinComparator;
    //数据源
    private List<User> sourceDataList;

    @Override
    protected void initView(View root) {
        mRecyclerView = root.findViewById(R.id.recycler_view_osplinkman_friend);
        mRecyclerView.setLayoutManager(mLayoutManager = new LinearLayoutManager(mActivity));
        mRecyclerView.addItemDecoration(new PinnedHeaderItemDecoration());

    }

    @Override
    protected void initData() {
        super.initData();

    }

    @Override
    protected void onFirstInit() {
        super.onFirstInit();
        // 实例化汉字转拼音类
        pinyinComparator = new PinyinComparator();
        sourceDataList = new ArrayList<User>();
        newFriendCount = WeChatDBManager.getInstance().getNewFriendUnread();
        groupNotiveCount = WeChatDBManager.getInstance().getGroupNoticveUnreadCount();
        List<User> myList = new ArrayList<User>();
        myList.addAll(WeChatDBManager.getInstance().getallFriends());
        sourceDataList.addAll(mPresenter.filledData(myList));
        // 根据a-z进行排序源数据
        Collections.sort(sourceDataList, pinyinComparator);
        initAdapter();
        initListener();
        mAdapter.setData(getDataList(sourceDataList));

    }

    @Override
    protected void initListener() {
        super.initListener();
        /**
         * 解决第一个item无法触发长按事件
         */
        mRecyclerView.setmPinnedHeaderLongClickListener(new PinnedHeaderRecyclerView.OnPinnedHeaderLongClickListener() {
            @Override
            public void onPinnedHeaderLongClick(int adapterPosition) {
                ToastUtils.showShort(R.string.wechatview_group_manager);
            }
        });
        /**
         * 当标题栏被悬浮的时候的点击功能
         */
        mRecyclerView.setOnPinnedHeaderClickListener(new PinnedHeaderRecyclerView.OnPinnedHeaderClickListener() {
            @Override
            public void onPinnedHeaderClick(int adapterPosition) {
                mAdapter.switchExpand(adapterPosition);
                //标题栏被点击之后，滑动到指定位置
                mLayoutManager.scrollToPositionWithOffset(adapterPosition, 0);
            }
        });
        /**
         * 列表好友成员点击事件
         */
        mAdapter.setOnItemClickListener(new XingJiFriendsGroupAdapter.OnItemClickListener() {
            @Override
            public void onItemClickListener(User subItem, int position) {
                Toast.makeText(mActivity,subItem.getReMark()+ResStringUtil.getString(R.string.wechatview_clicked),Toast.LENGTH_SHORT).show();
            }
        });
        /**
         * 列表好友成员长按事件
         */
        mAdapter.setOnItemLongClickListener(new XingJiFriendsGroupAdapter.OnItemLongClickListener() {
            @Override
            public void onItemLongClickListener(User subItem, int position) {
                ToastUtils.showShort(subItem.getReMark()+ResStringUtil.getString(R.string.wechatview_long_clicked)+position);
            }
        });
        /**
         * 列表好友分组长按事件
         */
        mAdapter.setOnTitleItemLongClickListener(new XingJiFriendsGroupAdapter.OnTitleItemLongClickListener() {
            @Override
            public void onTitleItemLongClickListener() {
                ToastUtils.showShort(R.string.wechatview_group_manager);
            }
        });
    }

    @Override
    protected void initAdapter() {

        mAdapter = new XingJiFriendsGroupAdapter(mActivity);

        mRecyclerView.setAdapter(mAdapter);
    }

    private List<ExpandGroupItemEntity<String,User>> getDataList(List<User> sourceDataList) {
        List<ExpandGroupItemEntity<String, User>> dataList = new ArrayList<>();
        for (int group = 0; group < 1; group++) {
            ExpandGroupItemEntity<String, User> groupItem = new ExpandGroupItemEntity<>();
            groupItem.setExpand(false);
            groupItem.setParent(ResStringUtil.getString(R.string.wechatview_my_friend));
            groupItem.setChildList(sourceDataList);
            dataList.add(groupItem);
        }

        return dataList;
    }

    @Override
    protected  PublicNumberListPresenter initPresenter() {
        return new PublicNumberListPresenter(this);
    }

    @Override
    protected int getContentLayoutId() {
        return R.layout.espwechatview_fragment_xingji_friends;
    }


    @Override
    public void filterDataSuccess(List<User> userList) {

    }
}
