package com.efounder.chat.event;


import com.efounder.chat.model.Task;

import java.util.List;

/**
 * 任务待办或已办条目个数
 * Created by slp on 2018/1/20.
 */

public class ProcessedTaskListSizeEvent {

    private final List<Task> mData;

    public ProcessedTaskListSizeEvent(int size, List<Task> mData) {
        this.size = size;
        this.mData = mData;
    }

    int size;

    public int getSize() {
        return size;
    }

    public List<Task> getDataList() {
        return mData;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
