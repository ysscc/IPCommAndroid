package com.efounder.chat.model;

import com.efounder.message.struct.IMStruct002;
import com.efounder.mobilecomps.contacts.User;

import java.io.Serializable;
import java.util.List;

public class ChatListItem implements Serializable, Cloneable {// 1.实现Serializable接口
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /**
     * 登录的用户id
     */
    private int loginUserId;
    /**
     * 当前聊天的用户id
     */
    private int userId;
    /**
     * 角标
     **/
    private int badgernum = 0;
    /**
     * 是否免打扰
     **/
    private Boolean isBother = false;
    /**
     * 角是否置顶
     **/
    private Boolean isTop = false;
    /**
     * 是否正在发送
     **/
    private Boolean isSend = false;
    /**
     * 名称
     **/
    private String name;
    /**
     * 类型（单聊群聊，公众号）
     **/
    private int chatType;
    /**
     * 头像
     **/
    private String avatar;
    /**
     * 消息结构体
     **/
    private IMStruct002 struct002;
    private User user;
    private Group group;
    private List<IMStruct002> voiceList;//列表中未读的语音消息

    public ChatListItem() {
        super();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getLoginUserId() {
        return loginUserId;
    }

    public void setLoginUserId(int loginUserId) {
        this.loginUserId = loginUserId;
    }

    public Boolean getIsSend() {
        return isSend;
    }

    public void setIsSend(Boolean isSend) {
        this.isSend = isSend;
    }

    public int getBadgernum() {
        return badgernum;
    }

    public void setBadgernum(int badgernum) {
        this.badgernum = badgernum;
    }

    public Boolean getIsBother() {
        return isBother;
    }

    public void setIsBother(Boolean isBother) {
        this.isBother = isBother;
    }

    public Boolean getIsTop() {
        return isTop;
    }

    public void setIsTop(Boolean isTop) {
        this.isTop = isTop;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getChatType() {
        return chatType;
    }

    public void setChatType(int chatType) {
        this.chatType = chatType;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public IMStruct002 getStruct002() {
        return struct002;
    }

    public void setStruct002(IMStruct002 struct002) {
        this.struct002 = struct002;
    }

    public List<IMStruct002> getVoiceList() {
        return voiceList;
    }

    public void setVoiceList(List<IMStruct002> voiceList) {
        this.voiceList = voiceList;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        ChatListItem chatListItem = (ChatListItem) super.clone();
        if (user != null) {
            chatListItem.user = (User) user.clone();
        }
        if (group != null) {
            chatListItem.group = (Group) group.clone();
        }
        return chatListItem;
    }
}