package com.efounder.chat.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.efounder.chat.R;
import com.efounder.chat.adapter.AddPublicUserListAdapter;
import com.efounder.chat.http.GetHttpUtil;
import com.efounder.chat.service.MessageService;
import com.efounder.mobilecomps.contacts.User;
import com.efounder.util.ToastUtil;
import com.efounder.utils.ResStringUtil;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import static com.efounder.frame.utils.NetStateBroadcastReceiver.isNetActive;

/**
 * @author yqs
 */
public class AddPublicFriendsTwoActivity extends BaseActivity {

    private String uid;
    List<User> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addfriends_two);
        list = new ArrayList<>();

        final RelativeLayout re_search = (RelativeLayout) this
                .findViewById(R.id.re_search);
        final TextView tv_search = (TextView) re_search
                .findViewById(R.id.tv_search);
        final EditText et_search = (EditText) this.findViewById(R.id.et_search);
        et_search.setHint(ResStringUtil.getString(R.string.chat_search_app_number));
        et_search.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (s.length() > 0) {
                    re_search.setVisibility(View.VISIBLE);
                    tv_search.setText(et_search.getText().toString().trim());
                } else {

                    re_search.setVisibility(View.GONE);
                    tv_search.setText("");

                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void afterTextChanged(Editable s) {

            }
        });
        re_search.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                uid = et_search.getText().toString().trim();
                if (uid == null || uid.equals("")) {
                    return;
                }
                if (!isNetActive()) {//无网络
                    ToastUtil.showToast(AddPublicFriendsTwoActivity.this, R.string.network_anomalies);
                    return;
                }
                try {
                    GetHttpUtil.searchPublicNumber(AddPublicFriendsTwoActivity.this, uid, new GetHttpUtil.SearchPublicNumberCallBack() {

                        @Override
                        public void getPublicSuccess(List<User> users, boolean isSuccess) {
                            if (isSuccess) {
                                if (users.size() == 0) {
                                    Toast toast = Toast.makeText(AddPublicFriendsTwoActivity.this, "没有匹配的用户", Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.CENTER, 0, 0);
                                    toast.show();

                                    return;
                                }
                                list.addAll(users);
                                generateListView(list);
                            }
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        });

    }

    public void generateListView(final List<User> users) {
        if (users.size() == 1) {
            Intent intent = new Intent(AddPublicFriendsTwoActivity.this,
                    AddPublicFriendInfoActivity.class);
            intent.putExtra("user", users.get(0));
            startActivity(intent);
            AddPublicFriendsTwoActivity.this.finish();
            return;
        }
        ListView listView = (ListView) findViewById(R.id.listView_addfriend);
        AddPublicUserListAdapter adapter = new AddPublicUserListAdapter(users);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                Intent intent = new Intent(AddPublicFriendsTwoActivity.this,
                        AddPublicFriendInfoActivity.class);
                intent.putExtra("user", users.get(position));
                startActivity(intent);
                AddPublicFriendsTwoActivity.this.finish();
            }
        });

    }

    @Override
    public void back(View view) {
        finish();
    }

}