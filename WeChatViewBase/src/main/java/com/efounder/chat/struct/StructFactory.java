package com.efounder.chat.struct;

import com.core.xml.StubObject;
import com.efounder.chat.R;
import com.efounder.chat.utils.FileEditTimeUtil;
import com.efounder.chat.utils.FileSizeUtil;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.message.struct.IMStruct002;
import com.efounder.utils.ResStringUtil;

import net.sf.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.efounder.frame.utils.Constants.CHAT_USER_ID;

/**
 * 消息体工厂类
 *
 * @author hudq
 */
public class StructFactory {

    /**
     * 0:个人到个人
     **/
    public static final byte TO_USER_TYPE_PERSONAL = 0;
    /**
     * 1:个人到群组
     **/
    public static final byte TO_USER_TYPE_GROUP = 1;
    /**
     * 2:公众号
     **/
    public static final byte TO_USER_TYPE_OFFICIAL_ACCOUNT = 2;

    private static StructFactory factory;


    private StructFactory() {
        super();
    }

    public static StructFactory getInstance() {
        if (factory == null) {
            factory = new StructFactory();
        }
        return factory;
    }

    /**
     * 创建文字消息（普通文本）
     * @param content 内容
     * @param toUserType 要发送到的用户类型
     * @return
     */
    public IMStruct002 createTextStruct(String content, byte toUserType) {
        IMStruct002 imStruct002 = new IMStruct002();
        long time = System.currentTimeMillis();
        //todo 以前普通文本消息使用的是 subtype 为0 现在我们改造成使用json 格式，消息类型为 11


        imStruct002.setBody(content.getBytes());
        imStruct002.setTime(time);
        imStruct002.setToUserType(toUserType);
        byte messagechildType6 = MessageChildTypeConstant.subtype_text;
        imStruct002.setMessageChildType(messagechildType6);

        return imStruct002;
    }

    //创建json格式的文本消息
    public IMStruct002 createJsonTextStruct(String content, byte toUserType) {
        IMStruct002 imStruct002 = new IMStruct002();
        long time = System.currentTimeMillis();
        imStruct002.setBody(content.getBytes());
        imStruct002.setTime(time);
        imStruct002.setToUserType(toUserType);
        byte messagechildType6 = MessageChildTypeConstant.subtype_format_text;
        imStruct002.setMessageChildType(messagechildType6);
        return imStruct002;
    }

    /**
     * 创建图片消息
     * @param url 图片在云盘的url
     * @param path 图片本地路径
     * @param scale 图片宽高比例 (4:3)
     * @param toUserType 要发送到的用户类型
     * @return
     */
    public IMStruct002 createImageStruct(String url, String path, String scale, byte toUserType) {
        IMStruct002 imStruct002 = null;
        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put("url", url);
        map.put("path", path);
        map.put("scale", scale);
        JSONObject jsonObject = JSONObject.fromObject(map);
        String message = jsonObject.toString();
        long time = System.currentTimeMillis();
        if (url != null && !"".equals(url)) {
            imStruct002 = new IMStruct002();
            imStruct002.setMessage(message);
            imStruct002.setTime(time);
            imStruct002.setToUserType(toUserType);
            imStruct002.setFromUserId(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)));
            byte messagechildType = MessageChildTypeConstant.subtype_image;
            imStruct002.setMessageChildType(messagechildType);
        }
        return imStruct002;
    }

    @Deprecated
    public IMStruct002 createMapStruct(String path, byte toUserType) {
        IMStruct002 imStruct002 = null;
        long time = System.currentTimeMillis();
        if (path != null && !"".equals(path)) {
            imStruct002 = new IMStruct002();
            //加.pic的原因是改变图片的扩展名，使其不再图库显示
            imStruct002.setBody(path.getBytes());
            imStruct002.setTime(time);
            imStruct002.setToUserType(toUserType);
            byte messagechildType = MessageChildTypeConstant.subtype_location;
            imStruct002.setMessageChildType(messagechildType);
        }
        return imStruct002;
    }

    @Deprecated
    public IMStruct002 createMapStruct1(String path) {
        IMStruct002 imStruct002 = null;
        long time = System.currentTimeMillis();
        if (path != null && !"".equals(path)) {
            imStruct002 = new IMStruct002();
            //加.pic的原因是改变图片的扩展名，使其不再图库显示
            imStruct002.setBody(("file://" + path + ".pic").getBytes());
            imStruct002.setTime(time);
            imStruct002.setToUserType(TO_USER_TYPE_PERSONAL);
            byte messagechildType = MessageChildTypeConstant.subtype_location;
            imStruct002.setMessageChildType(messagechildType);
        }
        return imStruct002;
    }

    //创建语音消息
    public IMStruct002 createVoiceStruct(String url, String path, String voiceTime, byte toUserType) {
        IMStruct002 imStruct002 = null;
        long time = System.currentTimeMillis();
        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put("url", url);
        map.put("path", path);
        map.put("time", voiceTime);
        map.put("device", "android");
        JSONObject jsonObject = JSONObject.fromObject(map);
        String message = jsonObject.toString();
//		if (!(new File(filePath).exists())) {
//			return imStruct002;
//		}
        imStruct002 = new IMStruct002();
        imStruct002.setMessage(message);
//		imStruct002.setBody(filePath.getBytes());
//		imStruct002.setBody("http://oap8mw8pg.bkt.clouddn.com/FsQDVnTolFH9W-XsqQtUc1kfnM9k".getBytes());
//		PublicMessageUtils.sendMsg(filePath);
        imStruct002.setTime(time);
        imStruct002.setToUserType(toUserType);
        imStruct002.setFromUserId(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)));
        byte messagechildType = MessageChildTypeConstant.subtype_voice;
        imStruct002.setMessageChildType(messagechildType);

        return imStruct002;
    }


    public IMStruct002 createOfficialAccountStruct(String json) {
        if (json == null) {
            return null;
        }
        IMStruct002 imStruct002 = new IMStruct002();
//		String body = "{\"content\":[{\"title\":\"公众号头标题\",\"image\":\"assets://00.png\",\"url\":\"http://www.sina.com.cn/\"},{\"title\":\"公共号底部标题\",\"image\":\"assets://01.png\",\"url\":\"http://www.163.com/\"},{\"title\":\"公共号底部标题\",\"image\":\"assets://02.png\",\"url\":\"http://www.163.com/\"}]}";
        imStruct002.setBody(json.getBytes());
        imStruct002.setTime(System.currentTimeMillis());
        byte childType4 = (byte) 2;
        imStruct002.setToUserType(childType4);
        byte messagechildType4 = MessageChildTypeConstant.subtype_officalAccount;
        imStruct002.setMessageChildType(messagechildType4);

        return imStruct002;
    }

    //创建视频消息
    public IMStruct002 createVideoStruct(String url, String path, String videoTime, byte toUserType) {
        IMStruct002 imStruct002 = null;
        long time = System.currentTimeMillis();
        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put("url", url);
        map.put("path", path);
        map.put("time", videoTime);
        JSONObject jsonObject = JSONObject.fromObject(map);
        String message = jsonObject.toString();
        if (url != null && !"".equals(url)) {
            imStruct002 = new IMStruct002();
            //加.pic的原因是改变图片的扩展名，使其不再图库显示
//			imStruct002.setBody((url).getBytes());
//			imStruct002.setBody(("http://oap8mw8pg.bkt.clouddn.com/FuU6x8HQ_AAnWUhL9Y5J0qyZ9uK_").getBytes());
            imStruct002.setMessage(message);
            imStruct002.setTime(time);
            imStruct002.setToUserType(toUserType);
            byte messagechildType = MessageChildTypeConstant.subtype_smallVideo;
            imStruct002.setMessageChildType(messagechildType);
        }

        return imStruct002;
    }

    public IMStruct002 createFormStruct(String json) {
        IMStruct002 imStruct002 = new IMStruct002();
        try {
            imStruct002.setBody(json.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        long time = System.currentTimeMillis();
        imStruct002.setTime(time);
        imStruct002.setToUserType(TO_USER_TYPE_PERSONAL);
        imStruct002.setMessageChildType(MessageChildTypeConstant.subtype_form);
        return imStruct002;
    }

    //创建web网页的消息
    public IMStruct002 createWebViewStruct(String json, byte toUserType) {
        IMStruct002 imStruct002 = new IMStruct002();
        try {
            imStruct002.setBody(json.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        long time = System.currentTimeMillis();
        imStruct002.setTime(time);
        imStruct002.setToUserType(toUserType);
        imStruct002.setMessageChildType(MessageChildTypeConstant.subtype_officalweb);
        return imStruct002;
    }

    //通用生页面的消息
    public IMStruct002 createCommonStruct(StubObject stubObject, byte toUserType) {

        Map<String, String> map = new HashMap<>();
        Hashtable<String, String> hashtable = stubObject.getStubTable();

        map.put("title", "这是标题");
        // map.put("subTitle", "这是副标题");
        map.put("image", "http://panserver.solarsource.cn:9692/panserver/files/3df20c10-4f0a-4ae6-ac8d-7b5299c92f1d/download");
        map.put("smallIcon",
                "http://panserver.solarsource.cn:9692/panserver/files/3df20c10-4f0a-4ae6-ac8d-7b5299c92f1d/download");
        map.put("time", "2017.06.23");
        map.put("content", "第一“革”，实质上革的是政府的“越位权力”。这种审批发证的权力，直接从高度集中的计划经济年代承续而来。原来政府搞计划下指标，现在大多数计划取消了，但很多审批事项并未相应取消，“越位”的管理体制和权力仍保留着。更关键的是，如李克强一再指出，一些政府部门的观念并未相应转变，仍停留在“重审批、轻监管、弱服务”的阶段。");
        map.put("systemName", "QQ浏览器");
        if (hashtable.get("viewType").equals("webView")) {
            map.put("title", "《变形金刚5：最后的骑士》预告片");
            map.put("content", "独家预告片");

        }
        if (hashtable.get("viewType").equals("native")) {
            map.put("title", "这是标题");
            map.put("content", "这是内容详情，这是内容详情，这是内容详情..");
            map.put("systemName", "原生页面");

        }
        if (hashtable.get("viewType").equals("reactNative")) {
            map.put("title", "点击打开RN页面");
            map.put("content", "这是内容详情，这是内容详情，这是内容详情..");
            map.put("systemName", "RN页面");

        }
        map.put("viewType", hashtable.get("viewType"));
        if (hashtable.containsKey("url")) {
            map.put("url", hashtable.get("url"));
        }
        if (hashtable.containsKey("state")) {
            map.put("urlState", hashtable.get("state"));
        }
        if (hashtable.containsKey("tcms")) {
            map.put("tcms", hashtable.get("tcms"));
        }
        if (hashtable.containsKey("show")) {
            map.put("show", hashtable.get("show"));
        }
        if (hashtable.containsKey("width")) {
            map.put("width", hashtable.get("width"));
        }
        if (hashtable.containsKey("height")) {
            map.put("height", hashtable.get("width"));
        }
        if (hashtable.containsKey("tcms")) {
            map.put("tcms", hashtable.get("tcms"));
        }

        org.json.JSONObject jsonObject = new org.json.JSONObject(map);
        String content = jsonObject.toString();


        IMStruct002 imStruct002 = new IMStruct002();
        try {
            imStruct002.setBody(content.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        long time = System.currentTimeMillis();
        imStruct002.setTime(time);
        imStruct002.setToUserType(toUserType);
        imStruct002.setMessageChildType(MessageChildTypeConstant.subtype_common);
        return imStruct002;
    }

    //创建文件消息
    public IMStruct002 createFileStruct(String url, String resUrl, String path, String property, byte toUserType) {
        IMStruct002 imStruct002 = null;
        String fileType = null;
        String fileName = path.substring(path.lastIndexOf("/") + 1, path.length());
        if (null != fileName && fileName.contains(".")) {
            fileType = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
        } else {
            fileType = "";
        }
        long time = System.currentTimeMillis();
        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put("FileId", resUrl);
        map.put("url", url);
        map.put("FileLocalPath", path);
        map.put("FileSize", property);
        map.put("FileName", fileName);
        map.put("FileType", fileType);
        JSONObject jsonObject = JSONObject.fromObject(map);
        String message = jsonObject.toString();
        if (resUrl != null && !"".equals(resUrl)) {
            imStruct002 = new IMStruct002();
            imStruct002.setMessage(message);
            imStruct002.setTime(time);
            imStruct002.setToUserType(toUserType);
            byte messageChildType = MessageChildTypeConstant.subtype_file;
            imStruct002.setMessageChildType(messageChildType);
        }

        return imStruct002;
    }

    //创建位置消息
    public IMStruct002 createLocationStruct(String resUrl, String path, String property, byte toUserType) {
        IMStruct002 imStruct002 = null;
        JSONObject jsonObject = JSONObject.fromObject(property);
        jsonObject.put("url", resUrl);//地图截图 url
        jsonObject.put("localPath", path);//地图截图本地路径

        imStruct002 = new IMStruct002();
        imStruct002.setMessage(jsonObject.toString());
        imStruct002.setTime(System.currentTimeMillis());
        imStruct002.setToUserType(toUserType);
        byte messageChildType = MessageChildTypeConstant.subtype_location;
        imStruct002.setMessageChildType(messageChildType);
        return imStruct002;
    }

    //创建动画消息
    public IMStruct002 createShakeImstruct002(String name) {
        if (!"gold".equals(name)) {
            return null;
        }
        IMStruct002 imStruct002 = new IMStruct002();
        //{"type":"animation","name":"gold","no":"00001","duration":"2.000000","repeat":"1"}
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("type", "animation");
        jsonObject.put("no", "00001");
        jsonObject.put("name", name);
        jsonObject.put("duration", "2.000000");
        jsonObject.put("repeat", "1");

        try {
            imStruct002.setBody(jsonObject.toString().getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        long time = System.currentTimeMillis();
        imStruct002.setTime(time);
        imStruct002.setMessageChildType(MessageChildTypeConstant.subtype_anim);
        return imStruct002;
    }

    //创建密图消息
    public IMStruct002 createSecretImgImstrct002(String resUrl, String path, String property, byte toUserType) {
        IMStruct002 imStruct002 = null;
        JSONObject jsonObject = JSONObject.fromObject(property);
        jsonObject.put("url", resUrl);//加密文件 url
        jsonObject.put("path", path);//图片本地路径

        String message = jsonObject.toString();
        long time = System.currentTimeMillis();
        if (resUrl != null && !"".equals(resUrl)) {
            imStruct002 = new IMStruct002();
            imStruct002.setMessage(message);
            imStruct002.setTime(time);
            imStruct002.setToUserType(toUserType);
            imStruct002.setFromUserId(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)));
            byte messagechildType = MessageChildTypeConstant.subtype_secret_image;
            imStruct002.setMessageChildType(messagechildType);
        }

        return imStruct002;
    }

    //创建密件消息
    public IMStruct002 createSecretFileImstrct002(String resUrl, String path, String property, byte toUserType) {
        IMStruct002 imStruct002 = null;
        String fileType;
        String fileName = path.substring(path.lastIndexOf("/") + 1, path.length());
        fileName = fileName.replace(".bin", "");
        if (null != fileName && fileName.contains(".")) {
//            fileType = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
            fileType = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
        } else {
            fileType = "";
        }
        JSONObject jsonObject = JSONObject.fromObject(property);
        jsonObject.put("url", resUrl);//加密文件 url
        jsonObject.put("FileLocalPath", path);//图片本地路径
        jsonObject.put("FileSize", FileSizeUtil.getAutoFileOrFilesSize(path));
        jsonObject.put("FileName", fileName);
        jsonObject.put("FileType", fileType);
        jsonObject.put("FileTime", FileEditTimeUtil.getFileEditTime(path));
        String message = jsonObject.toString();
        long time = System.currentTimeMillis();
        if (resUrl != null && !"".equals(resUrl)) {
            imStruct002 = new IMStruct002();
            imStruct002.setMessage(message);
            imStruct002.setTime(time);
            imStruct002.setToUserType(toUserType);
            imStruct002.setFromUserId(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)));
            byte messagechildType = MessageChildTypeConstant.subtype_secret_file;
            imStruct002.setMessageChildType(messagechildType);
        }

        return imStruct002;
    }

    //创建分享网页的消息
    public IMStruct002 createShareWebStruct(String url, String imgUrl, String subject, String text, byte toUserType) {
        IMStruct002 imStruct002 = null;
        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put("url", url);
        map.put("imgUrl", imgUrl);
        map.put("subject", subject);
        map.put("text", text);
        JSONObject jsonObject = JSONObject.fromObject(map);
        String message = jsonObject.toString();
        imStruct002 = new IMStruct002();
        imStruct002.setMessage(message);
        long time = System.currentTimeMillis();
        imStruct002.setTime(time);
        imStruct002.setToUserType(toUserType);
        imStruct002.setFromUserId(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)));
        byte messagechildType = MessageChildTypeConstant.subtype_share_web;
        imStruct002.setMessageChildType(messagechildType);
        return imStruct002;
    }

    /**
     * 创建撤回的系统消息
     *
     * @param messageID 消息id
     * @param toUserId
     * @param chatType
     * @return
     */
    public IMStruct002 createRecallMessage(String messageID, int toUserId, byte chatType) {
        IMStruct002 imStruct002 = null;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("CMD", "recallMessage");
        jsonObject.put("messageID", messageID);
        jsonObject.put("chatType", chatType);
        String message = jsonObject.toString();
        imStruct002 = new IMStruct002();
        imStruct002.setMessage(message);
        long time = System.currentTimeMillis();
        imStruct002.setTime(time);
        imStruct002.setToUserType(chatType);
        imStruct002.setToUserId(toUserId);
        imStruct002.setFromUserId(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)));
        byte messagechildType = MessageChildTypeConstant.subtype_common_system_info;
        imStruct002.setMessageChildType(messagechildType);
        return imStruct002;
    }

    /**
     * 创建红包消息
     *
     * @param chatType
     * @return
     */
    public IMStruct002 createRedPackageMessage(String message, int toUserId, byte chatType) {
        JSONObject jsonObject = new JSONObject();
        IMStruct002 imStruct002 = new IMStruct002();
        imStruct002.setMessage(message);
        long time = System.currentTimeMillis();
        imStruct002.setTime(time);
        imStruct002.setToUserType(chatType);
        imStruct002.setToUserId(toUserId);
        imStruct002.setFromUserId(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)));
        byte messagechildType = MessageChildTypeConstant.subtype_red_package;
        imStruct002.setMessageChildType(messagechildType);
        return imStruct002;
    }

    /**
     * 创建语音识别消息
     *
     * @param resUrl
     * @param path
     * @param property
     * @param toUserType
     * @return
     */
    public IMStruct002 createSpeechRecgonitionImstrct002(String resUrl, String path, String property, byte toUserType) {
        IMStruct002 imStruct002 = null;
        JSONObject jsonObject = JSONObject.fromObject(property);
        jsonObject.put("url", resUrl);//语音文件 url
        jsonObject.put("path", path);//语音文件本地路径
        jsonObject.put("device", "android");
        imStruct002 = new IMStruct002();
        imStruct002.setMessage(jsonObject.toString());
        imStruct002.setTime(System.currentTimeMillis());
        imStruct002.setToUserType(toUserType);
        byte messageChildType = MessageChildTypeConstant.subtype_speechRecognize;
        imStruct002.setMessageChildType(messageChildType);
        return imStruct002;
    }

    /**
     * 创建通用的提醒消息 tips消息
     *
     * @param type       类型
     * @param content    通知内容
     * @param json       通知别的字段的json
     * @param toUserType
     * @return
     */
    public IMStruct002 createCommonNotificationImstruct002(String type, String content, String json, byte toUserType) {
        IMStruct002 imStruct002 = null;
        JSONObject jsonObject = null;
        try {
            jsonObject = JSONObject.fromObject(json);
        } catch (Exception e) {
        }
        if (jsonObject.isNullObject()) {
            jsonObject = new JSONObject();
        }
        //通知类型
        jsonObject.put("type", type);
        jsonObject.put("content", content);
        imStruct002 = new IMStruct002();
        imStruct002.setMessage(jsonObject.toString());
        imStruct002.setTime(System.currentTimeMillis());
        imStruct002.setToUserType(toUserType);
        byte messageChildType = MessageChildTypeConstant.subtype_chat_tips;
        imStruct002.setMessageChildType(messageChildType);
        return imStruct002;
    }

    /**
     * 分享商品的消息类型
     *
     * @param goodId      商品id
     * @param goodCover   图片url
     * @param goodName    名称
     * @param description 简介(现在换成价格了)
     * @return
     */
    public IMStruct002 createShareGoodsStruct(String goodId, String goodCover, String goodName, String description, byte toUserType) {
        IMStruct002 imStruct002 = new IMStruct002();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("title", goodName);
        jsonObject.put("image", goodCover);
        jsonObject.put("smallIcon", ResStringUtil.getString(R.string.wechatview_app_icon_url));
        jsonObject.put("time", "");
        jsonObject.put("content", description);
        jsonObject.put("systemName", ResStringUtil.getString(R.string.wrchatview_star_mall));
        jsonObject.put("AndroidShow", "com.efounder.activity.ProductDetailsActivity");
        jsonObject.put("show", "EShopGoodsDetailsController");
        jsonObject.put("viewType", "display");
        jsonObject.put("goodId", goodId);
        imStruct002.setMessage(jsonObject.toString());
        long time = System.currentTimeMillis();
        imStruct002.setTime(time);
        imStruct002.setToUserType(toUserType);
        imStruct002.setFromUserId(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)));
        byte messagechildType = MessageChildTypeConstant.subtype_common;
        imStruct002.setMessageChildType(messagechildType);
        return imStruct002;
    }

    /**
     * 创建分享名片的消息
     *
     * @param name
     * @param avatar
     * @param id
     * @param isUser 是否是分享的个人
     * @return
     */
    public IMStruct002 createShareNameCardStruct(String name, String avatar, int id, boolean isUser) {
        IMStruct002 imStruct002 = new IMStruct002();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("title", name);
        jsonObject.put("smallIcon", ResStringUtil.getString(R.string.wechatview_app_icon_url));
        jsonObject.put("time", "");

        if (isUser) {
            jsonObject.put("AndroidShow", "com.efounder.chat.activity.JFUserInfoActivity");
            jsonObject.put("show", "EFUserDetailViewController");
            jsonObject.put("userType", "user");
            jsonObject.put("systemName", ResStringUtil.getString(R.string.wechatview_person_name_card));
            jsonObject.put("content", ResStringUtil.getString(R.string.im_chat_number) + ":" + id);
            if (avatar.startsWith("http")) {
                jsonObject.put("image", avatar);
            } else {
                jsonObject.put("image", ResStringUtil.getString(R.string.wechatview_default_user_avatar_url));
            }
            jsonObject.put("image", avatar);
        } else {
            jsonObject.put("AndroidShow", "com.efounder.chat.activity.AddGroupUserInfoActivity");
            jsonObject.put("show", "GroupApplicantViewController");
            jsonObject.put("userType", "group");
            jsonObject.put("systemName", ResStringUtil.getString(R.string.wechatview_group_card));
            jsonObject.put("content", ResStringUtil.getString(R.string.chatgroupsettingactivity_qh) + id);
            //群组没有头像，使用默认图
            if (avatar.startsWith("http")) {
                jsonObject.put("image", avatar);
            } else {
                jsonObject.put("image", ResStringUtil.getString(R.string.wechatview_default_group_avatar_url));
            }

        }

        jsonObject.put("viewType", "display");
        jsonObject.put("id", id);

        imStruct002.setMessage(jsonObject.toString());
        long time = System.currentTimeMillis();
        imStruct002.setTime(time);
        imStruct002.setFromUserId(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)));
        byte messagechildType = MessageChildTypeConstant.subtype_common;
        imStruct002.setMessageChildType(messagechildType);
        return imStruct002;
    }

    /**
     * 创建 是否正在输入中的（对方正在输入...）
     *
     * @param toUserId    要发送的人
     * @param isInputting 是否正在输入
     */

    public IMStruct002 createWhetherBeInputtingImStruct(int toUserId, boolean isInputting) {
        IMStruct002 imStruct002 = null;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("CMD", "inputting");
        jsonObject.put("isInput", isInputting ? 1 : 0);
        jsonObject.put("fromUserId", Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)));

        String message = jsonObject.toString();
        imStruct002 = new IMStruct002();
        imStruct002.setMessage(message);
        long time = System.currentTimeMillis();
        imStruct002.setTime(time);
        imStruct002.setToUserType(StructFactory.TO_USER_TYPE_PERSONAL);
        imStruct002.setToUserId(toUserId);
        imStruct002.setFromUserId(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)));
        byte messagechildType = MessageChildTypeConstant.subtype_common_system_info;
        imStruct002.setMessageChildType(messagechildType);
        return imStruct002;
    }


}
