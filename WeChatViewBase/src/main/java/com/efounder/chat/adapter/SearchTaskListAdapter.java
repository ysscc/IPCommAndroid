package com.efounder.chat.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.model.Task;
import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.chat.widget.BadgeView;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.frame.utils.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by will on 17-5-8.
 * 任务列表适配器
 */

public class SearchTaskListAdapter extends RecyclerView.Adapter<SearchTaskListAdapter.MyViewHolder> {

    private Context context;
    private List<Task> mData;
//    private ImageLoader imageLoader;
//    private DisplayImageOptions options;

    private OnItemClickListener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickListener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }

    public SearchTaskListAdapter(Context context, List<Task> mData) {
        this.context = context;
        this.mData = mData;
    }

    public void updateSearchResult(ArrayList<Task> searchResultList) {
        if (searchResultList != null) {
            mData.clear();
            mData.addAll(searchResultList);
        }
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MyViewHolder viewHolder;
        if (EnvironmentVariable.getProperty(Constants.KEY_SETTING_APPID).equals("ZSYGXOSPMobile") || EnvironmentVariable.getProperty(Constants.KEY_SETTING_APPID).equals("ZSYGXOSPMobileYS")) {
            viewHolder = new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.task_list_item_search_gx, parent, false));
        } else {
            viewHolder = new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.task_list_item_search, parent, false));
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.badgeView.setBadgeMargin(3);
        holder.badgeView.setTextSize(13);
        holder.badgeView.setTag("badge");
//        imageLoader.displayImage(mData.get(position).getIcon(),holder.icon,options);
        if (!"".equals(mData.get(position).getIcon()) && null != mData.get(position).getIcon()) {
            LXGlideImageLoader.getInstance().displayImage(context, holder.icon, mData.get(position).getIcon());
        }
        //如果是铁工的工程，则前两个为他们的待办项目
        if (EnvironmentVariable.getProperty(Constants.KEY_SETTING_APPID).equals("OSPMobileLiveAppTG") ||
                EnvironmentVariable.getProperty(Constants.KEY_SETTING_APPID).equals("OSPMobileLiveAppTGTest")) {
            if (position == 0) {
                holder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.financal_service));
                //            holder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.hr_flow_jbsc));
            } else if (position == 1) {
                holder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.oa));
                //            holder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.hr_flow_sysq));
            }
        }

        //共享任务搜索列表，背景图四色循环
        if (EnvironmentVariable.getProperty(Constants.KEY_SETTING_APPID).equals("ZSYGXOSPMobile") || EnvironmentVariable.getProperty(Constants.KEY_SETTING_APPID).equals("ZSYGXOSPMobileYS")) {
            switch (position % 4) {
                case 0:
                    holder.icon.setBackgroundResource(R.color.task_item_1);
                    break;
                case 1:
                    holder.icon.setBackgroundResource(R.color.task_item_2);
                    break;
                case 2:
                    holder.icon.setBackgroundResource(R.color.task_item_3);
                    break;
                case 3:
                    holder.icon.setBackgroundResource(R.color.task_item_4);
                    break;
            }
        } else {
            switch (mData.get(position).getId()) {
                case "HR_FLOW_JBSQ":
                    holder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.hr_flow_jbsc));
                    break;
                case "HR_FLOW_SYSQ":
                    holder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.hr_flow_sysq));
                    break;
                case "HR_LDHTSQ_LC":
                    holder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.hr_ldhtsq_lc));
                    break;
                case "HR_MSKH_LC":
                    holder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.hr_mskh_lc));
                    break;
                case "HR_ZPJH_LC":
                    holder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.hr_zpjh_lc));
                    break;
                case "HT_CGHTSQKJ":
                    holder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ht_cghtsqkj));
                    break;
                case "HT_HGBLC":
                    holder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ht_hgblc));
                    break;
                case "HT_QTHTLC":
                    holder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ht_qthtlc));
                    break;
                case "HT_SCBLC":
                    holder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ht_scblc));
                    break;
                case "HT_XMSQ":
                    holder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ht_xmsq));
                    break;
                case "HT_YYBFZLC":
                    holder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ht_yybfzlc));
                    break;
                case "QJSQLC":
                    holder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.qjsqlc));
                    break;
                default:
                    holder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.qjsqlc));
            }
        }
        holder.tvTitle.setText(mData.get(position).getTitle());
        holder.tvInfo.setText(mData.get(position).getInfo());
        if (mData.get(position).getTodoNum() == 0) {
            holder.badgeView.setVisibility(View.INVISIBLE);
        } else {
            holder.badgeView.setVisibility(View.VISIBLE);
            holder.badgeView.setGravity(Gravity.CENTER);
            if (mData.get(position).getTodoNum() > 99) {
                holder.badgeView.setText("99+");
            } else {
                holder.badgeView.setText(String.valueOf(mData.get(position).getTodoNum()));
            }
            holder.badgeView.setBadgeBackgroundColor(context.getResources().getColor(
                    R.color.red));
            holder.badgeView.show();
        }

        if (mOnItemClickLitener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = holder.getLayoutPosition();
                    mOnItemClickLitener.onItemClick(holder.itemView, pos);
                }
            });

            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    int pos = holder.getLayoutPosition();
                    mOnItemClickLitener.onItemLongClick(holder.itemView, pos);
                    return false;
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout itemView;
        ImageView icon;
        TextView tvTitle, tvInfo;
        View containerView;
        BadgeView badgeView;

        public MyViewHolder(View view) {
            super(view);
            itemView = (RelativeLayout) view.findViewById(R.id.rl_taskItem);
            icon = (ImageView) view.findViewById(R.id.iv_icon);
            tvTitle = (TextView) view.findViewById(R.id.tv_title);
            tvInfo = (TextView) view.findViewById(R.id.tv_msg);
            containerView = view.findViewById(R.id.todo_num);
            badgeView = new BadgeView(context, containerView);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);

        void onItemLongClick(View view, int position);
    }

}
