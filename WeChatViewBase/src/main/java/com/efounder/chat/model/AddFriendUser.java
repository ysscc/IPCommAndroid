package com.efounder.chat.model;

import java.io.Serializable;

public class AddFriendUser implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** id  **/
	private int id;
	/** 姓名  **/
	private String name;
	/** 域用户  **/
	private String adName;
	/** 编号  **/
	private String number;
	/** 手机号  **/
	private String telphone;



	/** 头像url **/
	private String  avatar;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAdName() {
		return adName;
	}
	public void setAdName(String adName) {
		this.adName = adName;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getTelphone() {
		return telphone;
	}
	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}
	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

}
