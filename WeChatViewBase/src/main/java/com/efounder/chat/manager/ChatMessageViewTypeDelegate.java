package com.efounder.chat.manager;

import com.efounder.chat.struct.MessageChildTypeConstant;
import com.efounder.chat.utils.IMStruct002Util;
import com.efounder.message.struct.IMStruct002;

/**
 * 聊天view 类型管理
 *
 * @autor yqs
 * @date 2018/11/15 10:14
 **/
public class ChatMessageViewTypeDelegate {
    public static final int VIEW_TYPE_OUTGOING = 0;//发出的消息
    public static final int VIEW_TYPE_INCOMMING = 1;//收到的消息
    public static final int VIEW_TYPE_OFFICIAL_ACCTOUNT = 2;//应用号消息（两边没有头像）
    public static final int VIEW_TYPE_TIPS = 3;//撤回消息（包括群通知消息等提示消息）
    public static final int VIEW_TYPE_SYSTEM = 4;//系统消息
    private int myUserId;


    public ChatMessageViewTypeDelegate(int myUserId) {
        this.myUserId = myUserId;
    }

    /**
     * 获取消息展示的底部view 类型
     *
     * @param iMStruct002
     * @return
     */
    public int getItemViewType(IMStruct002 iMStruct002) {
        short chidlMessageType = iMStruct002.getMessageChildType();
        if (iMStruct002.isRecall()) {
            //撤回消息
            return VIEW_TYPE_TIPS;
        }




        if (chidlMessageType == MessageChildTypeConstant.subtype_chat_tips) {
            //通知消息
            return VIEW_TYPE_TIPS;
        }

        if (checkNoAvatar(chidlMessageType) || chidlMessageType ==MessageChildTypeConstant.subtype_custom_without_avatar) {
            //没有头像的布局
            return VIEW_TYPE_OFFICIAL_ACCTOUNT;
        }

        int toUserType = iMStruct002.getToUserType();
//        if (toUserType == StructFactory.TO_USER_TYPE_OFFICIAL_ACCOUNT) {
//            return VIEW_TYPE_OFFICIAL_ACCTOUNT;
//        } else {
            int fromUserId = iMStruct002.getFromUserId();
            if (myUserId == fromUserId) {
                //消息我发送的
                if (IMStruct002Util.isSystemMessage(iMStruct002)) {
                    return VIEW_TYPE_SYSTEM;
                }
                return VIEW_TYPE_OUTGOING;
            } else {
                //消息不是我发送的
                if (iMStruct002.getMessageChildType() == MessageChildTypeConstant.subtype_officalAccount
                        || iMStruct002.getMessageChildType() == MessageChildTypeConstant.subtype_officalweb) {
                    return VIEW_TYPE_OFFICIAL_ACCTOUNT;
                } else {
                    return VIEW_TYPE_INCOMMING;
                }
            }
      //  }


    }

    /**
     * 检查消息类型是否具有头像的布局
     *
     * @param chidlMessageType
     * @return
     */
    private boolean checkNoAvatar(short chidlMessageType) {
        if (chidlMessageType == MessageChildTypeConstant.subtype_gxtask) {
            return true;
        }
        return false;

    }

    public int getViewTypeCount() {
        return 5;
    }

}
