 
package com.efounder.chat.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.efounder.chat.R;
import com.efounder.chat.http.WeChatHttpLoginManager;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.frame.baseui.EFFragment;
import com.efounder.utils.ResStringUtil;

import static com.efounder.frame.utils.Constants.CHAT_PASSWORD;
import static com.efounder.frame.utils.Constants.CHAT_USER_ID;

/**
 * 登陆页面
 * 
 */
@Deprecated
public class LoginFragment extends EFFragment implements WeChatHttpLoginManager.LoginListener,OnClickListener{
	private static final String TAG = "LoginFragment";
	private View view;
	private LoginFragmentCallBack callBack;
	
    private EditText et_usertel;
    private EditText et_password;
    private Button btn_login;
    private Button btn_qtlogin;
    
    //后台连接
    WeChatHttpLoginManager weChatHttpRequest;
    ProgressDialog dialog;

    @Override
    public void onAttach(Activity context) {
    	try {
			callBack = (LoginFragmentCallBack) context;
		} catch (Exception e) {
			Log.e(TAG, "使用LoginFragment，必须实现LoginFragmentCallBack接口");
			e.printStackTrace();
		}
    	super.onAttach(context);
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater,
    		 ViewGroup container,  Bundle savedInstanceState) {
    	view = inflater.inflate(R.layout.fragment_login, container, false);
    	weChatHttpRequest = WeChatHttpLoginManager.getInstance(getActivity());
    	weChatHttpRequest.addLoginListener(this);

    	initView(view);
        //getDeviceInfo();
    	return view;
    }
    
    private void initView(View view){
        et_usertel = (EditText) view.findViewById(R.id.et_usertel);
        et_password = (EditText) view.findViewById(R.id.et_password);
        btn_login = (Button) view.findViewById(R.id.btn_login);
        btn_qtlogin = (Button) view.findViewById(R.id.btn_qtlogin);
        // 监听多个输入框

        et_usertel.addTextChangedListener(new TextChange());
        et_password.addTextChangedListener(new TextChange());

        btn_login.setOnClickListener(this);
        btn_qtlogin.setOnClickListener(this);
        
        dialog = new ProgressDialog(getActivity());
    }
    
    

    // EditText监听器
    class TextChange implements TextWatcher {

        @Override
        public void afterTextChanged(Editable arg0) {

        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                int arg3) {

        }

        @Override
        public void onTextChanged(CharSequence cs, int start, int before,
                int count) {

            boolean Sign2 = et_usertel.getText().length() > 0;
            boolean Sign3 = et_password.getText().length() > 0;

            if (Sign2 & Sign3) {
                btn_login.setTextColor(0xFFFFFFFF);
                btn_login.setEnabled(true);
            }
            // 在layout文件中，对Button的text属性应预先设置默认值，否则刚打开程序的时候Button是无显示的
            else {
                btn_login.setTextColor(0xFFD0EFC6);
                btn_login.setEnabled(false);
            }
        }

    }


    public interface LoginFragmentCallBack{
    	public void onLoginSuccess();
    	public void onClickRegister();
    }

	@Override
	public void onLoginSuccess() {
		if (dialog.isShowing()) {
			dialog.dismiss();
		}
		//存储用户名，密码
		EnvironmentVariable.setProperty(CHAT_USER_ID,et_usertel.getText().toString().trim());
		EnvironmentVariable.setProperty(CHAT_PASSWORD,et_password.getText().toString().trim());
		System.out.println(EnvironmentVariable.getProperty(CHAT_USER_ID));
		System.out.println(EnvironmentVariable.getProperty(CHAT_PASSWORD));
		callBack.onLoginSuccess();
	}

	@Override
	public void onLoginFail(String errorMsg) {
		// TODO Auto-generated method stub
		if (dialog.isShowing()) {
			dialog.dismiss();
		}
		Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_LONG).show();
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btn_login) {//登录
            final String password = et_password.getText().toString().trim();
            final String usertel = et_usertel.getText().toString().trim();
            
    		dialog.setMessage(ResStringUtil.getString(R.string.wrchatview_logining));
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.show();
            weChatHttpRequest.httpLogin(usertel, password);

		}else if (v.getId() == R.id.btn_qtlogin) {//注册
			callBack.onClickRegister();
		}
		
	}


	@Override
	public void onDestroy() {
		super.onDestroy();
	}
}
