package com.efounder.chat.item;

import android.content.Context;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.TypedValue;
import android.view.View;

import com.efounder.chat.R;
import com.efounder.chat.item.manager.IMessageItem;
import com.efounder.chat.struct.MessageChildTypeConstant;
import com.efounder.chat.utils.SmileUtils;
import com.efounder.frame.ViewSize;
import com.efounder.message.struct.IMStruct002;
import com.efounder.ui.util.DisplayUtil;
import com.efounder.util.JSONUtil;

/**
 * 
 * @author lch
 *  支持文本、超链接、打电话
 *
 */
public class TextMessageItem extends AppCompatTextView implements IMessageItem {

	public TextMessageItem(Context context) {
		super(context);
		//this.setTextSize(15);
		this.setTextSize(TypedValue.COMPLEX_UNIT_SP,16); //16SP
		this.setMaxWidth(DisplayUtil.getMobileWidth(context)*2/3);
		this.setTextColor(getResources().getColor(R.color.black_deep_chat));
		this.setPadding(12,5,12,5);
	}

	@Override
	public View messageView() {
		return this;
	}

	@Override
	public ViewSize messageViewSize() {
		return null;
	}

	@Override
	public void setIMStruct002(IMStruct002 message) {
		byte[] body0 = message.getBody();
		try {
			if (message.getMessageChildType()== MessageChildTypeConstant.subtype_mZoneNotification){
				try {
					String text = JSONUtil.parseJson(new String(body0, "utf-8")).get("showMsg").getAsString();
					this.setText(SmileUtils.getSmiledText(this.getContext(), text));
				} catch (IllegalStateException e) {
					this.setText(SmileUtils.getSmiledText(this.getContext(), new String(body0, "utf-8")));
				}
			}else {
				this.setText(SmileUtils.getSmiledText(this.getContext(), new String(body0, "utf-8")));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean getIsInUse() {
		return this.isShown();
	}

	@Override
	public void setIsInUse(boolean isInUse) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void prepareForReuse() {
		
	}
}
