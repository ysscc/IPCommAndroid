//package com.efounder.chat.item;
//
//import android.app.Activity;
//import android.content.Context;
//import android.content.res.AssetFileDescriptor;
//import android.util.AttributeSet;
//import android.view.View;
//import android.widget.LinearLayout.LayoutParams;
//import android.widget.Toast;
//
//import com.efounder.chat.item.manager.IMessageItem;
//import com.efounder.frame.ViewSize;
//import com.efounder.message.struct.IMStruct002;
//import com.volokh.danylo.video_player_manager.manager.PlayerItemChangeListener;
//import com.volokh.danylo.video_player_manager.manager.SingleVideoPlayerManager;
//import com.volokh.danylo.video_player_manager.manager.VideoPlayerManager;
//import com.volokh.danylo.video_player_manager.meta.MetaData;
//import com.volokh.danylo.video_player_manager.ui.SimpleMainThreadMediaPlayerListener;
//import com.volokh.danylo.video_player_manager.ui.VideoPlayerView;
//
//import java.io.IOException;
//
///**
// *
// * @author lch 视频
// *
// */
//@Deprecated
//public class VideoMessageItem1 extends VideoPlayerView implements IMessageItem {
//
//	private String mVideoPath = null;
//	private Toast mToast = null;
//	private Context mContext;
//
//	 private VideoPlayerManager<MetaData> mVideoPlayerManager = new SingleVideoPlayerManager(new PlayerItemChangeListener() {
//	        @Override
//	        public void onPlayerItemChanged(MetaData metaData) {
//
//	        }
//	    });
//
//	 private AssetFileDescriptor mVideoFileDecriptor_sample_1;
//
//
//
//	 public VideoMessageItem1(Context context,AttributeSet attrs){
//	        super(context, attrs);
//	        mContext = context;
//	        LayoutParams lp = new LayoutParams(1100, 1000);
//	        this.setLayoutParams(lp);
//
//	        try {
//
//	            mVideoFileDecriptor_sample_1 = mContext.getAssets().openFd("video_sample_1.mp4");
//
//	        } catch (IOException e) {
//	            e.printStackTrace();
//	        }
//
//	        this.addMediaPlayerListener(new SimpleMainThreadMediaPlayerListener(){
//	            @Override
//	            public void onVideoPreparedMainThread() {
//	                // We hide the cover when video is prepared. Playback is about to start
//	            }
//
//	            @Override
//	            public void onVideoStoppedMainThread() {
//	                // We show the cover when video is stopped
//	            }
//
//	            @Override
//	            public void onVideoCompletionMainThread() {
//	                // We show the cover when video is completed
//	            }
//	        });
//
//	        mVideoPlayerManager.playNewVideo(null, this, mVideoFileDecriptor_sample_1);
//			//mVideoPath = getIntent().getStringExtra("videoPath");
//
//
//
//	    }
//
//	public VideoMessageItem1(Context context) {
//		this(context,null);
//
//
//	}
//
//	/*private PLMediaPlayer.OnInfoListener mOnInfoListener = new PLMediaPlayer.OnInfoListener() {
//		@Override
//		public boolean onInfo(PLMediaPlayer plMediaPlayer, int what, int extra) {
//			return false;
//		}
//	};
//
//	private PLMediaPlayer.OnErrorListener mOnErrorListener = new PLMediaPlayer.OnErrorListener() {
//		@Override
//		public boolean onError(PLMediaPlayer plMediaPlayer, int errorCode) {
//			switch (errorCode) {
//			case PLMediaPlayer.ERROR_CODE_INVALID_URI:
//				showToastTips("Invalid URL !");
//				break;
//			case PLMediaPlayer.ERROR_CODE_404_NOT_FOUND:
//				showToastTips("404 resource not found !");
//				break;
//			case PLMediaPlayer.ERROR_CODE_CONNECTION_REFUSED:
//				showToastTips("Connection refused !");
//				break;
//			case PLMediaPlayer.ERROR_CODE_CONNECTION_TIMEOUT:
//				showToastTips("Connection timeout !");
//				break;
//			case PLMediaPlayer.ERROR_CODE_EMPTY_PLAYLIST:
//				showToastTips("Empty playlist !");
//				break;
//			case PLMediaPlayer.ERROR_CODE_STREAM_DISCONNECTED:
//				showToastTips("Stream disconnected !");
//				break;
//			case PLMediaPlayer.ERROR_CODE_IO_ERROR:
//				showToastTips("Network IO Error !");
//				break;
//			case PLMediaPlayer.MEDIA_ERROR_UNKNOWN:
//			default:
//				showToastTips("unknown error !");
//				break;
//			}
//			// Todo pls handle the error status here, retry or call finish()
//			// If you want to retry, do like this:
//			// Return true means the error has been handled
//			// If return false, then `onCompletion` will be called
//			return true;
//		}
//	};
//
//	private PLMediaPlayer.OnCompletionListener mOnCompletionListener = new PLMediaPlayer.OnCompletionListener() {
//		@Override
//		public void onCompletion(PLMediaPlayer plMediaPlayer) {
//			showToastTips("Play Completed !");
//		}
//	};
//
//	private PLMediaPlayer.OnBufferingUpdateListener mOnBufferingUpdateListener = new PLMediaPlayer.OnBufferingUpdateListener() {
//		@Override
//		public void onBufferingUpdate(PLMediaPlayer plMediaPlayer, int precent) {
//		}
//	};
//
//	private PLMediaPlayer.OnSeekCompleteListener mOnSeekCompleteListener = new PLMediaPlayer.OnSeekCompleteListener() {
//		@Override
//		public void onSeekComplete(PLMediaPlayer plMediaPlayer) {
//		};
//	};
//
//	private PLMediaPlayer.OnVideoSizeChangedListener mOnVideoSizeChangedListener = new PLMediaPlayer.OnVideoSizeChangedListener() {
//		@Override
//		public void onVideoSizeChanged(PLMediaPlayer plMediaPlayer, int width,
//				int height) {
//		}
//	};
//*/
//	private void showToastTips(final String tips) {
//		// TODO
//		if (false) {
//			return;
//		}
//		((Activity)mContext).runOnUiThread(new Runnable() {
//			@Override
//			public void run() {
//				if (mToast != null) {
//					mToast.cancel();
//				}
//				mToast = Toast.makeText(mContext, tips,
//						Toast.LENGTH_SHORT);
//				mToast.show();
//			}
//		});
//	}
//
//	private boolean isLiveStreaming(String url) {
//		if (url.startsWith("rtmp://")
//				|| (url.startsWith("http://") && url.endsWith(".m3u8"))
//				|| (url.startsWith("http://") && url.endsWith(".flv"))) {
//			return true;
//		}
//		return false;
//	}
//
//	@Override
//	public View messageView() {
//		return this;
//	}
//
//	@Override
//	public ViewSize messageViewSize() {
//		return null;
//	}
//
//	@Override
//	public void setIMStruct002(IMStruct002 message) {
//		// TODO Auto-generated method stub
//
//	}
//
//	@Override
//	public boolean getIsInUse() {
//		return this.isShown();
//	}
//
//	@Override
//	public void setIsInUse(boolean isInUse) {
//		// TODO Auto-generated method stub
//
//	}
//
//	@Override
//	public void prepareForReuse() {
//		// TODO Auto-generated method stub
//
//	}
//}
