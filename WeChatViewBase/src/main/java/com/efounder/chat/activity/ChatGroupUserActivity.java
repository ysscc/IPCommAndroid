package com.efounder.chat.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;

import com.efounder.chat.R;
import com.efounder.chat.fragment.GroupUserFragment;

/**
 * 群成员列表界面
 * @author YQS
 */
@SuppressLint("InflateParams")
public class ChatGroupUserActivity extends BaseActivity  {

    private final String TAG = "ChatGroupUserActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatgroupuser);

        GroupUserFragment fragment = (GroupUserFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment_groupuser);
        }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * 返回
     *
     * @param view
     */
    @Override
    public void back(View view) {
        finish();
    }

}
