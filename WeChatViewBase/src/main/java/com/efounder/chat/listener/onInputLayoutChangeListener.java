package com.efounder.chat.listener;

import android.graphics.Rect;
import android.view.View;
import android.view.ViewTreeObserver;

import com.utilcode.util.LogUtils;

/**
 * author : zzj
 * e-mail : zhangzhijun@pansoft.com
 * date   : 2018/9/1317:17
 * desc   : 监听输入的关闭和打开
 * version: 1.0
 */
public abstract class onInputLayoutChangeListener implements ViewTreeObserver.OnGlobalLayoutListener  {

    private View rootView;
    private int curRectHeight = -1;

    public onInputLayoutChangeListener(View rootView) {
        this.rootView = rootView;
    }

    @Override
    public void onGlobalLayout() {
        Rect rect = new Rect();
        rootView.getWindowVisibleDisplayFrame(rect);
        int displayHight = rect.bottom;
        int height = rootView.getHeight();
//        LogUtils.e("RectHeight:" + displayHight + ",DecorViewHeigt:" + height +"----"+rect.top);
//        if (curRectHeight != displayHight) {
//            curRectHeight = displayHight;
//            onLayoutChange(displayHight, height);
//        }
        onLayoutChange(displayHight, height);
    }

    public abstract void onLayoutChange(int intputTop, int windowHeight);
}
