package com.efounder.chat.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by 11236 on 2017/3/12.
 */

public class DBHelper extends SQLiteOpenHelper {
    private static final String DBNAME = "dkdb.db";
    private static final int VERSION = 1;
    private static DBHelper instance = null;

    public synchronized static SQLiteDatabase getInstance(Context context) {
        if (instance == null) {
            instance = new DBHelper(context);
        }
        return instance.getReadableDatabase();
    }

    public DBHelper(Context context) {
        super(context, DBNAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "create table t_daka(_id integer primary key autoincrement,name text,job text,group_name text,am_time integer,pm_time integer,date text,am_state text,pm_state text,curr_month text,am_address text,pm_address text)";
        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
