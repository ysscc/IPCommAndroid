package com.efounder.chat.model;

import com.core.xml.StubObject;

/**
 * 首页下方菜单点击事件
 * （目前event 只用来处理点击同一按钮）
 *
 * @author YQS
 */
public class TabMenuClickEvent {

    // tab position
    private int position;
    //tab 的 stubobject
    private StubObject stubObject;

    public TabMenuClickEvent() {
    }

    public TabMenuClickEvent(int position, StubObject stubObject) {
        this.position = position;
        this.stubObject = stubObject;
    }

    public int getPosition() {
        return position;
    }

    public TabMenuClickEvent setPosition(int position) {
        this.position = position;
        return this;
    }

    public StubObject getStubObject() {
        return stubObject;
    }

    public TabMenuClickEvent setStubObject(StubObject stubObject) {
        this.stubObject = stubObject;
        return this;
    }

    //比对 当前页面 StubObject 是否输入事件中的stubobjet 的子菜单

    public boolean isChildPage(StubObject childStubObject) {
        if (childStubObject == null) {
            return false;
        }
        if ((childStubObject.getID().equals(stubObject.getID())
                || childStubObject.getString("_parentID", "").equals(stubObject.getID()))) {
            return true;
        }
        return false;
    }
}
