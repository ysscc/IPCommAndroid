package com.efounder.chat.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.efounder.chat.R;
import com.efounder.chat.adapter.ChatRoomAdapter;
import com.efounder.chat.db.WeChatDBManager;
import com.efounder.chat.model.Group;
import com.efounder.utils.ResStringUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

/**
 * 选择群组的 activity
 *
 * @author yqs
 */

public class SelectGroupActivity extends BaseActivity {

    private SwipeMenuListView groupListView;
    protected List<Group> grouplist;
    private ChatRoomAdapter groupAdapter;
    private TextView tv_total;
    private Group deleteGroup;
    private final String TAG = "SelectGroupActivity";
    private SwipeMenuCreator creator;
    private AsyncTask<Void, Void, List<Group>> asyncTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mychatroom);


        grouplist = new ArrayList<Group>();

        groupListView = (SwipeMenuListView) findViewById(R.id.groupListView);
        groupListView.setIsNeedIntercept(true);

        View headerView = LayoutInflater.from(this).inflate(
                R.layout.item_mychatroom_header, null);
        View footerView = LayoutInflater.from(this).inflate(
                R.layout.item_mychatroom_footer, null);
        tv_total = (TextView) footerView.findViewById(R.id.tv_total);
        tv_total.setText(String.valueOf(grouplist.size()) + ResStringUtil.getString(R.string.chat_individual) + getString(R.string.chat_room_name));

        groupAdapter = new ChatRoomAdapter(this, grouplist);
        groupListView.addHeaderView(headerView);
        groupListView.addFooterView(footerView);
        groupListView.setAdapter(groupAdapter);

        groupListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //如果点击第一个和最后一个，取消点击事件
                if (i == grouplist.size() + 1 || i == 0) {
                    return;
                }
                // 返回选择结果
                final int position = i - 1;
                Intent intent = new Intent();
                intent.putExtra("groupId", groupAdapter.getItem(position).getGroupId());
                intent.putExtra("groupName", groupAdapter.getItem(position).getGroupName());

                setResult(RESULT_OK, intent);
                finish();

            }
        });

        creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {

                // 删除按钮
//                SwipeMenuItem deleteItem = new SwipeMenuItem(ChatRoomActivity.this);
//                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
//                        0x3F, 0x25)));
//                deleteItem.setWidth(dp2px(90));
//                deleteItem.setTitle("退出");
//                deleteItem.setTitleSize(18);
//                deleteItem.setTitleColor(Color.WHITE);
//                menu.addMenuItem(deleteItem);
            }
        };

        // set creator
        groupListView.setMenuCreator(creator);

        //标题
        TextView tvTitle = (TextView) this.findViewById(R.id.tv_title);
        tvTitle.setText(ResStringUtil.getString(R.string.chat_choose) + getString(R.string.chat_room_name));
        //加号按钮
        ImageView iv_add = (ImageView) this.findViewById(R.id.iv_add);
        iv_add.setVisibility(View.INVISIBLE);
        ImageView iv_search = (ImageView) this.findViewById(R.id.iv_search);
//        iv_add.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(SelectGroupActivity.this, CreatChatRoomActivity.class);
//                startActivity(intent);
//            }
//
//        });
        iv_search.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

            }

        });
    }

    @Override
    public void onResume() {
        super.onResume();
        loadDataByAsync();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }


    /**
     * 返回
     *
     * @param view
     */
    @Override
    public void back(View view) {
        finish();
    }


    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }


    //数据库中读取列表数据
    private void loadDataByAsync() {
        asyncTask = new AsyncTask<Void, Void, List<Group>>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showLoading(ResStringUtil.getString(R.string.common_text_loading));
            }

            @Override
            protected List<Group> doInBackground(Void... params) {
                List<Group> groups = WeChatDBManager.getInstance().getallGroupWithOutUsers();

                return groups;
            }

            @Override
            protected void onPostExecute(List<Group> result) {
                super.onPostExecute(result);
                dismissLoading();
                grouplist.clear();
                grouplist.addAll(result);
                tv_total.setText(String.valueOf(grouplist.size()) + ResStringUtil.getString(R.string.chat_individual) + getString(R.string.chat_room_name));
                groupAdapter.notifyDataSetChanged();
            }

            @Override
            protected void onCancelled() {
                super.onCancelled();
                dismissLoading();
            }
        };
        asyncTask.executeOnExecutor(Executors.newCachedThreadPool());

    }


}
