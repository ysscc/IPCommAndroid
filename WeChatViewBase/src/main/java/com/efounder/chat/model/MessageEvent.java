package com.efounder.chat.model;

public class MessageEvent  {
	public static  final int UPDATE =1;
	public static  final int DELETE =2;

private int type;//消息类型更新或者删除
	private ChatListItem chatListItem;

	public MessageEvent() {
	}
	public MessageEvent(ChatListItem chatListItem,int type) {
        this.type =type;
		this.chatListItem = chatListItem;
	}

	public ChatListItem getChatListItem() {
		return chatListItem;
	}

	public void setChatListItem(ChatListItem chatListItem) {
		this.chatListItem = chatListItem;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
}
