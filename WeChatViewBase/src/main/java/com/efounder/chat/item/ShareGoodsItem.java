package com.efounder.chat.item;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.item.manager.IMessageItem;
import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.frame.ViewSize;
import com.efounder.message.struct.IMStruct002;
import com.efounder.ui.util.DisplayUtil;
import com.utilcode.util.ActivityUtils;

/**
 * Created by Marcello on 2018/10/9.
 * 分享商品的item
 */
@Deprecated
public class ShareGoodsItem extends LinearLayout implements IMessageItem {
    private Context mContext;
    private LinearLayout rootView;
    private ImageView imageView;
    private TextView tvTitle, tvMessage;

    private IMStruct002 imStruct002;

    public ShareGoodsItem(Context context) {
        super(context);
        this.mContext = context;
        initView(context);
    }

    @Override
    public void setIMStruct002(IMStruct002 message) {
        imStruct002 = message;
        String messageBody = imStruct002.getMessage();

        try {
            net.sf.json.JSONObject jsonObject = net.sf.json.JSONObject.fromObject(messageBody);
            if (jsonObject != null) {
                tvTitle.setText(jsonObject.optString("subject", ""));
                tvMessage.setText(jsonObject.optString("text", ""));
                String url = jsonObject.optString("imgUrl");
                LXGlideImageLoader.getInstance().displayImage(mContext, imageView,
                        url, R.drawable.share_web, R.drawable.share_web);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public View messageView() {
        return this;
    }

    @Override
    public ViewSize messageViewSize() {
        return null;
    }

    @Override
    public boolean getIsInUse() {
        return this.isShown();
    }

    @Override
    public void setIsInUse(boolean isInUse) {

    }

    @Override
    public void prepareForReuse() {

    }

    private void initView(final Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        rootView = (LinearLayout) inflater.inflate(R.layout.chat_item_share, this);
        imageView = rootView.findViewById(R.id.imageview);
        tvTitle = rootView.findViewById(R.id.tvTitle);
        tvMessage = rootView.findViewById(R.id.tvMessage);

        int width = DisplayUtil.getMobileWidth(context) * 2 / 3 - 20;
        LayoutParams lp = new LayoutParams(width, LayoutParams.MATCH_PARENT);
        this.setLayoutParams(lp);

        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String messageBody = imStruct002.getMessage();
                try {
                    net.sf.json.JSONObject jsonObject = net.sf.json.JSONObject.fromObject(messageBody);
                    if (jsonObject != null) {
                        String className = ActivityUtils.getTopActivity().getResources().getString(R.string.product_detail_actiivity_class);
                        Intent intent = new Intent();
                        intent.putExtra("goodbh", jsonObject.getString("goodId"));
                        intent.setClass(ActivityUtils.getTopActivity(), Class.forName(className));
                        ActivityUtils.getTopActivity().startActivity(intent);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
