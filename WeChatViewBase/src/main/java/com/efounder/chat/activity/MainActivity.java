//package com.efounder.chat.activity;
//
//import android.content.Context;
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentTransaction;
//import android.util.Log;
//import android.view.KeyEvent;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.Toast;
//
//import com.efounder.chat.R;
//import com.efounder.chat.fragment.ChatListFragment;
//import com.efounder.chat.fragment.ContactsFragment;
//import com.efounder.chat.fragment.FindFragment;
//import com.efounder.chat.fragment.TabFragment;
//import com.efounder.chat.http.GetHttpUtil;
//import com.efounder.chat.http.GetHttpUtil.GetHttpUtilListener;
//import com.efounder.chat.model.Group;
//import com.efounder.chat.service.MessageService;
//import com.efounder.chat.service.SystemInfoService;
//import com.efounder.chat.view.AddPopWindow;
//import com.efounder.frame.tabbar.EFTabBar;
//import com.efounder.frame.tabbar.EFTabBarItem;
//import com.efounder.message.db.MessageDBHelper;
//import com.efounder.message.db.MessageDBManager;
//import com.efounder.message.struct.IMStruct002;
//import com.efounder.mobilecomps.contacts.User;
////import com.efounder.EFTabBar;
////import com.efounder.EFTabBar.OnTabBarItemClickListener;
////import com.efounder.EFTabBarItem;
//
//import org.json.JSONException;
//
//import java.io.UnsupportedEncodingException;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
//public class MainActivity extends BaseActivity implements
//		com.efounder.frame.tabbar.EFTabBar.OnTabBarItemClickListener, GetHttpUtilListener {
//	private static final String TAG = "MainActivity";
//
//	private Context context = this;
//	private com.efounder.frame.tabbar.EFTabBar EFTabBar;
//	private int currentPosition = -1;
//	private Fragment[] fragments;
//	private ImageView iv_add;
//	private ImageView iv_search;
//	private Intent i;
//	private Intent msgIntent;
//	private Intent addFriendIntent;
//
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_main);
//		GetHttpUtil.setGetHttpUtilListener(this);
//
//		// 从服务器请求联系人和群组数据
//		try {
//			GetHttpUtil.getGroupListByLoginId(this);
//			GetHttpUtil.getUserData(MainActivity.this);
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
////		i = new Intent(context, NetworkStateService.class);
////		startService(i);
//		msgIntent = new Intent(context, MessageService.class);
//		startService(msgIntent);
//		addFriendIntent = new Intent(context, SystemInfoService.class);
//		startService(addFriendIntent);
//		initView();
//
//		// test();
//	}
//
//	private void initView() {
//		LinearLayout tabbarLayout = (LinearLayout) findViewById(R.id.tabbarLayout);
//		// 1.初始化TabBar
//		List<EFTabBarItem> EFTabBarItems = createTabbarItems();
//		EFTabBar = new EFTabBar(this, EFTabBarItems);
//		// tabBar.setPadding(0, 20, 0, 20);
//		// tabBar.setBackgroundColor(0xFFff0000);
//		// tabBar.setSelectedTab(1);
//		EFTabBar.setTabSelectedColor(getResources().getColor(R.color.chat_green));
//		EFTabBar.setTabColor(getResources().getColor(R.color.chat_gray_dark));
//		EFTabBar.setTabPadding(20);
//		EFTabBar.setTabTextSize(13);
//		EFTabBar.setTabPadding(17);
//		EFTabBar.setTabBackgroundColor(getResources()
//				.getColor(R.color.chat_white));
//		// 2.TabBar添加OnTabClickListener
//		EFTabBar.setOnTabBarItemClickListener(this);
//
//		tabbarLayout.addView(EFTabBar);
//		iv_add = (ImageView) this.findViewById(R.id.iv_add);// 加号按钮
//		iv_search = (ImageView) this.findViewById(R.id.iv_search);// 搜索按钮
//		iv_add.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				AddPopWindow addPopWindow = new AddPopWindow(MainActivity.this);
//				addPopWindow.showPopupWindow(iv_add);
//			}
//
//		});
//		iv_search.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//
//			}
//
//		});
//
//	}
//
//	/**
//	 * 创建底部TabbarItem
//	 *
//	 * @return
//	 */
//	private List<EFTabBarItem> createTabbarItems() {
//		List<EFTabBarItem> EFTabBarItems = new ArrayList<EFTabBarItem>();
//
//		EFTabBarItem item1 = new EFTabBarItem(context, "聊天", getResources()
//				.getDrawable(R.drawable.weixin_normal));
//		EFTabBarItems.add(item1);
//		EFTabBarItem item2 = new EFTabBarItem(context, "通讯录", getResources()
//				.getDrawable(R.drawable.contact_list_normal));
//		EFTabBarItems.add(item2);
//		EFTabBarItem item3 = new EFTabBarItem(context, "发现", getResources()
//				.getDrawable(R.drawable.find_normal));
//		EFTabBarItems.add(item3);
//		EFTabBarItem item4 = new EFTabBarItem(context, "我", getResources()
//				.getDrawable(R.drawable.profile_normal));
//		EFTabBarItems.add(item4);
//		// 创建出Fragment
//		fragments = new Fragment[EFTabBarItems.size()];
//		showFragment(0);
//		return EFTabBarItems;
//	}
//
//	@Override
//	public void onTabBarItemClick(EFTabBarItem EFTabBarItem, int position) {
//		if (position == currentPosition) {
//			return;
//		}
//		// tabBar.clearIndicator(position);
//		EFTabBar.setSelectedTab(position);
//		Toast.makeText(context,
//				"位置：" + position + "," + EFTabBarItem.getTabText(),
//				Toast.LENGTH_SHORT).show();
//		// TODO tabBar点击事件
//		showFragment(position);
//
//	}
//
//	/**
//	 * 显示某个位置的Fragment
//	 *
//	 * @param position
//	 */
//	private void showFragment(int position) {
//		FragmentManager fragmentManager = getSupportFragmentManager();
//		FragmentTransaction fragmentTransaction = fragmentManager
//				.beginTransaction();
//		if (fragments[position] == null) {// 如果没有实例化Fragment，则创建
//			switch (position) {
//			case 0:
//				fragments[position] = new ChatListFragment();
//				break;
//
//			case 1:
//				fragments[position] = new ContactsFragment();
//				break;
//			case 2:
//				fragments[position] = new FindFragment();
//				break;
//			case 3:
//				//fragments[position] = new SimpleSettingFragment("");
//				break;
//
//			default:
//				fragments[position] = new TabFragment();
//				Bundle bundle = new Bundle();
//				bundle.putString("title", "点击的位置：" + position);
//				fragments[position].setArguments(bundle);
//				break;
//			}
//			fragmentTransaction.add(R.id.fragment_container,
//					fragments[position]);
//		} else {// 如果已经实例化，则显示
//			fragmentTransaction.show(fragments[position]);
//		}
//
//		if (currentPosition != -1 && fragments[currentPosition] != null) {// 隐藏上一个位置的Fragment
//			fragmentTransaction.hide(fragments[currentPosition]);
//		}
//
//		fragmentTransaction.commit();
//		// !!最后，记录position
//		currentPosition = position;
//	}
//
//	private void test() {
//		// WeChatHttpRequest httpRequest = new WeChatHttpRequest(this);
//		// httpRequest.httpLogin("", "");
//		List<IMStruct002> struct002s = new ArrayList<IMStruct002>();
//		MessageDBHelper helper = new MessageDBHelper(this);
//		MessageDBManager dbManager = new MessageDBManager(helper);
//		IMStruct002 struct002_1 = new IMStruct002();
//		struct002_1.setMsgId(101);
//		struct002_1.setFromUserId(0);
//		struct002_1.setToUserId(1);
//		struct002_1.setMessageChildType((byte) 0);
//
//		IMStruct002 struct002_2 = new IMStruct002();
//		struct002_2.setMsgId(102);
//		struct002_2.setFromUserId(0);
//		struct002_2.setToUserId(1);
//		struct002_2.setMessageChildType((byte) 0);
//
//		struct002s.add(struct002_1);
//		struct002s.add(struct002_2);
//		dbManager.insert(struct002s);
//		// ///////////////////////////////////////////////////
//
//		struct002_1.setState(0);
//		struct002_1.setTime(new Date().getTime());
//		Log.i(TAG, "插入的时间----------" + struct002_1.getTime());
//		try {
//			struct002_1.setBody("你好".getBytes("UTF-8"));
//			Log.i(TAG, "插入的body----------"
//					+ new String(struct002_1.getBody(), "utf-8"));
//		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
//		}
//		dbManager.update(struct002s);
//		// /////////////////////////////////////////////////////
//		List<IMStruct002> list = dbManager.query("select * from struct002");
//		Log.i(TAG, "取出的时间----------" + list.get(0).getTime());
//		try {
//			Log.i(TAG, "取出的body----------"
//					+ new String(list.get(0).getBody(), "utf-8"));
//		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
//		}
//
//		dbManager.delete(list);
//	}
//
//	private long exitTime = 0;
//
//	@Override
//	public boolean onKeyDown(int keyCode, KeyEvent event) {
//		if (keyCode == KeyEvent.KEYCODE_BACK
//				&& event.getAction() == KeyEvent.ACTION_DOWN) {
//			if ((System.currentTimeMillis() - exitTime) > 2000) {
//				Toast.makeText(getApplicationContext(), "再按一次退出程序",
//						Toast.LENGTH_SHORT).show();
//				exitTime = System.currentTimeMillis();
//			} else {
//				moveTaskToBack(false);
//				stopService(i);
//				stopService(msgIntent);
//				finish();
//
//			}
//			return true;
//		}
//		return super.onKeyDown(keyCode, event);
//	}
//
//	@Override
//	protected void onDestroy() {
//		super.onDestroy();
////		DBManager.close();
//	}
//
//	@Override
//	public void onCreateGroupSuccess(boolean isSuccess,Group group) {
//
//
//	}
//
//	@Override
//	public void onGetGroupUserSuccess(List<User> groupUsers, int groupId) {
//
//	}
//
//	@Override
//	public void onAddUsersSuccess(boolean isSuccess) {
//
//
//	}
//
//	@Override
//	public void onGetGroupListSuccess(List<Integer> groupIds) {
//		for (int id : groupIds) {
//			try {
//				GetHttpUtil.getGroupUsers(MainActivity.this, id);
//			} catch (JSONException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//
//	}
//}
