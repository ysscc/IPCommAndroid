package com.efounder.chat.model;

/**
 * Created by zhangshunyun on 2017/9/22.
 *
 * 通知糖足rn 保存消息发送成功
 */

public class NoticeMessageEvent {

    public String toUserID;
    public String state;


    public NoticeMessageEvent(String toUserID, String state){
        this.toUserID = toUserID;
        this.state = state;
    }
    public NoticeMessageEvent(){

    }
    public String getToUserID() {
        return toUserID;
    }

    public String getState() {
        return state;
    }
}
