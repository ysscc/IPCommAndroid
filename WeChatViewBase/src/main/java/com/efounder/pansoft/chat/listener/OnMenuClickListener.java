package com.efounder.pansoft.chat.listener;


import com.efounder.chat.model.ChatMenuModel;
import com.efounder.message.struct.IMStruct002;
import com.efounder.pansoft.chat.model.FileItem;

import java.util.LinkedList;
import java.util.List;

/**
 * Menu items' callbacks
 */
public interface OnMenuClickListener {

    /**
     * Fires when send button is on click.
     *发送文本消息
     * @param input Input content
     * @return boolean
     */
    boolean onSendTextMessage(CharSequence input);

    /**
     * 发送拼好的消息
     * @param imStruct002
     * @return
     */
    boolean onSendStruct002Message(IMStruct002 imStruct002);
    /**
     * Files when send photos or videos.
     * When construct send message, you need to judge the type
     * of file item, according to
     *
     * @param list List of file item objects
     */
//    void onSendFiles(List<FileItem> list);

    void onSendFiles(List<String> mSelectedPics, boolean isRawPic);

    /**
     * Fires when voice button is on click.
     * 录音按钮被点击
     */
    boolean switchToMicrophoneMode();

    /**
     * Fires when photo button is on click.
     * 选择图片按钮被点击
     */
    boolean switchToGalleryMode();

    /**
     * Fires when camera button is on click.
     * 拍照按钮被点击
     */
    boolean switchToCameraMode();

    /**
     * Fires when emoji button is on click.
     * 表情按钮被点击
     */
    boolean switchToEmojiMode();
    /**
     *
     * 加号更多中的item被点击
     */
    void clickMoreMenuItem(ChatMenuModel model);


    /**
     * shake item被点击
     * @param
     */
    void onSendShakeAnim(String name);
}