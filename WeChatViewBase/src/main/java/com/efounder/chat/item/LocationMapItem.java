package com.efounder.chat.item;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.efounder.chat.R;
import com.efounder.chat.item.manager.IMessageItem;
import com.efounder.chat.utils.ChatActivitySkipUtil;
import com.efounder.frame.ViewSize;
import com.efounder.message.struct.IMStruct002;
import com.efounder.ui.util.DisplayUtil;
import com.efounder.view.RoundFrameLayout;

import java.io.File;

/**
 * 位置item
 *
 * @author YQS
 */
public class LocationMapItem extends RoundFrameLayout implements IMessageItem, OnClickListener {

    private static final String TAG = "LocationMapItem";


    private Context mContext;
    private LayoutInflater mInflater;
    private RoundFrameLayout contentRl;
    private ImageView mMapView;
    //详细地址
    private TextView addressTv;
    //地点名称
    private TextView tvNameView;
    private IMStruct002 iMStruct002;


    private String picPath;
    private String details;
    private String address;
    private String name;
    private String latitude;
    private String longitude;

    private MultiTransformation multi;

    public LocationMapItem(Context context) {
        super(context);
        this.mContext = context;
        mInflater = LayoutInflater.from(context);
        initView();
        initListener();
//         multi = new MultiTransformation(
//                new CenterCrop(),
//                 new RoundedCornersTransformation(11, 0,
//                         RoundedCornersTransformation.CornerType.TOP));

    }

    private void initListener() {
        this.setOnClickListener(this);
    }


    private void initView() {
        contentRl = (RoundFrameLayout) mInflater.inflate(R.layout.chat_item_location_map, this);

        RelativeLayout relativeLayout = contentRl.findViewById(R.id.rllayout);
//        GradientDrawable myGrad = (GradientDrawable) relativeLayout.getBackground();
//        myGrad.setColor(mContext.getResources().getColor(R.color.white));
        mMapView = (ImageView) contentRl.findViewById(R.id.iv_map);
        addressTv = (TextView) contentRl.findViewById(R.id.tv_address);
        tvNameView = contentRl.findViewById(R.id.tv_name);


        FrameLayout.LayoutParams lp = new LayoutParams(
                DisplayUtil.getMobileWidth(mContext) * 2 / 3 + 50, ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setLayoutParams(lp);
    }

    private void showPic() {

        Glide.with(mContext).load(picPath)
                .apply(new RequestOptions()
                                //.error(R.drawable.loading_image_background)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                        //  .bitmapTransform(multi)
                )
//                .listener(new RequestListener<Drawable>() {
//                    @Override
//                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//                        return false;
//                    }
//
//                    @Override
//                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
//                        return false;
//                    }
//                })
                .into(mMapView);
        //  EventBus.getDefault().post(new NotifyChatUIRefreshEvent());

    }

    public void setUpImageViewParams(String scale) {

        int width = Integer.valueOf(scale.substring(0, scale.indexOf(":")));
        int height = Integer.valueOf(scale.substring(scale.indexOf(":") + 1, scale.length()));
        int reSizeWidth = this.getWidth() - 20;
        final int reSizeHeight = (int) (height * ((float) reSizeWidth / (float) width));
        // mMapView.getLayoutParams().width =reSizeWidth;
        // mMapView.getLayoutParams().height = reSizeHeight;
        mMapView.getLayoutParams().height = this.getLayoutParams().width * 3 / 5;

//        mMapView.post(new Runnable() {
//
//            @Override
//            public void run() {
//                mMapView.setLayoutParams(new RelativeLayout.LayoutParams(LayoutParams.FILL_PARENT, reSizeHeight));
//            }
//        });
    }

    @Override
    public View messageView() {

        return this;
    }

    @Override
    public ViewSize messageViewSize() {
        return null;
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        intent.putExtra("latitude", latitude);
        intent.putExtra("longitude", longitude);
        intent.putExtra("address", address);
        intent.putExtra("name", name);
        ChatActivitySkipUtil.startViewMapDetailActivity(mContext, intent);
    }

    @Override
    public void setIMStruct002(IMStruct002 message) {
        this.iMStruct002 = message;
        details = new String(iMStruct002.getBody());
        Log.i(TAG, "---details---" + details);


        try {
            net.sf.json.JSONObject jsonObject = net.sf.json.JSONObject.fromObject(details);
            String imageUrl = jsonObject.optString("url", "");
            String localPath = jsonObject.optString("localPath", "");

            name = jsonObject.optString("name", "");
            address = jsonObject.optString("specific", "");
            latitude = jsonObject.optString("latitude", "");
            longitude = jsonObject.optString("longitude", "");
            picPath = imageUrl;
            File file = new File(localPath);
            if (file.exists()) {
                picPath = localPath;
            }

            setUpImageViewParams(jsonObject.optString("scale", "500:500"));

            showPic();

            tvNameView.setText(name);
            addressTv.setText(address);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean getIsInUse() {
        return this.isShown();
    }

    @Override
    public void setIsInUse(boolean isInUse) {
        // TODO Auto-generated method stub

    }

    @Override
    public void prepareForReuse() {
    }

}
