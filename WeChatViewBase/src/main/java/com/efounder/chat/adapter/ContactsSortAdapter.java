package com.efounder.chat.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.efounder.chat.R;
import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.chat.widget.BadgeView;
import com.efounder.mobilecomps.contacts.User;
import com.efounder.thirdpartycomps.stickylistheaders.StickyListHeadersAdapter;

import java.util.List;
import java.util.Locale;

/**
 * 通讯录adapter ps：数据传入钱需要先进行排序
 *
 * @author hudq
 */
public class ContactsSortAdapter extends BaseAdapter implements
        StickyListHeadersAdapter, SectionIndexer {

    private List<User> list = null;
    private Context mContext;

    public ContactsSortAdapter(Context mContext, List<User> list) {
        this.mContext = mContext;
        this.list = list;
    }

    /**
     * 当ListView数据发生变化时,调用此方法来更新ListView
     *
     * @param list
     */
    public void updateListView(List<User> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public int getCount() {
        return list.size();
    }

    public Object getItem(int position) {
        return list.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder = null;
        final User user = list.get(position);
        if (view == null) {
            viewHolder = new ViewHolder();
            view = LayoutInflater.from(mContext).inflate(
                    R.layout.contacts_item, viewGroup, false);
            viewHolder.text = (TextView) view.findViewById(R.id.title);
            viewHolder.avatar = (ImageView) view.findViewById(R.id.avatar);
            //iconcontainerforbadage
            //viewHolder.iconcontainerforbadage = (LinearLayout) view.findViewById(R.id.iconcontainerforbadage);

            viewHolder.badge = new BadgeView(mContext, viewHolder.avatar);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        if (user.getReMark() == null || user.getReMark().equals("")) {
            viewHolder.text.setText(user.getNickName());
        } else {
            viewHolder.text.setText(user.getReMark());
        }
        viewHolder.badge.setVisibility(View.GONE);
//        String avatarStr = "drawable://";
        int avatarId = R.drawable.default_useravatar;
        // TODO 头像 viewHolder.avatar
        // avatarStr = avatarStr + R.drawable.default_useravatar;
        if ("群聊".equals(user.getNickName())) {
//            avatarStr = avatarStr + R.drawable.icon_group_chat;
            avatarId = R.drawable.icon_group_chat;
            // viewHolder.avatar.setImageResource(R.drawable.icon_group_chat);
        } else if ("组织机构".equals(user.getNickName())) {
//            avatarStr = avatarStr + R.drawable.icon_organization;
            avatarId = R.drawable.icon_organization;

            //viewHolder.avatar.setImageResource(R.drawable.icon_organization);
        } else if ("人力资源系统".equals(user.getNickName())) {
//            avatarStr = avatarStr + R.drawable.rlzy;
            avatarId = R.drawable.rlzy;

            //viewHolder.avatar.setImageResource(R.drawable.rlzy);
        } else if ("内控系统".equals(user.getNickName())) {
//            avatarStr = avatarStr + R.drawable.neikong;
            avatarId = R.drawable.neikong;

            // viewHolder.avatar.setImageResource(R.drawable.neikong);
        } else if ("固定资产系统".equals(user.getNickName())) {
//            avatarStr = avatarStr + R.drawable.gdzc;
            avatarId = R.drawable.gdzc;

            // viewHolder.avatar.setImageResource(R.drawable.gdzc);
        } else if ("财务核算系统".equals(user.getNickName())) {
//            avatarStr = avatarStr + R.drawable.czhs;
            avatarId = R.drawable.czhs;

            // viewHolder.avatar.setImageResource(R.drawable.czhs);
        } else if ("生产监控系统".equals(user.getNickName())) {
//            avatarStr = avatarStr + R.drawable.scjk2;
            avatarId = R.drawable.scjk2;

            // viewHolder.avatar.setImageResource(R.drawable.scjk2);
        } else if ("生产日报系统".equals(user.getNickName())) {
//            avatarStr = avatarStr + R.drawable.bj;
            avatarId = R.drawable.bj;

            // viewHolder.avatar.setImageResource(R.drawable.bj);
        } else if ("CRM系统".equals(user.getNickName())) {
            //avatarStr = avatarStr +R.drawable.crm;
            avatarId = R.drawable.crm;

            // viewHolder.avatar.setImageResource(R.drawable.crm);
        } else if ("普联餐厅".equals(user.getNickName())) {
//            avatarStr = avatarStr + R.drawable.ct;
            avatarId = R.drawable.ct;

            //  viewHolder.avatar.setImageResource(R.drawable.ct);
        } else if ("普联健身".equals(user.getNickName())) {
//            avatarStr = avatarStr + R.drawable.jianshen;
            avatarId = R.drawable.jianshen;

            // viewHolder.avatar.setImageResource(R.drawable.jianshen);
        } else if ("奥体中心".equals(user.getNickName())) {
//            avatarStr = avatarStr + R.drawable.atzx;
            avatarId = R.drawable.atzx;

            // viewHolder.avatar.setImageResource(R.drawable.atzx);
        } else if ("乒乓球室".equals(user.getNickName())) {
//            avatarStr = avatarStr + R.drawable.ppq;
            avatarId = R.drawable.ppq;

            //  viewHolder.avatar.setImageResource(R.drawable.ppq);
        } else if ("篮球场".equals(user.getNickName())) {
//            avatarStr = avatarStr + R.drawable.lanqiu;
            avatarId = R.drawable.lanqiu;

            //viewHolder.avatar.setImageResource(R.drawable.lanqiu);
        } else if ("足球场".equals(user.getNickName())) {
//            avatarStr = avatarStr + R.drawable.zuqiu;
            avatarId = R.drawable.zuqiu;

            // viewHolder.avatar.setImageResource(R.drawable.zuqiu);
        } else if ("新的朋友".equals(user.getNickName())) {
//            avatarStr = avatarStr + R.drawable.icon_official_account;
            avatarId = R.drawable.icon_official_account;

            if (user.getBadgenum() != 0) {
                viewHolder.badge.setVisibility(View.VISIBLE);
                viewHolder.badge.setBadgeMargin(3);
                viewHolder.badge.setTextSize(13);
                viewHolder.badge.setTag("badge");
                viewHolder.badge.setText(String.valueOf(user.getBadgenum()));
                viewHolder.badge.setBadgeBackgroundColor(mContext.getResources().getColor(
                        R.color.chat_badge_color));
                viewHolder.badge.show();
            } else {
                viewHolder.badge.setVisibility(View.GONE);
            }

        } else if ("群通知".equals(user.getNickName())) {
//            avatarStr = avatarStr + R.drawable.icon_groupnotice;
            avatarId = R.drawable.icon_groupnotice;

            if (user.getBadgenum() != 0) {
                viewHolder.badge.setVisibility(View.VISIBLE);
                viewHolder.badge.setBadgeMargin(3);
                viewHolder.badge.setTextSize(13);
                viewHolder.badge.setTag("badge");
                viewHolder.badge.setText(String.valueOf(user.getBadgenum()));
                viewHolder.badge.setBadgeBackgroundColor(mContext.getResources().getColor(
                        R.color.chat_badge_color));
                viewHolder.badge.show();
            } else {
                viewHolder.badge.setVisibility(View.GONE);
            }
        } else {

        }

        if (user != null && user.getAvatar()!=null && user.getAvatar().contains("http")) {
            LXGlideImageLoader.getInstance().showRoundUserAvatar(mContext, viewHolder.avatar, user.getAvatar()
                    ,LXGlideImageLoader.DEFATLT_AVATAR_RADIUS_10);
        } else {
            //LXGlideImageLoader.getInstance().displayImage(mContext, viewHolder.avatar, avatarId, R.drawable.default_useravatar, R.drawable.default_useravatar);
            LXGlideImageLoader.getInstance().showRoundUserAvatar(mContext, viewHolder.avatar, avatarId
                    ,LXGlideImageLoader.DEFATLT_AVATAR_RADIUS_10);

        }
//        try {
//            imageLoader.displayImage(URLModifyDynamic.getInstance().replace(avatarStr), viewHolder.avatar, options);
//        } catch (Exception e) {
//            e.printStackTrace();
//            viewHolder.avatar.setImageResource(R.drawable.default_useravatar);
//        }

        return view;

    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;

        if (convertView == null) {
            holder = new HeaderViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(
                    R.layout.contacts_header, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.header);

            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }

        // set header text as first char in name
        CharSequence headerChar = list.get(position).getSortLetters().substring(0, 1);
        holder.text.setText(headerChar);
        // 隐藏最顶端headerView
        if ("↑".equals(headerChar)) {
            holder.text.setVisibility(View.GONE);
        } else {
            holder.text.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    /**
     * 根据ListView的当前位置获取分类的首字母的Char ascii值
     */
    @Override
    public int getSectionForPosition(int position) {
        return list.get(position).getSortLetters().charAt(0);
    }

    /**
     * 根据分类的首字母的Char ascii值获取其第一次出现该首字母的位置
     */
    @Override
    public int getPositionForSection(int section) {
        for (int i = 0; i < getCount(); i++) {
            String sortStr = list.get(i).getSortLetters();
            char firstChar = sortStr.toUpperCase(Locale.getDefault()).charAt(0);
            if (firstChar == section) {
                return i;
            }
        }

        return -1;
    }

    @Override
    public Object[] getSections() {
        return list.toArray();
    }

    @Override
    public long getHeaderId(int position) {
        return list.get(position).getSortLetters().charAt(0);
    }

    class HeaderViewHolder {
        TextView text;
    }

    class ViewHolder {
        TextView text;
        ImageView avatar;
        LinearLayout iconcontainerforbadage;
        public BadgeView badge;
    }

    /**
     * 异步加载头像
     */
//    public void initImageLoader() {
//        // 初始化异步加载图片的类
//        imageLoader = ImageLoader.getInstance();
//        // 设置异步加载图片的配置信息
//        options = ImageUtil.getImageLoaderOptions(R.drawable.default_useravatar);
//
//    }

}