package com.efounder.chat.interf;

import android.view.View;

import com.efounder.chat.model.ChatTopFloatModel;

/**
 * 聊天界面上方显示的悬浮窗接口
 * @author wang
 */
public interface IChatTopFloatView<T> {

    /**
     * 获取子view
     * @return 子view，然后加到父view中
     */
    View getCustomContentView();

    void setParentView(View parentView);

    View getParentView();

    /**
     * set data给子view，让子view处理
     * @param data data
     */
    void setData(ChatTopFloatModel<T> data);

    /**
     * 显示
     */
    void show();

    /**
     * 隐藏
     */
    void hide();

    /**
     * 设置父view的页面边距，
     * @param left 边距，dp
     */
    void setParentMargin(int left, int top, int right, int bottom);

}
